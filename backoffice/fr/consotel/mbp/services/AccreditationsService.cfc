<cfcomponent name="AccreditationsService" alias="fr.consotel.mbp.services.AccreditationsService" output="false">
	<cffunction name="sendMailUserAcces" access="remote" returntype="numeric">
		<cfargument name="civilite" required="true" type="string">
		<cfargument name="nom" required="true" type="String">	
		<cfargument name="prenom" required="true" type="String">
		<cfargument name="email" required="true" type="String">
		<cfargument name="mdp" required="true" type="String">
		
		<cfset adressMail = email>
	    <cfset rsltMail = 0>
			<cftry>
	           	<cfmail from="support@consotel.fr" to="#adressMail#" subject="Votre acces MBP" server="mail.consotel.fr:26">
#civilite# #nom# #prenom#

Vos parametres de connexion à l'interface MBP sont :
	- login : "#email#"
	- mot de passe :  "#mdp#"
	            </cfmail>
	       	<cfset rsltMail = 1>
	       	<cfcatch>
	            <cfset rsltMail = 0>
	        </cfcatch>
	        </cftry>
	    <cfreturn rsltMail>
	</cffunction>
	
	<cffunction name="getPortefeuille" access="remote" returntype="Query">
		<cfargument name="idihm" type="Numeric">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_accreditation.portefeuille_user">
			<cfprocparam value="#idihm#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="p_result">
		</cfstoredproc>	
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="savePortefeuille" access="remote" returntype="Numeric">
		<cfargument name="idihm" type="numeric">
		<cfargument name="arr" type="Array">
		<cfset tmp = 0>
		<cfloop index="i" from="1" to=#ArrayLen(arr)#>	
			<cfstoredproc datasource="MBPOFFRE" procedure="pkg_accreditation.adddelportefeuille">
				<cfprocparam value="1" cfsqltype="CF_SQL_INTEGER" type="in"> 
				<cfprocparam value="#idihm#" cfsqltype="CF_SQL_INTEGER" type="in"> 
				<cfprocparam value="#val(arr[i]['id'])#" cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">		
			</cfstoredproc>
			<cfif p_result eq -1>
				<cfset tmp=0>
			<cfelse>
				<cfset tmp=1>	
			</cfif>
		</cfloop>
		
		<cfreturn tmp>
	</cffunction>
	
	<cffunction name="deletePortefeuille" access="remote" returntype="Numeric">
		<cfargument name="idihm" type="numeric">
		<cfargument name="arr" type="Array">
		<cfset tmp = 0>
		<cfloop index="i" from="1" to=#ArrayLen(arr)#>	
			<cfstoredproc datasource="MBPOFFRE" procedure="pkg_accreditation.adddelportefeuille">
				<cfprocparam value="0" cfsqltype="CF_SQL_INTEGER" type="in"> 
				<cfprocparam value="#idihm#" cfsqltype="CF_SQL_INTEGER" type="in"> 
				<cfprocparam value="#val(arr[i]['id'])#" cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">		
			</cfstoredproc>
			<cfif p_result eq -1>
				<cfset tmp=0>
			<cfelse>
				<cfset tmp=1>	
			</cfif>
		</cfloop>
		
		<cfreturn tmp>
	</cffunction>
	
 	<cffunction name="deleteUserAcces" access="remote" returntype="Any">
		<cfargument name="idihmuser" type="Numeric">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_accreditation.logindel">
			<cfprocparam value="#idihmuser#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam variable="p_result" type="out" cfsqltype="CF_SQL_INTEGER">
		</cfstoredproc>
		<cfreturn> p_result>
	</cffunction> 
	<cffunction name="searchClient" access="remote" returntype="query" description="">
		<cfargument name="companyID" type="numeric">
		<cfargument name="corporatename" type="String">
		<cfargument name="bool_externe" type="numeric">
		
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_accreditation.SEARCHACCR">
			<cfprocparam value="#companyID#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#corporatename#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#bool_externe#" cfsqltype="CF_SQL_INTEGER" type="in">			
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result> 
	</cffunction> 
	
<!--- 	<cffunction name="sendMailUserAcces" access="remote" returntype="Numeric" description="">
		<cfargument name="idihmuser" type="Numeric">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_accreditation.envoi_accces">
			<cfprocparam value="#idihmuser#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result> 
	</cffunction> --->
	
	<cffunction name="createUser" access="remote" returntype="numeric" description="">
		<cfargument name="idihm" type="numeric">
		<cfargument name="profilid" type="numeric">
		<cfargument name="idcreateur" type="numeric">
		<cfargument name="personid" type="numeric">
		<cfargument name="consotelid" type="numeric">
		<cfargument name="civilite" type="string">
		<cfargument name="nom" type="string">
		<cfargument name="prenom" type="string">
		<cfargument name="pwd" type="string">
		<cfargument name="email" type="string">
		<cfargument name="telephone" type="string">
		<cfargument name="description" type="string">
		<cfargument name="bool" type="numeric">
		
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_accreditation.LOGINIU">
			<cfprocparam value="#idihm#" type="in" cfsqltype="CF_SQL_NUMERIC">
			<cfprocparam value="#profilid#" type="in" cfsqltype="CF_SQL_NUMERIC">
			<cfprocparam value="#idcreateur#" type="in" cfsqltype="CF_SQL_NUMERIC">
			<cfprocparam value="#personid#" type="in" cfsqltype="CF_SQL_NUMERIC" null="true">
			<cfprocparam value="#consotelid#" type="in" cfsqltype="CF_SQL_NUMERIC">
			<cfprocparam value="#civilite#" type="in" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#nom#" type="in" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#prenom#" type="in" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#pwd#" type="in" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#email#" type="in" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#telephone#" type="in" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#description#" type="in" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#bool#" type="in" cfsqltype="CF_SQL_NUMERIC">
			<cfprocparam variable="p_result" type="out" cfsqltype="CF_SQL_INTEGER">
		</cfstoredproc>
		<cfreturn p_result> 
	</cffunction>
	
	<cffunction name="getListeSociete" access="remote" returntype="query" description="">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_accreditation.ListCompany">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result> 
	</cffunction>
	
	<cffunction name="getListeProfil" access="remote" returntype="query" description="">
		<cfstoredproc datasource="MBPOFFRE" procedure="Pkg_accreditation.IHMPROFILE">
			<cfprocresult name="p_result">
		</cfstoredproc>
	<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getListeSocietePortefeuille" access="remote" returntype="query" description="">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_accreditation.portefeuille_comp">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result> 
	</cffunction>
	
</cfcomponent>