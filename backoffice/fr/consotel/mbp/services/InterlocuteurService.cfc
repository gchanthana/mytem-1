<cfcomponent output="false">
	<cffunction name="removeXInterlocuteurs" access="remote" returntype="numeric">
		<cfargument name="tab_idinterlocuteurs" type="array" required="true">
		
		
		<cfset liste_interlocuteurs = arrayToList(tab_idinterlocuteurs,",")>
	
		<cfset len = arrayLen(tab_idinterlocuteurs)>
		
		<cfset r = -1>
		<cfloop index="i" from="1" to="#len#">
			<cfset r = deleteInterlocuteur(tab_idinterlocuteurs[i])>
		</cfloop>
				
		<cfreturn r>
	</cffunction>
<cffunction name="deleteInterlocuteur" access="remote" returntype="numeric" description="">
	<cfargument name="idcontacts_client" type="numeric" required="true">
		<cfstoredproc datasource="MBPOFFRE" procedure="Pkg_mbp_global.InterlocuteurDel">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idcontacts_client#" null="false">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
		</cfstoredproc>
	<cfreturn p_retour>
</cffunction>
<cffunction name="majInterlocuteur" access="remote" returntype="numeric" description="">
	<cfargument name="idcontacts_client" type="numeric" required="true">
	<cfargument name="companyid" type="numeric" required="true">
	<cfargument name="nom" type="string" required="true">
	<cfargument name="prenom" type="string" required="true">
	<cfargument name="tel_fixe" type="string" required="true">
	<cfargument name="tel_mobile" type="string" required="true">
	<cfargument name="email" type="string" required="true">
	<cfargument name="fonction" type="string" required="true">
	<cfargument name="bool_interne" type="numeric" required="true">
	<cfstoredproc datasource="MBPOFFRE" procedure="Pkg_mbp_global.InterlocuteurIU">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idcontacts_client#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#companyid#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#nom#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#prenom#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#tel_fixe#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#tel_mobile#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#email#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#fonction#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#bool_interne#" null="false">
		<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
	</cfstoredproc>
	<cfreturn p_retour>
</cffunction>

<cffunction name="getInterlocuteurProfil" access="remote" returntype="query" description="">
	<cfstoredproc datasource="MBPOFFRE" procedure="pkg_mbp_global.InterlocuteurProfil">
	<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>

<cffunction name="getInterlocuteursList" access="remote" returntype="query" description="">
	<cfargument name="idSociete" type="numeric" required="true">
	<cfargument name="typeUser" type="String" required="true">
	<cfstoredproc datasource="MBPOFFRE" procedure="pkg_mbp_global.InterlocuteursList">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSociete#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#typeUser#" null="false">
	<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
</cfcomponent>