<cfcomponent name="commandeService">
	<cffunction name="getInfosBlocCommande" access="remote" returntype="query" description="">
		<cfargument name="idSociete" type="numeric" required="true">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_mbp_global.getInfosBlockCommande">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSociete#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result> 
	</cffunction> 
	
	<cffunction name="searchListeCommande" access="remote" returntype="Query">
		<cfargument name="numeroCmd" type="Numeric">
		<cfargument name="etatCmd" type="String">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.search">
			<cfprocparam value="#numeroCmd#" cfsqltype="CF_SQL_INTEGER" null="true">
			<cfprocparam value="#etatCmd#" cfsqltype="CF_SQL_VARCHAR" null="true">
			<cfprocresult name="p_result">
		</cfstoredproc>
	
	<cfreturn> p_result>
	</cffunction>
	
	<cffunction name="listetTypePack" access="remote" returntype="Query">
		<cfargument name="idSociete" type="Numeric">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.list_packs">
			<cfprocparam value="#idSociete#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="q_result">
		</cfstoredproc>
		<cfreturn q_result> 
	</cffunction>

	<cffunction name="listeFonctionPack" access="remote" returntype="Query">
		<cfargument name="id" type="String">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.list_fctbase">
			<cfprocparam value="#id#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="q_result">
		</cfstoredproc>
		<cfreturn q_result> 
	</cffunction>

	<cffunction name="suppModele" access="remote" returntype="Query">
		<cfargument name="id" type="String">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.delete_modele">
			<cfprocparam value="#id#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam variable="result" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn q_result> 
	</cffunction>

	<cffunction name="listeEquipement" access="remote" returntype="Query">
		<cfargument name="enr_playid" type="Numeric">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.list_equipements">
			<cfprocparam value="#enr_playid#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="q_result">
		</cfstoredproc>
		<cfreturn q_result> 
	</cffunction>

	<!--- <cffunction name="listeEquipement" access="remote" returntype="Query">
		<cfargument name="enr_playid" type="Numeric">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.list_equipements">
			<cfprocparam value="#enr_playid#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="q_result">
		</cfstoredproc>
		<cfreturn q_result> 
	</cffunction>
	 --->
	<cffunction name="listeDetailEquipement" access="remote" returntype="Struct">
		<cfargument name="marque" type="String">
		<cfargument name="model" type="String">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.detail_equipement">
			<cfprocparam value="#marque#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#model#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocresult name="result1" resultset="1">
			<cfprocresult name="result2" resultset="2">
		</cfstoredproc>
		
		<cfset obj= StructNew()>
		<cfset obj.PRIX = result1>
		<cfset obj.CARACTERISTIQUE = result2>		
		<cfreturn obj>
	</cffunction>

	<cffunction name="listOption" access="remote" returntype="Query">	
		<cfargument name="packcode" type="String">
		<cfargument name="idcmd" type="Numeric">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.list_options">
			<cfprocparam value="#packcode#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#idcmd#" cfsqltype="CF_SQL_INTEGER" type="in" null="true">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="compatibiliteOption" access="remote" returntype="Struct">	
		<cfargument name="packcode" type="String" required="true">
		<cfargument name="list" type="string">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.compatibilite_option">
			<cfprocparam value="#packcode#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#list#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam variable="code" cfsqltype="CF_SQL_INTEGER" type="out">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfset obj= StructNew()>
		<cfset obj.CODE = code>
		<cfset obj.LISTE= p_result>		
		<cfreturn obj>

	</cffunction>	

	<cffunction name="GetInfosCmd" access="remote" returntype="query">
		<cfargument name="id" type="Numeric">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.GET_INFOSAUVEGARDE">
			<cfprocparam value="#id#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>	

	<cffunction name="getTarifPack" access="remote" returntype="query">
		<cfargument name="enr_playid" type="Numeric">
		<cfargument name="offercode" type="String">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.get_tarif_pack">
			<cfprocparam value="#enr_playid#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#offercode#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocresult name="p_result">
		</cfstoredproc>		
		<cfreturn p_result>
	</cffunction>

	<cffunction name="getTarifOption" access="remote" returntype="query">
		<cfargument name="enr_playid" type="Numeric">
		<cfargument name="offercode" type="String">
		<cfargument name="attributecode" type="String">
		<cfargument name="valuecode" type="String">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.get_tarif">
			<cfprocparam value="#enr_playid#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#offercode#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#attributecode#" cfsqltype="CF_SQL_VARCHAR" type="in" null="true">
			<cfprocparam value="#valuecode#" cfsqltype="CF_SQL_VARCHAR" type="in" null="true">
			<cfprocresult name="p_result">
		</cfstoredproc>		
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getTarifEquipement" access="remote" returntype="query">
		<cfargument name="enr_playid" type="Numeric">
		<cfargument name="equipementcode" type="String">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.get_tarif_equipement">
			<cfprocparam value="#enr_playid#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#equipementcode#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocresult name="p_result">
		</cfstoredproc>		
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getTarifGlobal" access="remote" returntype="Array">
		<cfargument name="arr" type="Array">
		<cfset arrEngagement=arraynew(1)>
		
		<cfloop index="i" from="1" to=#ArrayLen(arr)#>
			<cfif arr[i]['type'] eq "PACK">
				<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.get_tarif_pack">
					<cfprocparam value="-1" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam value="#arr[i]['id']#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocresult name="p_result">
				</cfstoredproc>	
			</cfif>
			<cfif arr[i]['type'] eq "EQUIPEMENT">
				<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.get_tarif_equipement">
					<cfprocparam value="-1" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam value="#arr[i]['id']#" cfsqltype="CF_SQL_VARCHAR" type="in">
					<cfprocresult name="p_result">
				</cfstoredproc>		
			</cfif>
			<cfif arr[i]['type'] eq "OPTION">
				<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.get_tarif">
					<cfprocparam value="-1" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam value="#arr[i]['id']#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocparam value="#arr[i]['attributecode']#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocparam value="#arr[i]['valuecode']#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocresult name="p_result">
				</cfstoredproc>		
			</cfif>
			<cfset arrayappend(arrEngagement,p_result)>	
		</cfloop>
		<cfreturn arrEngagement>
	</cffunction>

	<cffunction name="listSite" access="remote" returntype="query">	
		<cfargument name="companyID" type="numeric">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.List_Site">
			<cfprocparam value="#companyID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="listCollaborateur" access="remote" returntype="query">	
		<cfargument name="siteID" type="numeric">
		<cfargument name="companyID" type="numeric">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.List_Collaborateur">
			<cfprocparam value="#siteID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#companyID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="listSDA" access="remote" returntype="query">	
		<cfargument name="siteID" type="numeric">
		<cfargument name="companyID" type="numeric">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.List_NDS">
			<cfprocparam value="#siteID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#companyID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>

	<cffunction name="addUser" access="remote" returntype="Numeric">
		<cfargument name="siteID" type="Numeric">
		<cfargument name="companyID" type="Numeric">	
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.addUser">
			<cfprocparam value="#siteID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#companyID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
			
	<cffunction name="valideCommande" access="remote" returntype="Numeric">
		<cfargument name="p_idihm_user" type="Numeric">
		<cfargument name="p_companyid" type="Numeric">
		<cfargument name="p_siteid" type="Numeric">
		<cfargument name="p_idtype_demande" type="Numeric">
		<cfargument name="p_idcode_operation" type="Numeric">
		<cfargument name="p_label" type="String">	
		<cfargument name="p_qte" type="Numeric">
		<cfargument name="p_obligationduration" type="Numeric">
						
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.valide_commande">
			<cfprocparam value="#p_idihm_user#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#p_companyid#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#p_siteid#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#p_idtype_demande#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#p_idcode_operation#" cfsqltype="CF_SQL_INTEGER" type="in">			
			<cfprocparam value="#p_label#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#p_qte#" cfsqltype="CF_SQL_INTEGER" type="in">			
			<cfprocparam value="#p_obligationduration#" cfsqltype="CF_SQL_INTEGER" type="in">			
			<cfprocparam variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
			
	<cffunction name="validePack" access="remote" returntype="Numeric">
		<cfargument name="p_idgestion_pack" type="Numeric">
		<cfargument name="p_idgestion_person" type="Any">
		<cfargument name="p_IDCODE_OPERATION" type="Numeric">
		<cfargument name="p_companyid" type="Numeric">
		<cfargument name="p_siteid" type="Numeric">
		<cfargument name="p_nds" type="String">	
		<cfargument name="p_msisdn" type="String">	
		<cfargument name="p_code_rio" type="String">	
		<cfargument name="p_useridbew" type="String">	
		<cfargument name="p_c_personid" type="Numeric">
		<cfargument name="p_collaborateur" type="String">	
		<cfargument name="p_date_portabilite" type="String">
		<cfargument name="p_DUREE_ENGAGEMENT" type="numeric">	
		
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.valide_item">
			<cfprocparam value="#p_idgestion_pack#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#p_idgestion_person#" cfsqltype="CF_SQL_INTEGER" null="true">
			<cfprocparam value="#p_IDCODE_OPERATION#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#p_companyid#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#p_siteid#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#p_nds#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#p_msisdn#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#p_code_rio#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#p_useridbew#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#p_c_personid#" cfsqltype="CF_SQL_INTEGER">			
			<cfprocparam value="#p_collaborateur#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#p_date_portabilite#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#p_DUREE_ENGAGEMENT#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="valideOption" access="remote" returntype="Numeric">
		<cfargument name="tab" type="Array">
		<cfset result = 1>
		
		<cfloop index="i" from="1" to=#ArrayLen(Tab)#>
			<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.valide_item_cat">
				<cfprocparam value="#val(Tab[i]['idgestion_item'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#val(Tab[i]['offercode'])#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam value="#val(Tab[i]['attributecode'])#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam value="#val(Tab[i]['valuecode'])#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam value="#val(Tab[i]['engagement'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">		
			</cfstoredproc>
			<cfif p_result eq -1>
				<cfset result = -1>
			</cfif>
		</cfloop>
		<cfreturn result>
	</cffunction>

	<cffunction name="valideEquipement" access="remote" returntype="Numeric">
		<cfargument name="tab" type="array">
		<cfset result = 1>
		
		<cfloop index="i" from="1" to=#ArrayLen(Tab)#>
			<cfstoredproc datasource="MBPOFFRE" procedure="pkg_commande.valide_item_equipt">
				<cfprocparam value="#val(Tab[i]['idgestion_item'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#val(Tab[i]['equipmentcode'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#val(Tab[i]['engagement'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
			<cfif p_result eq -1>
				<cfset result = -1>
			</cfif>
		</cfloop>
		<cfreturn result>
	</cffunction>
	
</cfcomponent>
