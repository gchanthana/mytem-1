<cfcomponent name="siteService">
	
	<cffunction name="getInfosBlocSite" access="remote" returntype="query" description="">
		<cfargument name="idSociete" type="numeric" required="true">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_mbp_global.getInfosBlockSite">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSociete#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	
	<cffunction name="getListeSite" access="remote" returntype="query" description="">
		<cfargument name="idSociete" type="numeric" required="true">
		<cfargument name="search" type="string" required="false" default="">
		<cfstoredproc datasource="MBPOFFRE" procedure="Pkg_mbp_global.getListeSites">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSociete#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#search#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	
	<cffunction name="getListeSiteRecherchePack" access="remote" returntype="query" description="">
		<cfargument name="idSociete" type="numeric" required="true">
		<cfargument name="loginID" type="numeric" required="true">
		<cfstoredproc datasource="MBPOFFRE" procedure="Pkg_parc.ListeSites">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSociete#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#loginID#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	
	<cffunction name="getGestionnairesSite" access="remote" returntype="query" description="">		
		<cfargument name="idSite" type="numeric" required="true">
		<cfargument name="idCompany" type="numeric" required="true">
		
		<cfstoredproc datasource="MBPOFFRE" procedure="Pkg_mbp_global.getGestionnairesSite">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSite#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idCompany#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="majGestionnairesSite" access="remote" returntype="numeric" description="">
		
		<cfargument name="idSite" type="numeric" required="true">
		<cfargument name="idCompany" type="numeric" required="true">
		
		<cfargument name="idGestionnaire" type="numeric" required="true">
		<cfargument name="idGestionnaireDelegue" type="numeric" required="true">
		
		<cfargument name="idihm_user" type="numeric" required="true">
		
		<cfstoredproc datasource="MBPOFFRE" procedure="Pkg_mbp_global.majGestionnairesSite">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSite#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGestionnaire#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGestionnaireDelegue#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idihm_user#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idCompany#" null="false">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
		
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="majRegleLivraison" access="remote" returntype="numeric" description="">
		<cfargument name="idSite" type="numeric" required="true">
		<cfargument name="idCompany" type="numeric" required="true">
		
		<cfargument name="boolCentralise" type="numeric" required="true">
		
		<cfargument name="idihm_user" type="numeric" required="true">
		
		<cfstoredproc datasource="MBPOFFRE" procedure="Pkg_mbp_global.majRegleLivraison">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSite#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#boolCentralise#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idihm_user#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idCompany#" null="false">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction> 
	
	
	<cffunction name="getInfosTechSite" access="remote" returntype="query" description="">
		
		<cfargument name="idSite" type="numeric" required="true">
		<cfargument name="idCompany" type="numeric" required="true">
		
		<cfargument name="idihm_user" type="numeric" required="true">
		
		<cfstoredproc datasource="MBPOFFRE" procedure="Pkg_mbp_global.getInfosTechSite">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSite#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idihm_user#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idCompany#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getAnnuaireActeursSociete" access="remote" returntype="query" description="">
		<cfargument name="idSociete" type="numeric" required="true">
		<cfargument name="typeUser" type="String" required="true">
			<cfstoredproc datasource="MBPOFFRE" procedure="Pkg_mbp_global.getAnnuaireActeursSociete">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSociete#" null="false">
				<cfprocparam type="In" cfsqltype="cf_SQL_VARCHAR" value="#typeUser#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	
	<cffunction name="getAllGestionnairesClient" access="remote" returntype="query" description="">
		<cfargument name="idSociete" type="numeric" required="true">
			<cfstoredproc datasource="MBPOFFRE" procedure="Pkg_mbp_global.getAllGestionnairesClient">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSociete#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	
	<cffunction name="getListeSociete" access="remote" returntype="query" description="">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_accreditation.ListCompany">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result> 
	</cffunction>
	
	<cffunction name="getListeCollabOfSite" access="remote" returntype="query" description="">
		<cfargument name="idSite" type="numeric" required="true">
		<cfargument name="idCompany" type="numeric" required="true">
		
		<cfstoredproc datasource="MBPOFFRE" procedure="Pkg_parc.ListeCollaborateur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSite#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idCompany#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
		
</cfcomponent>