<!-- Création des campagnes pour une racine, tel que definit dans la table contrat -->
<cfstoredproc datasource="ROCOFFRE" procedure="pkg_tellcost.GlobalLaunchCampagne">
	<cfprocparam variable="p_retour" cfsqltype="CF_SQL_INTEGER" type="out">
</cfstoredproc>

<cfif p_retour eq 1>
<cfquery datasource="ROCOFFRE" name="qgetInfo">
select tl.login, tl.contenu_texto, gc.libelle_groupe_client, tca.idtc_campagne
from tc_login tl, tc_contrat tc, tc_campagne tca, groupe_client gc
WHERE tl.iidtc_campagne=tca.idtc_campagne
AND tca.idcontrat=tc.idcontrat
AND tc.idracine=gc.idgroupe_client
AND tca.date_envoi IS NULL
and tca.statut='PRE'
order by tca.idtc_campagne, tl.login
</cfquery>

<cfoutput query="qgetInfo" group="idtc_campagne">
<cfoutput>
	<cfmail from="brice.miramont@consotel.fr" to="#login#@contact-everyone.fr" server="mail.consotel.fr" 
			port="26" subject="#libelle_groupe_client#_#idtc_campagne#">#contenu_texto#


</cfmail>
	</cfoutput>
	<!-- Mise à jour de la campagne -> date d'envoi des SMS' -->
	<cfstoredproc datasource="ROCOFFRE" procedure="pkg_tellcost.sendCampagne">
		<cfprocparam value="#idtc_campagne#" cfsqltype="CF_SQL_INTEGER" type="in">
		<cfprocparam variable="retour" cfsqltype="CF_SQL_INTEGER" type="out">
	</cfstoredproc>
</cfoutput>
</cfif>

<!--- 
<cfabort>

<cfoutput query="qgetInfo" group="sous_tete">
<cfmail from="brice.miramont@consotel.fr" to="#sous_tete#@contact-everyone.fr" server="mail.consotel.fr" port="26" subject="consoSMS#now()#">
consoSMS #short_mois# 
Code:#code#
<cfoutput group="type_theme">
<cfif type_theme eq "Consommations">Consos<cfelse>Abos</cfif>
<cfoutput><cfif type_theme eq "Consommations"><cfif unite_appel eq "S"><cfset h=int(volume/3600)><cfset m=int((volume-(h*3600))/60)><cfset s=volume-h*3600-m*60>#LsNumberFormat(evaluate(h),"00")#h#LsNumberFormat(evaluate(m),"00")#mn#LsNumberFormat(evaluate(s),"00")#s : #LsCurrencyFormat(montant)# <cfif left(delta_montant,1) eq "+">(#delta_montant#)</cfif><cfelse>#volume# #unite_appel# : #LsCurrencyFormat(montant)# <cfif left(delta_montant,1) eq "+">(#delta_montant#)</cfif></cfif><cfelse>#LsCurrencyFormat(montant)#</cfif>
</cfoutput></cfoutput>


</cfmail>    
</cfoutput>
 --->

