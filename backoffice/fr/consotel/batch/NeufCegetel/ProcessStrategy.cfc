<cfcomponent name="ProcessStrategy" output="false">

	<!--- Méthode pour se logger, on ramène le cookie --->
	<cffunction name="Login" access="remote" returntype="String" output="true">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		<cfabort showerror="Methode Abstraite. Vérifier votre code.">
	</cffunction>
	
	<!--- Méthode pour préparer l'extranet avant d'accéder aux factures --->
	<cffunction name="PrepareExtranet" access="remote" returntype="array" output="true">
		<cfargument name="jsession" type="String">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		<cfabort showerror="Methode Abstraite. Vérifier votre code.">
	</cffunction>
	
	<!--- Méthode pour se connecter au compte (chez Cegetel Seulement) --->
	<cffunction name="connectCompte" access="remote" returntype="string" output="true">
		<cfargument name="jsession" type="String">
		<cfargument name="IdCompte" type="String">
		<cfabort showerror="Methode Abstraite. Vérifier votre code.">
	</cffunction>
	
	<!--- Méthode pour aller chercher la liste des factures --->
	<cffunction name="getListeFactures" access="remote" returntype="array" output="true">
		<cfargument name="jsession" type="String">
		<cfabort showerror="Methode Abstraite. Vérifier votre code.">
	</cffunction>
	
	<!--- Méthode pour aller télécharger le fichier --->
	<cffunction name="getFile" access="remote" returntype="string" output="true">
		<cfargument name="jsession" type="String">
		<cfargument name="ElementFacture" type="array">
		<cfabort showerror="Methode Abstraite. Vérifier votre code.">
	</cffunction>
	
</cfcomponent>