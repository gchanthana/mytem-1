<cfcomponent name="ExtranetNeufCegetelCDR" output="true">
	<cfset variables.instance.dateDansAdresse1=0>
		
	<cffunction name="Main" access="remote" returntype="String" output="true">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		<cfargument name="extranet" type="String">
		<cfargument name="nbreMois" type="numeric">
		<cfargument name="shiftMois" type="numeric">
		<cfargument name="compte" type="string">
		<cfif Trim(compte) eq "">
			<CFSET compte="INCONNU">
		</cfif>
		<!--- Client Neuf Cegetel --->
		<cfif Trim(extranet) eq 1>
			<cfset extranet='Neuf'>
		<!--- Client Cegetel --->
		<cfelseif Trim(extranet) eq 2>
			<cfset extranet='Cegetel'>
		<cfelseif Trim(extranet) eq 4>
			<cfset extranet='NeufMulti'>
		</cfif>
		<cfset tempChaine = "<html><body><table border='0'>">
		<cfset compteurFichier=0>
		<cfoutput>
		<!--- <table border="1"><tr><td>#compte#<td>#login#</td><td>#pwd#</td></tr> --->
		<!--- Met en place la strategie selon le type d'extranet --->
		<!--- Les valeurs sont: Neuf et Cegetel --->
		<cfset setStrategy(extranet)>
		<!--- On se log --->		
		<cfset jCookie=variables.instance.ProcessStrategy.Login(login,pwd)>
		<!--- Prï¿½paration de l'extranet avant d'aller ï¿½ la page des factures --->
		<!--- Ramï¿½ne la liste des comptes concernï¿½s --->
		<cfset aClients=variables.instance.ProcessStrategy.PrepareExtranet(jCookie,login,pwd)>
		<!--- Pour chaque compte on va a la page de facture et on tï¿½lï¿½charge --->
		<cfset tempChaine = tempChaine & "<tr><td>Fichiers TÃ©lÃ©chargÃ©s</td></tr>">
		<cfset tempChaine = tempChaine & "<tr><td><hr></td></tr>">
		<!--- <tr><td>Nbr Compte<td>#ArrayLen(aClients)#</td><td></td></tr>
		</table> --->
		<cfif ArrayLen(aClients) eq 2>
			<cfloop from="1" to="#ArrayLen(aClients)#" index="i">
				<!--- Va cherche le cookie si nï¿½cessaire (Cegetel) --->
				<cfset jCookie2=variables.instance.ProcessStrategy.connectCompte(jCookie,aClients[i])>
				<!--- <cfoutput><b>#i#</b>:#getToken(aClients[i],6,"#chr(34)#")#:#getToken(getToken(aClients[i],8,"#chr(34)#"),4,"-")#<br></cfoutput> --->
				<!--- Va chercher les factures --->
				<cfset aListeFacture=variables.instance.ProcessStrategy.getListeFactures(jCookie2,nbreMois,shiftMois)>
				<!--- <cfoutput>#i#:#ArrayLen(aListeFacture)#<br></cfoutput> --->
				<!--- Pour chaque facture, on va chercher le fichier --->
				<cfflush interval="20">
				<cfloop from="1" to="#ArrayLen(aListeFacture)#" index="j">
					<cfset EtatFichier=variables.instance.ProcessStrategy.getFile(jCookie2,aListeFacture[j],compte)>
					<cfif EtatFichier neq "">
						<cfset compteurFichier=compteurFichier+1>
						<!--- <cfset tempChaine = tempChaine & "<tr><td>#EtatFichier#</td></tr>"> --->
					</cfif>
				</cfloop>
			</cfloop>
		<cfelseif ArrayLen(aClients) gt 2>
			<cfloop from="3" to="#ArrayLen(aClients)#" index="i">
				<!--- Va cherche le cookie si nï¿½cessaire (Cegetel) --->
				<cfset jCookie2=variables.instance.ProcessStrategy.connectCompte(jCookie,aClients[i])>
				<!--- <cfoutput><b>#i#</b>:#getToken(aClients[i],6,"#chr(34)#")#:#getToken(getToken(aClients[i],8,"#chr(34)#"),4,"-")#<br></cfoutput> --->
				<!--- Va chercher les factures --->
				<cfset aListeFacture=variables.instance.ProcessStrategy.getListeFactures(jCookie2,nbreMois,shiftMois)>
				<!--- <cfoutput>#i#:#ArrayLen(aListeFacture)#<br></cfoutput> --->
				<!--- Pour chaque facture, on va chercher le fichier --->
				<cfflush interval="20">
				<cfloop from="1" to="#ArrayLen(aListeFacture)#" index="j">
					<cfset EtatFichier=variables.instance.ProcessStrategy.getFile(jCookie2,aListeFacture[j],compte)>
					<cfif EtatFichier neq "">
						<cfset compteurFichier=compteurFichier+1>
						<!--- <cfset tempChaine = tempChaine & "<tr><td>#EtatFichier#</td></tr>"> --->
					</cfif>
				</cfloop>
			</cfloop>
		<cfelse>
			<!--- Va cherche le cookie si nï¿½cessaire (Cegetel) --->
			<cfset jCookie2=variables.instance.ProcessStrategy.connectCompte(jCookie,"")>
			<!--- Va chercher les factures --->
			<cfset aListeFacture=variables.instance.ProcessStrategy.getListeFactures(jCookie2,nbreMois,shiftMois)>
			<cfdump var="#ArrayLen(aListeFacture)#"><br>
			<!--- Pour chaque facture, on va chercher le fichier --->
			<cfloop from="1" to="#ArrayLen(aListeFacture)#" index="j">
				<cfset EtatFichier=variables.instance.ProcessStrategy.getFile(jCookie2,aListeFacture[j],compte)>
				<cfif EtatFichier neq "">
					<cfset compteurFichier=compteurFichier+1>
					<!--- <cfset tempChaine = tempChaine & "<tr><td>#EtatFichier#</td></tr>"> --->
				</cfif>
			</cfloop>
		</cfif>
		
		<cfset tempChaine = tempChaine & "<tr><td><hr></td></tr>">		
		<cfset tempchaine = tempchaine & "</table></body></html>">
		<!--- <cfif compteurFichier gt 0>
			<cfmail from="Agent Oracle <production@consotel.fr>" to="import@consotel.fr" server="192.168.3.119" 
					port="26"  type="HTML" subject="[Extranet NeufCegetel]- #compte#">
				<style>
				html body {
					margin:0;
					padding:0;
					background: ##D4D0C8;
					font:x-small Verdana,Sans-serif;
				}
				p {
					font:bold x-small EurostileBold,verdana,arial,helvetica,serif;
				}
				td {
					font-size : x-small;
					/*color: ##629FB5;*/
				}
				</style>
						
				<p>Login : #login#<br>
				Compte : #compte#<br>
				</p>
				Fichiers : #compteurFichier#
			</cfmail>
		</cfif> --->
		</cfoutput>
		<cfflush interval="50">
		<cfreturn "Ok">
	</cffunction>
		
	<!--- Pour mettre en place la strategie --->
	<cffunction name="setStrategy" access="public" returntype="void" output="false" >
		<cfargument name="TYPE_EXTRANET" required="false" type="string" default="" displayname="string TYPE_EXTRANET" hint="type de pï¿½rimetre" />
		<cfset temp=createObject("component","#TYPE_EXTRANET#CDRProcessStrategy")>
		<cfset setProcessStrategy(#temp#)>
	</cffunction>
	
	<!--- Setter pour la strategie --->
	<cffunction name="setProcessStrategy" access="public" output="false" returntype="void">
		<cfargument name="newProcessStrategy" type="ProcessStrategy" required="true" />
		<cfset variables.instance.ProcessStrategy = arguments.newProcessStrategy />
	</cffunction>
</cfcomponent>