	<cfif IsDefined ('SESSION')>
		<cfif IsDefined ('SESSION.IS_CONNECTED')>
			<cfif SESSION.IS_CONNECTED EQ 0> 		
				<cflocation url="index_pc.cfm">
			</cfif>
		<cfelse>	 
			<cflocation url="index_pc.cfm">
		</cfif>	
	<cfelse>	 
		<cflocation url="index_pc.cfm">
	</cfif>

	<?xml version="1.0" encoding="iso-8859-1" ?>
	<!DOCTYPE html PUBLIC
	"-//WAPFORUM//DTD XHTML Mobile 1.0//EN"
	"http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
	<head>
		<title>
			TELLCOST
		</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
		<meta name="robots" content="all"/>
		<meta name="classification" content="Services"/>
		<meta name="copyright" content="ConsoTel SAS"/>
		<meta name="author" content="Guillaume RAMOS"/>
		<meta name="language" content="fr"/>
		<meta name="description" content="TELLCOST"/>
		<meta name="keywords" content="TELLCOST"/>

		<STYLE TYPE="TEXT/CSS">
			#MONCADRE {
			HEIGHT:100%;
			WIDTH:90%;
  		 	PADDING:4PX; 
			MARGIN-LEFT:10PX;
			MARGIN-RIGHT:10PX;
			MARGIN-TOP:10PX;
			MARGIN-BOTTOM:10PX; 
			}
			#MONCADRE1 {
			HEIGHT:100%;
			WIDTH:90%;
			}
			.CADREDUMENUGAUCHE {
			FLOAT:LEFT;
			TEXT-ALIGN:LEFT;
			WIDTH:90%;
			HEIGHT:100%;
			}
			.CADREDUMENUDROITE {
			FLOAT:RIGHT;
			TEXT-ALIGN:LEFT;
			WIDTH:20%;
			HEIGHT:100%;
			}
			#CADREHAUT {
			WIDTH:1024PX;
			HEIGHT:100PX;
			}
			#CADREHAUT2 {
			TEXT-ALIGN:CENTER;
			WIDTH:90%;
			HEIGHT:100%;
			PADDING:4PX; 
			MARGIN-LEFT:10PX;
			MARGIN-RIGHT:10PX;
			MARGIN-TOP:10PX;
			MARGIN-BOTTOM:10PX; 
			}
			
			#CADRECENTRALE { 
			MARGIN-LEFT:180PX;
			MIN-HEIGHT:150PX;
			MARGIN-RIGHT:180PX;
			BORDER-LEFT:0PX DASHED #FFFFFF; 
			BORDER-RIGHT:0PX DASHED #FFFFFF;
			BORDER-BOTTOM:0PX DASHED #FFFFFF;
			BORDER-TOP:0PX DASHED #FFFFFF;
			BACKGROUND-COLOR:#99CC99;
			}

		 	<!--- body {
			background:url(tellCostImg/fondtellcost.jpg) center repeat #fff;
			HEIGHT:100%;
			} ---> 
			
			td{ 
			border: solid black; 
			}
		</STYLE>

		<script language="Javascript">
			if (document.images) 
			{
			button1 = new Image
			button2 = new Image
		
			button1.src = "tellCostImg/deconnexion_n.gif"
			button2.src = "tellCostImg/deconnexion_n.gif"
			}
			
			if (document.images) 
			{
			button3 = new Image
			button4 = new Image
		
			button3.src = "tellCostImg/plus2detail_n.gif"
			button4.src = "tellCostImg/plus2detail_n.gif"
			}
			
			if (document.images) 
			{
			button5 = new Image
			button6 = new Image
		
			button5.src = "tellCostImg/mesoptions_n.gif"
			button6.src = "tellCostImg/mesoptions_n.gif"
			}
			
			if (document.images) 
			{
			button7 = new Image
			button8 = new Image
		
			button7.src = "tellCostImg/crmf_n.gif"
			button8.src = "tellCostImg/crmf_n.gif"
			}
			
			if (document.images) 
			{
			button9 = new Image
			button10 = new Image
		
			button9.src = "tellCostImg/Acceuil_n.gif"
			button10.src = "tellCostImg/Acceuil_n.gif"
			}
		</script> 
	</head>

<!--- Variables function --->

	<cfset Conseil = "">
	<cfset obj = createobject('component','TellCostService')>	
		
<!--- Fin variables function--->

	<BODY>
		<FONT face="Arial" size="3">
			<div align="center"> 
		 		<DIV id="CADREHAUT">
					 <br/><br/>
				 	<img src="tellCostImg/SocieteGeneral.jpg" alt="header" height="45" align="left"/>
				 	<img src="tellCostImg/tellCostSGMini.png" alt="header" height="45" align="right"/>
				</DIV>
				
				<DIV ID="CADREHAUT2">
					<table border="0" style="border:0px solid #000000;margin:auto;border-collapse: collapse;">
						<tr> 
							<td rowspan="2" style="border:0px">
								<a href="<cfoutput>details.cfm?#SESSION.URLTOKEN#</cfoutput>" style="text-decoration:none">
									<img src="tellCostImg/Acceuil_n.gif" alt="DETAILS" title="DETAILS" border="0" hspace="10%">
								</a>
							</td>
							<td rowspan="2" style="border:0px">
								<a href="<cfoutput>moredetails.cfm?#SESSION.URLTOKEN#</cfoutput>" style="text-decoration:none">
									<img src="tellCostImg/plus2detail_n.gif" alt="PLUSDEDETAILS" title="PLUS DE DETAILS" border="0" hspace="10%">
								</a>
							</td>
							<td rowspan="2" style="border:0px">
								<a href="<cfoutput>mesoptions.cfm?#SESSION.URLTOKEN#</cfoutput>" style="text-decoration:none">
									<img src="tellCostImg/visible/mesoptions_n.png" alt="MESOPTIONS" border="0" title="MES OPTIONS" hspace="10%">	
								</a>
							</td>
							<td rowspan="2" style="border:0px">
								<a style="text-decoration:none">
									<img src="tellCostImg/visible/reduire_blue.png" alt="REDUIRE" title="REDUIRE" border="0" hspace="10%"/>
								</a>
							</td>
							<td rowspan="2" style="border:0px">
								<a href="<cfoutput>index_pc.cfm</cfoutput>" style="text-decoration:none">
									<img src="tellCostImg/deconnexion_n.gif" alt="DECONNEXION" title="DECONNEXION" border="0" hspace="10%"/>
								</a>
							</td>
						</tr>
					</table>	
				</DIV> 	
				
				<DIV ID="MONCADRE">
					<!--- <img src="tellCostImg/tellCostTestMini.png" border="0"  weight="10"  align="left"/>
					<br/><br/> ---><br/>
				
					<div align="left">
						Votre ligne : <cfoutput>#SESSION.PHONENUMBER#</cfoutput>
					</div>
	
					<br/>
					
					<div align="left">
						Conseils du mois :
					</div>
	
					<br/><br/>
					
					<cfset q = obj.zQueryGetConseil(SESSION.IDTHEME,SESSION)> 
					<FONT face="Arial" size="3">
						<div align="justify"> 
							<cfoutput>#SESSION.LONG_THEME#</cfoutput>
						</div>
					</FONT>
				</DIV>
			
				<DIV ID="MONCADRE">
					<table border="0" style="border:0px solid #000000;margin:auto;border-collapse: collapse;">
						<tr>
							<td  align="left" style="border:0px solid #000000;">
								<img src="tellCostImg/tellCostTestMiniMini.png" border="0"  weight="0"  align="left"/>
							</td>
							<td align="center" style="width: 100%;border:0px solid #000000;">
								<p><a href="mailto:Monsuivi.consomobile@socgen.com?subject=Question tellcost" accesskey="3">Monsuivi.consomobile@socgen.com</a></p>
							</td>
							<td align="right" style="border:0px solid #000000;">
								<img src="tellCostImg/tellCostTestPowered.png" alt="header" height="35" align="right"/>
							</td>
						</tr>
					</table>
				</DIV>
			</div>
			
			<!--- <div align="center"> 
				<p><a href="mailto:Monsuivi.consomobile@socgen.com?subject=Question tellcost" accesskey="3">Monsuivi.consomobile@socgen.com</a></p>
			</div>
			 --->
			<!--- <DIV ID="MONCADRE">
				<table width="100%" border="0" style="border:0px solid #000000;margin:auto;border-collapse: collapse;"> 
					<tr>  
						<td rowspan="2" width="250" style="border:0px">
							<img src="tellCostImg/tellCostTestPowered.png" alt="header" height="35" align="right"/> 
						</td>  
					</tr> 
				</table> 
			</DIV> --->
		</FONT>
	</BODY>
</html>