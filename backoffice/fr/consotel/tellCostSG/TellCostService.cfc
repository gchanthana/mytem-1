<cfcomponent output="false">

<!--- VARIABLES --->

	<cfset OFFREDSN="ROCOFFRE">

<!--- FIN VARIABLES --->

<!--- PROCÉDURE --->

	<cffunction name="zQueryCheckLog" access="remote" returntype="Struct">
		<cfargument name="id" 		type="String" required="true">
		<cfargument name="pwd" 		type="String" required="true">
		<cfargument name="SESSIONN" type="Struct" required="true">
		
			<cfset SESSIONOBJ = structNew()>
			<cfset SESSIONOBJ = SESSIONN>
		
			<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_tellcost.validatelogin_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_login" 	value="#id#">   
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_pwd" 	value="#pwd#">                                                 	
	           	<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER"  variable="p_idsous_tete">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER"  variable="p_idperiode_mois">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER"  variable="p_idracine_master">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER"  variable="p_idtheme"> 
				<cfprocparam type="Out" cfsqltype="CF_SQL_VARCHAR2" variable="p_libelle_mois"> 
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER"  variable="p_idcamapgne"> 
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER"  variable="p_type_url"> 
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER"  variable="p_idracine"> 				
			</cfstoredproc>

			<cfif p_idsous_tete EQ 0>
				<cfset SESSIONOBJ.RETOUR = 0>
			<cfelse>
				<cfset SESSIONOBJ.IDSOUS_TETE = p_idsous_tete>
				<cfset SESSIONOBJ.IDRACINE_MASTER = p_idracine_master>
				<cfset SESSIONOBJ.IDPERIODE = p_idperiode_mois> 
				<cfset SESSIONOBJ.IDTHEME = p_idtheme>
				<cfset SESSIONOBJ.LIBELLE_MOIS = p_libelle_mois>
				<cfset SESSIONOBJ.IDCAMPAGNE = p_idcamapgne>
				<cfset SESSIONOBJ.LONG_THEME = "">
				<cfset SESSIONOBJ.TYPE_URL = p_type_url>
				<cfset SESSIONOBJ.IDRACINE = p_idracine>
				<cfset SESSIONOBJ.RETOUR = 1>
			</cfif>
			
			<!--- -4 CORRESPOND A LA VALIDITER DU LOGIN MET PAS POUR LA SOCIETE GENERAL --->
			<cfif SESSIONOBJ.RETOUR EQ 1>
				<cfif p_type_url EQ 2>
					<cfset SESSIONOBJ.RETOUR = 1>
				<cfelse>
					<cfset SESSIONOBJ.RETOUR = -4>
				</cfif>
			</cfif>
			
		<cfreturn SESSIONOBJ> 
	</cffunction> 

	<cffunction name="zQueryConso" access="remote" returntype="Query">
		<cfargument name="sousteteID" 	type="Numeric" required="true">
		<cfargument name="campagneID" 	type="Numeric" required="true">
		<cfargument name="affichageID" 	type="Numeric" required="true">
		<cfargument name="idracine" 	type="Numeric" required="true">

		 	<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_tellcost.getConso">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" variable="p_idsous_tete" 		value="#sousteteID#">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" variable="p_idcampagne" 		value="#campagneID#">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" variable="p_idtype_affichage" 	value="#affichageID#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<cffunction name="zQueryAbo" access="remote" returntype="Query">
		<cfargument name="sousteteID" type="Numeric" required="true">
		<cfargument name="campagneID" type="Numeric" required="true">
		
			<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_tellcost.getabo">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" variable="p_idsous_tete" 	value="#sousteteID#">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" variable="p_idcampagne" 	value="#campagneID#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction> 

	<cffunction name="zQueryGetConseil" access="remote" returntype="Struct">
		<cfargument name="themeID" 		type="Numeric" required="true">
		<cfargument name="SESSIONOBJ" 	type="Struct"  required="true">
		
			<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_tellcost.getConseilTheme_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idtheme"  value="#themeID#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#SESSIONOBJ.IDRACINE#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
			<cfset SESSIONOBJ.themeID = p_retour['THEMEID']>
			<cfset SESSIONOBJ.SHORT_THEME = p_retour['SHORT_DESC']>
			<cfset SESSIONOBJ.LONG_THEME = p_retour['LONG_DESC']>
		
		<cfreturn SESSIONOBJ>
	</cffunction> 
	
	<cffunction name="zAddDate" access="remote" returntype="Struct">
		<cfargument name="campagneID" type="Numeric" required="true">		
		<cfargument name="SESSIONOBJ" type="Struct"  required="true">
		
			<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_tellcost.getBillDates_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcampagne" 		value="#campagneID#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" 	value="#SESSIONOBJ.IDRACINE_MASTER#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
		
		 	<cfset SESSIONOBJ.DATEDEBUT = p_retour['DATE_DEB']>
			<cfset SESSIONOBJ.DATEFIN = p_retour['DATE_FIN']>
			<cfset SESSIONOBJ.DATEFACTURE = p_retour['DATE_FACT_OP']>
	
		<cfreturn SESSIONOBJ>
	</cffunction> 
	
<!--- FIN PROCÉDURE --->

</cfcomponent>