<cfcomponent output="false">
	<cfset containerProxy = createObject("component","fr.consotel.consoview.api.container.ApiContainerV1")>
	<!--- MISE A JOUR DU STATUS DU RAPPORT 
		http://download.oracle.com/docs/cd/E10415_01/doc/bi.1013/e12188/T421739T524310.htm#jobhistoryinfo
		Status : "Completed", "Error", "Running", "Scheduled", "Suspended" and "Unknown" 
	--->
	<cfset STATUS_CODES=structNew()>
	<cfset STATUS_CODES["S"]="Completed">
	<cfset STATUS_CODES["F"]="Error">
	<cfset STATUS_CODES["W"]="Unknown">
	<cfset STATUS_CODES["P"]="Suspended">
	
	<cfset mailDumpInfos ={}>
	<cfset mailDumpInfos["expediteur"] = "process_container@saaswedo.com">
	<cfset mailDumpInfos["destinataire"] = "monitoring@saaswedo.com">
	<cfset mailDumpInfos["server"] = "mail-cv.consotel.fr">
	<cfset mailDumpInfos["serverPort"] = "25">
	
	
	<!--- ********************************************** --->	
	<cffunction name="run" access="public" returntype="Numeric">
		<cfargument name="JOB_ID"		type="Numeric"	required="true">
		<cfargument name="STATUS"		type="String" 	required="true">
		<cfargument name="BI_SERVER"	type="String" 	required="true">
				
		<cflog text="********************************************** PROCESSCONTAINER - function run ">
		
	<!--- INSTANCIATION DE L'API CONTAINER --->	
		
		<cfset objRapport = containerProxy.getInfosFromJOBID(JOB_ID,BI_SERVER)>		
		
		<cflog text="********************************************** PROCESSCONTAINER - #objRapport.APP_LOGINID#">
		<cflog text="********************************************** PROCESSCONTAINER - #objRapport.IDRACINE# ">
		<cflog text="********************************************** PROCESSCONTAINER - #STATUS#  ">
		<cflog text="********************************************** PROCESSCONTAINER - #objRapport.GLOBALIZATION#">		
		<cflog text="********************************************** PROCESSCONTAINER - #objRapport.LASTNAME#  #objRapport.FIRSTNAME#">
		<cflog text="********************************************** PROCESSCONTAINER - #objRapport.UUID# ">
		<cflog text="********************************************** PROCESSCONTAINER - #objRapport.ColumnList#">
		
		<cftry>
	<!--- MISE A JOUR DU STATUS DU RAPPORT --->
			<cflog  text="********************************************** PROCESSCONTAINER - fonction run : #Ucase(arguments.STATUS)#">
			
			<cflog text="********************************************** PROCESSCONTAINER - CODEAPPLICATION = #objRapport.CODEAPPLICATION# ">		
			
			<cfif #Ucase(arguments.STATUS)# eq Ucase(STATUS_CODES["F"])>
				<cfset varfct = fctRapportErreur(objRapport.APP_LOGINID,objRapport.IDRACINE,objRapport.UUID,objRapport.NOM_DETAILLE_RAPPORT,objRapport.DATE_DEMANDE,objRapport.RACINE,objRapport.MAIL,STATUS,objRapport.GLOBALIZATION,objRapport.LASTNAME,objRapport.FIRSTNAME,objRapport.CODEAPPLICATION)>
			<cfelseif #Ucase(arguments.STATUS)# eq Ucase(STATUS_CODES["S"])>
				<cfset varfct = fctRapportGenere(objRapport.APP_LOGINID,objRapport.IDRACINE,objRapport.UUID,objRapport.NOM_DETAILLE_RAPPORT,objRapport.DATE_DEMANDE,objRapport.RACINE,objRapport.MAIL,STATUS,objRapport.GLOBALIZATION,objRapport.LASTNAME,objRapport.FIRSTNAME,objRapport.CODEAPPLICATION)>
			<cfelseif (#Ucase(arguments.STATUS)# eq Ucase(STATUS_CODES["W"])) OR (#Ucase(arguments.STATUS)# eq Ucase(STATUS_CODES["P"]))>
				<cfset varfct = fctRapportErreur(objRapport.APP_LOGINID,objRapport.IDRACINE,objRapport.UUID,objRapport.NOM_DETAILLE_RAPPORT,objRapport.DATE_DEMANDE,objRapport.RACINE,objRapport.MAIL,STATUS,objRapport.GLOBALIZATION,objRapport.LASTNAME,objRapport.FIRSTNAME,objRapport.CODEAPPLICATION)>
			<cfelse>
				<cfmail from="#mailDumpInfos.expediteur#" 
							  to="#mailDumpInfos.destinataire#" 
							  type="html" 
							  server="#mailDumpInfos.server#" 
							  port="#mailDumpInfos.serverPort#" 
							  charset="utf-8" wraptext="74"
						subject="(container) Status inconnu">
					Le fichier #objRapport.NOM_DETAILLE_RAPPORT#.#objRapport.FORMAT_FICHIER# dont le jobID est #arguments.JOB_ID#, a un status inconnu et donc non géré par le processContainer.
					<br>
					Cordialement.
				</cfmail>
			</cfif> 
			
			<cflog  text="********************************************** PROCESSCONTAINER - fonction run : varfct = #varfct#">
			<cfreturn varfct>
		<cfcatch>
			<cflog  text="********************************************** PROCESSCONTAINER - fonction run : erreur">
			<cfset str= "#cfcatch.Type# : #cfcatch.Message# - #cfcatch.Detail#">
			<cflog text="********************************************** #str#">
			<cfreturn -1>
		</cfcatch>	
		</cftry>
	</cffunction>
<!--- QUAND LE RAPPORT EST CORRECTEMENT GENERE --->	
	<cffunction name="fctRapportGenere" access="private" returntype="Numeric">
		<cfargument name="APP_LOGINID"			type="Numeric"	required="true">
		<cfargument name="IDRACINE"				type="Numeric"	required="true">
		<cfargument name="UUID" 				type="String"	required="true">
		<cfargument name="NOM_DETAILLE_RAPPORT" type="String"	required="true">
		<cfargument name="DATE_DEMANDE" 		type="String"	required="true">
		<cfargument name="PERIMETRE" 			type="String"	required="true">
		<cfargument name="MAIL_DESTINATAIRE" 	type="String"	required="true">
		<cfargument name="STATUS" 				type="String"	required="true">
		<cfargument name="GLOBALIZATION"		type="string"	required="true">
		<cfargument name="LASTNAME"				type="string"	required="true">
		<cfargument name="FIRSTNAME"			type="string"	required="true">
		<cfargument name="CODEAPPLICATION"		type="Numeric"	required="true">
		
		<cflog text="********************************************** PROCESSCONTAINER - function fctRapportGenere">

	<!--- ENVOI D'UN MAIL AU CLIENT POUR L'AVERTIR QUE LE RAPPORT EST DISPONIBLE SUR CONTAINER --->	
		<cfset knowStruct1=structNew()>
		<cfset knowStruct1["NOM_DETAILLE_RAPPORT"]	=#arguments.NOM_DETAILLE_RAPPORT#>
		<cfset knowStruct1["DATE_DEMANDE"]			=#arguments.DATE_DEMANDE#>
		<cfset knowStruct1["PERIMETRE"]				=#arguments.PERIMETRE#>
		<cfset knowStruct1["MAIL_DESTINATAIRE"]		=#arguments.MAIL_DESTINATAIRE#>
		<cfset knowStruct1["APP_LOGINID"]			=#arguments.APP_LOGINID#>
		<cfset knowStruct1["IDRACINE"]				=#arguments.IDRACINE#>
		<cfset knowStruct1["GLOBALIZATION"]			=#arguments.GLOBALIZATION#>
		<cfset knowStruct1["LASTNAME"]				=#arguments.LASTNAME#>
		<cfset knowStruct1["FIRSTNAME"]				=#arguments.FIRSTNAME#>
		<cfset knowStruct1["CODEAPPLICATION"]		=#arguments.CODEAPPLICATION#>								
		<!--- Pour connaitre le droit de notification du client demandeur du rapport --->
		<cfset maj = containerProxy.MAJStatus(UUID, 2)>
		<cfset containerProxy.MAJTaille(UUID)>

		<cftry>
		<!--- REMPLACEMENT DU FICHIER OUTPUT PAR L'UUID --->	
			
			

			<cflog text="********************************************** processContainer - fctRapportGenere : maj du status : #maj#">
			<cfset knowStatus=containerProxy.KNOWMailRight(APP_LOGINID,IDRACINE,"GENERE")>
			<cflog text="********************************************** processContainer - fctRapportGenere : droit de notification : #knowStatus#">
			<cfif knowStatus EQ 1>
				<!--- si le client a le droit de recevoir un mail --->
				<cfset knowStatus=containerProxy.SENDMailFactory("Publication","Client",knowStruct1)>
				<cflog text="********************************************** processContainer - fctRapportGenere : mail envoye ? #knowStatus#">
				<cflog text="********************************************** processContainer - Calcul de la taille du rapport">
				<cfset tailleCalc = 0>
			<cfelse>
				<cfset knowStatus = -1>
			</cfif>
		<cfcatch type="any">
				<cfset str= "#cfcatch.Type# : #cfcatch.Message# - #cfcatch.Detail#">
				<cflog text="********************************************** #str#">
				<cfset knowStatus = -1>
		</cfcatch>
		</cftry>
		<cfreturn knowStatus>
	</cffunction>
<!--- QUAND LE RAPPORT EST EN ERREUR --->	
	<cffunction name="fctRapportErreur" access="private" returntype="Numeric">
		<cfargument name="APP_LOGINID"			type="Numeric"	required="true">
		<cfargument name="IDRACINE"				type="Numeric"	required="true">
		<cfargument name="UUID" 				type="String"	required="true">
		<cfargument name="NOM_DETAILLE_RAPPORT" type="String"	required="true">
		<cfargument name="DATE_DEMANDE" 		type="String"	required="true">
		<cfargument name="PERIMETRE" 			type="String"	required="true">
 		<cfargument name="MAIL_DESTINATAIRE" 	type="String"	required="true">
		<cfargument name="STATUS" 				type="String"	required="true">
		<cfargument name="GLOBALIZATION"		type="String"	required="true">
		<cfargument name="LASTNAME"				type="string"	required="true">
		<cfargument name="FIRSTNAME"			type="string"	required="true">
		<cfargument name="CODEAPPLICATION"		type="Numeric"	required="true">
		
		<cflog text="********************************************** PROCESSCONTAINER - function fctRapportErreur">
	<!---
		<cfset maj = containerProxy.MAJStatus(UUID, 3)>
	--->

	<!--- ENVOI D'UN MAIL AU CLIENT POUR L'AVERTIR QUE LE RAPPORT EST DISPONIBLE SUR CONTAINER --->	
		<cfset knowStruct1=structNew()>
		<cfset knowStruct1["NOM_DETAILLE_RAPPORT"]	=#arguments.NOM_DETAILLE_RAPPORT#>
		<cfset knowStruct1["DATE_DEMANDE"]			=#arguments.DATE_DEMANDE#>
		<cfset knowStruct1["PERIMETRE"]				=#arguments.PERIMETRE#>
<!--- 		<cfset knowStruct1["MAIL_DESTINATAIRE"]		=#arguments.MAIL_DESTINATAIRE#> --->
		<cfset knowStruct1["MAIL_DESTINATAIRE"]		=#arguments.MAIL_DESTINATAIRE#>
		<cfset knowStruct1["APP_LOGINID"]			=#arguments.APP_LOGINID#>
		<cfset knowStruct1["IDRACINE"]				=#arguments.IDRACINE#>
		<cfset knowStruct1["GLOBALIZATION"]			=#arguments.GLOBALIZATION#>
		<cfset knowStruct1["LASTNAME"]				=#arguments.LASTNAME#>
		<cfset knowStruct1["FIRSTNAME"]				=#arguments.FIRSTNAME#>	
		<cfset knowStruct1["CODEAPPLICATION"]		=#arguments.CODEAPPLICATION#>			
		<!--- Pour connaitre le droit de notification du client demandeur du rapport --->
		<cftry>
			<cfset knowStatus=containerProxy.SENDMailFactory("Erreur","Production",knowStruct1)>
					
			<cflog text="********************************************** PROCESSCONTAINER - send Mail Production : #knowstatus#">
				
			<cfset knowStatus=containerProxy.KNOWMailRight(APP_LOGINID,IDRACINE,"Erreur")>
			<cflog text="************************** PROCESSCONTAINER - droit de notification : #knowstatus#">
			<cfif knowStatus EQ 1>
				<!--- si le client a le droit de recevoir un mail --->
				<cfset knowStatus=containerProxy.SENDMailFactory("Erreur","Client",knowStruct1)>
				<cflog text="********************************************** PROCESSCONTAINER - send mail client : #knowstatus#">
			<cfelse>
				<cfset knowStatus = -1>
			</cfif>
		<cfcatch type="any">
			<cfset str= "#cfcatch.Type# : #cfcatch.Message# - #cfcatch.Detail#">
			<cflog text="********************************************** #str#">
			<cfset knowStatus = -1>
		</cfcatch>
		</cftry>
		<cfreturn knowStatus>
	</cffunction>
</cfcomponent>
