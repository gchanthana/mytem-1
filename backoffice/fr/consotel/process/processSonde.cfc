<cfcomponent output="false">
<!--- MISE A JOUR DU STATUS DU RAPPORT 
		http://download.oracle.com/docs/cd/E10415_01/doc/bi.1013/e12188/T421739T524310.htm#jobhistoryinfo
		Status : "Completed", "Error", "Running", "Scheduled", "Suspended" and "Unknown" 
	--->
	<cfset STATUS_CODES=structNew()>
	<cfset STATUS_CODES["S"]="Completed">
	<cfset STATUS_CODES["F"]="Error">
	<cfset STATUS_CODES["W"]="Unknown">
	<cfset STATUS_CODES["P"]="Suspended">
	<!--- ********************************************** --->	
	<cffunction name="run" access="public" returntype="Numeric">
		<cflog text="************************ POST PROCESS - SONDE : OK">
		<cfreturn 1>
	</cffunction>
</cfcomponent>