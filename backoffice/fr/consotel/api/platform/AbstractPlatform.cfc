<cfcomponent displayname="fr.consotel.api.platform.AbstractPlatform" implements="fr.consotel.api.platform.Platform">
	<cfset VARIABLES["level"]="NULL">
	
	<cffunction access="public" name="getInstance" returntype="fr.consotel.api.platform.Platform" hint="Singleton method">
		<cflock scope="Application" type="exclusive" timeout="10">
			<cfif structKeyExists(APPLICATION["API"],"platformInstance")>
				<!--- Case 1 : platformInstance KEY is not created yet --->
				<cfif APPLICATION["API"]["platformInstance"].getLevel() EQ getLevel()>
					<cfreturn APPLICATION["API"]["platformInstance"]>
				</cfif>
			<cfelse>
				<!--- Cases 2 OR 3 :
					- platformInstance KEY exists but is has not correct level value 
					- platformInstance KEY exists and has correct level value
				--->
				<cfset APPLICATION["API"]["platformInstance"]=THIS>
				<cfreturn THIS>
			</cfif>
		</cflock>
	</cffunction>
	
	<cffunction access="public" name="getLevel" returntype="string" hint="Returns platform level">
		<cfreturn VARIABLES["level"]>
	</cffunction>
</cfcomponent>