<cfcomponent displayname="fr.consotel.api.platform.Development" extends="fr.consotel.api.platform.AbstractPlatform">
	<cffunction access="public" name="getInstance" returntype="fr.consotel.api.platform.Platform" hint="Singleton method">
		<cfset VARIABLES["level"]="DEV">
		<!--- Can change bip dns at implementation time (not possible at runtime)
		<cfset VARIABLES["BIP_DNS"]="bip-dev.consotel.fr">
		 --->
		<cfset VARIABLES["BIP_DNS"]="db-5.consotel.fr:7501">
		<cfreturn SUPER.getInstance()>
	</cffunction>
</cfcomponent>