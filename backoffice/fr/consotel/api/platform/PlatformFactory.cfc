<cfcomponent displayname="fr.consotel.api.platform.PlatformFactory" hint="Requires application scope enabled">
	<cffunction access="public" name="getPlatformInstance" returntype="fr.consotel.api.platform.Platform" hint="Returns platform singleton instance">
		<cfset var platformClass="NULL">
		<cftry>
			<cfif structIsEmpty(APPLICATION)>
				<cftrace category="API" type="error" text="Application scope must be enabled" abort="true">
			</cfif>
			<!--- Initialize application scope structure --->
			<cflock scope="Application" type="exclusive" timeout="10">
				<cfif NOT structKeyExists(APPLICATION,"API")>
					<cfset APPLICATION["API"]=structNew()>
				</cfif>
			</cflock>
			<cfset appLevel=getApplicationLevel()>
			<cfswitch expression="#appLevel#">
				<cfcase value="PROD">
					<cfset platformClass="fr.consotel.api.platform.Production">
				</cfcase>
				<cfcase value="INT">
					<cfset platformClass="fr.consotel.api.platform.Integration">
				</cfcase>
				<cfcase value="DEV">
					<cfset platformClass="fr.consotel.api.platform.Development">
				</cfcase>
				<cfdefaultcase>
					<cftrace category="API" type="error" text="Unknown application level : #appLevel#" abort="true">
				</cfdefaultcase>
			</cfswitch>
			<!--- Call Platform Singleton method --->
			<cfreturn createObject("component",platformClass).getInstance()>
			<cfcatch type="any">
				<cflog type="error" text="#CFCATCH.Message# || #CFCATCH.Detail#">
				<cftrace category="API" type="error" text="getPlatformInstance() - Exception" abort="true">
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="private" name="getApplicationLevel" returntype="string" hint="Returns application level (PROD,INT,DEV,etc...)">
		<cfset var appLevel="NULL">
		<cftry>
			<cfset var templateApp=createObject("component","fr.consotel.consoview.Application")>
			<cfif structKeyExists(templateApp,"BI_SERVER")>
				<cfset parseBipUrlArray=listToArray(templateApp["BI_SERVER"],"/",false)>
				<cfset bipServerName=parseBipUrlArray[2]>
				<cfif arrayLen(parseBipUrlArray) EQ 3>
					<cfswitch expression="#bipServerName#">
						<cfcase value="bip-prod.consotel.fr">
							<cfset appLevel="PROD">
						</cfcase>
						<cfcase value="bip-int.consotel.fr">
							<cfset appLevel="INT">
						</cfcase>
						<cfcase value="bip-dev.consotel.fr">
							<cfset appLevel="DEV">
						</cfcase>
						<cfdefaultcase>
							<cftrace category="API" type="error" text="Unknown BIP Server Name : #bipServerName#. Return level value : NULL">
						</cfdefaultcase>
					</cfswitch>
				</cfif>
			<cfelse>
				<cftrace category="API" type="error" text="Unable to find BI_SERVER key. Return level value : NULL">
			</cfif>
			<cfcatch type="any">
				<cflog type="error" text="#CFCATCH.Message# || #CFCATCH.Detail#">
				<cftrace category="API" type="error" text="getApplicationLevel() - Exception">
			</cfcatch>
		</cftry>
		<cfreturn appLevel>
	</cffunction>
</cfcomponent>