<cfsetting enablecfoutputonly="true">
<cfheader statuscode="200">
<cflog text="************* CFMX - BIP-NOTIF-INT - BIP SCHEDULER EVENT HANDLER - #CGI.REMOTE_HOST# *************">
<cftry>
	<cfset CV_USER="consoview">
	<cfset CV_PWD="public">
	<cfset MAIL_SERVER="mail-cv.consotel.fr">
	<cfset HANDLER_FROM_ADDR="#CGI.SERVER_NAME#@saaswedo.com">
	<cfset DEV_SUPPORT_ADDR="monitoring@saaswedo.com">
	<cfset EXCEPTION_SUBJECT_PREFIX="POST-PROCESS BIP">
	<cfparam name="URL.BIP" default="-">
	<cfset erreurProxy = createObject("component","fr.consotel.process.processContainerErreur")>
	
	<cflog text="Serveur BackOffice : #CGI.SERVER_NAME#">
	<cflog text="Serveur de mail (MAIL SERVER) : #MAIL_SERVER#">
	<cflog text="Expediteur (HANDLER_FROM_ADDR) : #HANDLER_FROM_ADDR#">
	<cflog text="Adresse Support de dev (DEV_SUPPORT_ADDR) : #DEV_SUPPORT_ADDR#">
	<cflog text="Prefixe du sujet (EXCEPTION_SUBJECT_PREFIX) : #EXCEPTION_SUBJECT_PREFIX#">
	
	<cfif structKeyExists(URL,"EVENT")>
		<cfset eventArrayList=arrayNew(1)>
		<cfset tmpEventArrayList=listToArray(URL["EVENT"],".")>
		<cfset tmpJobId=tmpEventArrayList[arrayLen(tmpEventArrayList)]>
		<cfloop index="i" from="1" to="#(arrayLen(tmpEventArrayList) - 1)#">
			<cfset eventArrayList[i]=tmpEventArrayList[i]>
		</cfloop>
		<cfset eventArrayList[1]=arrayToList(eventArrayList,".")>
		<cfset eventArrayList[2]=tmpJobId>
		<cfif arrayLen(eventArrayList) EQ 2>
			<cfif eventArrayList[1] EQ CV_USER>
				<cfset biService=createObject("webservice","http://" & LCASE(URL["BIP"]) & "/xmlpserver/services/PublicReportService?WSDL")>
				<cfset jobInfo=biService.getScheduledReportHistoryInfo(eventArrayList[2],eventArrayList[1],CV_PWD,"All",FALSE)>
				<!--- Statut par défaut envoyé au module container --->
				<cfset jobStatus="Error">
				<cfset containerService=createObject("component","fr.consotel.consoview.api.container.ApiContainerV1")>
				<cfif arrayLen(jobInfo) GTE 1>
					<cfset tmpObj=jobInfo[1]>
					<cfset jobStatus=tmpObj.getStatus()>
					<cfset processName=containerService.getCodeProcess(eventArrayList[2],#URL.BIP#)>
					<cfif processName EQ "-1">
						<cflog text="************* POST PROCESS - BIP-NOTIF-INT - PROCESS INCONNU : #processName# pour #eventArrayList[1]# | #eventArrayList[2]# *************">
					<cfelse>
						<cfset processApi=createObject("component","fr.consotel.process.process" & processName)>
						<cfset runStatus=processApi.run(VAL(eventArrayList[2]),jobStatus,#URL.BIP#)>
						<cfif runStatus NEQ 1>
							<cfset erreurProxy.run(tmpJobId,jobStatus,URL.BIP)>
							<cfmail from="#HANDLER_FROM_ADDR#" to="#DEV_SUPPORT_ADDR#" subject="#EXCEPTION_SUBJECT_PREFIX# (EXCEPTION) : Valeur de retour la méthode run du composant PROCESS" 
								type="html" charset="utf-8" wraptext="72"  server="#MAIL_SERVER#">
								<cfoutput>
									<b>Serveur BackOffice :</b> #CGI.SERVER_NAME#<br />
									<b>Motif :</b> La valeur de retour est #runStatus#. La valeur attendue est 1<br />
									<b>Source : http-event-listener.cfm</b><br />
									<b>Dump des paramètres HTTP (GET) provenant de BI Publisher :</b><br />
								</cfoutput>
								<cfdump var="#URL#" expand="true" label="URL"><br />
							</cfmail>
						</cfif>
					</cfif>
				<cfelse>
					<cfset processName=containerService.getCodeProcess(eventArrayList[2],#URL.BIP#)>
					<cfif processName EQ "-1">
						<cflog text="************* POST PROCESS - BIP-NOTIF-INT - PROCESS INCONNU : #processName# pour #eventArrayList[1]# | #eventArrayList[2]# *************">
					<cfelse>
						<cflog text="************* POST PROCESS - BIP-NOTIF-INT - PROCESS ERREUR CONTAINER : Historique du JOB non défini (#processName#) *************">
						<cfset erreurProxy.run(tmpJobId,jobStatus,URL.BIP)>
						<cfmail from="#HANDLER_FROM_ADDR#" to="#DEV_SUPPORT_ADDR#" subject="#EXCEPTION_SUBJECT_PREFIX# : Pas d'historique pour le JOBID #tmpJobId#" 
							type="html" charset="utf-8" wraptext="72"  server="#MAIL_SERVER#">
							<cfoutput>
								<b>Serveur BackOffice :</b> #CGI.SERVER_NAME#<br />
								<b>Motif :</b> La taille du tableau JOBINFO est #arraylen(jobInfo)#. La valeur attendue est au moins 1<br />
								<b>Page de traitement : #CGI.SCRIPT_NAME#</b><br />
								<b>JOBID : #tmpJobId#</b><br />
								<b><i>Le contenu de ce mail a été modifié par rapport au ticket :<br>
								http://nathalie.consotel.fr/issues/5828<br>
								</i></b><br>
								L'historique du JOB est nécessaire pour le traitement de la notification correspondante<br>
								Les causes peuvent etre entre autres :
								- Historique supprimé de l'historique BIP (Exemple : Supprimé par un utilisateur depuis l'historique de l'interface web de BIP)<br>
								- La visibilité de son historique est privée et n'est pas accessible par l'utilisateur utilisé par le traitment de notification (consoview)<br>
								- JOBID inexistant dans l'historique du serveur BIP concerné<br>
								- Serveur BIP non patché<br>
							</cfoutput>
							<cfdump var="#URL#" label="Paramètre HTTP/GET"><br />
							<cfdump var="#jobInfo#" label="Tableau des d'historique du JOB">
						</cfmail>
					</cfif>
				</cfif>
			<cfelse>
				<cflog text="#CGI.SERVER_NAME# : HANDLING REPORT FROM USER #eventArrayList[1]# - NOTHING TO DO FOR THIS USER">
			</cfif>
		<cfelse>
			<cfmail from="#HANDLER_FROM_ADDR#" to="#DEV_SUPPORT_ADDR#" subject="#EXCEPTION_SUBJECT_PREFIX# (EXCEPTION) : La taille du tableau eventArrayList est différente de 2" 
				type="html" charset="utf-8" wraptext="72"  server="#MAIL_SERVER#">
				<cfoutput>
					<b>Serveur BackOffice :</b> #CGI.SERVER_NAME#<br />
					<b>Motif :</b> La valeur de ARRAYLEN(eventArrayList) est #ARRAYLEN(eventArrayList)#. La valeur attendue est 2<br />
					<b>Source : http-event-listener.cfm</b><br />
					<b>Dump des paramètres HTTP (GET) provenant de BI Publisher :</b><br />
					<br><br>
					Le message HTTP émis par BIP est d'un format innatendu :
				</cfoutput>
				<cfdump var="#URL#" expand="true" label="URL"><br />
			</cfmail>
		</cfif>
	</cfif>
	<cfcatch type="any">
		<cflog text="====== (BIP) HTTP EVENT LISTENER v1.3 - EXCEPTION CATCHED - Server : #CGI.SERVER_NAME# - Remote : #CGI.REMOTE_HOST# ======">
		<cfmail from="#HANDLER_FROM_ADDR#" to="#DEV_SUPPORT_ADDR#" subject="#EXCEPTION_SUBJECT_PREFIX# (EXCEPTION) : Exception rencontrée" 
			type="html" charset="utf-8" wraptext="72"  server="#MAIL_SERVER#">
			<cfif isDefined("URL")>
				<cfoutput>
					<b>Dump des paramètres HTTP (GET) provenant de BI Publisher :</b><br />
				</cfoutput>
				<cfdump var="#URL#" expand="true" label="URL"><br />
			</cfif>
			<cfoutput>
				<b>Dump du CFCATCH :</b><br />
			</cfoutput>
			<cfdump var="#CFCATCH#" expand="true">
		</cfmail>
	</cfcatch>
</cftry>
<cflog text="************* END - CFMX - BIP SCHEDULER EVENT HANDLER *************">
