<cfinterface displayName="fr.consotel.api.utils.map.KeyListValidatorStrategy" hint="Interface of structure key list validator strategies">
	<cffunction access="public" name="validateKeys" returntype="boolean" hint="Validates a structure key list. Returns TRUE if valid and FALSE otherwise">
		<cfargument name="mapToValidate" type="struct" hint="Structure which key list is to validate">
		<cfargument name="requiredKeyList" type="any" hint="Required key list (LIST)">
		<cfargument name="optionalKeyList" type="any" hint="Optional key list (LIST)">
	</cffunction>
</cfinterface>