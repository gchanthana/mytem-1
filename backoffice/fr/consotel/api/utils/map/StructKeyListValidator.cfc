<cfcomponent displayname="fr.consotel.api.utils.map.StructKeyListValidator" implements="fr.consotel.api.utils.map.KeyListValidatorStrategy">
	<!--- TODO : Benchmark Performance/Memory with DefaultKeyListValidator --->
	<!--- 
		ALGORITHM : O(2*K*N) N is MAX(listLen(requiredKeyList),listLen(optionalKeyList)) and K keys count in mapToValidate
		1) SLOWER THAN StructKeyListValidator
	 --->
	<cffunction access="public" name="validateKeys" returntype="boolean" hint="Uses TWO temporary map for algorithm">
		<cfargument name="mapToValidate" type="struct">
		<cfargument name="requiredKeyList" type="any">
		<cfargument name="optionalKeyList" type="any">
		<cfset var tmpRequiredKeyMap=structNew()>
		<cfset var tmpOptionalKeyMap=structNew()>
		<cfset var tmpMapToValidate=arguments["mapToValidate"]>
		<!--- Step 1 : Building temporary map for REQUIRED key list --->
		<cfloop index="requiredKey" list="#arguments["requiredKeyList"]#">
			<cfset tmpRequiredKeyMap[requiredKey]=TRUE>
		</cfloop>
		<!--- Step 2 : Building temporary map for OPTIONAL key list --->
		<cfloop index="optionalKey" list="#arguments["optionalKeyList"]#">
			<cfset tmpOptionalKeyMap[optionalKey]=FALSE>
		</cfloop>
		<!--- Step 3 : Loop over mapToValidate key list --->
		<cfloop item="mapKey" collection="#tmpMapToValidate#">
			<!--- NOT FOUND IN REQUIRED KEY LIST --->
			<cfif NOT structKeyExists(tmpRequiredKeyMap,mapKey)>
				<!--- NOT FOUND IN OPTIONAL KEY LIST --->
				<cfif NOT structKeyExists(tmpOptionalKeyMap,mapKey)>
					<cftrace category="API-UTILS" type="error" text="validateStructFromKeyList() - Unknown property key : #mapKey#">
					<cfreturn FALSE>
				</cfif>
			<cfelse>
				<!--- mapKey found in REQUIRED KEY LIST : Update temporary required key list map --->
				<cfset structDelete(tmpRequiredKeyMap,mapKey,true)>
			</cfif>
		</cfloop>
		<!--- TEMPORARY REQUIRED KEY LIST IS NOT EMPTY --->
		<cfif NOT structIsEmpty(tmpRequiredKeyMap)>
			<cftrace category="API-UTILS" type="error"
				text="validateStructFromKeyList() - Missing required properties : #structKeyList(tmpRequiredKeyMap)#">
			<cfreturn FALSE>
		</cfif>
		<cfreturn TRUE>
	</cffunction>
</cfcomponent>