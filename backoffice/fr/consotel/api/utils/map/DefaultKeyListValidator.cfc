<cfcomponent displayname="fr.consotel.api.utils.map.DefaultKeyListValidator" implements="fr.consotel.api.utils.map.KeyListValidatorStrategy">
	<!--- TODO : Benchmark Performance/Memory with StructKeyListValidator --->
	<!--- DESCRIPTION :
		ONLY STRUCTURES WITH WITHOUT FOREIGN/UNKNOWN KEY ARE VALID. THUS ALL KEY MUST BE IN REQUIRED OR OPTIONAL KEY
		ALGORITHM : O(2*K*N) N is MAX(listLen(requiredKeyList),listLen(optionalKeyList)) and K keys count in mapToValidate
		1) FASTER THAN StructKeyListValidator
	 --->
	<cffunction access="public" name="validateKeys" returntype="boolean" hint="Uses TWO list for algorithm">
		<cfargument name="mapToValidate" type="struct">
		<cfargument name="requiredKeyList" type="any">
		<cfargument name="optionalKeyList" type="any">
		<cfset var requiredKeyIndex=0>
		<cfset var tmpMapToValidate=arguments["mapToValidate"]>
		<cfset var tmpRequiredKeyList=arguments["requiredKeyList"]>
		<cfset var tmpOptionalKeyList=arguments["optionalKeyList"]>
		<!--- Step 1 : Loop over map to validate keys --->
		<cfloop item="mapKey" collection="#tmpMapToValidate#">
			<!--- mapKey not found in REQUIRED KEYS --->
			<cfset requiredKeyIndex=listFindNoCase(tmpRequiredKeyList,mapKey)>
			<cfif requiredKeyIndex LTE 0>
				<!--- mapKey not found in OPTIONAL KEYS --->
				<cfif listFindNoCase(tmpOptionalKeyList,mapKey) LTE 0>
					<cftrace category="API-UTILS" type="warning" text="validateStructFromKeyList() - Unknown property : #mapKey#">
					<cfreturn FALSE>
				</cfif>
			<cfelse>
				<!--- Update temporary REQUIRED KEY LIST --->
				<cfset tmpRequiredKeyList=listDeleteAt(tmpRequiredKeyList,requiredKeyIndex)>
			</cfif>
		</cfloop>
		<!--- TEMPORARY REQUIRED KEY LIST IS NOT EMPTY --->
		<cfif listLen(tmpRequiredKeyList) GT 0>
			<cftrace category="API-UTILS" type="error" text="validateStructFromKeyList() - Missing required properties : #tmpRequiredKeyList#">
			<cfreturn FALSE>
		</cfif>
		<cfreturn TRUE>
	</cffunction>
</cfcomponent>