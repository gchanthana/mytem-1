<cfcomponent displayname="fr.consotel.api.utils.map.StructUtils" hint="Strategy context (Can be wrapped in a custom context)">
	<cffunction access="public" name="setKeyListValidatorStrategy" returntype="fr.consotel.api.utils.map.StructUtils"
				 hint="Sets key list validator strategy">
		<cfargument name="keyListValidator" type="fr.consotel.api.utils.map.KeyListValidatorStrategy">
		<cfset VARIABLES["keyListValidator"]=arguments["keyListValidator"]>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="public" name="validateStructFromKeyList" returntype="boolean"
				hint="Validates a structure key list. Returns TRUE if valid and FALSE otherwise">
		<cfargument name="mapToValidate" type="struct">
		<cfargument name="requiredKeyList" type="any">
		<cfargument name="optionalKeyList" type="any">
		<cfreturn VARIABLES["keyListValidator"].validateKeys(arguments["mapToValidate"],
								arguments["requiredKeyList"],arguments["optionalKeyList"])>
	</cffunction>
	
	<cffunction access="public" name="mapContains" returntype="boolean"
				hint="Returns TRUE if at least one element in keyList is present in sourceMap and FALSE otherwise">
		<cfargument name="sourceMap" type="struct">
		<cfargument name="keyList" type="any">
		<cfset var tmpSourceMap=arguments["sourceMap"]>
		<cfloop item="mapKey" collection="#tmpSourceMap#">
			<cfif listFindNoCase(arguments["keyList"],mapKey) GT 0>
				<cfreturn TRUE>
			</cfif>
		</cfloop>
		<cfreturn FALSE>
	</cffunction>
</cfcomponent>