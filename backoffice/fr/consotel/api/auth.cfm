<cflog type="information" text="AUTH JOB EVENT LISTENER - (#CGI.REMOTE_ADDR#:#CGI.REMOTE_PORT#)[#CGI.HTTP_HOST#] >>> #CGI.SERVER_NAME#">
<cfif isDefined("FORM")>
	<cflog type="information" text=">> #structKeyList(FORM)#">
	<cfset keys="JOBNAME,JOBID,REPORT_URL,STATUS,FIELDNAMES">
	<cfset isValidForm=TRUE>
	<cfloop index="key" list="#keys#">
		<cfset isValidForm AND structKeyExists(FORM,key)>
	</cfloop>
	<cfif (LEN(structKeyList(FORM)) EQ LEN(keys)) AND isValidForm>
		<cflog type="information" text="AUTH JOB EVENT LISTENER - JOBINFO :">
		<cflog type="information" text=">> JOBNAME : #FORM["JOBNAME"]#">
		<cflog type="information" text=">> JOBID : #FORM["JOBID"]#">
		<cflog type="information" text=">> REPORT_URL : #FORM["REPORT_URL"]#">
		<cflog type="information" text=">> STATUS : #FORM["STATUS"]#">
	<cfelse>
		<cflog type="error" text="AUTH JOB EVENT LISTENER - INVALID FORM">
	</cfif>
</cfif>
