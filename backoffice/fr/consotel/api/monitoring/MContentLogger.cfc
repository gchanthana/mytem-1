<cfcomponent hint="Logging d evenement dans la table mnt_detail" output="false">
	
	<!---========================= Paramètres du mail ================================--->
	<cfset errorMessage = {}>
	<cfset errorMessage.from = "MLogger <log@saaswedo.com>">
	<cfset errorMessage.to = "monitoring@saaswedo.com">
	<cfset errorMessage.bcc = "monitoring@saaswedo.com">
	<cfset errorMessage.subject = "[EVT-9999] Erreur dans l enregistrement d'un log de monitoring">
	<cfset errorMessage.server = "mail-cv.consotel.fr">
	<cfset errorMessage.port = "25">
	<cfset errorMessage.type = 	type="text/html">
	<!--- =========================================================================== --->
	
	
	<cffunction name="logItWithContent" hint="log un evenement de type Mlog avec les fichiers associés" access="remote" output="false" returntype="void">
		<cfargument name="mLog" type="MLog" hint="l event a logger" required="true" />
		<cfargument name="arrayOfMFile" type="array" hint="Tableau de fichier" required="true" />
		
		<cfset result = 0>
		<cfset t_name = createUuid()>
		
		<cfthread action="run" name="#t_name#" pmlog="#mLog#" pafile = "#arrayOfMFile#" perrormessage="#errorMessage#">
		
			<cftry>
				
				<cfset uuid =  pmlog.getUUID()>
		    	<cfset idMnt_id =  pmlog.getIDMNT_ID()>
		    	<cfset idMnt_cat = pmlog.getIDMNT_CAT()>
 		    	<cfset subjectid = left(trim(pmlog.getSUBJECTID()),190)>
		    	<cfset body = pmlog.getBODY()>	
				
				<cfset bool_racine_null = true>
				<cfset idracine = pmLog.getIDRACINE()>	    	
				<cfif idracine gt 0>
					<cfset bool_racine_null = false>
				</cfif>
				
		    	<cfquery name="qInsert" dataSource="ROCOFFRE">
		       		INSERT into mnt_detail(IDMNT_ID,IDMNT_CAT,BODY,SUBJECTID,UUID,IDRACINE)
		       		values
		       		(<cfqueryparam cfsqltype="cf_sql_integer" value="#idMnt_id#" >,
		       		 <cfqueryparam cfsqltype="cf_sql_integer" value="#idMnt_cat#">,
		       		 <cfqueryparam cfsqltype="cf_sql_clob" value="#body#">,
		       		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#subjectid#">,
		       		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#uuid#">,
					 <cfqueryparam cfsqltype="cf_sql_integer" value="#idracine#" null="#bool_racine_null#" >)		       		
		       	</cfquery>
				
				
				
				<!--- Récupération de l'id d'insertion --->
				
				<cfquery name="qGetID" dataSource="ROCOFFRE">
		       		select ID from mnt_detail 
					where uuid = '#uuid#'
		       	</cfquery>
				
				<cfif 	qGetID.recordCount gt 0>			

					<cfset idMlog = qGetID['ID'][1]>
				
					<cfset aln = ArrayLen(pafile)>
					<!--- s'il y a des fichiers joints --->
							
					<cfif aln gt 0>
						<cfloop from="1" to="#aln#" index="i">
														
							<cfset fsFilename = pafile[i].getPath() & '/' & pafile[i].getFileName()>
							<cfif FileExists(fsFilename)>
							
									<cffile action="READBINARY" file="#fsFilename#" variable="t">
									<cfset fichier = t>
									<cfif Not isBinary(t)>
										<cffile action="READ" file="#fsFilename#" variable="u">
										<cfset fichier = u>
									</cfif>
									<cfquery name="qGetID" dataSource="ROCOFFRE">
										INSERT into mnt_content(idmnt_detail,filename,filecontent)
										VALUES(#idMlog#,'#pafile[i].getFileName()#',<cfqueryparam value="#fichier#" cfsqltype="CF_SQL_BLOB">)
									</cfquery>
							</cfif>
							
						</cfloop>		    		
					</cfif>
				</cfif>		
			<cfcatch type="any">
				
			  	<cfmail attributecollection="#errorMessage#">
				   		  			
					<cfdump var="#cfcatch#">
					 										
				</cfmail>
				
			</cfcatch>
			
			</cftry>
			
		</cfthread>
	</cffunction>	
</cfcomponent>