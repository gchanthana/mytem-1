<cfcomponent displayname="Mlog" output="false">

	<cfproperty name="IDMNT_CAT" hint="categorie du log" type="numeric" />
	<cfproperty name="SUBJECTID" hint="sujet du log" type="string" />
	<cfproperty name="IDMNT_ID" hint="identifiant du log" type="numeric" />
	<cfproperty name="BODY" displayname="Corps du message de log" type="any" />
	<cfproperty name="UUID" displayname="identifiant unique de référence" type="any" />
	<cfproperty name="IDRACINE" displayname="identifiant unique de référence" type="numeric" />
	
	<cfscript>
		this.setSUBJECTID("");
		this.setUUID(createUUid());
		this.setIDRACINE(-1);
	</cfscript>
	
	<cffunction name="getIDRACINE" access="public" output="false" returntype="numeric">
		<cfreturn this.IDRACINE />
	</cffunction>

	<cffunction name="setIDRACINE" access="public" output="false" returntype="void">
		<cfargument name="IDRACINE" type="numeric" required="true" />
		<cfset this.IDRACINE = arguments.IDRACINE />
		<cfreturn />
	</cffunction>
	
	<cffunction name="getIDMNT_CAT" access="public" output="false" returntype="numeric">
		<cfreturn this.IDMNT_CAT />
	</cffunction>

	<cffunction name="setIDMNT_CAT" access="public" output="false" returntype="void">
		<cfargument name="IDMNT_CAT" type="numeric" required="true" />
		<cfset this.IDMNT_CAT = arguments.IDMNT_CAT />
		<cfreturn />
	</cffunction>

	<cffunction name="getIDMNT_ID" access="public" output="false" returntype="numeric">
		<cfreturn this.IDMNT_ID />
	</cffunction>

	<cffunction name="setIDMNT_ID" access="public" output="false" returntype="void">
		<cfargument name="IDMNT_ID" type="numeric" required="true" />
		<cfset this.IDMNT_ID = arguments.IDMNT_ID />
		<cfreturn />
	</cffunction>

	<cffunction name="getBODY" access="public" output="false" returntype="any">
		<cfreturn this.BODY />
	</cffunction>

	<cffunction name="setBODY" access="public" output="false" returntype="void">
		<cfargument name="BODY" type="any" required="true" />
		<cfset this.BODY = arguments.BODY />
		<cfreturn />
	</cffunction>
	
	<cffunction name="getSUBJECTID" access="public" output="false" returntype="string">		
		<cfreturn this.SUBJECTID />
	</cffunction>

	<cffunction name="setSUBJECTID" access="public" output="false" returntype="void">
		<cfargument name="SUBJECTID" type="string" required="true" />
		<cfset this.SUBJECTID = arguments.SUBJECTID />
		<cfreturn />
	</cffunction>
	
	<cffunction name="setUUID" access="public" output="false" returntype="void">
		<cfargument name="UUID" type="string" required="true" />
		<cfset this.UUID = arguments.UUID />
		<cfreturn />
	</cffunction>
	
	<cffunction name="getUUID" access="public" output="false" returntype="string">		
		<cfreturn this.UUID />
	</cffunction>
	
</cfcomponent>