<cfcomponent displayname="CreateUser" implements="fr.consotel.api.mdm.IFunction">

	<cffunction name="execute">
	
		<cfargument name="group"	 	type="String" 	required="true" hint="Nom du groupe pour l'utilisateur">
		<cfargument name="login" 		type="String" 	required="true" hint="Login pour l'utilisateur">
		<cfargument name="password" 	type="String" 	required="true" hint="Mot de passe pour l'utilisateur">
		<cfargument name="role" 		type="String" 	required="true" hint="Rôle pour l'utilisateur (i.e : ADMIN, SUPPORT, USER, GUEST)">
		<cfargument name="idracine" 	type="Numeric" 	required="true" hint="Idracine courant">
			
			<cfset var infos			= structNew()><!--- cette structure est obligatoire pour récupérer les infos sur le serveur MDM ainsi que le code function --->
			<cfset var infos.idracine 	= idracine><!--- il faudra rajouter des informations dans la structure et les gérer dans get_ListeCodeFunction_ServeurMDM() --->
						
			<cfset var infosMDM = createObject("component", "fr.consotel.api.mdm.CodeFunction").get_ListeCodeFunction_ServeurMDM(infos)>
		
			<cfset param = structNew() >
			<cfset StructInsert(param, "group", "#group#", 1)>
			<cfset StructInsert(param, "login", "#login#", 1)>
			<cfset StructInsert(param, "password", "#password#", 1)>
			<cfset StructInsert(param, "role", "#role#", 1)>
			<cfset StructInsert(param, "servermdmpwd", "#infosMDM.servermdmpwd#", 1)>
			<cfset StructInsert(param, "servermdmuser", "#infosMDM.servermdmuser#", 1)>
			<cfset StructInsert(param, "servermdm", "#infosMDM.servermdm#", 1)>

			<cfset createUser = createObject("component","fr.consotel.api.mdm.createuser.CreateUser#infosMDM.codefunction#")>
			<cfset createUser.action(param)>
				
	</cffunction>

</cfcomponent>
