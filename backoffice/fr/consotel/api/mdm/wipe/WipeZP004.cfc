<cfcomponent displayname="WipeZP004" >

	<cffunction name="action">
	
		<cfargument name="param" type="struct" required="true">
		
<!--- 
			<cfset code_action = createObject("component", "fr.consotel.api.mdm.CodeFunction").getListFunctionCode(waitTime, waitTime)>
 --->

		
			<cfset service = createObject("component","fr.consotel.api.mdm.partner.public.Zenprise").getPublicZenprise()>
			
			<cfif param.eraseData EQ true>

				<cfset variable = service.corporateDataWipeDevice(param.serialNumber, param.imei, param.waitTime)>
			
			<cfelse>

				<cfset variable = service.wipeDevice(param.serialNumber, param.imei, param.erasedMemoryCard, param.waitTime)>
			
			</cfif>

	</cffunction>

</cfcomponent>
