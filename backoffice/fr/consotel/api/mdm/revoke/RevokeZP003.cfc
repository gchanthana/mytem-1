<cfcomponent displayname="RevokeZP003">

	<cffunction name="action">
	
		<cfargument name="param" type="struct" required="true">
		
		<cfset var service = createObject("component","fr.consotel.api.mdm.partner.public.Zenprise").getPublicZenprise()>
		<cfset var revokeResult = service.revokeDevice(param.serialNumber, param.imei)>
	
	</cffunction>

</cfcomponent>
