<cfcomponent output="false" implements="fr.consotel.api.mdm.IFunction">

	<cffunction name="execute">
		<cfargument name="serialNumber" 	type="String" 	required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" 			type="String" 	required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="waitTime" 		type="Numeric" 	required="true" hint="Délai d'attente pour de la réponse de l'opération en millisecondes">
			<cfset var myTEM=createObject("component","fr.saaswedo.api.myTEM.mdm.MyTEM").getInstance()>
			<cfset var iDeviceMngt=myTEM.getIDeviceManagement()>
			<cfset locateDevice = iDeviceMngt.locate(serialNumber,imei,waitTime)>
			<cflog type="information" text="MDM Locate status for device [#serialNumber#,#imei#] : #locateDevice#">
			<cfif locateDevice EQ 3>
				<cfthrow type="Custom" message="[MDM] Locate error - Return code : #locateDevice#" errorcode="MDM_LOCATE_ERROR">
			</cfif>
	</cffunction>

</cfcomponent>