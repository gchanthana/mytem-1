<cfcomponent displayname="CreateUserZP001" >

	<cffunction name="action">
	
		<cfargument name="param" type="struct" required="true">			
			<!---
			<cfset var service = createObject("component","fr.consotel.api.mdm.partner.public.Zenprise").getPublicZenprise(#param.servermdmuser#,
																															#param.servermdmpwd#,
																															"https://" & #param.servermdm# & "/zdm/services/Version?WSDL")>	
			--->
			<cfset var service = createObject("component","fr.consotel.api.mdm.partner.public.Zenprise").getPublicZenprise()>
			<cfset service.removeUser(param.login)>
			
			<cfset var variable = service.createUser(param.group, param.login, param.password, param.role)>

	</cffunction>

</cfcomponent>
