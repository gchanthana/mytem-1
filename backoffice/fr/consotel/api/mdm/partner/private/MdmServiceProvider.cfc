<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.MdmServiceProvider"
hint="Fournisseur des services des API partenaires privées MDM. Cette implémentation représente le point d'accès aux API partenaires privées
Les méthodes de cette implémentation ne sont pas soumises à la vérification de l'authentification">
	<cffunction access="public" name="getServiceProvider" returntype="fr.consotel.api.mdm.partner.private.MdmServiceProvider" hint="Retourne une instance de cette classe">
		<cfargument name="authCredentials" type="Struct" required="true" hint="Credentials d'authentification contenant au moins les clés suivantes :
		- DOMAIN : Valeur utilisée par l'API pour déterminer l'implémentation fr.consotel.api.mdm.auth.ClientAuthManager à utiliser pour vérifier l'authentification de l'utilisateur
		<br>Peut contenir les clés supplémentaires suivantes :<br>
		LOGIN (Login), PWD (Mot de passe. Obligatoire si LOGIN est spécifié), WS_ENDPOINT (Endpoint). Le serveur ConsoTel est utilisé par défaut">
		<cfset var userCredentials=ARGUMENTS["authCredentials"]>
		<!--- Instanciation et validation des credentials d'authentification d'accès pour toues Web Services MDM --->
		<cfif NOT structKeyExists(VARIABLES,"SERVICE_REPOSITORIES")>
			<cfset VARIABLES["SERVICE_REPOSITORIES"]={
				ZENPRISE=createObject("component","fr.consotel.api.mdm.partner.private.ws.management.Zenprise").getWSRepository(userCredentials),
				FSECURE=createObject("component","fr.consotel.api.mdm.partner.private.ws.security.FSecure").getWSRepository(userCredentials)
			}>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="public" name="getManagementService" returntype="fr.consotel.api.mdm.partner.private.ws.IManagementService"
	hint="Retourne une instance du service MDM de gestion des équipements mobiles">
		<cfset var zenprise=getServiceRepository("ZENPRISE")>
		<!--- Instanciation ou Récupération du service --->
		<cfif NOT structKeyExists(VARIABLES,"MANAGEMENT_SERVICE")>
			<cfset var managementService=createObject("component","fr.consotel.api.mdm.partner.private.ws.management.ManagementService").getService(zenprise)>
			<cfset VARIABLES["MANAGEMENT_SERVICE"]=managementService>
		</cfif>
		<cfreturn VARIABLES["MANAGEMENT_SERVICE"]>
	</cffunction>
	
	<cffunction access="public" name="getSecurityService" returntype="fr.consotel.api.mdm.partner.private.ws.ISecurityManagement"
	hint="Retourne une instance du service MDM de gestion de la sécurité des équipements mobiles">
		<cfset var fsecure=getServiceRepository("FSECURE")>
		<!--- Instanciation ou Récupération du service --->
		<cfif NOT structKeyExists(VARIABLES,"SECURITY_SERVICE")>
			<cfset var securityService=createObject("component","fr.consotel.api.mdm.partner.private.ws.security.SecurityManagement").getService(fsecure)>
			<cfset VARIABLES["SECURITY_SERVICE"]=securityService>
		</cfif>
		<cfreturn VARIABLES["SECURITY_SERVICE"]>
	</cffunction>
	
	<cffunction access="private" name="getServiceRepositories" returntype="Struct"
	hint="Retourne une structure dans laquelle chaque clé identifie une implémentation fr.consotel.api.mdm.partner.private.ws.WSRepository associée à l'instance correspondante">
		<cfif structKeyExists(VARIABLES,"SERVICE_REPOSITORIES")>
			<cfreturn VARIABLES["SERVICE_REPOSITORIES"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="La liste des entité Service Repository n'est pas définie (Mdm Service Provider)">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="getServiceRepository" returntype="fr.consotel.api.mdm.partner.private.ws.WSRepository"
	hint="Retourne une structure dans laquelle chaque clé identifie un service MDM associé à l'instance correspondante">
		<cfargument name="repositoryName" type="String" required="true" hint="Clé contenue dans la structure retournée par getServiceRepositories()">
		<cfset var repositoryKey=ARGUMENTS["repositoryName"]>
		<cfset var serviceRepositories=getServiceRepositories()>
		<cfif structKeyExists(serviceRepositories,repositoryKey)>
			<cfreturn serviceRepositories[repositoryKey]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'entité Service Repository #repositoryKey# n'est pas définie (Mdm Service Provider)">
		</cfif>
	</cffunction>
</cfcomponent>