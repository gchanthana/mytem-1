<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.auth.DemoIdentityProvider"
extends="fr.consotel.api.mdm.partner.private.auth.ClientIdentityProvider" hint="Exemple d'implémentation d'une stratégie d'authentification">
	<cffunction access="package" name="assertAuthIdentity" returntype="boolean" hint="Retourne TRUE si les condition suivantes sont vérifiées et FALSE sinon :
	- Une session correspondant aux credentials fournis est présente et valide sur le BackOffice de http://cv-dev.consotel.fr/
	- La session correspondante est correctement authentifiée et n'est pas expirée
	- L'utilisateur identifié a accès à au moins un module par rapport à la requête retournée par fr.consotel.consoview.M00.UserAccessService.getModuleUserAccess()
	- L'URL accédée est /fr/consotel/demo/mdm/index.cfm">
		<cfargument name="authIdentity" type="Struct" required="true" hint="Valeur retournée par la méthode getAuthIdentity() de cette implémentation">
		<cfset var userIdentity=ARGUMENTS["authIdentity"]>
		<cfset var urlAssertion=(CGI.SCRIPT_NAME EQ "/fr/consotel/demo/mdm/index.cfm")>
		<cfset var authAssertion=FALSE>
		<!--- Vérification de la validité des credentials d'authentification avec l'API ConsoView --->
		<cfset var AUTH_RESULT="">
		<cfset var cvAuthRequest=getCvAuthRequest()>
		<cfhttp attributecollection="#cvAuthRequest#" result="AUTH_RESULT">
			<cfhttpparam type="cookie" name="JSESSIONID" value="#userIdentity.JSESSIONID#">
			<cfhttpparam type="cookie" name="CFID" value="#userIdentity.CFID#">
			<cfhttpparam type="cookie" name="CFTOKEN" value="#userIdentity.CFTOKEN#">
		</cfhttp>
		<cfset var AUTH_HEADER=AUTH_RESULT["ResponseHeader"]>
		<!--- Authentification non validée (e.g : Session expirée, Session invalide, etc...) : Le code du statut HTTP est différent de 200 --->
		<cfif structKeyExists(AUTH_HEADER,"server-error") AND AUTH_HEADER["server-error"]>
			<cfset var invalidSessionTag="is undefined in SESSION">
			<cfset var invalidSessionTagItems=reMatchNoCase("[.]*" & invalidSessionTag & "[.]*",AUTH_HEADER["Explanation"])>
			<!--- TRUE si les credentials identifient une session authentifiée et non expirée. FALSE sinon --->
			<cfset authAssert=(arrayLen(invalidSessionTagItems) EQ 0)>
		<!--- Authentification validée (i.e : Session valide) : Le code du statut HTTP vaut 200 --->
		<cfelse>
			<cfset var httpStatusCode=VAL(AUTH_RESULT["StatusCode"])>
			<cfif httpStatusCode EQ 200>
				<!--- Réponse de l'API ConsoView : Liste des modules accessibles pour l'utilisateur identifié par les credentials fournis (QUERY) --->
				<cfset var AUTH_RESPONSE_CONTENT=AUTH_RESULT["FileContent"]>
				<cfif isWDDX(AUTH_RESPONSE_CONTENT)>
					<cfset var AUTH_CONTENT="">
					<cfwddx action="wddx2cfml" input="#AUTH_RESPONSE_CONTENT#" output="AUTH_CONTENT">
					<cfreturn userHasAccess(AUTH_CONTENT)>
				<cfelse>
					<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le contenu retourné par l'API ConsoView n'est pas au format WDDX">
				</cfif>
				<!--- MAJ de la valeur du statut d'authentification ConsoView --->
				<cfset authAssertion=TRUE>
			<cfelse>
				<cfset var cvErrorMsg=AUTH_HEADER["Explanation"]>
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'API ConsoView a retourné un code d'erreur HTTP : #httpStatusCode#" detail="#cvErrorMsg#">
			</cfif>
		</cfif>
		<cfreturn (urlAssertion AND authAssertion)>
	</cffunction>

	<cffunction access="package" name="getAuthIdentity" returntype="Struct" hint="La structure retournée est authCredentials">
		<cfargument name="authCredentials" type="Struct" required="true" hint="Structure fournie à l'implémentation MdmServiceProvider comme credentials d'authentification
		Les clés suivantes doivent au moins être présentes dans cette structure et chaque valeur provient du COOKIE provenant de http://cv-dev.consotel.fr/ :
		- JSESSIONID : Valeur de la clé correspondante dans le COOKIE
		- CFID : Valeur de la clé correspondante dans le COOKIE
		- CFTOKEN : Valeur de la clé correspondante dans le COOKIE">
		<cfset var userCredentials=ARGUMENTS["authCredentials"]>
		<cfif structKeyExists(userCredentials,"JSESSIONID") AND structKeyExists(userCredentials,"CFID") AND structKeyExists(userCredentials,"CFTOKEN")>
			<cfreturn userCredentials>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le contenu des credentials fournis est invalide">
		</cfif>
	</cffunction>

	<cffunction access="private" name="userHasAccess" returntype="boolean" hint="Retourne TRUE le module utilisant cette API est accessible par l'utilisateur et FALSE sinon">
		<cfargument name="moduleList" type="Query" required="true" hint="Liste des modules accessibles par l'utilisateur identifié dans assertAuthIdentity()">
		<cfset var moduleQuery=ARGUMENTS["moduleList"]>
		<cfreturn moduleQuery.recordcount GT 0>
	</cffunction>

	<cffunction access="private" name="getCvAuthRequest" returntype="Struct" hint="Retourne une structure représentant l'opération effectuée pour vérifier l'authentification">
		<cfreturn {
			url=getCvEndpoint() & "?method=" & getCvOperationName()
		}>
	</cffunction>

	<cffunction access="private" name="getCvOperationName" returntype="String" hint="Retourne le nom de l'opération ConsoView utilisée pour vérifier l'authentification">
		<cfreturn "getModuleUserAccess">
	</cffunction>
	
	<cffunction access="private" name="getCvEndpoint" returntype="String" hint="Retourne le Endpoint utilisé pour accéder à l'API ConsoView">
		<cfreturn "http://" & getCvHost() & "/fr/consotel/consoview/M00/UserAccessService.cfc">
	</cffunction>
	
	<cffunction access="private" name="getCvHost" returntype="String" hint="Retourne le HostName du Endpoint utilisé pour accéder à l'API ConsoView">
		<cfreturn "sun-dev.consotel.fr">
	</cffunction>
</cfcomponent>