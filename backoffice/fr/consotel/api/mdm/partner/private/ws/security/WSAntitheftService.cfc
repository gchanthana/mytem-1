<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.security.WSAntitheftService"
implements="fr.consotel.api.mdm.partner.private.ws.IWSAntitheftService" hint="Implémentation fournissant les méthodes : Anti-Theft Services
Dans la version actuelle de cette classe : Les méthodes retournent des implémentations CFMX correspondant aux types utilisés par le service FSecure
Les méthodes de cette implémentation sont soumises à la vérification de l'authentification">
	<!--- fr.consotel.api.mdm.partner.private.ws.security.WSAntitheftService --->
	<cffunction access="package" name="getWSService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSAntitheftService" hint="Retourne une instance de cette classe">
		<cfargument name="serviceRepository" type="fr.consotel.api.mdm.partner.private.ws.security.FSecure" required="true" hint="Implémentation d'accès aux Web Services">
		<cfif NOT structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfset VARIABLES["WS_REPOSITORY"]=ARGUMENTS["serviceRepository"]>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="getWSRepository" returntype="fr.consotel.api.mdm.partner.private.ws.security.FSecure" hint="Implémentation d'accès aux Web Services">
		<cfif structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfreturn VARIABLES["WS_REPOSITORY"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'entité MDM Service Repository n'est pas définie">
		</cfif>
	</cffunction>
	
	<!--- Anti-Theft Services --->
	<cffunction access="public" name="getLockSMS" returntype="String" hint="Retourne un SMS qui peut être envoyé par SMSC pour vérrouiller un équipement">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<!--- Validation des credentials d'authentification --->
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<!--- Implémentation du service FSecure correspondant --->
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.getLockSMS(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="public" name="getWipeSMS" returntype="String" hint="Retourne un SMS qui peut être envoyé par SMSC pour effectuer l'action WIPE sur un équipement">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.getWipeSMS(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="public" name="getResetAntitheftSettingsSMS" returntype="String" hint="Retourne un SMS qui peut être envoyé par SMSC pour réinitialiser la configuration Antitheft d'un équipement">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.getResetAntitheftSettingsSMS(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="public" name="lockDevice" returntype="String" hint="Vérrouille un équipement et retourne un identifiant de la commande effectuée">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.lockDevice(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="public" name="wipeDevice" returntype="String" hint="Effectue l'action WIPE sur un équipement et retourne un identifiant de la commande effectuée">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.wipeDevice(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="public" name="resetAntitheftSettings" returntype="String" hint="Réinitialise la configuration Antitheft d'un équipement et retourne un identifiant de la commande effectuée">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.resetAntitheftSettings(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="public" name="locateDevice" returntype="String" hint="Envoi un SMS de requête de localisation à un équipement et retourne un identifiant de la commande effectuée">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.locateDevice(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="public" name="getAntitheftCommandLog" returntype="Any" hint="Retourne les logs des commandes Anti-Theft pour une Single License">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn serviceStub.getAntitheftCommandLog(ARGUMENTS["singleLicenseKey"])>
	</cffunction>
	
	<cffunction access="public" name="getLocationResponseLog" returntype="Any" hint="Retourne les logs des commandes Anti-Theft de localisation pour une Single License">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn serviceStub.getLocationResponseLog(ARGUMENTS["singleLicenseKey"])>
	</cffunction>
	
	<cffunction access="public" name="getLocateSMS" returntype="String" hint="Retourne un SMS qui peut être envoyé par SMSC pour retourner la réponse de localisation du client AntiTheft">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.getLocateSMS(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="public" name="activateAlarm" returntype="String" hint="Envoi un SMS d'activation d'alarme à un équipement et retourne un identifiant de la commande effectuée">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.activateAlarm(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="public" name="getAlarmSMS" returntype="String" hint="Retourne un SMS qui peut être envoyé par SMSC pour activer l'alarme du client">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.getAlarmSMS(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="public" name="getATMessageResponseLog" returntype="Any" hint="Retourne les logs pour une commande spécifique">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfargument name="commandId" type="String" required="true" hint="Identifiant de la commande Anti-Theft">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn serviceStub.getATMessageResponseLog(ARGUMENTS["singleLicenseKey"],ARGUMENTS["commandId"])>
	</cffunction>
</cfcomponent>