<cfinterface author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.IWSMultiLicenseService" hint="Interface décrivant les méthodes sur service : Multi License Services">
	<cffunction access="public" name="deleteMultiLicense" returntype="String" hint="Supprime une licence multiple. Retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
	</cffunction>
	
	<cffunction access="public" name="searchMultiLicensesInCorporation" returntype="Any" hint="Recherche les licences multiples d'une corporation et retourne 40 résultats par page de résultat">
		<cfargument name="corporationId" type="String" required="true" hint="Identifiant de la corporation">
		<cfargument name="pageNumber" type="String" required="true" hint="Numéro de la page de résultat à retourner">
	</cffunction>

	<cffunction access="public" name="createMultiLicense" returntype="String" hint="Crée une licence multiple dans la corporation par défaut et retourne la clé correspondante">
		<cfargument name="expirationDate" type="String" required="true" hint="Date d'expiration de la licence au format YYYY-MM-DD. Format optionnel du time zone : EET (GMT+2, GMT+3)">
		<cfargument name="maxNumberOfLicenses" type="String" required="true" hint="Nombre max de single licence qui peuvent être activée dans la licence multiple à créer">
	</cffunction>
	
	<cffunction access="public" name="createMultiLicenseInCorporation" returntype="String" hint="Crée une licence multiple dans une corporation et retourne la clé correspondante">
		<cfargument name="corporationId" type="String" required="true" hint="Identifiant de la corporation">
		<cfargument name="expirationDate" type="String" required="true" hint="Date d'expiration de la licence au format YYYY-MM-DD. Format optionnel du time zone : EET (GMT+2, GMT+3)">
		<cfargument name="maxNumberOfLicenses" type="String" required="true" hint="Nombre max de single licence qui peuvent être activées dans la licence multiple">
	</cffunction>

	<cffunction access="public" name="updateMultiLicenseExpirationDate" returntype="String"
	hint="Met à jour la date d'expiration d'une licence multiple et retourne TRUE si en cas de succès ou FALSE sinon">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfargument name="expirationDate" type="String" required="true" hint="Date d'expiration de la licence au format YYYY-MM-DD. Format optionnel du time zone : EET (GMT+2, GMT+3)">
	</cffunction>

	<cffunction access="public" name="updateMultiLicenseMaximumNumberOfLicenses" returntype="String"
	hint="Met à jour le nombre max de licence d'une licence multiple et retourne TRUE si en cas de succès ou FALSE sinon">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfargument name="maxNumberOfLicenses" type="String" required="true" hint="Nombre max de single licence qui peuvent être activées dans la licence multiple">
	</cffunction>
	
	<cffunction access="public" name="updateMultiLicenseStatus" returntype="String" hint="Met à jour le statut d'une licence multiple et retourne TRUE si en cas de succès ou FALSE sinon">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfargument name="multLicenseStatus" type="String" required="true" hint="Statut de la licence. Valeurs possibles : Activate, Inactivate">
	</cffunction>

	<cffunction access="public" name="getMultiLicenseExpirationDate" returntype="String"
	hint="Retourne la date d'expiration d'un licence multiple au format YYYY-MM-DD. Format optionnel du time zone : EET (GMT+2, GMT+3)">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
	</cffunction>

	<cffunction access="public" name="getMultiLicenseStatus" returntype="String" hint="Retourne le statut d'une licence multiple">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
	</cffunction>
	
	<cffunction access="public" name="updateMultiLicensePhoneNumberRegistration" returntype="String" 
	hint="Met à jour le mode d'enregistrement d'une licence multiple. Retourne TRUE si en cas de succès ou FALSE sinon">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfargument name="phoneNumberRegistration" type="String" required="true" hint="Mode d'enregistrement<br>
		Valeurs possibles : 0 (Enregistrement non envoyé), 1 (Demande confirmation utilisateur avant l'envoi), 2 (Envoi l'enregistrement sans confirmation utilisateur)">
	</cffunction>
	
	<cffunction access="public" name="getMultiLicenseProperties" returntype="Any" hint="Retourne les informations contractuelles d'une licence multiple">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
	</cffunction>

	<cffunction access="public" name="getMultiLicenseCount" returntype="String" hint="Retourne le nombre de licence multiples dans la corporation par défaut">
	</cffunction>
	
	<cffunction access="public" name="getMultiLicenseCountInCorporation" returntype="String" hint="Retourne le nombre de licence multiples d'une corporation">
		<cfargument name="corporationId" type="String" required="true" hint="Identifiant de la corporation">
	</cffunction>
	
	<cffunction access="public" name="searchMultiLicenses" returntype="Any" hint="Retourne le résultat d'une recherche de licence multiple dans la corporation par défaut">
		<cfargument name="pageNumber" type="String" required="true" hint="Numéro de la page de résultat à retourner">
	</cffunction>
	
	<cffunction access="public" name="getMultiLicenseReport" returntype="Any" hint="Retourne les statistiques d'une licence multiple">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
	</cffunction>
	
	<cffunction access="public" name="setDefaultClientActivationState" returntype="String"
	hint="Définit l'état d'activation par défaut du client pour une licence multiple. Retourne TRUE si en cas de succès ou FALSE sinon">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfargument name="state" type="String" required="true" hint="Etat d'activation par défaut. Valeur possibles : 1 (VALID), 2 (TRIAL), 3 (SUBSCRIBED)">
	</cffunction>
	
	<cffunction access="public" name="getDefaultMultiLicense" returntype="Any" hint="Retourne les infos de la licence multiple par défaut du partenaire">
	</cffunction>
</cfinterface>