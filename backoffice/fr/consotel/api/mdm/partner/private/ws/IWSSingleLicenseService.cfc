<cfinterface author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.IWSSingleLicenseService" hint="Interface décrivant les méthodes sur service : Single License Services">
	<cffunction access="public" name="searchSingleLicenses" returntype="Any" hint="Retourne le résultat d'une recherche de single licence">
		<cfargument name="singleLicenseKeySearch" type="String" required="true" hint="Chaine à recherche dans la valeur de la clé">
		<cfargument name="phoneSearch" type="String" required="true" hint="Chaine à recherche dans le numéro de téléphone">
		<cfargument name="firstNameSearch" type="String" required="true" hint="Chaine à recherche dans le prénom">
		<cfargument name="lastNameSearch" type="String" required="true" hint="Chaine à recherche dans le nom">
		<cfargument name="imeiSearch" type="String" required="true" hint="Chaine à recherche dans IMEI">
		<cfargument name="stateSearch" type="String" required="true" hint="Etat à recherche. Valeurs possibles : 0 (Not activated), 1 (Up to date), 3 (Very old), 4 (Out dated). La valeur 2 ne semble pas définie">
		<cfargument name="pageNumber" type="String" required="true" hint="Numéro de la page de résultat à retourner">
	</cffunction>

	<cffunction access="public" name="searchSingleLicenseByIMSI" returntype="String" hint="Recherche une single licence à partir du code IMSI et retourne la clé correspondante">
		<cfargument name="IMSI" type="String" required="true" hint="Code IMSI">
	</cffunction>

	<cffunction access="public" name="deleteSingleLicense" returntype="String" hint="Supprime une single licence. Retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Clé de la licence">
	</cffunction>
	
	<cffunction access="public" name="getSingleLicenseCount" returntype="String" hint="Retourne le nombre de single licence d'une licence multiple">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
	</cffunction>
	
	<cffunction access="public" name="getSingleLicenseExpirationDate" returntype="String"
	hint="Retourne la date d'expiration d'une single licence au format YYYY-MM-DD. Format optionnel du time zone : EET (GMT+2, GMT+3)">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Clé de la licence">
	</cffunction>
	
	<cffunction access="public" name="getDefaultSingleLicenseCount" returntype="String" hint="Retourne le nombre de single licence dans la licence multiple par défaut">
	</cffunction>
	
	<cffunction access="public" name="createSingleLicense" returntype="String" hint="Crée une single licence dans le contrat par défaut du partenaire et retourne la clé correspondante">
		<cfargument name="expirationDate" type="String" required="true" hint="Date d'expiration de la licence au format YYYY-MM-DD">
		<cfargument name="firstName" type="String" required="true" hint="Prénom du souscripteur (120 caractères max)">
		<cfargument name="lastName" type="String" required="true" hint="Nom du souscripteur (120 caractères max)">
		<cfargument name="email" type="String" required="true" hint="Email du souscripteur (120 caractères max)">
		<cfargument name="phone" type="String" required="true" hint="Numéro de téléphone (e.g : +000000000000). 9 à 20 caractères">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (22 caractères max)">
		<cfargument name="imsi" type="String" required="true" hint="Code IMSI de la carte SIM (15 caractères max)">
	</cffunction>
	
	<cffunction access="public" name="createSingleLicenseInMultiLicense" returntype="String" hint="Crée une single licence dans une licence multiple et retourne la clé correspondante">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfargument name="firstName" type="String" required="true" hint="Prénom du souscripteur (120 caractères max)">
		<cfargument name="lastName" type="String" required="true" hint="Nom du souscripteur (120 caractères max)">
		<cfargument name="email" type="String" required="true" hint="Email du souscripteur (120 caractères max)">
		<cfargument name="phone" type="String" required="true" hint="Numéro de téléphone (e.g : +000000000000). 9 à 20 caractères">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (22 caractères max)">
		<cfargument name="imsi" type="String" required="true" hint="Code IMSI de la carte SIM (15 caractères max)">
	</cffunction>
	
	<cffunction access="public" name="resetSingleLicenseImeiChanges" returntype="String"
	hint="Réinitialise le nombre de changement IMEI si le seuil max a été atteint. Retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Clé de la single licence">
	</cffunction>
	
	<cffunction access="public" name="updateSingleLicenseUserProperties" returntype="String" hint="Met à jour une single licence. Retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Clé de la single licence">
		<cfargument name="firstName" type="String" required="true" hint="Prénom du souscripteur (120 caractères max)">
		<cfargument name="lastName" type="String" required="true" hint="Nom du souscripteur (120 caractères max)">
		<cfargument name="email" type="String" required="true" hint="Email du souscripteur (120 caractères max)">
		<cfargument name="phone" type="String" required="true" hint="Numéro de téléphone (e.g : +000000000000). 9 à 20 caractères">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (22 caractères max)">
		<cfargument name="imsi" type="String" required="true" hint="Code IMSI de la carte SIM (15 caractères max)">
	</cffunction>
	
	<cffunction access="public" name="getSingleLicenseProperties" returntype="Any" hint="Retourne les propriétés d'une single licence">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Clé de la single licence">
	</cffunction>	
	
	<cffunction access="public" name="getSingleLicenseUserProperties" returntype="Any" hint="Retourne les infos d'une single licence">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Clé de la single licence">
	</cffunction>
	
	<cffunction access="public" name="updateSingleLicenseExpirationDate" returntype="String" hint="Met à jour la date d'expiration d'une single licence. Retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Clé de la single licence">
		<cfargument name="expirationDate" type="String" required="true" hint="Date d'expiration de la licence au format YYYY-MM-DD">
	</cffunction>
	
	<cffunction access="public" name="searchSingleLicenseByMSISDN" returntype="String" hint="Recherche la single licence correspondant au MSISDN fourni">
		<cfargument name="MSISDN" type="String" required="true" hint="MSISDN (e.g : +000000000000)">
	</cffunction>
</cfinterface>