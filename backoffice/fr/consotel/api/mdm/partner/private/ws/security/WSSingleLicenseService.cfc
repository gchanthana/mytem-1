<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.security.WSSingleLicenseService"
implements="fr.consotel.api.mdm.partner.private.ws.IWSSingleLicenseService" hint="Implémentation fournissant les méthodes : Other Services
Dans la version actuelle de cette classe : Les méthodes retournent des implémentations CFMX correspondant aux types utilisés par le service FSecure
Les méthodes de cette implémentation sont soumises à la vérification de l'authentification">
	<!--- fr.consotel.api.mdm.partner.private.ws.security.WSSingleLicenseService --->
	<cffunction access="package" name="getWSService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSSingleLicenseService" hint="Retourne une instance de cette classe">
		<cfargument name="serviceRepository" type="fr.consotel.api.mdm.partner.private.ws.security.FSecure" required="true" hint="Implémentation d'accès aux Web Services">
		<cfif NOT structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfset VARIABLES["WS_REPOSITORY"]=ARGUMENTS["serviceRepository"]>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="getWSRepository" returntype="fr.consotel.api.mdm.partner.private.ws.security.FSecure" hint="Implémentation d'accès aux Web Services">
		<cfif structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfreturn VARIABLES["WS_REPOSITORY"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'entité MDM Service Repository n'est pas définie">
		</cfif>
	</cffunction>
	
	<!--- Single Licence Services --->
	<cffunction access="public" name="searchSingleLicenses" returntype="Any" hint="Retourne le résultat d'une recherche de single licence">
		<cfargument name="singleLicenseKeySearch" type="String" required="true" hint="Chaine à recherche dans la valeur de la clé">
		<cfargument name="phoneSearch" type="String" required="true" hint="Chaine à recherche dans le numéro de téléphone">
		<cfargument name="firstNameSearch" type="String" required="true" hint="Chaine à recherche dans le prénom">
		<cfargument name="lastNameSearch" type="String" required="true" hint="Chaine à recherche dans le nom">
		<cfargument name="imeiSearch" type="String" required="true" hint="Chaine à recherche dans IMEI">
		<cfargument name="stateSearch" type="String" required="true" hint="Etat à recherche. Valeurs possibles : 0 (Not activated), 1 (Up to date), 3 (Very old), 4 (Out dated). La valeur 2 ne semble pas définie">
		<cfargument name="pageNumber" type="String" required="true" hint="Numéro de la page de résultat à retourner">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn serviceStub.searchSingleLicenses(
			ARGUMENTS["singleLicenseKeySearch"],ARGUMENTS["phoneSearch"],ARGUMENTS["firstNameSearch"],ARGUMENTS["lastNameSearch"],
			ARGUMENTS["imeiSearch"],ARGUMENTS["stateSearch"],ARGUMENTS["pageNumber"]
		)>
	</cffunction>

	<cffunction access="public" name="searchSingleLicenseByIMSI" returntype="String" hint="Recherche une single licence à partir du code IMSI et retourne la clé correspondante">
		<cfargument name="IMSI" type="String" required="true" hint="Code IMSI">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.searchSingleLicenseByIMSI(ARGUMENTS["IMSI"]))>
	</cffunction>

	<cffunction access="public" name="deleteSingleLicense" returntype="String" hint="Supprime une single licence. Retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Clé de la licence">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn yesNoFormat(toString(serviceStub.deleteSingleLicense(ARGUMENTS["singleLicenseKey"])))>
	</cffunction>
	
	<cffunction access="public" name="getSingleLicenseCount" returntype="String" hint="Retourne le nombre de single licence d'une licence multiple">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.getSingleLicenseCount(ARGUMENTS["multiLicenseKey"]))>
	</cffunction>
	
	<cffunction access="public" name="getSingleLicenseExpirationDate" returntype="String"
	hint="Retourne la date d'expiration d'une single licence au format YYYY-MM-DD. Format optionnel du time zone : EET (GMT+2, GMT+3)">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Clé de la licence">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.getSingleLicenseExpirationDate(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="public" name="getDefaultSingleLicenseCount" returntype="String" hint="Retourne le nombre de single licence dans la licence multiple par défaut">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.getDefaultSingleLicenseCount())>
	</cffunction>
	
	<cffunction access="public" name="createSingleLicense" returntype="String" hint="Crée une single licence dans le contrat par défaut du partenaire et retourne la clé correspondante">
		<cfargument name="expirationDate" type="String" required="true" hint="Date d'expiration de la licence au format YYYY-MM-DD">
		<cfargument name="firstName" type="String" required="true" hint="Prénom du souscripteur (120 caractères max)">
		<cfargument name="lastName" type="String" required="true" hint="Nom du souscripteur (120 caractères max)">
		<cfargument name="email" type="String" required="true" hint="Email du souscripteur (120 caractères max)">
		<cfargument name="phone" type="String" required="true" hint="Numéro de téléphone (e.g : +000000000000). 9 à 20 caractères">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (22 caractères max)">
		<cfargument name="imsi" type="String" required="true" hint="Code IMSI de la carte SIM (15 caractères max)">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.createSingleLicense(
			ARGUMENTS["expirationDate"],ARGUMENTS["firstName"],ARGUMENTS["lastName"],ARGUMENTS["email"],
			ARGUMENTS["phone"],ARGUMENTS["imei"],ARGUMENTS["imsi"]
		))>
	</cffunction>
	
	<cffunction access="public" name="createSingleLicenseInMultiLicense" returntype="String" hint="Crée une single licence dans une licence multiple et retourne la clé correspondante">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfargument name="firstName" type="String" required="true" hint="Prénom du souscripteur (120 caractères max)">
		<cfargument name="lastName" type="String" required="true" hint="Nom du souscripteur (120 caractères max)">
		<cfargument name="email" type="String" required="true" hint="Email du souscripteur (120 caractères max)">
		<cfargument name="phone" type="String" required="true" hint="Numéro de téléphone (e.g : +000000000000). 9 à 20 caractères">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (22 caractères max)">
		<cfargument name="imsi" type="String" required="true" hint="Code IMSI de la carte SIM (15 caractères max)">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.createSingleLicenseInMultiLicense(
			ARGUMENTS["multiLicenseKey"],ARGUMENTS["firstName"],ARGUMENTS["lastName"],ARGUMENTS["email"],
			ARGUMENTS["phone"],ARGUMENTS["imei"],ARGUMENTS["imsi"]
		))>
	</cffunction>
	
	<cffunction access="public" name="resetSingleLicenseImeiChanges" returntype="String"
	hint="Réinitialise le nombre de changement IMEI si le seuil max a été atteint. Retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Clé de la single licence">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn yesNoFormat(toString(serviceStub.resetSingleLicenseImeiChanges(ARGUMENTS["singleLicenseKey"])))>
	</cffunction>
	
	<cffunction access="public" name="updateSingleLicenseUserProperties" returntype="String" hint="Met à jour une single licence. Retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Clé de la single licence">
		<cfargument name="firstName" type="String" required="true" hint="Prénom du souscripteur (120 caractères max)">
		<cfargument name="lastName" type="String" required="true" hint="Nom du souscripteur (120 caractères max)">
		<cfargument name="email" type="String" required="true" hint="Email du souscripteur (120 caractères max)">
		<cfargument name="phone" type="String" required="true" hint="Numéro de téléphone (e.g : +000000000000). 9 à 20 caractères">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (22 caractères max)">
		<cfargument name="imsi" type="String" required="true" hint="Code IMSI de la carte SIM (15 caractères max)">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn yesNoFormat(toString(serviceStub.updateSingleLicenseUserProperties(
			ARGUMENTS["singleLicenseKey"],ARGUMENTS["firstName"],ARGUMENTS["lastName"],ARGUMENTS["email"],
			ARGUMENTS["phone"],ARGUMENTS["imei"],ARGUMENTS["imsi"]
		)))>
	</cffunction>
	
	<cffunction access="public" name="getSingleLicenseProperties" returntype="Any" hint="Retourne les propriétés d'une single licence">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Clé de la single licence">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn serviceStub.getSingleLicenseProperties(ARGUMENTS["singleLicenseKey"])>
	</cffunction>	
	
	<cffunction access="public" name="getSingleLicenseUserProperties" returntype="Any" hint="Retourne les infos d'une single licence">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Clé de la single licence">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn serviceStub.getSingleLicenseUserProperties(ARGUMENTS["singleLicenseKey"])>
	</cffunction>
	
	<cffunction access="public" name="updateSingleLicenseExpirationDate" returntype="String" hint="Met à jour la date d'expiration d'une single licence. Retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Clé de la single licence">
		<cfargument name="expirationDate" type="String" required="true" hint="Date d'expiration de la licence au format YYYY-MM-DD">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn yesNoFormat(toString(serviceStub.updateSingleLicenseExpirationDate(ARGUMENTS["singleLicenseKey"],ARGUMENTS["expirationDate"])))>
	</cffunction>
	
	<cffunction access="public" name="searchSingleLicenseByMSISDN" returntype="String" hint="Recherche la single licence correspondant au MSISDN fourni">
		<cfargument name="MSISDN" type="String" required="true" hint="MSISDN (e.g : +000000000000)">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.searchSingleLicenseByMSISDN(ARGUMENTS["MSISDN"]))>
	</cffunction>
</cfcomponent>