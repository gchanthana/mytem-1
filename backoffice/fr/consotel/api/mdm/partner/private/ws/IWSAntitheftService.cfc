<cfinterface author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.IWSAntitheftService" hint="Interface décrivant les méthodes sur service : Antitheft Services">
	<cffunction access="public" name="getLockSMS" returntype="String" hint="Retourne un SMS qui peut être envoyé par SMSC pour vérrouiller un équipement">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
	</cffunction>
	
	<cffunction access="public" name="getWipeSMS" returntype="String" hint="Retourne un SMS qui peut être envoyé par SMSC pour effectuer l'action WIPE sur un équipement">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
	</cffunction>
	
	<cffunction access="public" name="getResetAntitheftSettingsSMS" returntype="String" hint="Retourne un SMS qui peut être envoyé par SMSC pour réinitialiser la configuration Antitheft d'un équipement">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
	</cffunction>
	
	<cffunction access="public" name="lockDevice" returntype="String" hint="Vérrouille un équipement et retourne un identifiant de la commande effectuée">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
	</cffunction>
	
	<cffunction access="public" name="wipeDevice" returntype="String" hint="Effectue l'action WIPE sur un équipement et retourne un identifiant de la commande effectuée">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
	</cffunction>
	
	<cffunction access="public" name="resetAntitheftSettings" returntype="String" hint="Réinitialise la configuration Antitheft d'un équipement et retourne un identifiant de la commande effectuée">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
	</cffunction>
	
	<cffunction access="public" name="locateDevice" returntype="String" hint="Envoi un SMS de requête de localisation à un équipement et retourne un identifiant de la commande effectuée">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
	</cffunction>
	
	<cffunction access="public" name="getAntitheftCommandLog" returntype="Any" hint="Retourne les logs des commandes Anti-Theft pour une Single License">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
	</cffunction>
	
	<cffunction access="public" name="getLocationResponseLog" returntype="Any" hint="Retourne les logs des commandes Anti-Theft de localisation pour une Single License">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
	</cffunction>
	
	<cffunction access="public" name="getLocateSMS" returntype="String" hint="Retourne un SMS qui peut être envoyé par SMSC pour retourner la réponse de localisation du client AntiTheft">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
	</cffunction>
	
	<cffunction access="public" name="activateAlarm" returntype="String" hint="Envoi un SMS d'activation d'alarme à un équipement et retourne un identifiant de la commande effectuée">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
	</cffunction>
	
	<cffunction access="public" name="getAlarmSMS" returntype="String" hint="Retourne un SMS qui peut être envoyé par SMSC pour activer l'alarme du client">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
	</cffunction>
	
	<cffunction access="public" name="getATMessageResponseLog" returntype="Any" hint="Retourne les logs pour une commande spécifique">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfargument name="commandId" type="String" required="true" hint="Identifiant de la commande Anti-Theft">
	</cffunction>
</cfinterface>