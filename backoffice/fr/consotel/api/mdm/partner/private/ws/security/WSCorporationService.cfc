<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.security.WSCorporationService"
implements="fr.consotel.api.mdm.partner.private.ws.IWSCorporationService" hint="Implémentation fournissant les méthodes : Corporation Management Services
Dans la version actuelle de cette classe : Les méthodes retournent des implémentations CFMX correspondant aux types utilisés par le service FSecure
Les méthodes de cette implémentation sont soumises à la vérification de l'authentification
<br><b>Pour des raisons de performance : Certaines méthodes lèvent une exception provenant du Web Service lorsqu'aucune information ne correspond à l'identifiant fourni (e.g : corporationId)</b>
<br><i>Entres autres : La méthode searchCorporations() peut être utilisée pour vérifier l'existence d'un équipement avant d'appeler ces méthodes</i>">
	<!--- fr.consotel.api.mdm.partner.private.ws.security.WSCorporationService --->
	<cffunction access="package" name="getWSService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSCorporationService" hint="Retourne une instance de cette classe">
		<cfargument name="serviceRepository" type="fr.consotel.api.mdm.partner.private.ws.security.FSecure" required="true" hint="Implémentation d'accès aux Web Services">
		<cfif NOT structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfset VARIABLES["WS_REPOSITORY"]=ARGUMENTS["serviceRepository"]>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="getWSRepository" returntype="fr.consotel.api.mdm.partner.private.ws.security.FSecure" hint="Implémentation d'accès aux Web Services">
		<cfif structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfreturn VARIABLES["WS_REPOSITORY"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'entité MDM Service Repository n'est pas définie">
		</cfif>
	</cffunction>
	
	<!--- Corporation Services --->
	<cffunction access="public" name="searchCorporations" returntype="Any">
		<cfargument name="corporationName" type="String" required="true" hint="Chaine à rechercher dans le nom de l'entité Corporation">
		<cfargument name="corporationId" type="String" required="true" hint="Chaine à rechercher dans l'identifiant de l'entité Corporation">
		<cfargument name="pageNumber" type="String" required="true" hint="Numéro de la page de résultat à retourner (1 ou une chaine vide pour retourner la 1ère page de résultat)">
		<!--- Validation des credentials d'authentification --->
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<!--- Implémentation du service FSecure correspondant --->
		<cfset var corpService=fsecure.getCorporationService()>
		<cfreturn corpService.searchCorporations(ARGUMENTS["corporationName"],ARGUMENTS["corporationId"],ARGUMENTS["pageNumber"])>
	</cffunction>
	
	<cffunction access="public" name="getCorporationName" returntype="String" hint="Retourne le nom correspondant à corporationId
	<br><b>Lève une exception provenant du Web Service si la valeur corporationId ne correspond à aucune entité Corporation existante</b>">
		<cfargument name="corporationId" type="String" required="true">
		<!--- Validation des credentials d'authentification --->
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<!--- Implémentation du service FSecure correspondant --->
		<cfset var corpService=fsecure.getCorporationService()>
		<cfreturn corpService.getCorporationName(ARGUMENTS["corporationId"])>
	</cffunction>
	
	<cffunction access="public" name="getCorporationStatus" returntype="String" hint="Retourne le statut de l'entité Corporation correspondant à corporationId (Active ou Inactive)
	<br><b>Lève une exception provenant du Web Service si la valeur corporationId ne correspond à aucune entité Corporation existante</b>">
		<cfargument name="corporationId" type="String" required="true">
		<!--- Validation des credentials d'authentification --->
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<!--- Implémentation du service FSecure correspondant --->
		<cfset var corpService=fsecure.getCorporationService()>
		<cfreturn corpService.getCorporationStatus(ARGUMENTS["corporationId"])>
	</cffunction>
	
	<cffunction access="public" name="createCorporation" returntype="String" hint="Crée une entité Corporation avec le nom corporationName et retourne son identifiant">
		<cfargument name="corporationName" type="String" required="true">
		<!--- Validation des credentials d'authentification --->
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<!--- Implémentation du service FSecure correspondant --->
		<cfset var corpService=fsecure.getCorporationService()>
		<cfreturn corpService.createCorporation(ARGUMENTS["corporationName"])>
	</cffunction>
	
	<cffunction access="public" name="deleteCorporation" returntype="boolean"
	hint="Supprime le statut de l'entité Corporation correspondant à corporationId. Retourne TRUE si l'opération s'est effectuée avec succès et FALSE sinon
	<br><b>Lève une exception provenant du Web Service si la valeur corporationId ne correspond à aucune entité Corporation existante</b><br>
	<b>Une exception est levée si la Corporation contient au moins une licence multiple</b>">
		<cfargument name="corporationId" type="String" required="true">
		<!--- Validation des credentials d'authentification --->
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<!--- Implémentation du service FSecure correspondant --->
		<cfset var corpService=fsecure.getCorporationService()>
		<cfset var deleteStatus=corpService.deleteCorporation(ARGUMENTS["corporationId"])>
		<cfreturn yesNoFormat(deleteStatus)>
	</cffunction>
	
	<cffunction access="public" name="getCorporationCount" returntype="Numeric" hint="Retourne le nombre d'entité Corporation">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var corpService=fsecure.getCorporationService()>
		<cfset var wsResult=corpService.getCorporationCount()>
		<cfreturn VAL(toString(wsResult))>
	</cffunction>

	<cffunction access="public" name="updateCorporationName" returntype="Boolean" hint="Renomme une entité Corporation et retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="corporationId" type="String" required="true" hint="Identifiant de l'entité Corporation">
		<cfargument name="corporationName" type="String" required="true" hint="Nouveau nom à donner à l'entité Corporation">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var corpService=fsecure.getCorporationService()>
		<cfset var wsResult=corpService.updateCorporationName(ARGUMENTS["corporationId"],ARGUMENTS["corporationName"])>
		<cfreturn yesNoFormat(toString(wsResult))>
	</cffunction>

	<cffunction access="public" name="updateCorporationStatus" returntype="String" hint="Met à jour le statut d'une entité Corporation et retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="corporationId" type="String" required="true" hint="Identifiant de l'entité Corporation">
		<cfargument name="status" type="String" required="true" hint="Valeur à donner au statut. Valeurs possibles : Active, Inactive">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var corpService=fsecure.getCorporationService()>
		<cfset var wsResult=corpService.updateCorporationStatus(ARGUMENTS["corporationId"],ARGUMENTS["status"])>
		<cfreturn yesNoFormat(toString(wsResult))>
	</cffunction>
</cfcomponent>