<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.management.WSServerGroupService" implements="fr.consotel.api.mdm.partner.private.ws.IWSServerGroupService"
hint="Implémentation fournissant les méthodes du Service Xml Zenprise. Les méthodes de cette implémentation sont soumises à la vérification de l'authentification">
	<!--- fr.consotel.api.mdm.partner.private.ws.management.WSServerGroupService --->
	<cffunction access="package" name="getWSService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSServerGroupService" hint="Retourne une instance de cette classe">
		<cfargument name="serviceRepository" type="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" required="true" hint="Implémentation d'accès aux Web Services">
		<cfif NOT structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfset VARIABLES["WS_REPOSITORY"]=ARGUMENTS["serviceRepository"]>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="getWSRepository" returntype="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" hint="Implémentation d'accès aux Web Services">
		<cfif structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfreturn VARIABLES["WS_REPOSITORY"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'entité MDM Service Repository n'est pas définie">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="asJavaNullValue" returntype="Any" hint="Retourne la valeur nulle Java si stringValue est une chaine vide et stringValue sinon">
		<cfargument name="stringValue" type="String" required="true">
		<cfif TRIM(ARGUMENTS["stringValue"]) EQ "">
			<cfreturn javaCast("NULL",0)>
		<cfelse>
			<cfreturn ARGUMENTS["stringValue"]>
		</cfif>
	</cffunction>
	
	<!--- ServerGroup Service --->
	<cffunction access="public" name="addServerToServerGroup" returntype="Numeric" hint="Ajoute une serveur à un groupe de serveur et retourne 0 en cas de succès et -1 en cas d'erreur">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur">
		<cfargument name="serverId" type="numeric" required="true" hint="ID du serveur">
		<cfset var soapRequest="">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfxml variable="soapRequest">
			<cfoutput>
				<soapenv:Envelope xmlns:zenprise="zenprise" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
					xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
					<soapenv:Body>
						<zenprise:addServerToServerGroup>
							<zenprise:serverGroupId xsi:nil="false">#ARGUMENTS["serverGroupId"]#</zenprise:serverGroupId>
							<zenprise:serverId xsi:nil="false">#ARGUMENTS["serverId"]#</zenprise:serverId>
						</zenprise:addServerToServerGroup>
					</soapenv:Body>
				</soapenv:Envelope>
			</cfoutput>
		</cfxml>
		<cfset var soapResponse=zdmRepository.sendServerGroupRequest(soapRequest)>
		<cfif NOT yesNoFormat(arrayLen(xmlSearch(soapResponse,"//soapenv:Fault")))>
			<cfset var soapResultNode=soapResponse.xmlRoot.Body.addServerToServerGroupResponse.addServerToServerGroupReturn>
			<cfreturn VAL(toString(soapResultNode["xmlText"]))>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le service ServerGroup mentionne un message d'erreur SOAP (Zenprise)" detail="#toString(soapResponse)#">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="changeServerPriority" returntype="Numeric" hint="Change la priorité d'un serveur et retourne 0 en cas de succès et -1 en cas d'erreur">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur">
		<cfargument name="serverId" type="numeric" required="true" hint="ID du serveur">
		<cfargument name="isUpPriority" type="boolean" required="true" hint="TRUE pour une priorité élévée et FALSE sinon">
		<cfset var soapRequest="">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfxml variable="soapRequest">
			<cfoutput>
				<soapenv:Envelope xmlns:zenprise="zenprise" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
					xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
					<soapenv:Body>
						<zenprise:changeServerPriority>
							<zenprise:serverGroupId xsi:nil="false">#ARGUMENTS["serverGroupId"]#</zenprise:serverGroupId>
							<zenprise:serverId xsi:nil="false">#ARGUMENTS["serverId"]#</zenprise:serverId>
							<zenprise:isUpPriority xsi:nil="false">#ARGUMENTS["isUpPriority"]#</zenprise:isUpPriority>
						</zenprise:changeServerPriority>
					</soapenv:Body>
				</soapenv:Envelope>
			</cfoutput>
		</cfxml>
		<cfset var soapResponse=zdmRepository.sendServerGroupRequest(soapRequest)>
		<cfif NOT yesNoFormat(arrayLen(xmlSearch(soapResponse,"//soapenv:Fault")))>
			<cfset var soapResultNode=soapResponse.xmlRoot.Body.changeServerPriorityResponse.changeServerPriorityReturn>
			<cfreturn VAL(toString(soapResultNode["xmlText"]))>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le service ServerGroup mentionne un message d'erreur SOAP (Zenprise)" detail="#toString(soapResponse)#">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="createServer" returntype="Numeric" hint="Crée un nouveau serveur et retourne son ID ou -1 en cas d'erreur">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur">
		<cfargument name="serverName" type="String" required="true" hint="Nom du serveur">
		<cfargument name="serverHost" type="String" required="true" hint="Nom de machine du serveur">
		<cfargument name="serverPort" type="numeric" required="true" hint="Numéro de port du serveur">
		<cfargument name="serverIsSSL" type="boolean" required="true" hint="TRUE si le serveur utilise SSL et FALSE sinon">
		<cfargument name="serverUseProxy" type="boolean" required="true" hint="TRUE si le serveur utilise un proxy et FALSE sinon">
		<cfargument name="serverIsDefault" type="boolean" required="true" hint="TRUE si le serveur est à utiliser par défaut et FALSE sinon">
		<cfargument name="ipBegin" type="Array" required="true" hint="Tableau contenant la plage de début d'adresses IP (e.g : 192.168.1.1, 192.168.1.2, etc...)">
		<cfargument name="ipEnd" type="Array" required="true" hint="Tableau contenant la plage de fin d'adresses IP (e.g : 192.168.1.1, 192.168.1.2, etc...)">
		<cfset var soapRequest="">
		<cfset var ipBeginAddress=ARGUMENTS["ipBegin"]>
		<cfset var ipEndAddress=ARGUMENTS["ipEnd"]>
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfxml variable="soapRequest">
			<cfoutput>
				<soapenv:Envelope xmlns:zenprise="zenprise" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
					xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
					<soapenv:Body>
						<zenprise:createServer>
							<zenprise:serverGroupId xsi:nil="false">#ARGUMENTS["serverGroupId"]#</zenprise:serverGroupId>
							<zenprise:serverName xsi:nil="false">#ARGUMENTS["serverName"]#</zenprise:serverName>
							<zenprise:serverHost xsi:nil="false">#ARGUMENTS["serverHost"]#</zenprise:serverHost>
							<zenprise:serverPort xsi:nil="false">#ARGUMENTS["serverPort"]#</zenprise:serverPort>
							<zenprise:serverIsSSL xsi:nil="false">#ARGUMENTS["serverIsSSL"]#</zenprise:serverIsSSL>
							<zenprise:serverUseProxy xsi:nil="false">#ARGUMENTS["serverUseProxy"]#</zenprise:serverUseProxy>
							<zenprise:serverIsDefault xsi:nil="false">#ARGUMENTS["serverIsDefault"]#</zenprise:serverIsDefault>
							<zenprise:ipBegin xsi:nil="false">
							<cfloop index="i" from="1" to="#arrayLen(ipBeginAddress)#">
								<zenprise:ipAddress xsi:type="soapenc:string">#ipBeginAddress[i]#</zenprise:ipAddress>
							</cfloop>
							</zenprise:ipBegin>
							<zenprise:ipEnd xsi:nil="false">
							<cfloop index="i" from="1" to="#arrayLen(ipEndAddress)#">
								<zenprise:ipAddress xsi:type="soapenc:string">#ipEndAddress[i]#</zenprise:ipAddress>
							</cfloop>
							</zenprise:ipEnd>
						</zenprise:createServer>
					</soapenv:Body>
				</soapenv:Envelope>
			</cfoutput>
		</cfxml>
		<cfset var soapResponse=zdmRepository.sendServerGroupRequest(soapRequest)>
		<cfif NOT yesNoFormat(arrayLen(xmlSearch(soapResponse,"//soapenv:Fault")))>
			<cfset var soapResultNode=soapResponse.xmlRoot.Body.createServerResponse.createServerReturn>
			<cfreturn VAL(toString(soapResultNode["xmlText"]))>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le service ServerGroup mentionne un message d'erreur SOAP (Zenprise)" detail="#toString(soapResponse)#">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="createServerGroup" returntype="Numeric" hint="Crée un groupe de serveur et retourne son ID ou -1 en cas d'erreur">
		<cfargument name="serverGroupName" type="String" required="true" hint="Nom du groupe de serveur">
		<cfset var soapRequest="">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfxml variable="soapRequest">
			<cfoutput>
				<soapenv:Envelope xmlns:zenprise="zenprise" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
					xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
					<soapenv:Body>
						<zenprise:createServerGroup>
							<zenprise:serverGroupName xsi:nil="false">#ARGUMENTS["serverGroupName"]#</zenprise:serverGroupName>
						</zenprise:createServerGroup>
					</soapenv:Body>
				</soapenv:Envelope>
			</cfoutput>
		</cfxml>
		<cfset var soapResponse=zdmRepository.sendServerGroupRequest(soapRequest)>
		<cfif NOT yesNoFormat(arrayLen(xmlSearch(soapResponse,"//soapenv:Fault")))>
			<cfset var soapResultNode=soapResponse.xmlRoot.Body.createServerGroupResponse.createServerGroupReturn>
			<cfreturn VAL(toString(soapResultNode["xmlText"]))>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le service ServerGroup mentionne un message d'erreur SOAP (Zenprise)" detail="#toString(soapResponse)#">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="deleteServer" returntype="Numeric" hint="Supprime un ou plusieurs serveurs. Retourne 0 en cas de succès et -1 en cas d'erreur<br>
	<b>Bug (Zenprise) :</b> Cette méthode lève une exception car elle n'est pas correctement supportée">
		<cfargument name="serverIds" type="Array" required="true" hint="Tableau contenant les ID des serveurs à supprimer">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas supportée correctement par Zenprise actuellement">
		<cfset var soapRequest="">
		<cfset var serverIdArray=ARGUMENTS["serverIds"]>
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfxml variable="soapRequest">
			<cfoutput>
				<soapenv:Envelope xmlns:zenprise="zenprise" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
					xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
					<soapenv:Body>
						<zenprise:deleteServer>
							<zenprise:serverIds xsi:nil="false">
							<cfloop index="i" from="1" to="#arrayLen(serverIdArray)#">
								<zenprise:serverId xsi:type="soapenc:string">#serverIdArray[i]#</zenprise:serverId>
							</cfloop>
							</zenprise:serverIds>
						</zenprise:deleteServer>
					</soapenv:Body>
				</soapenv:Envelope>
			</cfoutput>
		</cfxml>
		<cfset var soapResponse=zdmRepository.sendServerGroupRequest(soapRequest)>
		<cfif NOT yesNoFormat(arrayLen(xmlSearch(soapResponse,"//soapenv:Fault")))>
			<cfset var soapResultNode=soapResponse.xmlRoot.Body.deleteServerResponse.deleteServerReturn>
			<cfreturn VAL(toString(soapResultNode["xmlText"]))>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le service ServerGroup mentionne un message d'erreur SOAP (Zenprise)" detail="#toString(soapResponse)#">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="deleteServerGroup" returntype="Numeric" hint="Supprime un groupe de serveur. Retourne 0 en cas de succès et -1 en cas d'erreur">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur">
		<cfset var soapRequest="">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfxml variable="soapRequest">
			<cfoutput>
				<soapenv:Envelope xmlns:zenprise="zenprise" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
					xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
					<soapenv:Body>
						<zenprise:deleteServerGroup>
							<zenprise:serverGroupId xsi:nil="false">#ARGUMENTS["serverGroupId"]#</zenprise:serverGroupId>
						</zenprise:deleteServerGroup>
					</soapenv:Body>
				</soapenv:Envelope>
			</cfoutput>
		</cfxml>
		<cfset var soapResponse=zdmRepository.sendServerGroupRequest(soapRequest)>
		<cfif NOT yesNoFormat(arrayLen(xmlSearch(soapResponse,"//soapenv:Fault")))>
			<cfset var soapResultNode=soapResponse.xmlRoot.Body.deleteServerGroupResponse.deleteServerGroupReturn>
			<cfreturn VAL(toString(soapResultNode["xmlText"]))>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le service ServerGroup mentionne un message d'erreur SOAP (Zenprise)" detail="#toString(soapResponse)#">
		</cfif>
	</cffunction>

	<cffunction access="public" name="getDefaultServerInfos" returntype="Array" hint="Retourne un tableau contenant les infos du serveur par défaut<br>
	Le 1er élément du tableau contient le nom d'une propriété et le 2ème élément contient la valeur correspondante et ainsi de suite">
		<cfset var soapRequest="">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfxml variable="soapRequest">
			<cfoutput>
				<soapenv:Envelope xmlns:zenprise="zenprise" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
					xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
					<soapenv:Body>
						<zenprise:getDefaultServerInfos/>
					</soapenv:Body>
				</soapenv:Envelope>
			</cfoutput>
		</cfxml>
		<cfset var soapResponse=zdmRepository.sendServerGroupRequest(soapRequest)>
		<cfif NOT yesNoFormat(arrayLen(xmlSearch(soapResponse,"//soapenv:Fault")))>
			<cfset var wsResult=[]>
			<cfset var soapResultList=soapResponse.xmlRoot.Body.getDefaultServerInfosResponse.getDefaultServerInfosReturn.xmlChildren>
			<cfset var listCount=arrayLen(soapResultList)>
			<cfloop index="i" from="1" to="#listCount#">
				<cfset wsResult[i]=toString(soapResultList[i]["xmlText"])>
			</cfloop>
			<cfreturn wsResult>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le service ServerGroup mentionne un message d'erreur SOAP (Zenprise)" detail="#toString(soapResponse)#">
		</cfif>
	</cffunction>

	<cffunction access="public" name="getServerFromServerGroup" returntype="Array" hint="Retourne un tableau contenant les infos d'un serveur d'un groupe de serveur<br>
	Le 1er élément du tableau contient le nom d'une propriété et le 2ème élément contient la valeur correspondante et ainsi de suite">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur">
		<cfargument name="serverId" type="numeric" required="true" hint="ID du serveur">
		<cfset var soapRequest="">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfxml variable="soapRequest">
			<cfoutput>
				<soapenv:Envelope xmlns:zenprise="zenprise" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
					xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
					<soapenv:Body>
						<zenprise:getServerFromServerGroup>
							<zenprise:serverGroupId xsi:nil="false">#ARGUMENTS["serverGroupId"]#</zenprise:serverGroupId>
							<zenprise:serverId xsi:nil="false">#ARGUMENTS["serverId"]#</zenprise:serverId>
						</zenprise:getServerFromServerGroup>
					</soapenv:Body>
				</soapenv:Envelope>
			</cfoutput>
		</cfxml>
		<cfset var soapResponse=zdmRepository.sendServerGroupRequest(soapRequest)>
		<cfif NOT yesNoFormat(arrayLen(xmlSearch(soapResponse,"//soapenv:Fault")))>
			<cfset var wsResult=[]>
			<cfset var soapResultList=soapResponse.xmlRoot.Body.getServerFromServerGroupResponse.getServerFromServerGroupReturn.xmlChildren>
			<cfset var listCount=arrayLen(soapResultList)>
			<cfloop index="i" from="1" to="#listCount#">
				<cfset wsResult[i]=toString(soapResultList[i]["xmlText"])>
			</cfloop>
			<cfreturn wsResult>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le service ServerGroup mentionne un message d'erreur SOAP (Zenprise)" detail="#toString(soapResponse)#">
		</cfif>
	</cffunction>

	<cffunction access="public" name="getServerListfromServerGroup" returntype="Array" hint="Retourne les infos de chaque serveur d'un groupe de serveur<br>
	Les infos sont retournées au même format que getServerFromServerGroup() et retournées à la suite serveur par serveur">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur ou -1 pour retourner tous les serveurs">
		<cfset var soapRequest="">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfxml variable="soapRequest">
			<cfoutput>
				<soapenv:Envelope xmlns:zenprise="zenprise" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
					xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
					<soapenv:Body>
						<zenprise:getServerListfromServerGroup>
							<zenprise:serverGroupId xsi:nil="false">#ARGUMENTS["serverGroupId"]#</zenprise:serverGroupId>
						</zenprise:getServerListfromServerGroup>
					</soapenv:Body>
				</soapenv:Envelope>
			</cfoutput>
		</cfxml>
		<cfset var soapResponse=zdmRepository.sendServerGroupRequest(soapRequest)>
		<cfif NOT yesNoFormat(arrayLen(xmlSearch(soapResponse,"//soapenv:Fault")))>
			<cfset var wsResult=[]>
			<cfset var soapResultList=soapResponse.xmlRoot.Body.getServerListfromServerGroupResponse.getServerListfromServerGroupReturn.xmlChildren>
			<cfset var listCount=arrayLen(soapResultList)>
			<cfloop index="i" from="1" to="#listCount#">
				<cfset wsResult[i]=toString(soapResultList[i]["xmlText"])>
			</cfloop>
			<cfreturn wsResult>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le service ServerGroup mentionne un message d'erreur SOAP (Zenprise)" detail="#toString(soapResponse)#">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="removeServerFromServerGroup" returntype="numeric"
	hint="Supprime un ou plusieurs serveurs d'un groupe de serveur. Retourne 0 en cas de succès et -1 en cas d'erreur">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur">
		<cfargument name="serverIds" type="Array" required="true" hint="Tableau contenant les ID des serveurs à supprimer">
		<cfset var soapRequest="">
		<cfset var serverIdArray=ARGUMENTS["serverIds"]>
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfxml variable="soapRequest">
			<cfoutput>
				<soapenv:Envelope xmlns:zenprise="zenprise" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
					xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
					<soapenv:Body>
						<zenprise:removeServerFromServerGroup>
							<zenprise:serverGroupId xsi:nil="false">#ARGUMENTS["serverGroupId"]#</zenprise:serverGroupId>
							<zenprise:serverIds xsi:nil="false">
							<cfloop index="i" from="1" to="#arrayLen(serverIdArray)#">
								<zenprise:serverId xsi:type="soapenc:string">#serverIdArray[i]#</zenprise:serverId>
							</cfloop>
							</zenprise:serverIds>
						</zenprise:removeServerFromServerGroup>
					</soapenv:Body>
				</soapenv:Envelope>
			</cfoutput>
		</cfxml>
		<cfset var soapResponse=zdmRepository.sendServerGroupRequest(soapRequest)>
		<cfif NOT yesNoFormat(arrayLen(xmlSearch(soapResponse,"//soapenv:Fault")))>
			<cfset var soapResultNode=soapResponse.xmlRoot.Body.removeServerFromServerGroupResponse.removeServerFromServerGroupReturn>
			<cfreturn VAL(toString(soapResultNode["xmlText"]))>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le service ServerGroup mentionne un message d'erreur SOAP (Zenprise)" detail="#toString(soapResponse)#">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="updateServer" returntype="Numeric" hint="Met à jour les infos d'un serveur et retourne son ID ou -1 en cas d'erreur">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur">
		<cfargument name="serverName" type="String" required="true" hint="Nom du serveur">
		<cfargument name="serverHost" type="String" required="true" hint="Nom de machine du serveur">
		<cfargument name="serverPort" type="numeric" required="true" hint="Numéro de port du serveur">
		<cfargument name="serverIsSSL" type="boolean" required="true" hint="TRUE si le serveur utilise SSL et FALSE sinon">
		<cfargument name="serverUseProxy" type="boolean" required="true" hint="TRUE si le serveur utilise un proxy et FALSE sinon">
		<cfargument name="serverIsDefault" type="boolean" required="true" hint="TRUE si le serveur est à utiliser par défaut et FALSE sinon">
		<cfargument name="ipBegin" type="Array" required="true" hint="Tableau contenant la plage de début d'adresses IP (e.g : 192.168.1.1, 192.168.1.2, etc...)">
		<cfargument name="ipEnd" type="Array" required="true" hint="Tableau contenant la plage de fin d'adresses IP (e.g : 192.168.1.1, 192.168.1.2, etc...)">
		<cfargument name="serverId" type="numeric" required="true" hint="ID du serveur">
		<cfset var soapRequest="">
		<cfset var ipBeginAddress=ARGUMENTS["ipBegin"]>
		<cfset var ipEndAddress=ARGUMENTS["ipEnd"]>
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfxml variable="soapRequest">
			<cfoutput>
				<soapenv:Envelope xmlns:zenprise="zenprise" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
					xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
					<soapenv:Body>
						<zenprise:updateServer>
							<zenprise:serverGroupId xsi:nil="false">#ARGUMENTS["serverGroupId"]#</zenprise:serverGroupId>
							<zenprise:serverName xsi:nil="false">#ARGUMENTS["serverName"]#</zenprise:serverName>
							<zenprise:serverHost xsi:nil="false">#ARGUMENTS["serverHost"]#</zenprise:serverHost>
							<zenprise:serverPort xsi:nil="false">#ARGUMENTS["serverPort"]#</zenprise:serverPort>
							<zenprise:serverIsSSL xsi:nil="false">#ARGUMENTS["serverIsSSL"]#</zenprise:serverIsSSL>
							<zenprise:serverUseProxy xsi:nil="false">#ARGUMENTS["serverUseProxy"]#</zenprise:serverUseProxy>
							<zenprise:serverIsDefault xsi:nil="false">#ARGUMENTS["serverIsDefault"]#</zenprise:serverIsDefault>
							<zenprise:ipBegin xsi:nil="false">
							<cfloop index="i" from="1" to="#arrayLen(ipBeginAddress)#">
								<zenprise:ipAddress xsi:type="soapenc:string">#ipBeginAddress[i]#</zenprise:ipAddress>
							</cfloop>
							</zenprise:ipBegin>
							<zenprise:ipEnd xsi:nil="false">
							<cfloop index="i" from="1" to="#arrayLen(ipEndAddress)#">
								<zenprise:ipAddress xsi:type="soapenc:string">#ipEndAddress[i]#</zenprise:ipAddress>
							</cfloop>
							</zenprise:ipEnd>
							<zenprise:serverId xsi:nil="false">#ARGUMENTS["serverId"]#</zenprise:serverId>
						</zenprise:updateServer>
					</soapenv:Body>
				</soapenv:Envelope>
			</cfoutput>
		</cfxml>
		<cfset var soapResponse=zdmRepository.sendServerGroupRequest(soapRequest)>
		<cfif NOT yesNoFormat(arrayLen(xmlSearch(soapResponse,"//soapenv:Fault")))>
			<cfset var soapResultNode=soapResponse.xmlRoot.Body.updateServerResponse.updateServerReturn>
			<cfreturn VAL(toString(soapResultNode["xmlText"]))>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le service ServerGroup mentionne un message d'erreur SOAP (Zenprise)" detail="#toString(soapResponse)#">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="updateServerGroup" returntype="Numeric" hint="Met à jour les infos d'un groupe de serveur et retourne son ID ou -1 en cas d'erreur">
		<cfargument name="serverGroupName" type="String" required="true" hint="Nom du groupe de serveur">
		<cfargument name="id" type="numeric" required="true" hint="N.D">
		<cfset var soapRequest="">
		<cfset var serverIdArray=ARGUMENTS["serverIds"]>
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfxml variable="soapRequest">
			<cfoutput>
				<soapenv:Envelope xmlns:zenprise="zenprise" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
					xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
					<soapenv:Body>
						<zenprise:updateServerGroup>
							<zenprise:serverGroupName xsi:nil="false">#ARGUMENTS["serverGroupName"]#</zenprise:serverGroupName>
							<zenprise:id xsi:nil="false">#ARGUMENTS["id"]#</zenprise:id>
						</zenprise:updateServerGroup>
					</soapenv:Body>
				</soapenv:Envelope>
			</cfoutput>
		</cfxml>
		<cfset var soapResponse=zdmRepository.sendServerGroupRequest(soapRequest)>
		<cfif NOT yesNoFormat(arrayLen(xmlSearch(soapResponse,"//soapenv:Fault")))>
			<cfset var soapResultNode=soapResponse.xmlRoot.Body.updateServerGroupResponse.updateServerGroupReturn>
			<cfreturn VAL(toString(soapResultNode["xmlText"]))>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le service ServerGroup mentionne un message d'erreur SOAP (Zenprise)" detail="#toString(soapResponse)#">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getServerGroupManager" returntype="Any" hint="N.D (Cette méthode lève une exception car elle n'est pas supportée par Zenprise actuellement)">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas supportée par Zenprise actuellement">
	</cffunction>
	
	<cffunction access="public" name="getServerManager" returntype="Any" hint="N.D (Cette méthode lève une exception car elle n'est pas supportée par Zenprise actuellement)">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas supportée par Zenprise actuellement">
	</cffunction>
	
	<cffunction access="public" name="setServerGroupManager" returntype="void" hint="N.D (Cette méthode lève une exception car elle n'est pas supportée par Zenprise actuellement)">
		<cfargument name="serverGroupManager" type="Struct" required="true" hint="Structure représentant un groupe de serveur">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas supportée par Zenprise actuellement">
	</cffunction>
	
	<cffunction access="public" name="setServerManager" returntype="void" hint="N.D (Cette méthode lève une exception car elle n'est pas supportée par Zenprise actuellement)">
		<cfargument name="serverManager" type="Struct" required="true" hint="Structure représentant un serveur">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas supportée par Zenprise actuellement">
	</cffunction>
</cfcomponent>