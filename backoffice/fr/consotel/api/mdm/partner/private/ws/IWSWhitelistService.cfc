<cfinterface author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.IWSWhitelistService" hint="Interface décrivant les méthodes du Service Whitelist Zenprise">
	<cffunction access="public" name="getAllApplicationsWhiteList" returntype="Array" hint="Retourne un tableau contenant la liste des noms des applications en whitelist définis sur le serveur d'un OS">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
	</cffunction>
	
	<cffunction access="public" name="getApplicationsInWhitelist" returntype="Array" hint="Retourne un tableau contenant la liste des applications d'une whitelist d'un OS<br>
	Chaque élément du tableau contient les propriétés suivantes : identifier, name">
		<cfargument name="applicationsWhitelistName" type="String" required="true" hint="Nom d'une whitelist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
	</cffunction>
	
	<cffunction access="public" name="getDefaultWhitelist" returntype="Array" hint="Retourne un tableau contenant la liste des application de la whitelist par défaut d'un OS<br>
	Chaque élément du tableau contient les propriétés suivantes : identifier, name">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
	</cffunction>
	
	<cffunction access="public" name="addWhiteList" returntype="Void" hint="Ajoute une whitelist d'un OS">
		<cfargument name="name" type="String" required="true" hint="Nom pour la whitelist">
		<cfargument name="description" type="String" required="true" hint="Description à associer à la whitelist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applications" type="Array" required="true" hint="Applications de la whitelist. Chaque élément du tableau est une structure contenant les clé suivantes : identifier, name">
	</cffunction>
	
	<cffunction access="public" name="updateWhiteList" returntype="Void" hint="Met à jour une whitelist d'un OS">
		<cfargument name="name" type="String" required="true" hint="Nom pour la whitelist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="newName" type="String" required="true" hint="Nouveau nom pour la whitelist">
		<cfargument name="newDescription" type="String" required="true" hint="Nouvelle description à associer à la whitelist">
		<cfargument name="applications" type="Array" required="true" hint="Applications de la whitelist. Chaque élément du tableau est une structure contenant les clé suivantes : identifier, name">
	</cffunction>
	
	<cffunction access="public" name="updateDefaultWhiteList" returntype="Void" hint="Met à jour la whitelist par défaut d'un OS">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applications" type="Array" required="true" hint="Applications de la whitelist. Chaque élément du tableau est une structure contenant les clé suivantes : identifier, name">
	</cffunction>
	
	<cffunction access="public" name="removeWhiteList" returntype="Void" hint="Supprime une whitelist d'un OS">
		<cfargument name="name" type="String" required="true" hint="Nom pour la whitelist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
	</cffunction>
	
	<cffunction access="public" name="addApplicationToWhiteList" returntype="Void" hint="Ajoute une application dans une whitelist d'un OS">
		<cfargument name="name" type="String" required="true" hint="Nom pour la whitelist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationName" type="String" required="true" hint="Valeur de la propriété name pour l'application">
		<cfargument name="applicationIdentifier" type="String" required="true" hint="Valeur de la propriété identifier pour l'application">
	</cffunction>
	
	<cffunction access="public" name="addApplicationToDefaultWhiteList" returntype="Void" hint="Ajoute une application dans la whitelist par défaut de l'OS">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationName" type="String" required="true" hint="Valeur de la propriété name pour l'application">
		<cfargument name="applicationIdentifier" type="String" required="true" hint="Valeur de la propriété identifier pour l'application">
	</cffunction>
	
	<cffunction access="public" name="updateApplicationInWhiteList" returntype="Void" hint="Met à jour une application dans une whitelist d'un OS">
		<cfargument name="name" type="String" required="true" hint="Nom pour la whitelist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationOldName" type="String" required="true" hint="Valeur actuelle de la propriété name de l'application">
		<cfargument name="applicationNewName" type="String" required="true" hint="Nouvelle valeur pour de la propriété name de l'application">
		<cfargument name="applicationNewIdentifier" type="String" required="true" hint="Nouvelle valeur pour de la propriété identifier de l'application">
	</cffunction>
	
	<cffunction access="public" name="updateApplicationInDefaultWhiteList" returntype="Void" hint="Met à jour une application dans la whitelist par défaut d'un OS">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationOldName" type="String" required="true" hint="Valeur actuelle de la propriété name de l'application">
		<cfargument name="applicationNewName" type="String" required="true" hint="Nouvelle valeur pour de la propriété name de l'application">
		<cfargument name="applicationNewIdentifier" type="String" required="true" hint="Nouvelle valeur pour de la propriété identifier de l'application">
	</cffunction>
	
	<cffunction access="public" name="removeApplicationFromWhiteList" returntype="Void" hint="Supprime une application d'une whitelist d'un OS">
		<cfargument name="name" type="String" required="true" hint="Nom pour la whitelist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationName" type="String" required="true" hint="Valeur de la propriété name de l'application">
	</cffunction>
	
	<cffunction access="public" name="removeApplicationFromDefaultWhiteList" returntype="Void" hint="Supprime une application de la whitelist par défaut d'un OS">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationName" type="String" required="true" hint="Valeur de la propriété name de l'application">
	</cffunction>
</cfinterface>