<cfinterface author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.IWSCfgProvisioningService" hint="Interface décrivant les méthodes du Service de gestion du Provisioning CFG">
	<cffunction access="public" name="addCfgFile" returntype="Void" hint="Ajoute un fichier de configuration avec son contenu">
		<cfargument name="strOSFamily" type="String" required="true" hint="OS cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier">
		<cfargument name="fileContent" type="Array" required="true" hint="Contenu du fichier en tant que tableau de bytes">
		<cfargument name="comment" type="String" required="true" hint="Commentaire à associer au fichier">
	</cffunction>
	
	<cffunction access="public" name="updateCfgFile" returntype="Void" hint="Met à jour le contenu d'un fichier de configuration">
		<cfargument name="strOSFamily" type="String" required="true" hint="OS cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="oldFileName" type="String" required="true" hint="Nom actuel du fichier de configuration">
		<cfargument name="newFileName" type="String" required="true" hint="Nouveau nom à donner au fichier de configuration ou une chaine vide pour garder oldFileName">
		<cfargument name="comment" type="String" required="true" hint="Commentaire à associer au fichier">
		<cfargument name="fileContent" type="Array" required="true" hint="Contenu du fichier en tant que tableau de bytes">
	</cffunction>
	
	<cffunction access="public" name="removeCfgFile" returntype="Void" hint="Supprime un fichier de configuration">
		<cfargument name="strOSFamily" type="String" required="true" hint="OS cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier">
	</cffunction>
	
	<cffunction access="public" name="getCfgFileInfo" returntype="Array" hint="Retourne les infos d'un fichier de configuration en tant que tableau<br>
	Les éléments du tableau sont par ordre d'apparition : Nom du fichier, Taille, Commentaire associé, Date de création ou d'upload, Date de moficiation">
		<cfargument name="strOSFamily" type="String" required="true" hint="OS cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier">
	</cffunction>
	
	<cffunction access="public" name="getCfgFileList" returntype="Array" hint="Retourne un tableau contenant la liste des fichiers de configuration">
		<cfargument name="strOSFamily" type="String" required="true" hint="OS cible (e.g : WINDOWS, ANDROID, iOS)">
	</cffunction>
	
	<cffunction access="public" name="validateCfgFile" returntype="Boolean" hint="Retourne TRUE si le contenu du fichier de configuration est valide et FALSE sinon<br>
	Une exception est levée par le service Zenprise lorsque la valeur retournée est false">
		<cfargument name="strOSFamily" type="String" required="true" hint="OS cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier">
	</cffunction>
</cfinterface>