<cfinterface author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.IWSTunnelService" hint="Interface décrivant les méthodes du Service Tunnel Zenprise<br>
Les propriétés pour un tunnel sont représentées en tant que structure contenant les clés suivantes :<br>
- deviceHost : Nom de machine de l'équipement (DEVICE)<br>
- devicePort : Numéro de port de l'équipement (DEVICE)<br>
- initiator : Initiateur (e.g : SERVER ou DEVICE)<br>
- maxConnect : Nombre max de connexion<br>
- name : Nom du tunnel<br>
- osFamily : OS (e.g : iOS, WINDOWS, ANDROID)<br>
- protocol : Protocole (e.g : GENERIC TCP ou FTP)<br>
- serverHost : Nom de machine du serveur<br>
- serverPort : Numéro de port du serveur<br>
- timeOut : Timeout en secondes<br>
- isNoRoaming : TRUE ou FALSE<br>
- isSSL : TRUE pour le support SSL et FALSE sinon">
	<cffunction access="public" name="removeTunnel" returntype="Void" hint="Supprime un tunnel">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="name" type="String" required="true" hint="Nom du tunnel à supprimer">
	</cffunction>
	
	<cffunction access="public" name="getTunnelList" returntype="Array" hint="Retourne la liste des noms des tunnels dans un tableau">
	</cffunction>

	<cffunction access="public" name="getOsTunnelList" returntype="Array" hint="Retourne la liste des noms des tunnels pour un OS dans un tableau">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
	</cffunction>
	
	<cffunction access="public" name="createTunnel" returntype="String" hint="Crée un tunnel et retourne son nom">
		<cfargument name="name" type="String" required="true" hint="Nom du tunnel à créer">
	</cffunction>
	
	<cffunction access="public" name="createTunnelWithProperties" returntype="Void" hint="Crée un tunnel avec les propriétés fournies">
		<cfargument name="tunnel" type="Struct" required="true" hint="Propriétés du tunnel à créer">
	</cffunction>
	
	<cffunction access="public" name="getTunnelProperty" returntype="Any" hint="Retourne les propriétés d'un tunnel">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="name" type="String" required="true" hint="Nom du tunnel à supprimer">
	</cffunction>
	
	<cffunction access="public" name="setTunnelProperty" returntype="Void" hint="Définit les propriétés d'un tunnel">
		<cfargument name="tunnel" type="Struct" required="true" hint="Propriétés pour le tunnel">
	</cffunction>
</cfinterface>