<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.management.ManagementService"
implements="fr.consotel.api.mdm.partner.private.ws.IManagementService" hint="Fournisseur des services MDM de gestion des équipements mobiles
Les méthodes de cette implémentation ne sont pas soumises à la vérification de l'authentification">
	<cffunction access="public" name="getService" returntype="fr.consotel.api.mdm.partner.private.ws.IManagementService" hint="Retourne une instance de cette classe">
		<cfargument name="serviceRepository" type="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" required="true" hint="Implémentation d'accès aux Web Services">
		<cfif NOT structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfset VARIABLES["WS_REPOSITORY"]=ARGUMENTS["serviceRepository"]>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="public" name="getDeviceService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSDeviceService" hint="Retourne le service de gestion des équipements">
		<cfset var zdmRepository=getWSRepository()>
		<!--- Instanciation ou Récupération du service --->
		<cfif NOT structKeyExists(VARIABLES,"DEVICE_SERVICE")>
			<cfset var deviceService=createObject("component","fr.consotel.api.mdm.partner.private.ws.management.WSDeviceService").getWSService(zdmRepository)>
			<cfset VARIABLES["DEVICE_SERVICE"]=deviceService>
		</cfif>
		<cfreturn VARIABLES["DEVICE_SERVICE"]>
	</cffunction>
	
	<cffunction access="public" name="getPackageService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSPackageService" hint="Retourne le service de gestion des packages">
		<cfset var zdmRepository=getWSRepository()>
		<!--- Instanciation ou Récupération du service --->
		<cfif NOT structKeyExists(VARIABLES,"PACKAGE_SERVICE")>
			<cfset var deviceService=createObject("component","fr.consotel.api.mdm.partner.private.ws.management.WSPackageService").getWSService(zdmRepository)>
			<cfset VARIABLES["PACKAGE_SERVICE"]=deviceService>
		</cfif>
		<cfreturn VARIABLES["PACKAGE_SERVICE"]>
	</cffunction>
	
	<cffunction access="public" name="getProvisioningService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSProvisioningService" hint="Retourne le service de gestion du Provisioning">
		<cfset var zdmRepository=getWSRepository()>
		<!--- Instanciation ou Récupération du service --->
		<cfif NOT structKeyExists(VARIABLES,"PROVISIONING_SERVICE")>
			<cfset var deviceService=createObject("component","fr.consotel.api.mdm.partner.private.ws.management.WSProvisioningService").getWSService(zdmRepository)>
			<cfset VARIABLES["PACKAGE_SERVICE"]=deviceService>
		</cfif>
		<cfreturn VARIABLES["PACKAGE_SERVICE"]>
	</cffunction>
	
	<cffunction access="public" name="getCfgProvisioningService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSCfgProvisioningService" hint="Retourne le service de gestion du Provisioning CFG">
		<cfset var zdmRepository=getWSRepository()>
		<!--- Instanciation ou Récupération du service --->
		<cfif NOT structKeyExists(VARIABLES,"CFG_PROVISIONING_SERVICE")>
			<cfset var deviceService=createObject("component","fr.consotel.api.mdm.partner.private.ws.management.WSCfgProvisioningService").getWSService(zdmRepository)>
			<cfset VARIABLES["PACKAGE_SERVICE"]=deviceService>
		</cfif>
		<cfreturn VARIABLES["PACKAGE_SERVICE"]>
	</cffunction>
	
	<cffunction access="public" name="getXmlService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSXmlService" hint="Retourne le service Xml">
		<cfset var zdmRepository=getWSRepository()>
		<!--- Instanciation ou Récupération du service --->
		<cfif NOT structKeyExists(VARIABLES,"XML_SERVICE")>
			<cfset var deviceService=createObject("component","fr.consotel.api.mdm.partner.private.ws.management.WSXmlService").getWSService(zdmRepository)>
			<cfset VARIABLES["XML_SERVICE"]=deviceService>
		</cfif>
		<cfreturn VARIABLES["XML_SERVICE"]>
	</cffunction>
	
	<cffunction access="public" name="getTunnelService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSTunnelService" hint="Retourne le service Tunnel">
		<cfset var zdmRepository=getWSRepository()>
		<!--- Instanciation ou Récupération du service --->
		<cfif NOT structKeyExists(VARIABLES,"TUNNEL_SERVICE")>
			<cfset var deviceService=createObject("component","fr.consotel.api.mdm.partner.private.ws.management.WSTunnelService").getWSService(zdmRepository)>
			<cfset VARIABLES["TUNNEL_SERVICE"]=deviceService>
		</cfif>
		<cfreturn VARIABLES["TUNNEL_SERVICE"]>
	</cffunction>
	
	<cffunction access="public" name="getWhitelistService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSWhitelistService" hint="Retourne le service Whitelist">
		<cfset var zdmRepository=getWSRepository()>
		<!--- Instanciation ou Récupération du service --->
		<cfif NOT structKeyExists(VARIABLES,"WHITELIST_SERVICE")>
			<cfset var deviceService=createObject("component","fr.consotel.api.mdm.partner.private.ws.management.WSWhitelistService").getWSService(zdmRepository)>
			<cfset VARIABLES["WHITELIST_SERVICE"]=deviceService>
		</cfif>
		<cfreturn VARIABLES["WHITELIST_SERVICE"]>
	</cffunction>
	
	<cffunction access="public" name="getBlacklistService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSBlacklistService" hint="Retourne le service Blacklist">
		<cfset var zdmRepository=getWSRepository()>
		<!--- Instanciation ou Récupération du service --->
		<cfif NOT structKeyExists(VARIABLES,"BLACKLIST_SERVICE")>
			<cfset var deviceService=createObject("component","fr.consotel.api.mdm.partner.private.ws.management.WSBlacklistService").getWSService(zdmRepository)>
			<cfset VARIABLES["BLACKLIST_SERVICE"]=deviceService>
		</cfif>
		<cfreturn VARIABLES["BLACKLIST_SERVICE"]>
	</cffunction>
	
	<cffunction access="public" name="getRegistryService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSRegistryService" hint="Retourne le service Registry">
		<cfset var zdmRepository=getWSRepository()>
		<!--- Instanciation ou Récupération du service --->
		<cfif NOT structKeyExists(VARIABLES,"REGISTRY_SERVICE")>
			<cfset var deviceService=createObject("component","fr.consotel.api.mdm.partner.private.ws.management.WSRegistryService").getWSService(zdmRepository)>
			<cfset VARIABLES["REGISTRY_SERVICE"]=deviceService>
		</cfif>
		<cfreturn VARIABLES["REGISTRY_SERVICE"]>
	</cffunction>
	
	<cffunction access="public" name="getServerGroupService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSServerGroupService" hint="Retourne le service ServerGroup">
		<cfset var zdmRepository=getWSRepository()>
		<!--- Instanciation ou Récupération du service --->
		<cfif NOT structKeyExists(VARIABLES,"SERVER_GROUP_SERVICE")>
			<cfset var deviceService=createObject("component","fr.consotel.api.mdm.partner.private.ws.management.WSServerGroupService").getWSService(zdmRepository)>
			<cfset VARIABLES["SERVER_GROUP_SERVICE"]=deviceService>
		</cfif>
		<cfreturn VARIABLES["SERVER_GROUP_SERVICE"]>
	</cffunction>
	
	<cffunction access="public" name="getUserService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSUserService" hint="Retourne le service UserGroup">
		<cfset var zdmRepository=getWSRepository()>
		<!--- Instanciation ou Récupération du service --->
		<cfif NOT structKeyExists(VARIABLES,"USER_GROUP_SERVICE")>
			<cfset var deviceService=createObject("component","fr.consotel.api.mdm.partner.private.ws.management.WSUserService").getWSService(zdmRepository)>
			<cfset VARIABLES["USER_GROUP_SERVICE"]=deviceService>
		</cfif>
		<cfreturn VARIABLES["USER_GROUP_SERVICE"]>
	</cffunction>
	
	<cffunction access="private" name="getWSRepository" returntype="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" hint="Implémentation d'accès aux Web Services">
		<cfif structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfreturn VARIABLES["WS_REPOSITORY"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'entité MDM Service Repository n'est pas définie">
		</cfif>
	</cffunction>
</cfcomponent>