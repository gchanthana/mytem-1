<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.security.WSMultiLicenseService"
implements="fr.consotel.api.mdm.partner.private.ws.IWSMultiLicenseService" hint="Implémentation fournissant les méthodes : MultiLicence Services
Dans la version actuelle de cette classe : Les méthodes retournent des implémentations CFMX correspondant aux types utilisés par le service FSecure
Les méthodes de cette implémentation sont soumises à la vérification de l'authentification">
	<!--- fr.consotel.api.mdm.partner.private.ws.security.WSMultiLicenseService --->
	<cffunction access="package" name="getWSService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSMultiLicenseService" hint="Retourne une instance de cette classe">
		<cfargument name="serviceRepository" type="fr.consotel.api.mdm.partner.private.ws.security.FSecure" required="true" hint="Implémentation d'accès aux Web Services">
		<cfif NOT structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfset VARIABLES["WS_REPOSITORY"]=ARGUMENTS["serviceRepository"]>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="getWSRepository" returntype="fr.consotel.api.mdm.partner.private.ws.security.FSecure" hint="Implémentation d'accès aux Web Services">
		<cfif structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfreturn VARIABLES["WS_REPOSITORY"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'entité MDM Service Repository n'est pas définie">
		</cfif>
	</cffunction>
	
	<!--- MultiLicence Services --->
	<cffunction access="public" name="deleteMultiLicense" returntype="String" hint="Supprime une licence multiple. Retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<!--- Validation des credentials d'authentification --->
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<!--- Implémentation du service FSecure correspondant --->
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn yesNoFormat(toString(serviceStub.deleteMultiLicense(ARGUMENTS["multiLicenseKey"])))>
	</cffunction>
	
	<cffunction access="public" name="searchMultiLicensesInCorporation" returntype="Any" hint="Recherche les licences multiples d'une corporation et retourne 40 résultats par page de résultat">
		<cfargument name="corporationId" type="String" required="true" hint="Identifiant de la corporation">
		<cfargument name="pageNumber" type="String" required="true" hint="Numéro de la page de résultat à retourner">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn serviceStub.searchMultiLicensesInCorporation(ARGUMENTS["corporationId"],ARGUMENTS["pageNumber"])>
	</cffunction>

	<cffunction access="public" name="createMultiLicense" returntype="String" hint="Crée une licence multiple dans la corporation par défaut et retourne la clé correspondante">
		<cfargument name="expirationDate" type="String" required="true" hint="Date d'expiration de la licence au format YYYY-MM-DD. Format optionnel du time zone : EET (GMT+2, GMT+3)">
		<cfargument name="maxNumberOfLicenses" type="String" required="true" hint="Nombre max de single licence qui peuvent être activée dans la licence multiple à créer">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.createMultiLicense(ARGUMENTS["expirationDate"],ARGUMENTS["maxNumberOfLicenses"]))>
	</cffunction>
	
	<cffunction access="public" name="createMultiLicenseInCorporation" returntype="String" hint="Crée une licence multiple dans une corporation et retourne la clé correspondante">
		<cfargument name="corporationId" type="String" required="true" hint="Identifiant de la corporation">
		<cfargument name="expirationDate" type="String" required="true" hint="Date d'expiration de la licence au format YYYY-MM-DD. Format optionnel du time zone : EET (GMT+2, GMT+3)">
		<cfargument name="maxNumberOfLicenses" type="String" required="true" hint="Nombre max de single licence qui peuvent être activées dans la licence multiple">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.createMultiLicenseInCorporation(ARGUMENTS["corporationId"],ARGUMENTS["expirationDate"],ARGUMENTS["maxNumberOfLicenses"]))>
	</cffunction>

	<cffunction access="public" name="updateMultiLicenseExpirationDate" returntype="String"
	hint="Met à jour la date d'expiration d'une licence multiple et retourne TRUE si en cas de succès ou FALSE sinon">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfargument name="expirationDate" type="String" required="true" hint="Date d'expiration de la licence au format YYYY-MM-DD. Format optionnel du time zone : EET (GMT+2, GMT+3)">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn yesNoFormat(toString(serviceStub.updateMultiLicenseExpirationDate(ARGUMENTS["multiLicenseKey"],ARGUMENTS["expirationDate"])))>
	</cffunction>

	<cffunction access="public" name="updateMultiLicenseMaximumNumberOfLicenses" returntype="String"
	hint="Met à jour le nombre max de licence d'une licence multiple et retourne TRUE si en cas de succès ou FALSE sinon">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfargument name="maxNumberOfLicenses" type="String" required="true" hint="Nombre max de single licence qui peuvent être activées dans la licence multiple">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn yesNoFormat(toString(serviceStub.updateMultiLicenseMaximumNumberOfLicenses(ARGUMENTS["multiLicenseKey"],ARGUMENTS["maxNumberOfLicenses"])))>
	</cffunction>
	
	<cffunction access="public" name="updateMultiLicenseStatus" returntype="String" hint="Met à jour le statut d'une licence multiple et retourne TRUE si en cas de succès ou FALSE sinon">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfargument name="multLicenseStatus" type="String" required="true" hint="Statut de la licence. Valeurs possibles : Activate, Inactivate">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn yesNoFormat(toString(serviceStub.updateMultiLicenseStatus(ARGUMENTS["multiLicenseKey"],ARGUMENTS["multLicenseStatus"])))>
	</cffunction>

	<cffunction access="public" name="getMultiLicenseExpirationDate" returntype="String"
	hint="Retourne la date d'expiration d'un licence multiple au format YYYY-MM-DD. Format optionnel du time zone : EET (GMT+2, GMT+3)">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.getMultiLicenseExpirationDate(ARGUMENTS["multiLicenseKey"]))>
	</cffunction>

	<cffunction access="public" name="getMultiLicenseStatus" returntype="String" hint="Retourne le statut d'une licence multiple">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.getMultiLicenseStatus(ARGUMENTS["multiLicenseKey"]))>
	</cffunction>
	
	<cffunction access="public" name="updateMultiLicensePhoneNumberRegistration" returntype="String"
	hint="Met à jour le mode d'enregistrement d'une licence multiple. Retourne TRUE si en cas de succès ou FALSE sinon">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfargument name="phoneNumberRegistration" type="String" required="true" hint="Mode d'enregistrement<br>
		Valeurs possibles : 0 (Enregistrement non envoyé), 1 (Demande confirmation utilisateur avant l'envoi), 2 (Envoi l'enregistrement sans confirmation utilisateur)">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn yesNoFormat(toString(serviceStub.updateMultiLicensePhoneNumberRegistration(ARGUMENTS["multiLicenseKey"],ARGUMENTS["phoneNumberRegistration"])))>
	</cffunction>
	
	<cffunction access="public" name="getMultiLicenseProperties" returntype="Any" hint="Retourne les informations contractuelles d'une licence multiple">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn serviceStub.getMultiLicenseProperties(ARGUMENTS["multiLicenseKey"])>
	</cffunction>

	<cffunction access="public" name="getMultiLicenseCount" returntype="String" hint="Retourne le nombre de licence multiples dans la corporation par défaut">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.getMultiLicenseCount())>
	</cffunction>
	
	<cffunction access="public" name="getMultiLicenseCountInCorporation" returntype="String" hint="Retourne le nombre de licence multiples d'une corporation">
		<cfargument name="corporationId" type="String" required="true" hint="Identifiant de la corporation">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.getMultiLicenseCountInCorporation(ARGUMENTS["corporationId"]))>
	</cffunction>
	
	<cffunction access="public" name="searchMultiLicenses" returntype="Any" hint="Retourne le résultat d'une recherche de licence multiple dans la corporation par défaut">
		<cfargument name="pageNumber" type="String" required="true" hint="Numéro de la page de résultat à retourner">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn serviceStub.searchMultiLicenses(ARGUMENTS["pageNumber"])>
	</cffunction>
	
	<cffunction access="public" name="getMultiLicenseReport" returntype="Any" hint="Retourne les statistiques d'une licence multiple">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn serviceStub.getMultiLicenseReport(ARGUMENTS["multiLicenseKey"])>
	</cffunction>
	
	<cffunction access="public" name="setDefaultClientActivationState" returntype="String"
	hint="Définit l'état d'activation par défaut du client pour une licence multiple. Retourne TRUE si en cas de succès ou FALSE sinon">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfargument name="state" type="String" required="true" hint="Etat d'activation par défaut. Valeur possibles : 1 (VALID), 2 (TRIAL), 3 (SUBSCRIBED)">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn yesNoFormat(toString(serviceStub.setDefaultClientActivationState(ARGUMENTS["multiLicenseKey"],ARGUMENTS["state"])))>
	</cffunction>
	
	<cffunction access="public" name="getDefaultMultiLicense" returntype="Any" hint="Retourne les infos de la licence multiple par défaut du partenaire">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn serviceStub.getDefaultMultiLicense()>
	</cffunction>
</cfcomponent>