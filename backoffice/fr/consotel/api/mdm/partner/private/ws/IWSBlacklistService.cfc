<cfinterface author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.IWSBlacklistService" hint="Interface décrivant les méthodes du Service Blacklist Zenprise">
	<cffunction access="public" name="getAllApplicationsBlackList" returntype="Array" hint="Retourne un tableau contenant la liste des noms des applications en blacklist définis sur le serveur d'un OS">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
	</cffunction>
	
	<cffunction access="public" name="getApplicationsInBlacklist" returntype="Array" hint="Retourne un tableau contenant la liste des applications d'une blacklist d'un OS<br>
	Chaque élément du tableau contient les propriétés suivantes : identifier, name">
		<cfargument name="applicationsBlacklistName" type="String" required="true" hint="Nom d'une blacklist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
	</cffunction>
	
	<cffunction access="public" name="getDefaultBlacklist" returntype="Array" hint="Retourne un tableau contenant la liste des application de la blacklist par défaut d'un OS<br>
	Chaque élément du tableau contient les propriétés suivantes : identifier, name">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
	</cffunction>
	
	<cffunction access="public" name="addBlackList" returntype="Void" hint="Ajoute une blacklist d'un OS">
		<cfargument name="name" type="String" required="true" hint="Nom pour la blacklist">
		<cfargument name="description" type="String" required="true" hint="Description à associer à la blacklist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applications" type="Array" required="true" hint="Applications de la blacklist. Chaque élément du tableau est une structure contenant les clé suivantes : identifier, name">
	</cffunction>
	
	<cffunction access="public" name="updateBlackList" returntype="Void" hint="Met à jour une blacklist d'un OS">
		<cfargument name="name" type="String" required="true" hint="Nom pour la blacklist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="newName" type="String" required="true" hint="Nouveau nom pour la blacklist">
		<cfargument name="newDescription" type="String" required="true" hint="Nouvelle description à associer à la blacklist">
		<cfargument name="applications" type="Array" required="true" hint="Applications de la blacklist. Chaque élément du tableau est une structure contenant les clé suivantes : identifier, name">
	</cffunction>
	
	<cffunction access="public" name="updateDefaultBlackList" returntype="Void" hint="Met à jour la blacklist par défaut d'un OS">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applications" type="Array" required="true" hint="Applications de la blacklist. Chaque élément du tableau est une structure contenant les clé suivantes : identifier, name">
	</cffunction>
	
	<cffunction access="public" name="removeBlackList" returntype="Void" hint="Supprime une blacklist d'un OS">
		<cfargument name="name" type="String" required="true" hint="Nom pour la blacklist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
	</cffunction>
	
	<cffunction access="public" name="addApplicationToBlackList" returntype="Void" hint="Ajoute une application dans une blacklist d'un OS">
		<cfargument name="name" type="String" required="true" hint="Nom pour la blacklist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationName" type="String" required="true" hint="Valeur de la propriété name pour l'application">
		<cfargument name="applicationIdentifier" type="String" required="true" hint="Valeur de la propriété identifier pour l'application">
	</cffunction>
	
	<cffunction access="public" name="addApplicationToDefaultBlackList" returntype="Void" hint="Ajoute une application dans la blacklist par défaut de l'OS">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationName" type="String" required="true" hint="Valeur de la propriété name pour l'application">
		<cfargument name="applicationIdentifier" type="String" required="true" hint="Valeur de la propriété identifier pour l'application">
	</cffunction>
	
	<cffunction access="public" name="updateApplicationInBlackList" returntype="Void" hint="Met à jour une application dans une blacklist d'un OS">
		<cfargument name="name" type="String" required="true" hint="Nom pour la blacklist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationOldName" type="String" required="true" hint="Valeur actuelle de la propriété name de l'application">
		<cfargument name="applicationNewName" type="String" required="true" hint="Nouvelle valeur pour de la propriété name de l'application">
		<cfargument name="applicationNewIdentifier" type="String" required="true" hint="Nouvelle valeur pour de la propriété identifier de l'application">
	</cffunction>
	
	<cffunction access="public" name="updateApplicationInDefaultBlackList" returntype="Void" hint="Met à jour une application dans la blacklist par défaut d'un OS">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationOldName" type="String" required="true" hint="Valeur actuelle de la propriété name de l'application">
		<cfargument name="applicationNewName" type="String" required="true" hint="Nouvelle valeur pour de la propriété name de l'application">
		<cfargument name="applicationNewIdentifier" type="String" required="true" hint="Nouvelle valeur pour de la propriété identifier de l'application">
	</cffunction>
	
	<cffunction access="public" name="removeApplicationFromBlackList" returntype="Void" hint="Supprime une application d'une blacklist d'un OS">
		<cfargument name="name" type="String" required="true" hint="Nom pour la blacklist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationName" type="String" required="true" hint="Valeur de la propriété name de l'application">
	</cffunction>
	
	<cffunction access="public" name="removeApplicationFromDefaultBlackList" returntype="Void" hint="Supprime une application de la blacklist par défaut d'un OS">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationName" type="String" required="true" hint="Valeur de la propriété name de l'application">
	</cffunction>
</cfinterface>