<cfinterface author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.IManagementService"
hint="Interface décrivant les méthodes du fournisseur des services MDM de gestion des équipements mobiles">
	<cffunction access="public" name="getDeviceService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSDeviceService" hint="Retourne le service de gestion des équipements mobiles">
	</cffunction>
	
	<cffunction access="public" name="getPackageService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSPackageService" hint="Retourne le service de gestion des packages">
	</cffunction>
	
	<cffunction access="public" name="getProvisioningService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSProvisioningService" hint="Retourne le service de gestion du Provisioning">
	</cffunction>
	
	<cffunction access="public" name="getCfgProvisioningService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSCfgProvisioningService" hint="Retourne le service de gestion du Provisioning CFG">
	</cffunction>
	
	<cffunction access="public" name="getXmlService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSXmlService" hint="Retourne le service Xml">
	</cffunction>
	
	<cffunction access="public" name="getTunnelService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSTunnelService" hint="Retourne le service Tunnel">
	</cffunction>
	
	<cffunction access="public" name="getWhitelistService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSWhitelistService" hint="Retourne le service Whitelist">
	</cffunction>
	
	<cffunction access="public" name="getBlacklistService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSBlacklistService" hint="Retourne le service Blacklist">
	</cffunction>
	
	<cffunction access="public" name="getRegistryService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSRegistryService" hint="Retourne le service Registry">
	</cffunction>
	
	<cffunction access="public" name="getServerGroupService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSServerGroupService" hint="Retourne le service ServerGroup">
	</cffunction>
	
	<cffunction access="public" name="getUserService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSUserService" hint="Retourne le service UserGroup">
	</cffunction>
</cfinterface>