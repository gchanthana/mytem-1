<cfinterface author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.IWSXmlService" hint="Interface décrivant les méthodes du Service XML Zenprise">
	<cffunction access="public" name="addXMLFile" returntype="Void" hint="Ajoute un fichier XML">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="newFile" type="String" required="true" hint="Nom à donner au fichier">
		<cfargument name="fileText" type="String" required="true" hint="Contenu du fichier">
		<cfargument name="comment" type="String" required="true" hint="Commentaires à associer au fichier">
	</cffunction>
	
	<cffunction access="public" name="updateXMLFile" returntype="Void" hint="Met à jour un fichier XML">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileName" type="String" required="true" hint="Nom actuel du fichier">
		<cfargument name="newFile" type="String" required="true" hint="Nom à donner au fichier ou une chaine vide pour garder le nom fileName">
		<cfargument name="fileText" type="String" required="true" hint="Contenu du fichier">
		<cfargument name="comment" type="String" required="true" hint="Commentaires à associer au fichier">
	</cffunction>
	
	<cffunction access="public" name="removeXMLFile" returntype="Void" hint="Supprime un fichier XML">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier à supprimer">
	</cffunction>
	
	<cffunction access="public" name="getXMLFileInfo" returntype="Array"
	hint="Retourne les infos du fichier dans un tableau contenant les éléments suivants dans l'ordre : Nom, Contenu, Commentaires, Date de création ou d'upload, Date de modification">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier à supprimer">
	</cffunction>
	
	<cffunction access="public" name="getXMLFileList" returntype="Array" hint="Retourne la liste des fichier XML dans un tableau">
	</cffunction>
	
	<cffunction access="public" name="validateXML" returntype="Boolean" hint="Retourne TRUE si le contenu XML est lève une exception sinon">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileText" type="String" required="true" hint="Contenu à valider">
	</cffunction>
</cfinterface>