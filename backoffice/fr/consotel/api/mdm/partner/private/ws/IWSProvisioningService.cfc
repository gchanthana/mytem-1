<cfinterface author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.IWSProvisioningService" hint="Interface décrivant les méthodes du Service de gestion du Provisioning">
	<cffunction access="public" name="getType" returntype="String" hint="Retourne le type de provisioning correspondant au fichier : CAB, APK, SCRIPT, FILE (Fichier générique)">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
	</cffunction>
	
	<cffunction access="public" name="getSize" returntype="Numeric" hint="Retourne la taiile du fichier en bytes">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
	</cffunction>
	
	<cffunction access="public" name="delete" returntype="Void" hint="Supprime un provisioning">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
	</cffunction>
	
	<cffunction access="public" name="rename" returntype="Void" hint="Renomme un provisioning">
		<cfargument name="oldFilename" type="String" required="true" hint="Nom actuel du fichier incluant l'extension et sans le chemin">
		<cfargument name="newFilename" type="String" required="true" hint="Nouveau nom à donner au fichier incluant l'extension et sans le chemin">
	</cffunction>
	
	<cffunction access="public" name="getPackageInfo" returntype="String" hint="Retourne les infos de provisioning d'un package (APK uniquement)">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
	</cffunction>
	
	<cffunction access="public" name="getContent" returntype="Array" hint="Retourne le contenu d'un fichier de type générique ou CAB,APK,MSCR en tant que tableau de bytes<br>
	Un tableau vide est retourné si la taille de provisioning est supérieure à la taille autorisée par défaut (64 KB). Dans ce cas il faut utiliser : getContentPart()">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
	</cffunction>
	
	<cffunction access="public" name="getContentPart" returntype="Array" hint="Retourne une partie du contenu d'un fichier. A utiliser lorsque getContent() retourne un tableau vide">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfargument name="offset" type="Numeric" required="true" hint="OFFSET de début">
		<cfargument name="length" type="Numeric" required="true" hint="Nombre de bytes à récupérer">
	</cffunction>
	
	<cffunction access="public" name="getComment" returntype="String" hint="Retourne le commentaire associé au fichier">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
	</cffunction>
	
	<cffunction access="public" name="setComment" returntype="Void" hint="Associe le commentaire au fichier">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfargument name="comment" type="String" required="true" hint="Commentaire à associer au fichier">
	</cffunction>
	
	<cffunction access="public" name="getVersion" returntype="String" hint="Retourne la version du provisioning du fichier (APK uniquement)">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
	</cffunction>
	
	<cffunction access="public" name="setContent" returntype="String" hint="Change le contenu du provisioning (Avec la possibilité de le renommer). Retourne un token pour l'envoi du provisioning">
		<cfargument name="oldFilename" type="String" required="true" hint="Nom actuel du fichier incluant l'extension et sans le chemin">
		<cfargument name="newFilename" type="String" required="true" hint="Nouveau nom à donner au fichier incluant l'extension et sans le chemin ou une chaine vide pour garder oldName">
		<cfargument name="content" type="Array" required="true" hint="Contenu du fichier en tant que tableau de bytes">
		<cfargument name="crc32" type="Numeric" required="true" hint="Valeur à utiliser pour la vérification CRC32 ou 0 pour ignorer la vérification">
		<cfargument name="fullSend" type="Boolean" required="true" hint="TRUE pour effectuer un transfert en une seule passe et FALSE sinon (i.e : Non partiel)">
	</cffunction>
	
	<cffunction access="public" name="getProvisioning" returntype="String" hint="Retourne la définition du provisioning au format JSON<br>
	Le contenu de la définition est décrit comme suit par la documentation Zenprise :<br>
	 <pre>Common information
        id (int): provisioning internal id
        type (string): provisioning type
            &quot;CAB&quot; for windows cabinet provisioning
            &quot;APK&quot; for android application provisioning
            &quot;SCRIPT&quot; for script provisioning
            &quot;FILE&quot; for generic file provisioning 
        filename (string): provisioning filename
        size (int): provisioning size
        lastUploadDate (date): last content upload date
        lastModificationDate (date): last definition modification date
        destinationPath (string): provisioning destination path (translated to human readable string)
        internalDestinationPath (string): provisioning destination path raw value
        ifExistsOption (int): Action to do when file already exists :
            1 (proposed constant name IFEXISTS_OPTION_COPY_IF_DIFFERENT) for : Copy file if files are different
            2 (proposed constant name IFEXISTS_OPTION_DO_NOT_COPY) for : Do not copy 
        comment (string): provisioning comment (generated for CAB and APK)
        crc32 (long): content crc-32 value (only lowest 32 bits are important)
    Windows cabinets (CAB) specific information
        fullApplicationName (string): CAB full application name
        editor (string): CAB editor name
        installType (int): Action to do for file installation :
            0 (proposed constant name INSTALL_TYPE_DEFAULT) for : Use default device configuration
            1 (proposed constant name INSTALL_TYPE_SILENT) for : Silent install (without user interface) 
        executeAuto (boolean): auto-execute CAB (install) after transfer
        delAfterInstall (boolean): delete CAB file after installation
        canUninstall (boolean): indicate if the CAB can be uninstalled by user
    Android applications (APK) specific information
        fullApplicationName (string): APK full application name
        editor (string): APK editor name
        version (string): APK version value
        revision (string): APK revision value
        packageInfo (string): APK package information
        executeAuto (boolean): auto-execute APK (install) after transfer
        delAfterInstall (boolean): delete APK file after installation
    Scripts specific information
        executeAuto (boolean): auto-execute script after transfer
        macroReplacement (boolean): Check this option to replace in the file a given token name by a device or user properties. Please refer to user documentation for expected token format.
    Generic files specific information
        readOnly (boolean): File must be a set as read only file (work on Windows platform)
        hidden (boolean): File must be a set as hidden file (work on Windows platform)
        macroReplacement (boolean): Check this option to replace in the file a given token name by a device or user properties. Please refer to user documentation for expected token format.</pre>">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
	</cffunction>
	
	<cffunction access="public" name="getEditor" returntype="String" hint="Retourne l'éditeur de provisioning correspondant (CAB uniquement)">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
	</cffunction>
	
	<cffunction access="public" name="getProvisionings" returntype="String" hint="Retourne la liste des provisioning au format JSON">
	</cffunction>
	
	<cffunction access="public" name="getDestinationPath" returntype="String" hint="Retourne le chemin de destination du fichier avec la translation utilisateur">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
	</cffunction>
	
	<cffunction access="public" name="setDestinationPath" returntype="Void" hint="Définit le chemin de destination du fichier avec la translation utilisateur">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfargument name="destinationPath" type="String" required="true" hint="Chemin de la destination avec le séparateur backslash">
	</cffunction>
	
	<cffunction access="public" name="getRevision" returntype="String" hint="Retourne la revision du provisioning du fichier (APK uniquement)">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
	</cffunction>
	
	<cffunction access="public" name="getIfExistsOption" returntype="Numeric" hint="Retourne le code de l'action effectuée lorsque le fichier existe déjà sur l'équipement<br>
	Les valeurs possibles sont : 1 (Copie le fichier si un fichier existe déjà avec un contenu différent), 2 (N'effectue pas de copie du fichier)">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
	</cffunction>
	
	<cffunction access="public" name="setIfExistsOption" returntype="Void" hint="Définit le code de l'action effectuée (ifExistsOption) lorsque le fichier existe déjà sur l'équipement">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfargument name="ifExistsOption" type="Numeric" required="true" hint="1 (Copie le fichier si un fichier existe déjà avec un contenu différent), 2 (N'effectue pas de copie du fichier)">
	</cffunction>
	
	<cffunction access="public" name="getInstallType" returntype="Numeric" hint="Retourne le type d'installation correspondant (CAB uniquement)<br>
	Les valeurs possibles sont : 0 (Installe avec la configuration par défaut), 1 (Installation silencieuse)">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
	</cffunction>
	
	<cffunction access="public" name="setInstallType" returntype="Void" hint="Spécifie le type d'installation pour le fichier (CAB uniquement)">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfargument name="installType" type="Numeric" required="true" hint="0 (Installe avec la configuration par défaut), 1 (Installation silencieuse). Implicitement à 0 si executeAuto est à FALSE">
	</cffunction>
	
	<cffunction access="public" name="getFullApplicationName" returntype="String" hint="Retourne le nom complet de l'application correspondante">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin (Uniquement : CAB, APK, IPA)">
	</cffunction>
	
	<cffunction access="public" name="addOrUpdateFile" returntype="Void" hint="Ajoute ou met à jour un fichier générique (Aucun des types : CAB, MSCR, APK)">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier (Sans le chemin)">
		<cfargument name="destinationPath" type="String" required="true" hint="Chemin de la destination avec le séparateur backslash">
		<cfargument name="macroReplacement" type="Boolean" required="true" hint="TRUE pour remplacer dans le fichier la valeur d'un token">
		<cfargument name="ifExistsOption" type="Numeric" required="true" hint="1 (Copie le fichier si un fichier existe déjà avec un contenu différent), 2 (N'effectue pas de copie du fichier)">
		<cfargument name="readOnly" type="Boolean" required="true" hint="TRUE pour mettre en lecture seule (Seulement sur Windows)">
		<cfargument name="hidden" type="Boolean" required="true" hint="TRUE pour masquer le fichier (Seulement sur Windows)">
		<cfargument name="comment" type="String" required="true" hint="Commentaire à associer au fichier">
		<cfargument name="content" type="Array" required="true" hint="Contenu du fichier en tant que tableau de bytes">
		<cfargument name="crc32" type="Numeric" required="true" hint="Si la valeur est différente de 0 alors une vérification CRC32 sera effectuée par rapport à celle fournie">
	</cffunction>
	
	<cffunction access="public" name="addOrUpdateAPK" returntype="Void" hint="Ajoute ou met à jour un package Android">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier (Incluant l'extension .apk et sans le chemin)">
		<cfargument name="destinationPath" type="String" required="true" hint="Chemin de la destination avec le séparateur backslash">
		<cfargument name="ifExistsOption" type="Numeric" required="true" hint="1 (Copie le fichier si un fichier existe déjà avec un contenu différent), 2 (N'effectue pas de copie du fichier)">
		<cfargument name="executeAuto" type="Boolean" required="true" hint="TRUE pour exécuter automatiquement à la fin du transfert du fichier">
		<cfargument name="delAfterInstall" type="Boolean" required="true" hint="TRUE pour supprimer le fichier après installation (Implicitement à FALSE si executeAuto est à FALSE)">
		<cfargument name="content" type="Array" required="true" hint="Contenu du fichier en tant que tableau de bytes">
		<cfargument name="crc32" type="Numeric" required="true" hint="Si la valeur est différente de 0 alors une vérification CRC32 sera effectuée par rapport à celle fournie">
	</cffunction>
	
	<cffunction access="public" name="addOrUpdateScript" returntype="Void" hint="Ajoute ou met à jour un fichier de script">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier (Incluant l'extension .mscr et sans le chemin)">
		<cfargument name="destinationPath" type="String" required="true" hint="Chemin de la destination avec le séparateur backslash">
		<cfargument name="macroReplacement" type="Boolean" required="true" hint="TRUE pour remplacer dans le fichier la valeur d'un token">
		<cfargument name="ifExistsOption" type="Numeric" required="true" hint="1 (Copie le fichier si un fichier existe déjà avec un contenu différent), 2 (N'effectue pas de copie du fichier)">
		<cfargument name="executeAuto" type="Boolean" required="true" hint="TRUE pour exécuter automatiquement à la fin du transfert du fichier">
		<cfargument name="comment" type="String" required="true" hint="Commentaire à associer au fichier">
		<cfargument name="content" type="Array" required="true" hint="Contenu du fichier en tant que tableau de bytes">
		<cfargument name="crc32" type="Numeric" required="true" hint="Si la valeur est différente de 0 alors une vérification CRC32 sera effectuée par rapport à celle fournie">
	</cffunction>
	
	<cffunction access="public" name="addOrUpdateCAB" returntype="Void" hint="Ajoute ou met à jour un fichier CAB (Windows)">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier (Incluant l'extension .cab et sans le chemin)">
		<cfargument name="destinationPath" type="String" required="true" hint="Chemin de la destination avec le séparateur backslash">
		<cfargument name="ifExistsOption" type="Numeric" required="true" hint="1 (Copie le fichier si un fichier existe déjà avec un contenu différent), 2 (N'effectue pas de copie du fichier)">
		<cfargument name="executeAuto" type="Boolean" required="true" hint="TRUE pour exécuter automatiquement à la fin du transfert du fichier">
		<cfargument name="installType" type="Numeric" required="true" hint="0 (Installe avec la configuration par défaut), 1 (Installation silencieuse) : Implicitement à 0 si executeAuto est à FALSE">
		<cfargument name="delAfterInstall" type="Boolean" required="true" hint="TRUE pour supprimer le fichier après installation (Implicitement à FALSE si executeAuto est à FALSE)">
		<cfargument name="content" type="Array" required="true" hint="Contenu du fichier en tant que tableau de bytes">
		<cfargument name="crc32" type="Numeric" required="true" hint="Si la valeur est différente de 0 alors une vérification CRC32 sera effectuée par rapport à celle fournie">
	</cffunction>
	
	<cffunction access="public" name="getMaximalTransferSize" returntype="Numeric" hint="Retourne la taille max utilisée pour les méthodes de transferts">
	</cffunction>
	
	<cffunction access="public" name="getFilenames" returntype="Array" hint="Retourne un tableau contenant la liste de tous les fichiers">
	</cffunction>
	
	<cffunction access="public" name="getCABFilenames" returntype="Array" hint="Retourne un tableau contenant la liste des fichiers CAB">
	</cffunction>
	
	<cffunction access="public" name="getAPKFilenames" returntype="Array" hint="Retourne un tableau contenant la liste des fichiers APK">
	</cffunction>
	
	<cffunction access="public" name="getScriptFilenames" returntype="Array" hint="Retourne un tableau contenant la liste des fichiers MSCR">
	</cffunction>
	
	<cffunction access="public" name="getGenericFilenames" returntype="Array" hint="Retourne un tableau contenant la liste des fichiers génériques">
	</cffunction>
	
	<cffunction access="public" name="hasCanUninstallFlag" returntype="Boolean" hint="Retourne TRUE si le fichier peut être désinstallé (CAB uniquement)">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
	</cffunction>
	
	<cffunction access="public" name="getCRC32" returntype="Numeric" hint="Retourne le résultat du test CRC32 sur le fichier">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
	</cffunction>
	
	<cffunction access="public" name="getLastModificationDate" returntype="Date" hint="Retourne la date de dernière modification du fichier">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
	</cffunction>
	
	<cffunction access="public" name="getLastUploadDate" returntype="Date" hint="Retourne la date de dernier upload du fichier">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
	</cffunction>
	
	<cffunction access="public" name="hasExecuteAutoFlag" returntype="Boolean" hint="Retourne le FLAG d'exécution automatique">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
	</cffunction>
	
	<cffunction access="public" name="setExecuteAutoFlag" returntype="Void" hint="Définit le FLAG d'exécution automatique">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfargument name="executeAuto" type="Boolean" required="true" hint="FLAG d'exécution automatique. TRUE pour une exécution auto et FALSE sinon">
	</cffunction>
	
	<cffunction access="public" name="hasDelAfterInstallFlag" returntype="Boolean" hint="Retourne TRUE si le fichier est supprimé après installation">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin (Uniquement CAB et APK)">
	</cffunction>
	
	<cffunction access="public" name="setDelAfterInstallFlag" returntype="Void" hint="Spécifie si le fichier est supprimé après installation">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin (Uniquement CAB et APK)">
		<cfargument name="delAfterInstall" type="Boolean" required="true" hint="TRUE pour supprimer le fichier après installation (Implicitement à FALSE lorsque executeAuto est à FALSE)">
	</cffunction>
	
	<cffunction access="public" name="getInternalDestinationPath" returntype="String" hint="Retourne le chemin de destination sans la translation utilisateur">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
	</cffunction>
	
	<cffunction access="public" name="hasMacroReplacementFlag" returntype="Boolean" hint="Retourne la valeur du FLAG de de remplacement de la valeur d'un token">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin (Uniquement MSCR et fichier générique)">
	</cffunction>
	
	<cffunction access="public" name="setMacroReplacementFlag" returntype="Void" hint="Définit la valeur du FLAG de de remplacement de la valeur d'un token">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin (Uniquement MSCR et fichier générique)">
		<cfargument name="macroReplacement" type="Boolean" required="true" hint="TRUE pour remplacer dans le fichier la valeur d'un token">
	</cffunction>
	
	<cffunction access="public" name="hasHiddenFlag" returntype="Boolean" hint="Retourne TRUE si le fichier est marqué comme masqué">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin (Uniquement fichier générique)">
	</cffunction>
	
	<cffunction access="public" name="setHiddenFlag" returntype="Void" hint="Spécifie si le fichier est à marquer comme masqué ou non">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin (Uniquement fichier générique)">
		<cfargument name="hiddenFlag" type="Boolean" required="true" hint="TRUE pour indiquer que le fichier est masqué et FALSE sinon">
	</cffunction>
	
	<cffunction access="public" name="hasReadOnlyFlag" returntype="Boolean" hint="Retourne TRUE si le fichier est marqué comme en lecture seule">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin (Uniquement fichier générique)">
	</cffunction>
	
	<cffunction access="public" name="setReadOnlyFlag" returntype="Void" hint="Spécifie si le fichier est à marquer comme en lecture seule ou non">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin (Uniquement fichier générique)">
		<cfargument name="readOnly" type="Boolean" required="true" hint="TRUE pour indiquer que le fichier est en lecture seule et FALSE sinon">
	</cffunction>
	
	<cffunction access="public" name="getCABProvisionings" returntype="String" hint="Retourne la liste des provisioning CAB au format JSON">
	</cffunction>
	
	<cffunction access="public" name="getAPKProvisionings" returntype="String" hint="Retourne la liste des provisioning APK au format JSON">
	</cffunction>
	
	<cffunction access="public" name="getScriptProvisionings" returntype="String" hint="Retourne la liste des provisioning MSCR au format JSON">
	</cffunction>
	
	<cffunction access="public" name="getGenericProvisionings" returntype="String" hint="Retourne la liste des provisioning de fichiers génériques au format JSON">
	</cffunction>
	
	<cffunction access="public" name="addOrUpdateProvisioning" returntype="String"
	hint="Crée ou met à jour la définition d'un provisioning. Retourne un token pour l'envoi du provisioning ou une chaine vide si aucun contenu n'a été envoyé ou si fullSend vaut TRUE">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfargument name="jsonProvisioning" type="String" required="true" hint="Définition du provisioning au format JSON ou une chaine vide pour une création<br>
		Le contenu de la définition est décrit comme suit par la documentation Zenprise :<br>
    <pre>Common information
        filename (optional string):
            For update: Rename provisioning entry. See rename(String, String) for renaming constraints.
            For create: One of this value or filename parameter is mandatory. Use this value only if filename parameter is undefined (ie null).
        destinationPath (optional string, default to &quot;\&quot;): provisioning destination path
            Use backslash (&quot;\&quot;) as path separator.
            A path must begin with backslash or percent (&quot;%&quot;) character. A backslash will be added at begin if first character isn&quot;t backslash or percent.
            empty path will be replaced by everywan folder path (&quot;%EveryWAN folder%\&quot;) 
        Special destinations paths can begin with:
            %My Documents%\ for Windows target only,
            %Windows%\ for Windows target only,
            %Program Files%\ for Windows target only,
            %Flash Storage%\ for Windows and Android targets only,
            %EveryWAN folder%\ for all (iPhone only work with this one). 
        Note: Some inserted value will be stored correctly but returned changed.
        For exemple if you insert string &quot;%5%\my_directory\&quot;, you&quot;ll get &quot;%My Documents%\my_directory\&quot;
        if you call getDestinationPath(String) (or use destinationPath in JSON).
        Use getInternalDestinationPath(String) (or use internalDestinationPath in JSON) to retrieve inserted value.
        Console will continue to translate special directory.
        ifExistsOption (optional int, default to 1): Action to do when file already exists :
            1 (proposed constant name IFEXISTS_OPTION_COPY_IF_DIFFERENT) for : Copy file if files are different
            2 (proposed constant name IFEXISTS_OPTION_DO_NOT_COPY) for : Do not copy Windows cabinets (CAB) specific information
        installType (optional int, default to 0): Action to do for file installation :
            0 (proposed constant name INSTALL_TYPE_DEFAULT) for : Use default device configuration
            1 (proposed constant name INSTALL_TYPE_SILENT) for : Silent install (without user interface)
            Note: forced to 0 (INSTALL_TYPE_DEFAULT) without automatique execution 
        executeAuto (optional boolean, default to false): auto-execute CAB (install) after transfer
        delAfterInstall (optional boolean, default to false): delete CAB file after installation. Forced to false without automatic execution
    Android applications (APK) specific information
        executeAuto (optional boolean, default to false): auto-execute APK (install) after transfer
        delAfterInstall (optional boolean, default to false): delete CAB file after installation. Forced to false without automatic execution
    Scripts specific information
        comment (optional string, default to empty string): provisioning comment (generated for CAB and APK)
        executeAuto (optional boolean, defaul to false): auto-execute script after transfer
        macroReplacement (optional boolean, default to false): Check this option to replace in the file a given token name by a device or user properties.
        Please refer to user documentation for expected token format.
    Generic files specific information
        comment (optional string, default to empty string): provisioning comment (generated for CAB and APK)
        readOnly (optional boolean, default to false): File must be a set as read only file (work on Windows platform)
        hidden (optional boolean, default to false): File must be a set as hidden file (work on Windows platform)
        macroReplacement (optional boolean, default to false): Check this option to replace in the file a given token name by a device or user properties.
        Please refer to user documentation for expected token format.</pre>">
		<cfargument name="content" type="Array" required="true" hint="Contenu du provisioning en tant que tableau de bytes.
		Fournir un tableau vide pour une création, Fournir un tableau contenant un chaine vide pour un mise à jour">
		<cfargument name="crc32" type="Numeric" required="true" hint="Valeur à utiliser pour la vérification CRC32 ou 0 pour ignorer la vérification">
		<cfargument name="fullSend" type="Boolean" required="true" hint="TRUE pour effectuer un envoi en une seule passe (i.e : Non partiel)">
	</cffunction>
	
	<cffunction access="public" name="continuePartialProvisioning" returntype="Void" hint="Ajoute le contenu à un provisioning partiellement effectué avec addOrUpdateProvisioning()">
		<cfargument name="token" type="String" required="true" hint="Token retourné par le provisioning partiel effectué avec addOrUpdateProvisioning()">
		<cfargument name="content" type="Array" required="true" hint="Contenu à ajouter au provisioning en tant que tableau de bytes">
		<cfargument name="crc32" type="Numeric" required="true" hint="Valeur CRC32 cumulée entre le contenu déjà provisionné et celui ajoute, utilisée pour la vérification CRC32">
	</cffunction>
	
	<cffunction access="public" name="endPartialProvisioning" returntype="Void" hint="Spécifie la fin d'un provisioning partiel">
		<cfargument name="token" type="String" required="true" hint="Token retourné par le provisioning partiel effectué avec addOrUpdateProvisioning()">
	</cffunction>
	
	<cffunction access="public" name="cancelPartialProvisioning" returntype="Void" hint="Supprime un provisioning partiel de la base">
		<cfargument name="token" type="String" required="true" hint="Token retourné par le provisioning partiel effectué avec addOrUpdateProvisioning()">
	</cffunction>
</cfinterface>