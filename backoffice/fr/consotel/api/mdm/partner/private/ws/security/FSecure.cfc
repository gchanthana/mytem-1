<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.security.FSecure" extends="fr.consotel.api.mdm.partner.private.ws.WSRepository"
hint="Implémentation Factory utilisée par l'API MDM pour instancier et accéder aux Web Services MDM de gestion de la sécurité des équipements mobiles (FSecure)
Les méthodes de cette implémentation sont soumises à la vérification de l'authentification">
	<cffunction access="public" name="getWSRepository" returntype="fr.consotel.api.mdm.partner.private.ws.WSRepository">
		<cfargument name="authCredentials" type="Struct" required="true">
		<!--- Constructeur de la classe parent --->
		<cfset SUPER.getWSRepository(ARGUMENTS["authCredentials"])>
		<!--- Credentials utilisés pour accéder aux Web Services F-Secure --->
		<cfset SUPER.setServiceCredentials("TSMmSMI1","TSMmSMI123")>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="public" name="authLogout" returntype="void" hint="Déconnecte tous les Web Services créés">
		<cfset var wsInstances=getWSInstances()>
		<cfset SUPER.setServiceCredentials("","")>
		<cfloop item="serviceName" collection="#wsInstances#">
			<cfset var serviceInstance=wsInstances[serviceName]>
			<cfset serviceInstance.setUsername("")>
			<cfset serviceInstance.setPassword("")>
		</cfloop>
	</cffunction>

	<cffunction access="package" name="getCorporationService" returntype="Any" hint="Retourne l'implémentation actuelle du service FSecure (MDM.FSECURECfcSoapBindingStub)">
		<cfreturn getWSStub("FSECURE")>
	</cffunction>
	
	<cffunction access="private" name="getWSStub" returntype="Any" hint="Retourne une instance de l'implémentation du service : serviceName">
		<cfargument name="serviceName" type="String" required="true" hint="Nom du service à instancier (e.g : FSECURE)">
		<cfset var serviceKey=ARGUMENTS["serviceName"]>
		<!--- Validation des credentials d'authentification --->
		<cfset SUPER.validateAuth()>
		<!--- Instanciation ou Récupération du service --->
		<cfset var wsInstances=getWSInstances()>
		<cfif serviceKey EQ "FSECURE">
			<cfif NOT structKeyExists(wsInstances,serviceKey)>
				<cfset wsInstances["FSECURE"]=SUPER.getServiceInstance()>
			</cfif>
			<!--- MAJ de la valeur si les credentials ont changés de valeur --->
			<cfset var CREDENTIALS=getServiceCredentials()>
			<cfif compareNoCase(wsInstances[serviceKey].getUsername(),CREDENTIALS["USER"])>
				<cfset wsInstances[serviceKey].setUsername(CREDENTIALS["USER"])>
				<cfset wsInstances[serviceKey].setPassword(CREDENTIALS["PWD"])>
			</cfif>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le service #serviceKey# n'existe pas">
		</cfif>
		<cfreturn wsInstances[serviceKey]>
	</cffunction>

	<cffunction access="private" name="getDefaultServiceEndpoint" returntype="String" hint="Retourne le Endpoint utilisé par défaut (FSECURE)">
		<cfreturn "FSECURE">
	</cffunction>
</cfcomponent>