<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.management.WSProvisioningService"
implements="fr.consotel.api.mdm.partner.private.ws.IWSProvisioningService"
hint="Implémentation fournissant les méthodes du Service EveryWanProvisioning. Les méthodes de cette implémentation sont soumises à la vérification de l'authentification
<br><b>Pour des raisons de performance : Certaines méthodes lèvent une exception provenant du Web Service lorsqu'aucun équipement ne correspond aux infos fournies</b>">
	<cffunction access="public" name="getType" returntype="String" hint="Retourne le type de provisioning correspondant au fichier : CAB, APK, SCRIPT, FILE (Fichier générique)">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn toString(provisioninService.getType(ARGUMENTS["filename"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getSize" returntype="Numeric" hint="Retourne la taiile du fichier en bytes">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn VAL(provisioninService.getSize(ARGUMENTS["filename"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="delete" returntype="Void" hint="Supprime un provisioning">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset provisioninService.delete(ARGUMENTS["filename"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="rename" returntype="Void" hint="Renomme un provisioning">
		<cfargument name="oldFilename" type="String" required="true" hint="Nom actuel du fichier incluant l'extension et sans le chemin">
		<cfargument name="newFilename" type="String" required="true" hint="Nouveau nom à donner au fichier incluant l'extension et sans le chemin">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset provisioninService.rename(ARGUMENTS["filename"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getPackageInfo" returntype="String" hint="Retourne les infos de provisioning d'un package (APK uniquement)">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn toString(provisioninService.getPackageInfo(ARGUMENTS["filename"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getContent" returntype="Array" hint="Retourne le contenu d'un fichier de type générique ou CAB,APK,MSCR en tant que tableau de bytes<br>
	Un tableau vide est retourné si la taille de provisioning est supérieure à la taille autorisée par défaut (64 KB). Dans ce cas il faut utiliser : getContentPart()">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset var byteArray=provisioninService.getContent(ARGUMENTS["filename"])>
			<cfif isDefined("byteArray")>
				<cfreturn byteArray>
			<cfelse>
				<cfreturn []>
			</cfif>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getContentPart" returntype="Array" hint="Retourne une partie du contenu d'un fichier. A utiliser lorsque getContent() retourne un tableau vide">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfargument name="offset" type="Numeric" required="true" hint="OFFSET de début">
		<cfargument name="length" type="Numeric" required="true" hint="Nombre de bytes à récupérer">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset var byteArray=provisioninService.getContent(ARGUMENTS["filename"],ARGUMENTS["offset"],ARGUMENTS["offset"])>
			<cfif isDefined("byteArray")>
				<cfreturn byteArray>
			<cfelse>
				<cfreturn []>
			</cfif>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getComment" returntype="String" hint="Retourne le commentaire associé au fichier">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn toString(provisioninService.getComment(ARGUMENTS["filename"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="setComment" returntype="Void" hint="Associe le commentaire au fichier">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfargument name="comment" type="String" required="true" hint="Commentaire à associer au fichier">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset provisioninService.setComment(ARGUMENTS["filename"],ARGUMENTS["comment"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getVersion" returntype="String" hint="Retourne la version du provisioning du fichier (APK uniquement)">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn toString(provisioninService.getVersion(ARGUMENTS["filename"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="setContent" returntype="String" hint="Change le contenu du provisioning (Avec la possibilité de le renommer). Retourne un token pour l'envoi du provisioning">
		<cfargument name="oldFilename" type="String" required="true" hint="Nom actuel du fichier incluant l'extension et sans le chemin">
		<cfargument name="newFilename" type="String" required="true" hint="Nouveau nom à donner au fichier incluant l'extension et sans le chemin ou une chaine vide pour garder oldName">
		<cfargument name="content" type="Array" required="true" hint="Contenu du fichier en tant que tableau de bytes">
		<cfargument name="crc32" type="Numeric" required="true" hint="Valeur à utiliser pour la vérification CRC32 ou 0 pour ignorer la vérification">
		<cfargument name="fullSend" type="Boolean" required="true" hint="TRUE pour effectuer un transfert en une seule passe et FALSE sinon (i.e : Non partiel)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset provisioninService.setContent(ARGUMENTS["oldFilename"],asJavaNullValue(ARGUMENTS["newFilename"]),ARGUMENTS["content"],ARGUMENTS["offset"],ARGUMENTS["fullSend"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getProvisioning" returntype="String" hint="Retourne la définition du provisioning au format JSON">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn provisioninService.getProvisioning(ARGUMENTS["filename"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getEditor" returntype="String" hint="Retourne l'éditeur de provisioning correspondant (CAB uniquement)">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn toString(provisioninService.getEditor(ARGUMENTS["filename"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getProvisionings" returntype="String" hint="Retourne la liste des provisioning au format JSON">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn provisioninService.getProvisionings()>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getDestinationPath" returntype="String" hint="Retourne le chemin de destination du fichier avec la translation utilisateur">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn toString(provisioninService.getDestinationPath(ARGUMENTS["filename"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="setDestinationPath" returntype="Void" hint="Définit le chemin de destination du fichier avec la translation utilisateur">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfargument name="destinationPath" type="String" required="true" hint="Chemin de la destination avec le séparateur backslash">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset provisioninService.setDestinationPath(ARGUMENTS["filename"],ARGUMENTS["destinationPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getRevision" returntype="String" hint="Retourne la revision du provisioning du fichier (APK uniquement)">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn toString(provisioninService.getRevision(ARGUMENTS["filename"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getIfExistsOption" returntype="Numeric" hint="Retourne le code de l'action effectuée lorsque le fichier existe déjà sur l'équipement<br>
	Les valeurs possibles sont : 1 (Copie le fichier si un fichier existe déjà avec un contenu différent), 2 (N'effectue pas de copie du fichier)">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn VAL(provisioninService.getIfExistsOption(ARGUMENTS["filename"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="setIfExistsOption" returntype="Void" hint="Définit le code de l'action effectuée (ifExistsOption) lorsque le fichier existe déjà sur l'équipement">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfargument name="ifExistsOption" type="Numeric" required="true" hint="1 (Copie le fichier si un fichier existe déjà avec un contenu différent), 2 (N'effectue pas de copie du fichier)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset provisioninService.setIfExistsOption(ARGUMENTS["filename"],ARGUMENTS["ifExistsOption"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getInstallType" returntype="Numeric" hint="Retourne le type d'installation correspondant (CAB uniquement)<br>
	Les valeurs possibles sont : 0 (Installe avec la configuration par défaut), 1 (Installation silencieuse)">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn VAL(provisioninService.getInstallType(ARGUMENTS["filename"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="setInstallType" returntype="Void" hint="Spécifie le type d'installation pour le fichier (CAB uniquement)">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfargument name="installType" type="Numeric" required="true" hint="0 (Installe avec la configuration par défaut), 1 (Installation silencieuse). Implicitement à 0 si executeAuto est à FALSE">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset provisioninService.setInstallType(ARGUMENTS["filename"],ARGUMENTS["installType"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getFullApplicationName" returntype="String" hint="Retourne le nom complet de l'application correspondante">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin (Uniquement : CAB, APK, IPA)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn VAL(provisioninService.getFullApplicationName(ARGUMENTS["filename"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="addOrUpdateFile" returntype="Void" hint="Ajoute ou met à jour un fichier générique (Aucun des types : CAB, MSCR, APK)">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier (Sans le chemin)">
		<cfargument name="destinationPath" type="String" required="true" hint="Chemin de la destination avec le séparateur backslash">
		<cfargument name="macroReplacement" type="Boolean" required="true" hint="TRUE pour remplacer dans le fichier la valeur d'un token">
		<cfargument name="ifExistsOption" type="Numeric" required="true" hint="1 (Copie le fichier si un fichier existe déjà avec un contenu différent), 2 (N'effectue pas de copie du fichier)">
		<cfargument name="readOnly" type="Boolean" required="true" hint="TRUE pour mettre en lecture seule (Seulement sur Windows)">
		<cfargument name="hidden" type="Boolean" required="true" hint="TRUE pour masquer le fichier (Seulement sur Windows)">
		<cfargument name="comment" type="String" required="true" hint="Commentaire à associer au fichier">
		<cfargument name="content" type="Array" required="true" hint="Contenu du fichier en tant que tableau de bytes">
		<cfargument name="crc32" type="Numeric" required="true" hint="Si la valeur est différente de 0 alors une vérification CRC32 sera effectuée par rapport à celle fournie">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset provisioninService.addOrUpdateFile(
				ARGUMENTS["filename"],ARGUMENTS["destinationPath"],ARGUMENTS["macroReplacement"],ARGUMENTS["ifExistsOption"],ARGUMENTS["readOnly"],
				ARGUMENTS["hidden"],ARGUMENTS["comment"],ARGUMENTS["content"],ARGUMENTS["crc32"]
			)>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="addOrUpdateAPK" returntype="Void" hint="Ajoute ou met à jour un package Android">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier (Incluant l'extension .apk et sans le chemin)">
		<cfargument name="destinationPath" type="String" required="true" hint="Chemin de la destination avec le séparateur backslash">
		<cfargument name="ifExistsOption" type="Numeric" required="true" hint="1 (Copie le fichier si un fichier existe déjà avec un contenu différent), 2 (N'effectue pas de copie du fichier)">
		<cfargument name="executeAuto" type="Boolean" required="true" hint="TRUE pour exécuter automatiquement à la fin du transfert du fichier">
		<cfargument name="delAfterInstall" type="Boolean" required="true" hint="TRUE pour supprimer le fichier après installation (Implicitement à FALSE si executeAuto est à FALSE)">
		<cfargument name="content" type="Array" required="true" hint="Contenu du fichier en tant que tableau de bytes">
		<cfargument name="crc32" type="Numeric" required="true" hint="Si la valeur est différente de 0 alors une vérification CRC32 sera effectuée par rapport à celle fournie">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset provisioninService.addOrUpdateAPK(
				ARGUMENTS["filename"],ARGUMENTS["destinationPath"],ARGUMENTS["ifExistsOption"],ARGUMENTS["executeAuto"],
				ARGUMENTS["delAfterInstall"],ARGUMENTS["content"],ARGUMENTS["crc32"]
			)>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="addOrUpdateScript" returntype="Void" hint="Ajoute ou met à jour un fichier de script">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier (Incluant l'extension .mscr et sans le chemin)">
		<cfargument name="destinationPath" type="String" required="true" hint="Chemin de la destination avec le séparateur backslash">
		<cfargument name="macroReplacement" type="Boolean" required="true" hint="TRUE pour remplacer dans le fichier la valeur d'un token">
		<cfargument name="ifExistsOption" type="Numeric" required="true" hint="1 (Copie le fichier si un fichier existe déjà avec un contenu différent), 2 (N'effectue pas de copie du fichier)">
		<cfargument name="executeAuto" type="Boolean" required="true" hint="TRUE pour exécuter automatiquement à la fin du transfert du fichier">
		<cfargument name="comment" type="String" required="true" hint="Commentaire à associer au fichier">
		<cfargument name="content" type="Array" required="true" hint="Contenu du fichier en tant que tableau de bytes">
		<cfargument name="crc32" type="Numeric" required="true" hint="Si la valeur est différente de 0 alors une vérification CRC32 sera effectuée par rapport à celle fournie">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset provisioninService.addOrUpdateScript(
				ARGUMENTS["osFamily"],ARGUMENTS["filename"],ARGUMENTS["destinationPath"],ARGUMENTS["macroReplacement"],ARGUMENTS["ifExistsOption"],
				ARGUMENTS["executeAuto"],ARGUMENTS["comment"],ARGUMENTS["content"],ARGUMENTS["crc32"]
			)>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="addOrUpdateCAB" returntype="Void" hint="Ajoute ou met à jour un fichier CAB (Windows)">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier (Incluant l'extension .cab et sans le chemin)">
		<cfargument name="destinationPath" type="String" required="true" hint="Chemin de la destination avec le séparateur backslash">
		<cfargument name="ifExistsOption" type="Numeric" required="true" hint="1 (Copie le fichier si un fichier existe déjà avec un contenu différent), 2 (N'effectue pas de copie du fichier)">
		<cfargument name="executeAuto" type="Boolean" required="true" hint="TRUE pour exécuter automatiquement à la fin du transfert du fichier">
		<cfargument name="installType" type="Numeric" required="true" hint="0 (Installe avec la configuration par défaut), 1 (Installation silencieuse) : Implicitement à 0 si executeAuto est à FALSE">
		<cfargument name="delAfterInstall" type="Boolean" required="true" hint="TRUE pour supprimer le fichier après installation (Implicitement à FALSE si executeAuto est à FALSE)">
		<cfargument name="content" type="Array" required="true" hint="Contenu du fichier en tant que tableau de bytes">
		<cfargument name="crc32" type="Numeric" required="true" hint="Si la valeur est différente de 0 alors une vérification CRC32 sera effectuée par rapport à celle fournie">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset provisioninService.addOrUpdateCAB(
				ARGUMENTS["filename"],ARGUMENTS["destinationPath"],ARGUMENTS["ifExistsOption"],ARGUMENTS["executeAuto"],
				ARGUMENTS["installType"],ARGUMENTS["delAfterInstall"],ARGUMENTS["content"],ARGUMENTS["crc32"]
			)>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getMaximalTransferSize" returntype="Numeric" hint="Retourne la taille max utilisée pour les méthodes de transferts">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn VAL(provisioninService.getMaximalTransferSize())>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getFilenames" returntype="Array" hint="Retourne un tableau contenant la liste de tous les fichiers">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset var fileNames=provisioninService.getFilenames()>
			<cfif isDefined("fileNames")>
				<cfreturn provisioninService.getFilenames()>
			<cfelse>
				<cfreturn []>
			</cfif>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getCABFilenames" returntype="Array" hint="Retourne un tableau contenant la liste des fichiers CAB">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset var fileNames=provisioninService.getCABFilenames()>
			<cfif isDefined("fileNames")>
				<cfreturn provisioninService.getFilenames()>
			<cfelse>
				<cfreturn []>
			</cfif>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getAPKFilenames" returntype="Array" hint="Retourne un tableau contenant la liste des fichiers APK">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset var fileNames=provisioninService.getAPKFilenames()>
			<cfif isDefined("fileNames")>
				<cfreturn provisioninService.getFilenames()>
			<cfelse>
				<cfreturn []>
			</cfif>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getScriptFilenames" returntype="Array" hint="Retourne un tableau contenant la liste des fichiers MSCR">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset var fileNames=provisioninService.getScriptFilenames()>
			<cfif isDefined("fileNames")>
				<cfreturn provisioninService.getFilenames()>
			<cfelse>
				<cfreturn []>
			</cfif>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getGenericFilenames" returntype="Array" hint="Retourne un tableau contenant la liste des fichiers génériques">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset var fileNames=provisioninService.getGenericFilenames()>
			<cfif isDefined("fileNames")>
				<cfreturn provisioninService.getFilenames()>
			<cfelse>
				<cfreturn []>
			</cfif>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="hasCanUninstallFlag" returntype="Boolean" hint="Retourne TRUE si le fichier peut être désinstallé (CAB uniquement)">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn provisioninService.hasCanUninstallFlag(ARGUMENTS["filename"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getCRC32" returntype="Numeric" hint="Retourne le résultat du test CRC32 sur le fichier">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn VAL(provisioninService.getCRC32(ARGUMENTS["filename"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getLastModificationDate" returntype="Date" hint="Retourne la date de dernière modification du fichier">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn provisioninService.getLastModificationDate(ARGUMENTS["filename"]).getTime()>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getLastUploadDate" returntype="Date" hint="Retourne la date de dernier upload du fichier">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn provisioninService.getLastUploadDate(ARGUMENTS["filename"]).getTime()>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="hasExecuteAutoFlag" returntype="Boolean" hint="Retourne le FLAG d'exécution automatique">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn provisioninService.hasExecuteAutoFlag(ARGUMENTS["filename"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="setExecuteAutoFlag" returntype="Void" hint="Définit le FLAG d'exécution automatique">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfargument name="executeAuto" type="Boolean" required="true" hint="FLAG d'exécution automatique. TRUE pour une exécution auto et FALSE sinon">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset provisioninService.setExecuteAutoFlag(ARGUMENTS["filename"],ARGUMENTS["executeAuto"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="hasDelAfterInstallFlag" returntype="Boolean" hint="Retourne TRUE si le fichier est supprimé après installation">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin (Uniquement CAB et APK)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn provisioninService.hasDelAfterInstallFlag(ARGUMENTS["filename"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="setDelAfterInstallFlag" returntype="Void" hint="Spécifie si le fichier est supprimé après installation">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin (Uniquement CAB et APK)">
		<cfargument name="delAfterInstall" type="Boolean" required="true" hint="TRUE pour supprimer le fichier après installation (Implicitement à FALSE lorsque executeAuto est à FALSE)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset provisioninService.setDelAfterInstallFlag(ARGUMENTS["filename"],ARGUMENTS["delAfterInstall"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getInternalDestinationPath" returntype="String" hint="Retourne le chemin de destination sans la translation utilisateur">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn toString(provisioninService.getInternalDestinationPath(ARGUMENTS["filename"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="hasMacroReplacementFlag" returntype="Boolean" hint="Retourne la valeur du FLAG de de remplacement de la valeur d'un token">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin (Uniquement MSCR et fichier générique)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn provisioninService.hasMacroReplacementFlag(ARGUMENTS["filename"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="setMacroReplacementFlag" returntype="Void" hint="Définit la valeur du FLAG de de remplacement de la valeur d'un token">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin (Uniquement MSCR et fichier générique)">
		<cfargument name="macroReplacement" type="Boolean" required="true" hint="TRUE pour remplacer dans le fichier la valeur d'un token">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset provisioninService.setMacroReplacementFlag(ARGUMENTS["filename"],ARGUMENTS["macroReplacement"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="hasHiddenFlag" returntype="Boolean" hint="Retourne TRUE si le fichier est marqué comme masqué">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin (Uniquement fichier générique)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn provisioninService.hasHiddenFlag(ARGUMENTS["filename"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="setHiddenFlag" returntype="Void" hint="Spécifie si le fichier est à marquer comme masqué ou non">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin (Uniquement fichier générique)">
		<cfargument name="hiddenFlag" type="Boolean" required="true" hint="TRUE pour indiquer que le fichier est masqué et FALSE sinon">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset provisioninService.setHiddenFlag(ARGUMENTS["filename"],ARGUMENTS["hiddenFlag"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="hasReadOnlyFlag" returntype="Boolean" hint="Retourne TRUE si le fichier est marqué comme en lecture seule">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin (Uniquement fichier générique)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn provisioninService.hasReadOnlyFlag(ARGUMENTS["filename"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="setReadOnlyFlag" returntype="Void" hint="Spécifie si le fichier est à marquer comme en lecture seule ou non">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin (Uniquement fichier générique)">
		<cfargument name="readOnly" type="Boolean" required="true" hint="TRUE pour indiquer que le fichier est en lecture seule et FALSE sinon">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset provisioninService.setReadOnlyFlag(ARGUMENTS["filename"],ARGUMENTS["readOnly"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getCABProvisionings" returntype="String" hint="Retourne la liste des provisioning CAB au format JSON">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn provisioninService.getCABProvisionings()>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getAPKProvisionings" returntype="String" hint="Retourne la liste des provisioning APK au format JSON">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn provisioninService.getAPKProvisionings()>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getScriptProvisionings" returntype="String" hint="Retourne la liste des provisioning MSCR au format JSON">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn provisioninService.getScriptProvisionings()>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getGenericProvisionings" returntype="String" hint="Retourne la liste des provisioning de fichiers génériques au format JSON">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfreturn provisioninService.getGenericProvisionings()>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="addOrUpdateProvisioning" returntype="String"
	hint="Crée ou met à jour la définition d'un provisioning. Retourne un token pour l'envoi du provisioning ou une chaine vide si aucun contenu n'a été envoyé ou si fullSend vaut TRUE">
		<cfargument name="filename" type="String" required="true" hint="Nom du fichier incluant l'extension et sans le chemin ou une chaine vide pour utiliser celui fourni dans jsonProvisioning">
		<cfargument name="jsonProvisioning" type="String" required="true" hint="Définition du provisioning au format JSON ou une chaine vide pour une création">
		<cfargument name="content" type="Array" required="true" hint="Contenu du provisioning en tant que tableau de bytes.
		Fournir un tableau vide pour une création, Fournir un tableau contenant un chaine vide pour un mise à jour">
		<cfargument name="crc32" type="Numeric" required="true" hint="Valeur à utiliser pour la vérification CRC32 ou 0 pour ignorer la vérification">
		<cfargument name="fullSend" type="Boolean" required="true" hint="TRUE pour effectuer un envoi en une seule passe (i.e : Non partiel)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cfset var jsonDef=ARGUMENTS["jsonProvisioning"]>
		<cfset var fileContent=ARGUMENTS["content"]>
		<!--- Détermine le contenu du fichier : TRUE si le contenu du fichier est non défini et FALSE sinon (e.g : Vide) --->
		<cfset var nullContent=FALSE>
		<cfif yesNoFormat(arrayLen(fileContent))>
			<cfset nullContent=FALSE>
			<cfif (arrayLen(fileContent) EQ 1) AND (fileContent[1] EQ "")>
				<cfset fileContent=[]>
			</cfif>
		<cfelse>
			<cfset nullContent=TRUE>
		</cfif>
		<!--- Définition du provisioning non défini --->
		<cfif TRIM(jsonDef) EQ "">
			<cftry>
				<cfif nullContent>
					<cfreturn toString(provisioninService.addOrUpdateProvisioning(
						asJavaNullValue(ARGUMENTS["filename"]),asJavaNullValue(ARGUMENTS["jsonProvisioning"]),asJavaNullValue(""),ARGUMENTS["crc32"],ARGUMENTS["fullSend"]
					))>
				<cfelse>
					<cfreturn toString(provisioninService.addOrUpdateProvisioning(
						asJavaNullValue(ARGUMENTS["filename"]),asJavaNullValue(ARGUMENTS["jsonProvisioning"]),fileContent,ARGUMENTS["crc32"],ARGUMENTS["fullSend"]
					))>
				</cfif>
				<cfcatch type="any">
					<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
					<cfrethrow>
				</cfcatch>
			</cftry>
		<!--- Définition du provisioning défini --->
		<cfelse>
			<cfif isJSON(jsonDef)>
				<cftry>
					<cfif nullContent>
						<cfreturn toString(provisioninService.addOrUpdateProvisioning(
							asJavaNullValue(ARGUMENTS["filename"]),ARGUMENTS["jsonProvisioning"],asJavaNullValue(""),ARGUMENTS["crc32"],ARGUMENTS["fullSend"]
						))>
					<cfelse>
						<cfreturn toString(provisioninService.addOrUpdateProvisioning(
							asJavaNullValue(ARGUMENTS["filename"]),ARGUMENTS["jsonProvisioning"],fileContent,ARGUMENTS["crc32"],ARGUMENTS["fullSend"]
						))>
					</cfif>
					<cfcatch type="any">
						<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
						<cfrethrow>
					</cfcatch>
				</cftry>
			<cfelse>
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le format JSON de la définition du provisioning est invalide">
			</cfif>
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="continuePartialProvisioning" returntype="Void" hint="Ajoute le contenu à un provisioning partiellement effectué avec addOrUpdateProvisioning()">
		<cfargument name="token" type="String" required="true" hint="Token retourné par le provisioning partiel effectué avec addOrUpdateProvisioning()">
		<cfargument name="content" type="Array" required="true" hint="Contenu à ajouter au provisioning en tant que tableau de bytes">
		<cfargument name="crc32" type="Numeric" required="true" hint="Valeur CRC32 cumulée entre le contenu déjà provisionné et celui ajoute, utilisée pour la vérification CRC32">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset provisioninService.continuePartialProvisioning(ARGUMENTS["token"],ARGUMENTS["content"],ARGUMENTS["crc32"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="endPartialProvisioning" returntype="Void" hint="Spécifie la fin d'un provisioning partiel">
		<cfargument name="token" type="String" required="true" hint="Token retourné par le provisioning partiel effectué avec addOrUpdateProvisioning()">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset provisioninService.endPartialProvisioning(ARGUMENTS["token"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="cancelPartialProvisioning" returntype="Void" hint="Supprime un provisioning partiel de la base">
		<cfargument name="token" type="String" required="true" hint="Token retourné par le provisioning partiel effectué avec addOrUpdateProvisioning()">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var provisioninService=zdmRepository.getProvisioningService()>
		<cftry>
			<cfset provisioninService.cancelPartialProvisioning(ARGUMENTS["token"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="package" name="getWSService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSProvisioningService" hint="Retourne une instance de cette classe">
		<cfargument name="serviceRepository" type="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" required="true" hint="Implémentation d'accès aux Web Services">
		<cfif NOT structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfset VARIABLES["WS_REPOSITORY"]=ARGUMENTS["serviceRepository"]>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="getWSRepository" returntype="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" hint="Implémentation d'accès aux Web Services">
		<cfif structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfreturn VARIABLES["WS_REPOSITORY"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'entité MDM Service Repository n'est pas définie">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="asJavaNullValue" returntype="Any" hint="Retourne la valeur nulle Java si stringValue est une chaine vide et stringValue sinon">
		<cfargument name="stringValue" type="String" required="true">
		<cfif TRIM(ARGUMENTS["stringValue"]) EQ "">
			<cfreturn javaCast("NULL",0)>
		<cfelse>
			<cfreturn ARGUMENTS["stringValue"]>
		</cfif>
	</cffunction>
</cfcomponent>