<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" extends="fr.consotel.api.mdm.partner.private.ws.WSRepository"
hint="Implémentation Factory utilisée par l'API MDM pour instancier et accéder aux Web Services MDM de gestion des équipements mobiles (Zenprise)
Les méthodes de cette implémentation sont soumises à la vérification de l'authentification<br>
Le nom du namespace utilisé pour le service ciblé doit toujours être <b>zenprise</b> et sa valeur doit aussi être <b>zenprise</b>">
	<cffunction access="public" name="getWSRepository" returntype="fr.consotel.api.mdm.partner.private.ws.WSRepository">
		<cfargument name="authCredentials" type="Struct" required="true" hint="Peut contenir les clés supplémentaires suivantes :<br>
		LOGIN (Login), PWD (Mot de passe. Obligatoire si LOGIN est spécifié), WS_ENDPOINT (Endpoint). Le serveur ConsoTel est utilisé par défaut">
		<cfset var userLogin="">
		<cfset var userPwd="">
		<cfset var wsEndpoint=getDefaultServiceEndpoint()>
		<cfset var AUTH=ARGUMENTS["authCredentials"]>
		<cfif structKeyExists(AUTH,"LOGIN") AND structKeyExists(AUTH,"PWD")>
			<cfset userLogin=AUTH["LOGIN"]>
			<cfset userPwd=AUTH["PWD"]>
		</cfif>
		<cfif structKeyExists(AUTH,"WS_ENDPOINT")>
			<cfset wsEndpoint=AUTH["WS_ENDPOINT"]>
		</cfif>
		<!--- Constructeur de la classe parent --->
		<cfset SUPER.getWSRepository(ARGUMENTS["authCredentials"])>
		<!--- Credentials utilisés pour accéder aux Web Services Zenprise --->
		<cfset SUPER.setServiceCredentials(userLogin,userPwd)>
		<cfreturn THIS>
	</cffunction>

	<cffunction access="package" name="notifyError" returntype="void" hint="Envoi un email à l'administrateur Zenprise avec le contenu de l'exception rencontrée">
		<cfargument name="exceptionObject" type="struct" required="true" hint="Structure contenant au moins les clés : Type, Message, Detail">
		<cfargument name="exceptionTagContext" type="Array" required="false" hint="Tableau correspondant à la clé CFCATCH.TagContext">
		<cfset var errorObject=ARGUMENTS["exceptionObject"]>
		<cfset var AUTH_COPY=duplicate(SUPER.getAuthCredentials())>
		<cflog type="error" text="NotifyError >> (#errorObject.TYPE#) #errorObject.MESSAGE# [#errorObject.DETAIL#]">
		<cfdump var="#ARGUMENTS.exceptionTagContext#" label="NotifyError - TagContext" output="console">
		<cftry>
			<cfset var userLogin=AUTH_COPY["LOGIN"]>
			<cfset var serviceEndpoint=AUTH_COPY["WS_ENDPOINT"]>
			<cfset var adminEmail=AUTH_COPY["ADMIN_EMAIL"]>
			<cfset var idGroupe=AUTH_COPY["IDGROUPE"]>
			<cfmail type="html" from="#getNotificationExpeditor()#" to="#adminEmail#" cc="#getNotificationEmailCC()#" subject="[Notification Zenprise] #errorObject.MESSAGE#">
				<cfoutput>
					<hr/>Notification d'erreur envoyée à #adminEmail# concernant l'instance Zenprise suivante :<br>
					idRacine : #idGroupe#<br>
					Service : #serviceEndpoint#<br>
					Utilisateur : #userLogin#<br><hr/>
				</cfoutput>
				<cfdump var="#errorObject#" label="Informations concernant l'erreur rencontrée">
				<cfif isDebugMode() AND structKeyExists(ARGUMENTS,"exceptionTagContext")>
					<br><cfdump var="#ARGUMENTS.exceptionTagContext#" label="Contexte d'exécution (Ce contenu est envoyé si le mode debug du BackOffice est activé)">
				</cfif>
			</cfmail>
			<cfcatch type="any">
				<cflog type="error" text="NotifyError Failed >> (#CFCATCH.TYPE#) #CFCATCH.MESSAGE# [#CFCATCH.DETAIL#]">
				<cfif structKeyExists(AUTH_COPY,"PWD")>
					<cfset structDelete(AUTH_COPY,"PWD")>
				</cfif>
				<cfmail type="html" from="#getNotificationExpeditor()#" to="#getNotificationEmailCC()#" cc="#getNotificationEmailCC()#"
					subject="Echec d'envoi de notification d'erreur Zenprise : #CFCATCH.MESSAGE#">
					Une notification d'erreur Zenprise n'a pas pu etre envoyée. L'administrateur Zenprise n'est pas notifié pour ce type d'erreur interne.<br><br>
					<cfdump var="#AUTH_COPY#" label="Paramètres de la notification">
					<cfdump var="#errorObject#" label="Contenu destiné à l'administrateur Zenprise"><br>
					<cfdump var="#CFCATCH#" label="Exception rencontrée durant l'envoi de la notification">
				</cfmail>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="package" name="getNotificationEmailCC" returntype="String" hint="Email en copie des notifications envoyées à l'administrateur Zenprise">
		<cfreturn "cedric.rapiera@consotel.fr">
	</cffunction>
	
	<cffunction access="package" name="getNotificationExpeditor" returntype="String" hint="Email par défaut utilisé en adresse expéditeur des notifications">
		<cfreturn "[Notification Zenprise] <error@consotel.fr>">
	</cffunction>

	<cffunction access="public" name="authLogout" returntype="void" hint="Déconnecte tous les Web Services créés">
		<cfset var wsInstances=getWSInstances()>
		<cfset SUPER.setServiceCredentials("","")>
		<cfloop item="serviceName" collection="#wsInstances#">
			<cfset var serviceInstance=wsInstances[serviceName]>
			<cfset serviceInstance.setUsername("")>
			<cfset serviceInstance.setPassword("")>
		</cfloop>
	</cffunction>

	<cffunction access="package" name="getDeviceService" returntype="Any" hint="Retourne l'implémentation du service EveryWanDevice">
		<cfreturn getWSStub("EveryWanDevice")>
	</cffunction>

	<cffunction access="package" name="getPackageService" returntype="Any" hint="Retourne l'implémentation du service EveryWanPackage">
		<cfreturn getWSStub("EveryWanPackage")>
	</cffunction>
	
	<cffunction access="package" name="getProvisioningService" returntype="Any" hint="Retourne l'implémentation du service EveryWanProvisioning">
		<cfreturn getWSStub("EveryWanProvisioning")>
	</cffunction>

	<cffunction access="package" name="getCfgProvisioningService" returntype="Any" hint="Retourne l'implémentation du service EveryWanCFG">
		<cfreturn getWSStub("EveryWanCFG")>
	</cffunction>
	
	<cffunction access="package" name="getXmlService" returntype="Any" hint="Retourne l'implémentation du service EveryWanXML">
		<cfreturn getWSStub("EveryWanXML")>
	</cffunction>
	
	<cffunction access="package" name="getTunnelService" returntype="Any" hint="Retourne l'implémentation du service EveryWanTunnel">
		<cfreturn getWSStub("EveryWanTunnel")>
	</cffunction>
	
	<cffunction access="package" name="getWhitelistService" returntype="Any" hint="Retourne l'implémentation du service Whitelist">
		<cfreturn getWSStub("Whitelist")>
	</cffunction>

	<cffunction access="package" name="sendWhitelistRequest" returntype="XML" hint="Envoi une requête SOAP vers le service Whitelist et retourne la réponse XML">
		<cfargument name="soapRequest" type="XML" required="true" hint="Requête SOAP">
		<cfreturn sendSoapRequest("zenprise","Whitelist",ARGUMENTS["soapRequest"])>
	</cffunction>
	
	<cffunction access="package" name="getBlacklistService" returntype="Any" hint="Retourne l'implémentation du service Blacklist">
		<cfreturn getWSStub("Blacklist")>
	</cffunction>
	
	<cffunction access="package" name="sendBlacklistRequest" returntype="XML" hint="Envoi une requête SOAP vers le service Blacklist et retourne la réponse XML (Case Insensitive)">
		<cfargument name="soapRequest" type="XML" required="true" hint="Requête SOAP">
		<cfreturn sendSoapRequest("zenprise","Blacklist",ARGUMENTS["soapRequest"])>
	</cffunction>
	
	<cffunction access="package" name="getRegistryService" returntype="Any" hint="Retourne l'implémentation du service EveryWanRegistry">
		<cfreturn getWSStub("EveryWanRegistry")>
	</cffunction>
	
	<cffunction access="package" name="getUserService" returntype="Any" hint="Retourne l'implémentation du service EveryWanUserGroup">
		<cfreturn getWSStub("EveryWanUserGroup")>
	</cffunction>
	
	<cffunction access="package" name="sendRegistryRequest" returntype="XML" hint="Envoi une requête SOAP vers le service Registry et retourne la réponse XML">
		<cfargument name="soapRequest" type="XML" required="true" hint="Requête SOAP">
		<cfreturn sendSoapRequest("zenprise","EveryWanRegistry",ARGUMENTS["soapRequest"])>
	</cffunction>
	
	<cffunction access="package" name="sendServerGroupRequest" returntype="XML" hint="Envoi une requête SOAP vers le service ServerGroup et retourne la réponse XML">
		<cfargument name="soapRequest" type="XML" required="true" hint="Requête SOAP">
		<cfreturn sendSoapRequest("zenprise","EveryWanServerGroup",ARGUMENTS["soapRequest"])>
	</cffunction>
	
	<cffunction access="private" name="sendSoapRequest" returntype="XML" hint="Envoi une requête SOAP vers la destination fournie et retourne la réponse XML (Case Insensitive)<br>
	Remplace la valeur de l'attribut xmlns:zenprise par le targetNamespace du service correspondant à la destination fournie<br>
	La requête HTTP est envoyée vers l'URL dont la valeur est la concaténation : SERVICE-URI/destination où SERVICE-URI est la valeur retournée par getServiceContextURI()<br>
	Les credentials définis par l'implémentation de la classe parent sont utilisés dans l'authentification HTTP en utilisant la méthode Basic (HTTP Basic Auth)<br>
	Une exception est levée si le namespace <b>nsName</b> n'est pas présent dans la requête SOAP">
		<cfargument name="nsName" type="String" required="true" hint="Nom associé au targetNamespace du service (e.g : zenprise)">
		<cfargument name="destination" type="String" required="true" hint="Destination de la requête SOAP (e.g : ServiceName)">
		<cfargument name="soapRequest" type="XML" required="true" hint="Requête SOAP">
		<cfif structKeyExists(soapRequest.xmlRoot.xmlAttributes,nsName)>
			<cfset var CREDENTIALS=getServiceCredentials()>
			<!--- Définition de la valeur du targetNamespace du service correspondant à la destination ciblée --->
			<cfset var targetNamespace=SUPER.getServiceContextURI() & "/" & ARGUMENTS["destination"]>
			<cfset soapRequest.xmlRoot.xmlAttributes["xmlns:zenprise"]=targetNamespace>
			<!--- Envoi la requête SOAP sur HTTP --->
			<cfset var soapResponse={fileContent="<ERROR>Voir le fichier de log</ERROR>"}>
			<cftry>
				<cfhttp url="#targetNamespace#" method="post" username="#CREDENTIALS.USER#" password="#CREDENTIALS.PWD#" result="soapResponse">
					<cfhttpparam type="header" name="SOAPAction" value=""/>
					<cfhttpparam type="header" name="accept-encoding" value="no-compression"/>
					<cfhttpparam type="xml" value="#TRIM(toString(ARGUMENTS.soapRequest))#"/>
				</cfhttp>
				<cfcatch type="any">
					<cfset notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
					<cfrethrow>
				</cfcatch>
			</cftry>
			<cfreturn xmlParse(soapResponse["fileContent"],FALSE)>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le namespace #ARGUMENTS.nsName# n'est pas présent dans la requête SOAP">
		</cfif> 
	</cffunction>
	
	<cffunction access="private" name="getWSStub" returntype="Any" hint="Retourne une instance de l'implémentation du service : serviceName">
		<cfargument name="serviceName" type="String" required="true" hint="Nom du service à instancier (e.g : EveryWanDevice)">
		<cfset var serviceKey=ARGUMENTS["serviceName"]>
		<!--- Validation des credentials d'authentification --->
		<cfset SUPER.validateAuth()>
		<!--- Instanciation ou Récupération du service --->
		<cfset var wsInstances=getWSInstances()>
		<cfset var serviceEndpoint=getServiceContextURI() & "/" & ARGUMENTS["serviceName"] & "?WSDL">
		<cfif NOT structKeyExists(wsInstances,serviceKey)>
			<cfset var serviceInstance="UNDEFINED">
			<cftry>
				<cfset serviceInstance=SUPER.createServiceInstance(serviceEndpoint)>
				<cfcatch type="any">
					<cfset notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
					<cfrethrow>
				</cfcatch>
			</cftry>
			<cfset wsInstances[serviceKey]=serviceInstance>
		</cfif>
		<!--- MAJ de la valeur si les credentials ont changés de valeur --->
		<cfset var CREDENTIALS=getServiceCredentials()>
		<cfif compareNoCase(wsInstances[serviceKey].getUsername(),CREDENTIALS["USER"])>
			<cfset wsInstances[serviceKey].setUsername(CREDENTIALS["USER"])>
			<cfset wsInstances[serviceKey].setPassword(CREDENTIALS["PWD"])>
		</cfif>
		<cfif isDebugMode()>
			<cflog type="information" text="Zenprise : #getServiceContextURI()# - Service : #ARGUMENTS.serviceName#">
		</cfif>
		<cfreturn wsInstances[serviceKey]>
	</cffunction>
	
	<cffunction access="private" name="getDefaultServiceEndpoint" returntype="String" hint="Retourne le Endpoint utilisé par défaut (ZENPRISE)">
		<cfreturn "">
	</cffunction>
	
	<!--- fr.consotel.api.remoting.RemoteService --->
	<cffunction access="private" name="createServiceInstance" returntype="Any" hint="Instancie et retourne une instance du Web Service indiqué">
		<cfargument name="serviceEndpoint" type="String" required="true" hint="Endpoint du WebService à instancier">
		<cftry>
			<cfreturn SUPER.createServiceInstance(ARGUMENTS["serviceEndpoint"])>
			<cfcatch type="any">
				<cfset notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>