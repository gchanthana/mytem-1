<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.public.FSecure" extends="fr.consotel.api.mdm.partner.public.FSecureSingleLicence"
hint="Point d'accès public aux services FSecure et sans authentification requise.<br><b>Voir les classes parent pour la documentation des méthodes héritées par cette implémentation</b><br><br>
Tous les résultats des méthodes de cette implémentation sont retournés au format JSON. Les paramètres fournis aux méthodes de cette implémentation doivent être typés en ColdFusion<br>
<b>En raison du bug CFMX 74841 (http://cfbugs.adobe.com/) :</b> Les résultats provenant du Web Service sont d'abord manuellement convertit en CFMX avant d'être convertit au format JSON
<br><b>Pour des raisons de performance : Certaines méthodes lèvent une exception provenant du Web Service lorsqu'aucune entité ne correspond aux infos fournies</b>
<br><i>Entres autres : La méthode searchCorporations() peut être utilisée pour vérifier l'existence d'un équipement avant d'appeler ces méthodes</i>">
	<!--- Corporation Service --->
	<cffunction access="remote" name="searchCorporations" returntype="String"
	hint="Recherche toutes les entités Corporation qui correspondent aux critères fournis et retourne les propriétés suivantes  (50 résultats par page max) :<br>
	- totalSearchResults : Nombre total de résultat de la recherche<br>
	- lastPageNumber : Numéro de la dernière page de résultat<br>
	- pageNumber : Numéro de la page de résultat qui est retournée par cette méthode<br>
	- searchResultsOnPage : Nombre total de résultat de la page pageNumber<br>
	- isLastPage : TRUE si la page pageNumber correspond à la dernière page de résultat (i.e : lastPageNumber)<br>
	- corporationRTO (Tableau contenant les résultat de la page pageNumber. Chaque élément contient les propriétés suivantes) :<br>
	corporationId (Identifiant de l'entité Corporation), name (Nom de l'entité Corporation), active (TRUE si l'entité Corporation est active et FALSE sinon)">
		<cfargument name="corporationName" type="String" required="true" hint="Chaine à rechercher dans le nom de l'entité Corporation">
		<cfargument name="corporationId" type="String" required="true" hint="Chaine à rechercher dans l'identifiant de l'entité Corporation">
		<cfargument name="pageNumber" type="String" required="true" hint="Numéro de la page de résultat à retourner (1 ou une chaine vide pour retourner la 1ère page de résultat)">
		<cfset var corpService=getSecurityService().getCorporationService()>
		<cfset var wsSearchCorp=corpService.searchCorporations(ARGUMENTS["corporationName"],ARGUMENTS["corporationId"],ARGUMENTS["pageNumber"])>
		<cfset var cfmxSearchResults={
			totalSearchResults=0, lastPageNumber=1, pageNumber=1, searchResultsOnPage=0, isLastPage=TRUE, corporationRTO=[]
		}>
		<cfif isDefined("wsSearchCorp")>
			<cfset structAppend(cfmxSearchResults,{
				totalSearchResults=VAL(toString(wsSearchCorp.getTotalSearchResults())), lastPageNumber=VAL(toString(wsSearchCorp.getLastPageNumber())),
				pageNumber=VAL(toString(wsSearchCorp.getPageNumber())), searchResultsOnPage=VAL(toString(wsSearchCorp.getSearchResultsOnPage())),
				isLastPage=yesNoFormat(toString(wsSearchCorp.getIsLastPage()))
			},TRUE)>
			<cfif cfmxSearchResults.searchResultsOnPage GT 0>
				<cfset var wsCorpRTO=wsSearchCorp.getCorporationRTO()>
				<cfset var searchNum=arrayLen(wsCorpRTO)>
				<cfloop index="i" from="1" to="#searchNum#">
					<cfset var corp=wsCorpRTO[i]>
					<cfset cfmxSearchResults["corporationRTO"][i]={
						corporationId=toString(corp.getCorporationId()), name=toString(corp.getName()), active=yesNoFormat(toString(corp.getActive()))
					}>
				</cfloop>
			</cfif>
		</cfif>
		<cfreturn serializeJSON(cfmxSearchResults)>
	</cffunction>
	
	<cffunction access="remote" name="getCorporationName" returntype="String" hint="Retourne le nom correspondant à corporationId
	<br><b>Lève une exception provenant du Web Service si la valeur corporationId ne correspond à aucune entité Corporation existante</b>">
		<cfargument name="corporationId" type="String" required="true">
		<cfset var corpService=getSecurityService().getCorporationService()>
		<cfset var wsCorpName=toString(corpService.getCorporationName(ARGUMENTS["corporationId"]))>
		<cfreturn serializeJSON(wsCorpName)>
	</cffunction>
	
	<cffunction access="remote" name="getCorporationStatus" returntype="String" hint="Retourne le statut de l'entité Corporation correspondant à corporationId (Active ou Inactive)
	<br><b>Lève une exception provenant du Web Service si la valeur corporationId ne correspond à aucune entité Corporation existante</b>">
		<cfargument name="corporationId" type="String" required="true">
		<cfset var corpService=getSecurityService().getCorporationService()>
		<cfset var wsCorpStatus=toString(corpService.getCorporationStatus(ARGUMENTS["corporationId"]))>
		<cfreturn serializeJSON(wsCorpStatus)>
	</cffunction>
	
	<cffunction access="remote" name="createCorporation" returntype="String" hint="Crée une entité Corporation avec le nom corporationName et retourne son identifiant">
		<cfargument name="corporationName" type="String" required="true">
		<cfset var corpService=getSecurityService().getCorporationService()>
		<cfset var wsCorpId=toString(corpService.createCorporation(ARGUMENTS["corporationName"]))>
		<cfreturn serializeJSON(wsCorpId)>
	</cffunction>
	
	<cffunction access="remote" name="deleteCorporation" returntype="String"
	hint="Supprime le statut de l'entité Corporation correspondant à corporationId. Retourne TRUE si l'opération s'est effectuée avec succès et FALSE sinon
	<br><b>Lève une exception provenant du Web Service si la valeur corporationId ne correspond à aucune entité Corporation existante</b><br>
	<b>Une exception est levée si la Corporation contient au moins une licence multiple</b>">
		<cfargument name="corporationId" type="String" required="true">
		<cfset var corpService=getSecurityService().getCorporationService()>
		<cfreturn serializeJSON(corpService.deleteCorporation(ARGUMENTS["corporationId"]))>
	</cffunction>
	
	<cffunction access="remote" name="getCorporationCount" returntype="Numeric" hint="Retourne le nombre d'entité Corporation">
		<cfset var corpService=getSecurityService().getCorporationService()>
		<cfreturn serializeJSON(corpService.getCorporationCount())>
	</cffunction>

	<cffunction access="remote" name="updateCorporationName" returntype="Boolean" hint="Renomme une entité Corporation et retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="corporationId" type="String" required="true" hint="Identifiant de l'entité Corporation">
		<cfargument name="corporationName" type="String" required="true" hint="Nouveau nom à donner à l'entité Corporation">
		<cfset var corpService=getSecurityService().getCorporationService()>
		<cfreturn serializeJSON(corpService.updateCorporationName(ARGUMENTS["corporationId"],ARGUMENTS["corporationName"]))>
	</cffunction>

	<cffunction access="remote" name="updateCorporationStatus" returntype="String" hint="Met à jour le statut d'une entité Corporation et retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="corporationId" type="String" required="true" hint="Identifiant de l'entité Corporation">
		<cfargument name="status" type="String" required="true" hint="Valeur à donner au statut. Valeurs possibles : Active, Inactive">
		<cfset var corpService=getSecurityService().getCorporationService()>
		<cfreturn serializeJSON(corpService.updateCorporationStatus(ARGUMENTS["corporationId"],ARGUMENTS["status"]))>
	</cffunction>
</cfcomponent>