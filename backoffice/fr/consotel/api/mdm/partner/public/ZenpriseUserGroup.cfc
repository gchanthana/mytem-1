<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.public.ZenpriseUserGroup" extends="fr.consotel.api.mdm.partner.public.ZenpriseServerGroup" hint="Implémentation UserGroup Service">
	<!--- UserGroup Service --->
	<cffunction access="remote" name="getRoles" returntype="String" hint="Retourne la liste des roles">
		<cfset var userService=getManagementService().getUserService()>
		<cfset var resultList=userService.getRoles()>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="getGroups" returntype="String" hint="Retourne la liste des groupes">
		<cfset var userService=getManagementService().getUserService()>
		<cfset var resultList=userService.getGroups()>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="createGroup" returntype="String" hint="Crée un groupe et retourne son nom">
		<cfargument name="groupName" type="String" required="true" hint="Nom du groupe à créer">
		<cfset var userService=getManagementService().getUserService()>
		<cfset var resultList=userService.createGroup(ARGUMENTS["groupName"])>
		<cfreturn serializeJSON(resultList)>
	</cffunction>

	<cffunction access="remote" name="removeGroup" returntype="Void" hint="supprime un groupe">
		<cfargument name="groupName" type="String" required="true" hint="Nom du groupe à supprimer">
		<cfset var userService=getManagementService().getUserService()>
		<cfset userService.removeGroup(ARGUMENTS["groupName"])>
	</cffunction>

	<cffunction access="remote" name="getGroupUsers" returntype="String" hint="Retourne la liste des utilisateur du groupe">
		<cfargument name="groupName" type="String" required="true" hint="Nom du groupe">
		<cfset var userService=getManagementService().getUserService()>
		<cfset var resultList=userService.getGroupUsers(ARGUMENTS["groupName"])>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="getUserGroup" returntype="String" hint="Retourne la liste des groupes d'un utilisateur">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfset var userService=getManagementService().getUserService()>
		<cfset var resultList=userService.getUserGroup(ARGUMENTS["userName"])>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="createUser" returntype="String" hint="Crée un utilisateur et retourne son nom">
		<cfargument name="groupName" type="String" required="true" hint="Nom du groupe">
		<cfargument name="login" type="String" required="true" hint="Nom de l'utilisateur i.e Login utilisateur">
		<cfargument name="password" type="String" required="true" hint="Mot de passe de l'utilisateur">
		<cfargument name="userRole" type="String" required="true" hint="Role de l'utilisateur (ADMIN, SUPPORT, USER, GUEST)">
		<cfset var userService=getManagementService().getUserService()>
		<cfset var resultList=userService.createUser(ARGUMENTS["groupName"],ARGUMENTS["login"],ARGUMENTS["password"],ARGUMENTS["userRole"])>
		<cfreturn serializeJSON(resultList)>
	</cffunction>

	<cffunction access="remote" name="setUserGroup" returntype="Void" hint="Ajoute l'utilisateur au groupe spécifié">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfargument name="groupName" type="String" required="true" hint="Nom du groupe">
		<cfset var userService=getManagementService().getUserService()>
		<cfset userService.setUserGroup(ARGUMENTS["userName"],ARGUMENTS["groupName"])>
	</cffunction>
	
	<cffunction access="remote" name="removeUser" returntype="Void" hint="Supprime un utilisateur">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfset var userService=getManagementService().getUserService()>
		<cfset userService.removeUser(ARGUMENTS["userName"])>
	</cffunction>
	
	<cffunction access="remote" name="removeUserFromGroup" returntype="Void" hint="Supprime un utilisateur d'un groupe">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfargument name="groupName" type="String" required="true" hint="Nom du groupe">
		<cfset var userService=getManagementService().getUserService()>
		<cfset userService.removeUserFromGroup(ARGUMENTS["userName"],ARGUMENTS["groupName"])>
	</cffunction>

	<cffunction access="remote" name="setUserPassword" returntype="Void" hint="Met à jour le mot de passe d'un utilisateur">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfargument name="newPassword" type="String" required="true" hint="Nouveau mot de passe de l'utilisateur">
		<cfset var userService=getManagementService().getUserService()>
		<cfset userService.setUserPassword(ARGUMENTS["userName"],ARGUMENTS["newPassword"])>
	</cffunction>

	<cffunction access="remote" name="checkUserPassword" returntype="String" hint="Retourne TRUE si le mot de passe spécifié est correct pour l'utilisateur concerné">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfargument name="password" type="String" required="true" hint="Mot de passe de l'utilisateur">
		<cfset var userService=getManagementService().getUserService()>
		<cfset var resultList=userService.checkUserPassword(ARGUMENTS["userName"],ARGUMENTS["password"])>
		<cfreturn serializeJSON(resultList)>
	</cffunction>

	<cffunction access="remote" name="getUserRoles" returntype="String" hint="Retourne les roles d'un utilisateur">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfset var userService=getManagementService().getUserService()>
		<cfset var resultList=userService.getUserRoles(ARGUMENTS["userName"])>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="getUserProperties" returntype="String" hint="Retourne les propriétés d'un utilisateur (En tant que tableau de String au format JSON)">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfset var userService=getManagementService().getUserService()>
		<cfset var resultList=userService.getUserProperties(ARGUMENTS["userName"])>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="putUserProperties" returntype="Void" hint="Spécifie une propriété pour un utilisateur">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfargument name="userProps" type="Array" required="true" hint="Tableau dans lequel chaque élément est une structure contenant les clés suivantes : NAME (Nom d'une propriété : String), VALUE (Valeur de la propriété : String)">
		<cfset var userService=getManagementService().getUserService()>
		<cfset userService.putUserProperties(ARGUMENTS["userName"],ARGUMENTS["userProps"])>
	</cffunction>
	
	<cffunction access="remote" name="setUserRoles" returntype="Void" hint="Spécifie les roles d'un utilisateur. <b>Dans la version actuelle de Zenprise, un utilisateur ne doit avoir qu'un et un seul role</b>">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfargument name="userRoles" type="Array" required="true" hint="Tableau contenant la liste des roles">
		<cfset var userService=getManagementService().getUserService()>
		<cfset userService.setUserRoles(ARGUMENTS["userName"],ARGUMENTS["userRoles"])>
	</cffunction>
	
	<cffunction access="remote" name="searchUserByName" returntype="String" hint="Retourne la liste des utilisateurs dont le nom contient la chaine spécifiée en paramètre">
		<cfargument name="searchInUserName" type="String" required="true" hint="Chaine à rechercher dans le nom des utilisateurs">
		<cfset var userService=getManagementService().getUserService()>
		<cfset var resultList=userService.searchUserByName(ARGUMENTS["searchInUserName"])>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="initializeUser" returntype="Void" hint="Crée une entrée LDAP pour l'utilisateur spécifié s'il n'est pas encore présent dans l'annuaire">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfset var userService=getManagementService().getUserService()>
		<cfset userService.initializeUser(ARGUMENTS["userName"])>
	</cffunction>
</cfcomponent>