<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.public.ZenpriseCfg" extends="fr.consotel.api.mdm.partner.public.ZenpriseBlacklist" hint="Implémentation CFG Provisioning">
	<!--- CFG Provisioning Service --->
	<cffunction access="remote" name="addCfgFile" returntype="Void" hint="Ajoute un fichier de configuration avec son contenu">
		<cfargument name="strOSFamily" type="String" required="true" hint="OS cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier">
		<cfargument name="fileContent" type="Array" required="true" hint="Contenu du fichier en tant que tableau de bytes">
		<cfargument name="comment" type="String" required="true" hint="Commentaire à associer au fichier">
		<cfset var cfgProvisioningService=getManagementService().getCfgProvisioningService()>
		<cfset cfgProvisioningService.addCfgFile(ARGUMENTS["strOSFamily"],ARGUMENTS["fileName"],ARGUMENTS["fileContent"],ARGUMENTS["comment"])>
	</cffunction>
	
	<cffunction access="remote" name="updateCfgFile" returntype="Void" hint="Met à jour le contenu d'un fichier de configuration">
		<cfargument name="strOSFamily" type="String" required="true" hint="OS cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="oldFileName" type="String" required="true" hint="Nom actuel du fichier de configuration">
		<cfargument name="newFileName" type="String" required="true" hint="Nouveau nom à donner au fichier de configuration ou une chaine vide pour garder oldFileName">
		<cfargument name="comment" type="String" required="true" hint="Commentaire à associer au fichier">
		<cfargument name="fileContent" type="Array" required="true" hint="Contenu du fichier en tant que tableau de bytes">
		<cfset var cfgProvisioningService=getManagementService().getCfgProvisioningService()>
		<cfset cfgProvisioningService.updateCfgFile(ARGUMENTS["strOSFamily"],ARGUMENTS["oldFileName"],ARGUMENTS["newFileName"],ARGUMENTS["comment"],ARGUMENTS["fileContent"])>
	</cffunction>
	
	<cffunction access="remote" name="removeCfgFile" returntype="Void" hint="Supprime un fichier de configuration">
		<cfargument name="strOSFamily" type="String" required="true" hint="OS cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier">
		<cfset var cfgProvisioningService=getManagementService().getCfgProvisioningService()>
		<cfset cfgProvisioningService.removeCfgFile(ARGUMENTS["strOSFamily"],ARGUMENTS["fileName"])>
	</cffunction>
	
	<cffunction access="remote" name="getCfgFileInfo" returntype="String" hint="Retourne les infos d'un fichier de configuration en tant que tableau<br>
	Les éléments du tableau sont par ordre d'apparition : Nom du fichier, Taille, Commentaire associé, Date de création ou d'upload, Date de moficiation">
		<cfargument name="strOSFamily" type="String" required="true" hint="OS cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier">
		<cfset var cfgFileinfos=[]>
		<cfset var cfgProvisioningService=getManagementService().getCfgProvisioningService()>
		<cfset var wsCfgFileInfos=cfgProvisioningService.getCfgFileInfo(ARGUMENTS["strOSFamily"],ARGUMENTS["fileName"])>
		<cfif isDefined("wsCfgFileInfos")>
			<cfset cfgFileinfos=wsCfgFileInfos>
		</cfif>
		<cfreturn serializeJSON(cfgFileinfos)>
	</cffunction>
	
	<cffunction access="remote" name="getCfgFileList" returntype="String" hint="Retourne un tableau contenant la liste des fichiers de configuration">
		<cfargument name="strOSFamily" type="String" required="true" hint="OS cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfset var cfgProvisioningService=getManagementService().getCfgProvisioningService()>
		<cfset var wsResult=cfgProvisioningService.getCfgFileList(ARGUMENTS["strOSFamily"])>
		<cfreturn serializeJSON(wsResult)>
	</cffunction>
	
	<cffunction access="remote" name="validateCfgFile" returntype="String" hint="Retourne TRUE si le contenu du fichier de configuration est valide et FALSE sinon<br>
	Une exception est levée par le service Zenprise lorsque la valeur retournée est false">
		<cfargument name="strOSFamily" type="String" required="true" hint="OS cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier">
		<cfset var cfgProvisioningService=getManagementService().getCfgProvisioningService()>
		<cfset var wsResult=cfgProvisioningService.validateCfgFile(ARGUMENTS["strOSFamily"],ARGUMENTS["fileName"])>
		<cfreturn serializeJSON(wsResult)>
	</cffunction>
</cfcomponent>