<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.public.Zenprise" extends="fr.consotel.api.mdm.partner.public.ZenpriseUserGroup"
hint="Point d'accès public aux services Zenprise et sans authentification requise<br><b>Voir les classes parent pour la documentation des méthodes héritées par cette implémentation</b><br><br>
Tous les résultats des méthodes de cette implémentation sont retournés au format JSON. Les paramètres fournis aux méthodes de cette implémentation doivent être typés en ColdFusion<br><br>
Les propriétés pour un tunnel sont représentées en tant que structure contenant les clés suivantes :<br>
- deviceHost : Nom de machine de l'équipement (DEVICE)<br>
- devicePort : Numéro de port de l'équipement (DEVICE)<br>
- initiator : Initiateur (e.g : SERVER ou DEVICE)<br>
- maxConnect : Nombre max de connexion<br>
- name : Nom du tunnel<br>
- osFamily : OS (e.g : iOS, WINDOWS, ANDROID)<br>
- protocol : Protocole (e.g : GENERIC TCP ou FTP)<br>
- serverHost : Nom de machine du serveur<br>
- serverPort : Numéro de port du serveur<br>
- timeOut : Timeout en secondes<br>
- isNoRoaming : TRUE ou FALSE<br>
- isSSL : TRUE pour le support SSL et FALSE sinon<br><br>
<b>En raison du bug CFMX 74841 (http://cfbugs.adobe.com/) :</b> Les résultats provenant du Web Service sont d'abord manuellement convertit en CFMX avant d'être convertit au format JSON
<br><b>Pour des raisons de performance : Certaines méthodes lèvent une exception provenant du Web Service lorsqu'aucun équipement ne correspond aux infos fournies</b>
<br><i>Les méthodes deviceExists() et getAllDevices() peuvent être utilisées pour vérifier l'existence d'un équipement avant d'appeler ces méthodes</i>">
	<!--- Device Service --->
	<cffunction access="remote" name="getMasterKeyList" returntype="String" hint="Retourne la liste des clés master (Master Keys) en tant que tableau">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement">
		<cfset var masterKeyList=[]>
		<cfset var deviceService=getManagementService().getDeviceService()>
		<cfset var masterKeys=deviceService.getDeploymentHisto(ARGUMENTS["serialNumber"],ARGUMENTS["imei"])>
		<!---
		<cfdump var="#masterKeys#" label="Master Keys">
		<cfset var masterKey="NULL">
		<cfloop index="i" from="1" to="#arrayLen(masterKeys)#">
			<cfset masterKey=masterKeys[i]>
			<cfset masterKeyList[i]={
				ACTION=masterKey.getAction(), DATE=masterKey.getDate().getTime(), RES_TYPE=masterKey.getResType(),
				SERIAL=masterKey.getSerial(), STATUS=masterKey.getStatus(), OWNER=masterKey.getOwner(), ARG1=masterKey.getArg1(), ARG2=masterKey.getArg2()
			}>
		</cfloop>
		<cfreturn serializeJSON(masterKeyList)>
		--->
		<cfthrow type="Custom" message="Cette méthode n'est pas supportée pour le moment">
	</cffunction>
	
	<cffunction access="remote" name="clearDeploymentHisto" returntype="Numeric" hint="Purge l'historique de déploiement. La valeur retournée n'est pas documentée par Zenprise">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement">
		<cfset var deviceService=getManagementService().getDeviceService()>
		<cfset var clearCode=deviceService.clearDeploymentHisto(ARGUMENTS["serialNumber"],ARGUMENTS["imei"])>
		<cfreturn serializeJSON(clearCode)>
	</cffunction>
	
	<cffunction access="remote" name="getDeploymentHisto" returntype="String"
	hint="Retourne l'historique de déploiement par ordre chronologique en tant que tableau. Chaque élément contient la valeur litérrale de l'évènement correspondant">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement">
		<cfset var deploymentHistoryResult=[]>
		<cfset var deviceService=getManagementService().getDeviceService()>
		<cfset var deploymentHistory=deviceService.getDeploymentHisto(ARGUMENTS["serialNumber"],ARGUMENTS["imei"])>
		<cfset var historyItem="NULL">
		<cfloop index="i" from="1" to="#arrayLen(deploymentHistory)#">
			<cfset historyItem=deploymentHistory[i]>
			<cfset deploymentHistoryResult[i]={
				ACTION=historyItem.getAction(), DATE=historyItem.getDate().getTime(), RES_TYPE=historyItem.getResType(),
				SERIAL=historyItem.getSerial(), STATUS=historyItem.getStatus(), OWNER=historyItem.getOwner(), ARG1=historyItem.getArg1(), ARG2=historyItem.getArg2()
			}>
		</cfloop>
		<cfreturn serializeJSON(deploymentHistoryResult)>
	</cffunction>
	
	<cffunction access="remote" name="getLastUser" returntype="String" hint="Retourne le login du dernier utilisateur Zenprise de l'équipement ou une chaine vide s'il n'est pas défini<br>
	Ne fonctionne que pour un iPhone et lève une exception com.zenprise.zdm.core.device.NoDeviceFoundException pour les autres équipements (e.g iPad, Samsung, etc...)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement">
		<cfset var deviceService=getManagementService().getDeviceService()>
		<cfset var wsLastUser=deviceService.getLastUser(ARGUMENTS["serialNumber"],ARGUMENTS["imei"])>
		<cfreturn serializeJSON(isDefined("wsLastUser") ? wsLastUser:"")>
	</cffunction>
	
	<cffunction access="remote" name="getManagedStatus" returntype="String"
	hint="Retourne ZDM pour un équipement géré par le MDM, UNMANAGED pour non géré et UNKNOWN sinon. Retourne toujours UNMANAGED pour tout équipement autre que l'iPhone">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfset var deviceService=getManagementService().getDeviceService()>
		<cfset var wsManagedStatus=deviceService.getManagedStatus(ARGUMENTS["serialNumber"],ARGUMENTS["imei"])>
		<cfreturn serializeJSON(isDefined("wsManagedStatus") ? wsManagedStatus:"UNKNOWN")>
	</cffunction>
	
	<cffunction access="remote" name="deviceExists" returntype="String" hint="Retourne TRUE si un équipement correspondant aux paramètres fournis existe">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfset var deviceService=getManagementService().getDeviceService()>
		<cfset var wsDeviceExists=deviceService.deviceExists(ARGUMENTS["serialNumber"],ARGUMENTS["imei"])>
		<cfreturn serializeJSON(wsDeviceExists)>
	</cffunction>

	<cffunction access="remote" name="getAllDevices" returntype="String" hint="Retourne un tableau contenant la liste des équipements mobiles.
	Chaque équipement contient les propriétés suivantes : IMEI, serialNumber, strongID (Les valeurs de ces champs sont préfixés et suffixés par le caractère espace)">
		<cfset var deviceService=getManagementService().getDeviceService()>
		<cfset var wsAllDevices=deviceService.getAllDevices()>
		<cfset var cfmxAllDevices=[]>
		<cfset var deviceNum=arrayLen(wsAllDevices)>
		<cfloop index="i" from="1" to="#deviceNum#">
			<cfset var currentDevice=wsAllDevices[i]>
			<cfset cfmxAllDevices[i]={
				imei=" " & currentDevice.getImei() & " ",serialNumber=" " & currentDevice.getSerialNumber() & " ", strongID=" " & currentDevice.getStrongID() & " "
			}>
		</cfloop>
		<cfreturn serializeJSON(cfmxAllDevices)>
	</cffunction>
	
	<cffunction access="remote" name="getDeviceInfo" returntype="String" hint="Vérifie si l'équipement correspondant existe et retourne les infos le concernant.
	<br>Une chaine vide ou un tableau vide est affecté comme valeur des propriétés si la valeur imei ne correspond à aucun équipement existant<br>
	Un équipement contient les informations suivantes : IMEI, serialNumber, firstConnectionDate (Chaine vide si non définie), lastAuthDate (Chaine vide si non définie)<br>
	- deviceProperties (Tableau dont chaque élément contient les propriétés suivantes) : name, value<br>
	- softwareInventory (Tableau dont chaque élément contient les propriétés suivantes) : name, author, version, size, installTimeStamp<br>
	<b>Les valeurs des champs IMEI et serialNumber sont préfixés et suffixés par le caractère espace</b>">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfset var deviceService=getManagementService().getDeviceService()>
		<cfset var wsDeviceExists=deviceService.deviceExists(ARGUMENTS["serialNumber"],ARGUMENTS["imei"])>
		<cfset var cfmxDeviceInfo={targetNode="", imei="", serialNumber="", firstConnectionDate="", lastAuthDate="", deviceProperties=[], softwareInventory=[]}>
		<cfif wsDeviceExists>
			<cfset var wsDeviceInfo=deviceService.getDeviceInfo(ARGUMENTS["serialNumber"],ARGUMENTS["imei"])>
			<cfset structAppend(cfmxDeviceInfo,{
				imei=" " & wsDeviceInfo.getIMEI() & " ", serialNumber=" " & wsDeviceInfo.getSerialNumber() & " "
			},TRUE)>
			<!--- Méthode getTargetNode() --->
			<cfset var targetNode=wsDeviceInfo.getTargetNode()>
			<cfset cfmxDeviceInfo.targetNode=isDefined("targetNode") ? targetNode:"">
			<!--- Méthode getFirstConnectionDate() et getLastAuthDate() --->
			<cfset var wsDate=wsDeviceInfo.getFirstConnectionDate()>
			<cfif isDefined("wsDate")>
				<cfset cfmxDeviceInfo.firstConnectionDate=parseDateTime(dateFormat(wsDate) & " " & timeFormat(wsDate))>
			</cfif>
			<!--- Méthode getLastAuthDate() --->
			<cfset wsDate=wsDeviceInfo.getLastAuthDate()>
			<cfif isDefined("wsDate")>
				<cfset cfmxDeviceInfo.lastAuthDate=parseDateTime(dateFormat(wsDate) & " " & timeFormat(wsDate))>
			</cfif>
			<!--- Méthode getDeviceProperties() --->
			<cfset var wsArray=wsDeviceInfo.getDeviceProperties()>
			<cfif isDefined("wsArray")>
				<cfset var arrayNum=arrayLen(wsArray)>
				<cfloop index="i" from="1" to="#arrayNum#">
					<cfset var deviceProp=wsArray[i]>
					<cfset cfmxDeviceInfo["deviceProperties"][i]={
						name=toString(deviceProp.getName()), value=toString(deviceProp.getValue())
					}>
				</cfloop>
			</cfif>
			<!--- Méthode getSoftwareInventory() --->
			<cfset var wsArray=wsDeviceInfo.getSoftwareInventory()>
			<cfif isDefined("wsArray")>
				<cfset var arrayNum=arrayLen(wsArray)>
				<cfloop index="i" from="1" to="#arrayNum#">
					<cfset var software=wsArray[i]>
					<cfset cfmxDeviceInfo["softwareInventory"][i]={
						name=toString(software.getName()), author=toString(software.getAuthor()), version=toString(software.getVersion()),
						size=VAL(toString(software.getSize())), installTimeStamp=VAL(toString(software.getInstallTimeStamp()))
					}>
				</cfloop>
			</cfif>
		</cfif>
		<cfreturn serializeJSON(cfmxDeviceInfo)>
	</cffunction>
	
	<cffunction access="remote" name="lockDevice" returntype="String" hint="Vérrouille l'équipement spécifié avec le nouveau code PIN newPinCode
	<br>Retourne une des valeurs suivantes : 0 (Succès), 1 (Si Connecté), 2 (Si Déconnecté), 3 (Echec)
	<br><b>Lève une exception provenant du Web Service si la valeur imei ne correspond à aucun équipement existant</b>">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="newPinCode" type="String" required="true" hint="Nouveau code PIN du vérrouillage (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="waitTime" type="Numeric" required="true" hint="Délai d'attente pour de la réponse de l'opération en millisecondes">
		<cfset var deviceService=getManagementService().getDeviceService()>
		<cfset var lockStatusCode=deviceService.lockDevice(ARGUMENTS["serialNumber"],ARGUMENTS["imei"],ARGUMENTS["newPinCode"],ARGUMENTS["waitTime"])>
		<cfreturn serializeJSON(lockStatusCode)>
	</cffunction>
	
	<cffunction access="remote" name="unlockDevice" returntype="String" hint="Retourne TRUE si un équipement correspondant aux paramètres fournis existe
	<br>Retourne une des valeurs suivantes : 0 (Succès), 1 (Si Connecté), 2 (Si Déconnecté), 3 (Echec)
	<br><b>Lève une exception provenant du Web Service si la valeur imei ne correspond à aucun équipement existant</b>">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="waitTime" type="Numeric" required="true" hint="Délai d'attente pour de la réponse de l'opération en millisecondes">
		<cfset var deviceService=getManagementService().getDeviceService()>
		<cfset var unlockStatusCode=deviceService.unlockDevice(ARGUMENTS["serialNumber"],ARGUMENTS["imei"],ARGUMENTS["waitTime"])>
		<cfreturn serializeJSON(unlockStatusCode)>
	</cffunction>
	
	<cffunction access="remote" name="wipeDevice" returntype="String" hint="Effectue l'opération WIPE pour l'équipement existant correspondant aux paramètres fournis
	Retourne une des valeurs suivantes : 0 (Succès), 1 (Si Connecté), 2 (Si Déconnecté), 3 (Echec)
	<br><b>Lève une exception provenant du Web Service si la valeur imei ne correspond à aucun équipement existant</b>">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="erasedMemoryCard" type="Boolean" required="true" hint="TRUE pour supprimer le contenu de la carte mémoire">
		<cfargument name="waitTime" type="Numeric" required="true" hint="Délai d'attente pour de la réponse de l'opération en millisecondes">
		<cfset var deviceService=getManagementService().getDeviceService()>
		<cfset var wipeStatusCode=deviceService.wipeDevice(ARGUMENTS["serialNumber"],ARGUMENTS["imei"],ARGUMENTS["erasedMemoryCard"],ARGUMENTS["waitTime"])>
		<cfreturn serializeJSON(wipeStatusCode)>
	</cffunction>

	<cffunction access="remote" name="revokeDevice" returntype="String" hint="Effectue l'opération REVOKE pour l'équipement existant correspondant aux paramètres fournis
	<br><b>Lève une exception provenant du Web Service si la valeur imei ne correspond à aucun équipement existant</b>">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfset var deviceService=getManagementService().getDeviceService()>
		<cfset var revokeStatus=deviceService.revokeDevice(ARGUMENTS["serialNumber"],ARGUMENTS["imei"])>
		<cfreturn serializeJSON(revokeStatus)>
	</cffunction>
	
	<cffunction access="remote" name="removeDevice" returntype="void" hint="Effectue l'opération REMOVE pour l'équipement existant correspondant aux paramètres fournis
	<br><b>Lève une exception provenant du Web Service si la valeur imei ne correspond à aucun équipement existant</b>">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfset var deviceService=getManagementService().getDeviceService()>
		<cfset deviceService.removeDevice(ARGUMENTS["serialNumber"],ARGUMENTS["imei"])>
	</cffunction>
	
	<cffunction access="remote" name="authorize" returntype="String" hint="Effectue l'opération AUTHORIZE pour l'équipement existant correspondant aux paramètres fournis<br>
	La valeur retournée n'est pas spécifiée par la documentation Zenprise'">
		<cfargument name="serial" type="String" required="true" hint="Serial de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfset var deviceService=getManagementService().getDeviceService()>
		<cfset var authorizeStatus=deviceService.authorize(ARGUMENTS["serial"],ARGUMENTS["imei"])>
		<cfreturn serializeJSON(authorizeStatus)>
	</cffunction>
	
	<cffunction access="remote" name="deviceDeploy" returntype="String"
	hint="(Device Service) Effectue une demande de déploiement d'Android/Windows pour un équipement connecté. Envoie un notification PUSH pour iOS<br>
	Retourne FALSE si l'équipement n'est pas connecté ou non managé et TRUE sinon">
		<cfargument name="serial" type="String" required="true" hint="Serial de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfset var deviceService=getManagementService().getDeviceService()>
		<cfset var deployStatus=deviceService.deploy(ARGUMENTS["serial"],ARGUMENTS["imei"])>
		<cfreturn serializeJSON(deployStatus)> 
	</cffunction>
	
	<cffunction access="remote" name="corporateDataWipeDevice" returntype="String" hint="Effectue l'opération WIPE pour l'équipement<br>
	Retourne une des valeurs suivantes : 0 (Succès), 1 (Si Connecté), 2 (Si Déconnecté), 3 (Echec)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="waitTime" type="Numeric" required="true" hint="Délai d'attente pour de la réponse de l'opération en millisecondes">
		<cfset var deviceService=getManagementService().getDeviceService()>
		<cfset var wipeStatus=deviceService.corporateDataWipeDevice(ARGUMENTS["serialNumber"],ARGUMENTS["imei"],ARGUMENTS["waitTime"])>
		<cfreturn serializeJSON(wipeStatus)>
	</cffunction>
	
	<cffunction access="remote" name="createUser" returntype="String" hint="Crée un utilisateur et retourne son login">
		<cfargument name="group" type="String" required="true" hint="Nom du groupe pour l'utilisateur">
		<cfargument name="login" type="String" required="true" hint="Login pour l'utilisateur">
		<cfargument name="password" type="String" required="true" hint="Mot de passe pour l'utilisateur">
		<cfargument name="role" type="String" required="true" hint="Rôle pour l'utilisateur (i.e : ADMIN, SUPPORT, USER, GUEST)">
		<cfset var userLogin = super.createUser(ARGUMENTS["group"],ARGUMENTS["login"],ARGUMENTS["password"],ARGUMENTS["role"])>
		<cfreturn serializeJSON(userLogin)>
	</cffunction>
	
	<cffunction access="remote" name="locateDevice" returntype="String" hint="Localise l'équipement.
	Retourne une des valeurs suivantes : 0 (Succès), 1 (Si Connecté), 2 (Si Déconnecté), 3 (Echec)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="waitTime" type="Numeric" required="true" hint="Délai d'attente pour de la réponse de l'opération en millisecondes">
		<cfset var deviceService=getManagementService().getDeviceService()>
		<cfset var locateStatus=deviceService.locateDevice(ARGUMENTS["serialNumber"],ARGUMENTS["imei"],ARGUMENTS["waitTime"])>
		<cfreturn serializeJSON(locateStatus)>
	</cffunction>
	
	<cffunction access="remote" name="putDeviceProperties" returntype="Void" hint="Effectue l'opération putDeviceProperties() du service DeviceService Zenprise">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="properties" type="Array" required="true"
		hint="Tableau où chaque élément est une structure contenant les clés suivantes : NAME (Nom de la propriété), VALUE (Valeur de la propriété)">
		<cfset var deviceService=getManagementService().getDeviceService()>
		<cfset deviceService.putDeviceProperties(ARGUMENTS["serialNumber"],ARGUMENTS["imei"],ARGUMENTS["properties"])>
	</cffunction>
	
	<cffunction access="remote" name="registerDeviceForUser" returntype="String"
	hint="Spécifie que l'équipement est utilisé par l'utilisateur. Crée l'équipement s'il n'existe pas et retourne la valeur STRONGID associé à l'équipement">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Case Sensisite (i.e : iOS, WINDOWS, ANDROID)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="userIdentifier" type="String" required="true" hint="Identifiant utilisateur">
		<cfset var deviceService=getManagementService().getDeviceService()>
		<cfset var registerStatus=deviceService.registerDeviceForUser(ARGUMENTS["osFamily"],ARGUMENTS["serialNumber"],ARGUMENTS["imei"],ARGUMENTS["userIdentifier"])>
		<cfreturn serializeJSON(registerStatus)>
	</cffunction>

	<cffunction access="remote" name="resetDeploymentState" returntype="Void" hint="Effectue l'opération resetDeploymentState() pour l'équipement">
		<cfargument name="serial" type="String" required="true" hint="Serial de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfset var deviceService=getManagementService().getDeviceService()>
		<cfset deviceService.resetDeploymentState(ARGUMENTS["serial"],ARGUMENTS["imei"])>
	</cffunction>
</cfcomponent>