<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.public.FSecureMultiLicence" extends="fr.consotel.api.mdm.partner.public.FSecureAntiTheft" hint="Implémentation MultiLicence FSecure">
	<!--- Multi Licence Service --->
	<cffunction access="remote" name="deleteMultiLicense" returntype="String" hint="Supprime une licence multiple. Retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfset var wsService=getSecurityService().getMultiLicenseService()>
		<cfreturn serializeJSON(wsService.deleteMultiLicense(ARGUMENTS["multiLicenseKey"]))>
	</cffunction>
	
	<cffunction access="remote" name="searchMultiLicensesInCorporation" returntype="String" hint="Recherche les licences multiples d'une corporation et retourne 40 résultats par page de résultat
	La structure retournée contient les clés suivantes :<br>
	- IsLastPage : TRUE si la page retournée est la dernière page et FALSE sinon<br>
	- LastPageNumber : Numéro de la dernière page de la recherche<br>
	- TotalSearchResults : Nombre total de résultats de la recherche<br>
	- PageNumber : Numéro de la page retournée<br>
	- SearchResultsOnPage: Nombre de résultat de la page retournée<br><br>
	- MultilicenseRTO : Tableau où chaque élément contient les clés suivantes :<br>
	- MultiLicenseKey : Clé de la licence multiple<br>
	- MaxNroOfLicenses : Nombre max de single licence pouvant etre activées<br>
	- UsedSingleLicenses : Nombre de single licence utilisées<br>
	- AvailableSingleLicenses : Nombre de single licence disponible<br>
	- ExpDate : Date d'expiration de la licence multiple<br>
	- Status : Statut de la licence multiple i.e Active ou Inactive">
		<cfargument name="corporationId" type="String" required="true" hint="Identifiant de la corporation">
		<cfargument name="pageNumber" type="String" required="true" hint="Numéro de la page de résultat à retourner">
		<cfset var wsService=getSecurityService().getMultiLicenseService()>
		<cfset var wsResult=wsService.searchMultiLicensesInCorporation(ARGUMENTS["corporationId"],ARGUMENTS["pageNumber"])>
		<cfset var resultList={
			IsLastPage=toString(wsResult.getIsLastPage()), LastPageNumber=toString(wsResult.getLastPageNumber()), TotalSearchResults=toString(wsResult.getTotalSearchResults()),
			PageNumber=toString(wsResult.getPageNumber()), SearchResultsOnPage=toString(wsResult.getSearchResultsOnPage())
		}>
		<cfset var wsRTO=wsResult.getMultilicenseRTO()>
		<cfif isDefined("wsRTO")>
			<cfset var rtoCount=arrayLen(wsRTO)>
			<cfloop index="i" from="1" to="#rtoCount#">
				<cfset resultList.MultilicenseRTO[i]={
					MultiLicenseKey=toString(wsRTO[i].getMultiLicenseKey()), MaxNroOfLicenses=toString(wsRTO[i].getMaxNroOfLicenses()), UsedSingleLicenses=toString(wsRTO[i].getUsedSingleLicenses()),
					AvailableSingleLicenses=toString(wsRTO[i].getAvailableSingleLicenses()), ExpDate=toString(wsRTO[i].getExpDate()), Status=toString(wsRTO[i].getStatus())
				}>
			</cfloop>
		</cfif>
		<cfreturn serializeJSON(resultList)>
	</cffunction>

	<cffunction access="remote" name="createMultiLicense" returntype="String" hint="Crée une licence multiple dans la corporation par défaut et retourne la clé correspondante">
		<cfargument name="expirationDate" type="String" required="true" hint="Date d'expiration de la licence au format YYYY-MM-DD. Format optionnel du time zone : EET (GMT+2, GMT+3)">
		<cfargument name="maxNumberOfLicenses" type="String" required="true" hint="Nombre max de single licence qui peuvent être activée dans la licence multiple à créer">
		<cfset var wsService=getSecurityService().getMultiLicenseService()>
		<cfreturn serializeJSON(wsService.createMultiLicense(ARGUMENTS["expirationDate"],ARGUMENTS["maxNumberOfLicenses"]))>
	</cffunction>
	
	<cffunction access="remote" name="createMultiLicenseInCorporation" returntype="String" hint="Crée une licence multiple dans une corporation et retourne la clé correspondante">
		<cfargument name="corporationId" type="String" required="true" hint="Identifiant de la corporation">
		<cfargument name="expirationDate" type="String" required="true" hint="Date d'expiration de la licence au format YYYY-MM-DD. Format optionnel du time zone : EET (GMT+2, GMT+3)">
		<cfargument name="maxNumberOfLicenses" type="String" required="true" hint="Nombre max de single licence qui peuvent être activées dans la licence multiple">
		<cfset var wsService=getSecurityService().getMultiLicenseService()>
		<cfreturn serializeJSON(wsService.createMultiLicenseInCorporation(ARGUMENTS["corporationId"],ARGUMENTS["expirationDate"],ARGUMENTS["maxNumberOfLicenses"]))>
	</cffunction>

	<cffunction access="remote" name="updateMultiLicenseExpirationDate" returntype="String"
	hint="Met à jour la date d'expiration d'une licence multiple et retourne TRUE si en cas de succès ou FALSE sinon">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfargument name="expirationDate" type="String" required="true" hint="Date d'expiration de la licence au format YYYY-MM-DD. Format optionnel du time zone : EET (GMT+2, GMT+3)">
		<cfset var wsService=getSecurityService().getMultiLicenseService()>
		<cfreturn serializeJSON(wsService.updateMultiLicenseExpirationDate(ARGUMENTS["multiLicenseKey"],ARGUMENTS["expirationDate"]))>
	</cffunction>

	<cffunction access="remote" name="updateMultiLicenseMaximumNumberOfLicenses" returntype="String"
	hint="Met à jour le nombre max de licence d'une licence multiple et retourne TRUE si en cas de succès ou FALSE sinon">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfargument name="maxNumberOfLicenses" type="String" required="true" hint="Nombre max de single licence qui peuvent être activées dans la licence multiple">
		<cfset var wsService=getSecurityService().getMultiLicenseService()>
		<cfreturn serializeJSON(wsService.updateMultiLicenseMaximumNumberOfLicenses(ARGUMENTS["multiLicenseKey"],ARGUMENTS["maxNumberOfLicenses"]))>
	</cffunction>
	
	<cffunction access="remote" name="updateMultiLicenseStatus" returntype="String" hint="Met à jour le statut d'une licence multiple et retourne TRUE si en cas de succès ou FALSE sinon">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfargument name="multLicenseStatus" type="String" required="true" hint="Statut de la licence. Valeurs possibles : Activate, Inactivate">
		<cfset var wsService=getSecurityService().getMultiLicenseService()>
		<cfreturn serializeJSON(wsService.updateMultiLicenseStatus(ARGUMENTS["multiLicenseKey"],ARGUMENTS["multLicenseStatus"]))>
	</cffunction>

	<cffunction access="remote" name="getMultiLicenseExpirationDate" returntype="String"
	hint="Retourne la date d'expiration d'un licence multiple au format YYYY-MM-DD. Format optionnel du time zone : EET (GMT+2, GMT+3)">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfset var wsService=getSecurityService().getMultiLicenseService()>
		<cfreturn serializeJSON(wsService.getMultiLicenseExpirationDate(ARGUMENTS["multiLicenseKey"]))>
	</cffunction>

	<cffunction access="remote" name="getMultiLicenseStatus" returntype="String" hint="Retourne le statut d'une licence multiple">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfset var wsService=getSecurityService().getMultiLicenseService()>
		<cfreturn serializeJSON(wsService.getMultiLicenseStatus(ARGUMENTS["multiLicenseKey"]))>
	</cffunction>
	
	<cffunction access="remote" name="updateMultiLicensePhoneNumberRegistration" returntype="String" 
	hint="Met à jour le mode d'enregistrement d'une licence multiple. Retourne TRUE si en cas de succès ou FALSE sinon">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfargument name="phoneNumberRegistration" type="String" required="true" hint="Mode d'enregistrement<br>
		Valeurs possibles : 0 (Enregistrement non envoyé), 1 (Demande confirmation utilisateur avant l'envoi), 2 (Envoi l'enregistrement sans confirmation utilisateur)">
		<cfset var wsService=getSecurityService().getMultiLicenseService()>
		<cfreturn serializeJSON(wsService.updateMultiLicensePhoneNumberRegistration(ARGUMENTS["multiLicenseKey"],ARGUMENTS["phoneNumberRegistration"]))>
	</cffunction>
	
	<cffunction access="remote" name="getMultiLicenseProperties" returntype="String"
	hint="Retourne les informations contractuelles d'une licence multiple dans un tableau où chaque élément contenant les clés suivantes<br>
	- MultiLicenseKey : Clé de la licence multiple<br>
	- MaxNroOfLicenses : Nombre max de single licence pouvant etre activées dans la licence multiple<br>
	- UsedSingleLicenses : Nombre de single licence utilisées<br>
	- AvailableSingleLicenses : Nombre de single licence disponibles<br>
	- ExpDate : Date d'expiration de la licence multiple<br>
	- Status : Statut de la licence multiple i.e Active ou Inactive">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfset var resultList=[]>
		<cfset var wsService=getSecurityService().getMultiLicenseService()>
		<cfset var wsResult=wsService.getMultiLicenseProperties(ARGUMENTS["multiLicenseKey"])>
		<cfset var wsRTO=wsResult.getMultilicenseRTO()>
		<cfif isDefined("wsRTO")>
			<cfset var rtoCount=arraylen(wsRTO)>
			<cfloop index="i" from="1" to="#rtoCount#">
				<cfset resultList[i]={
					MultiLicenseKey=toString(wsRTO[i].getMultiLicenseKey()), MaxNroOfLicenses=toString(wsRTO[i].getMaxNroOfLicenses()), UsedSingleLicenses=toString(wsRTO[i].getUsedSingleLicenses()),
					AvailableSingleLicenses=toString(wsRTO[i].getAvailableSingleLicenses()), ExpDate=toString(wsRTO[i].getExpDate()), Status=toString(wsRTO[i].getStatus())
				}>
			</cfloop>
		</cfif>
		<cfreturn serializeJSON(resultList)>
	</cffunction>

	<cffunction access="remote" name="getMultiLicenseCount" returntype="String" hint="Retourne le nombre de licence multiples dans la corporation par défaut">
		<cfset var wsService=getSecurityService().getMultiLicenseService()>
		<cfreturn serializeJSON(wsService.getMultiLicenseCount())>
	</cffunction>
	
	<cffunction access="remote" name="getMultiLicenseCountInCorporation" returntype="String" hint="Retourne le nombre de licence multiples d'une corporation">
		<cfargument name="corporationId" type="String" required="true" hint="Identifiant de la corporation">
		<cfset var wsService=getSecurityService().getMultiLicenseService()>
		<cfreturn serializeJSON(wsService.getMultiLicenseCountInCorporation(ARGUMENTS["corporationId"]))>
	</cffunction>
	
	<cffunction access="remote" name="searchMultiLicenses" returntype="String" hint="Retourne le résultat d'une recherche de licence multiple dans la corporation par défaut
	Les clés de la structure retournée sont :<br>
	- PageNumber : Numéro de la page de résultat retournée<br>
	- SearchResultsOnPage : Nombre de résultat de la page retournée<br>
	- IsLastPage : TRUE si la page retournée est la dernière page<br>
	- LastPageNumber : Numéro de la dernière page de résultat<br>
	- TotalSearchResults : Nombre total de résultats de la recherche<br><br>
	- MultilicenseRTO : Tableau contenant les résultat de la recherche. Chaque élément du tableau contient les éléments suivants :<br>
	- MultiLicenseKey : Clé d'une licence multiple<br>
	- MaxNroOfLicenses : Nombre max de single licence pouvant être activées dans la licence multiple<br>
	- UsedSingleLicenses : Nombre de single licence utilisées<br>
	- AvailableSingleLicenses : Nombre de single licence disponibles<br>
	- ExpDate : Date d'expiration de la licence multiple<br>
	- Status : Statut de la licence multiple i.e Active ou Inactive">
		<cfargument name="pageNumber" type="String" required="true" hint="Numéro de la page de résultat à retourner">
		<cfset var wsService=getSecurityService().getMultiLicenseService()>
		<cfset var wsResult=wsService.searchMultiLicenses(ARGUMENTS["pageNumber"])>
		<cfset var resultList={
			PageNumber=toString(wsResult.getPageNumber()), SearchResultsOnPage=toString(wsResult.getSearchResultsOnPage()), IsLastPage=toString(wsResult.getIsLastPage()),
			LastPageNumber=toString(wsResult.getLastPageNumber()), TotalSearchResults=toString(wsResult.getTotalSearchResults()),MultilicenseRTO=[]
		}>
		<cfset var wsRTO=wsResult.getMultilicenseRTO()>
		<cfif isDefined("wsRTO")>
			<cfset var rtoCount=arrayLen(wsRTO)>
			<cfloop index="i" from="1" to="#rtoCount#">
				<cfset resultList.MultilicenseRTO[i]={
					MultiLicenseKey=toString(wsRTO[i].getMultiLicenseKey()), MaxNroOfLicenses=toString(wsRTO[i].getMaxNroOfLicenses()),
					UsedSingleLicenses=toString(wsRTO[i].getUsedSingleLicenses()), AvailableSingleLicenses=toString(wsRTO[i].getAvailableSingleLicenses()),
					ExpDate=toString(wsRTO[i].getExpDate()), Status=toString(wsRTO[i].getStatus())
				}>
			</cfloop>
		</cfif>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="getMultiLicenseReport" returntype="String" hint="Retourne les statistiques d'une licence multiple en tant que structure contenant les clé suivantes :
	MaxNumberOfLicenses (Nombre total de single licence activées), UsedSingleLicenses (Nombre de single licence utilisées), AvailableSingleLicenses (Nombre de single licence disponibles)">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfset var wsService=getSecurityService().getMultiLicenseService()>
		<cfset var wsResult=wsService.getMultiLicenseReport(ARGUMENTS["multiLicenseKey"])>
		<cfset var infos={
			MaxNumberOfLicenses=toString(wsResult.getMaxNroOfLicenses()),UsedSingleLicenses=toString(wsResult.getUsedSingleLicenses()),
			AvailableSingleLicenses=toString(wsResult.getAvailableSingleLicenses())
		}>
		<cfreturn serializeJSON(infos)>
	</cffunction>
	
	<cffunction access="remote" name="setDefaultClientActivationState" returntype="String"
	hint="Définit l'état d'activation par défaut du client pour une licence multiple. Retourne TRUE si en cas de succès ou FALSE sinon">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfargument name="state" type="String" required="true" hint="Etat d'activation par défaut. Valeur possibles : 1 (VALID), 2 (TRIAL), 3 (SUBSCRIBED)">
		<cfset var wsService=getSecurityService().getMultiLicenseService()>
		<cfreturn serializeJSON(wsService.setDefaultClientActivationState(ARGUMENTS["multiLicenseKey"],ARGUMENTS["state"]))>
	</cffunction>
	
	<cffunction access="remote" name="getDefaultMultiLicense" returntype="String" hint="Retourne un tableau contenant les infos de la licence multiple par défaut du partenaire<br>
	Chaque élément du tableau contient les propriétés suivantes :<br>
	- MultiLicenseKey : Clé d'activation de la licence multiple<br>
	- MaxNumberOfLicenses : Nombre max de single licence pouvant être activée dans la licence multiple<br>
	- UsedSingleLicenses : Nombre de single licence utilisées dans la licence multiple<br>
	- AvailableSingleLicenses : Nombre de single licence disponibles dans la licence multiple<br>
	- ExpDate : Date d'expiration de la licence au format<br>
	- Status : Statut de la license i.e : Active ou Inactive">
		<cfset var resultList=[]>
		<cfset var wsService=getSecurityService().getMultiLicenseService()>
		<cfset var wsResult=wsService.getDefaultMultiLicense()>
		<cfset var wsRTO=wsResult.getMultilicenseRTO()>
		<cfif isDefined("wsRTO")>
			<cfset var rtoCount=arrayLen(wsRTO)>
			<cfloop index="i" from="1" to="#rtoCount#">
				<cfset resultList[i]={
					MultiLicenseKey=toString(wsRTO[i].getMultiLicenseKey()), MaxNumberOfLicenses=toString(wsRTO[i].getMaxNroOfLicenses()),
					UsedSingleLicenses=toString(wsRTO[i].getUsedSingleLicenses()), AvailableSingleLicenses=toString(wsRTO[i].getAvailableSingleLicenses()),
					ExpDate=toString(wsRTO[i].getExpDate()), Status=toString(wsRTO[i].getStatus())
				}>
			</cfloop>
		</cfif>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
</cfcomponent>