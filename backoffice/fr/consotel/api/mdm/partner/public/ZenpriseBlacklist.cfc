<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.public.ZenpriseBlacklist" extends="fr.consotel.api.mdm.partner.public.ZenpriseBase"
hint="Implémentation Blacklist. Une exception provenant du web service est systématiquement levée si une liste qui n'existe pas est utilisée dans ces méthodes">
	<!--- Blacklist Service --->
	<cffunction access="remote" name="getAllApplicationsBlackList" returntype="String" hint="Retourne un tableau contenant la liste des noms des applications en blacklist définis sur le serveur d'un OS">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfset var blacklistService=getManagementService().getBlacklistService()>
		<cfreturn serializeJSON(blacklistService.getAllApplicationsBlackList(ARGUMENTS["osFamily"]))>
	</cffunction>
	
	<cffunction access="remote" name="getApplicationsInBlacklist" returntype="String" hint="Retourne un tableau contenant la liste des applications d'une blacklist d'un OS<br>
	Chaque élément du tableau contient les propriétés suivantes : identifier, name">
		<cfargument name="applicationsBlacklistName" type="String" required="true" hint="Nom d'une blacklist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfset var blacklistService=getManagementService().getBlacklistService()>
		<cfreturn serializeJSON(blacklistService.getApplicationsInBlacklist(ARGUMENTS["applicationsBlacklistName"],ARGUMENTS["osFamily"]))>
	</cffunction>
	
	<cffunction access="remote" name="getDefaultBlacklist" returntype="String" hint="Retourne un tableau contenant la liste des application de la blacklist par défaut d'un OS<br>
	Chaque élément du tableau contient les propriétés suivantes : identifier, name">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfset var blacklistService=getManagementService().getBlacklistService()>
		<cfreturn serializeJSON(blacklistService.getDefaultBlacklist(ARGUMENTS["osFamily"]))>
	</cffunction>
	
	<cffunction access="remote" name="addBlackList" returntype="Void" hint="Ajoute une blacklist d'un OS et lève une exception si <b>name</b> existe déjà<br>
	<b>Bug (Zenprise) :</b> La valeur de la propriété <b>name</b> est toujours utilisée à la fois comme nom et identifiant  pour chaque application fournie">
		<cfargument name="name" type="String" required="true" hint="Nom pour la blacklist">
		<cfargument name="description" type="String" required="true" hint="Description à associer à la blacklist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applications" type="Array" required="true" hint="Applications de la blacklist. Chaque élément du tableau est une structure contenant les clé suivantes : identifier, name">
		<cfset var blacklistService=getManagementService().getBlacklistService()>
		<cfset blacklistService.addBlackList(ARGUMENTS["name"],ARGUMENTS["description"],ARGUMENTS["osFamily"],ARGUMENTS["applications"])>
	</cffunction>
	
	<cffunction access="remote" name="updateBlackList" returntype="Void" hint="Met à jour une blacklist d'un OS et lève une exception si <b>name</b> existe déjà<br>
	<b>Bug (Zenprise) :</b> Cette méthode lève toujours une exception car elle ne fonctionne pas (error code [1407]; Could not execute JDBC batch update. QueryTimeoutException)">
		<cfargument name="name" type="String" required="true" hint="Nom pour la blacklist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="newName" type="String" required="true" hint="Nouveau nom pour la blacklist">
		<cfargument name="newDescription" type="String" required="true" hint="Nouvelle description à associer à la blacklist">
		<cfargument name="applications" type="Array" required="true" hint="Applications de la blacklist. Chaque élément du tableau est une structure contenant les clé suivantes : identifier, name">
		<cfset var blacklistService=getManagementService().getBlacklistService()>
		<cfset blacklistService.updateBlackList(ARGUMENTS["name"],ARGUMENTS["osFamily"],ARGUMENTS["newName"],ARGUMENTS["newDescription"],ARGUMENTS["applications"])>
	</cffunction>
	
	<cffunction access="remote" name="updateDefaultBlackList" returntype="Void" hint="Met à jour la blacklist par défaut d'un OS<br>
	<b>Bug (Zenprise) :</b> Cette méthode lève toujours une exception car Zenprise n'autorise pas cette opération une fois la liste par défaut existante">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applications" type="Array" required="true" hint="Applications de la blacklist. Chaque élément du tableau est une structure contenant les clé suivantes : identifier, name">
		<cfset var blacklistService=getManagementService().getBlacklistService()>
		<cfset blacklistService.updateDefaultBlackList(ARGUMENTS["osFamily"],ARGUMENTS["applications"])>
	</cffunction>
	
	<cffunction access="remote" name="removeBlackList" returntype="Void" hint="Supprime une blacklist d'un OS">
		<cfargument name="name" type="String" required="true" hint="Nom pour la blacklist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfset var blacklistService=getManagementService().getBlacklistService()>
		<cfset blacklistService.removeBlackList(ARGUMENTS["name"],ARGUMENTS["osFamily"])>
	</cffunction>
	
	<cffunction access="remote" name="addApplicationToBlackList" returntype="Void" hint="Ajoute une application dans une blacklist d'un OS">
		<cfargument name="name" type="String" required="true" hint="Nom pour la blacklist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationName" type="String" required="true" hint="Valeur de la propriété name pour l'application">
		<cfargument name="applicationIdentifier" type="String" required="true" hint="Valeur de la propriété identifier pour l'application">
		<cfset var blacklistService=getManagementService().getBlacklistService()>
		<cfset blacklistService.addApplicationToBlackList(ARGUMENTS["name"],ARGUMENTS["osFamily"],ARGUMENTS["applicationName"],ARGUMENTS["applicationIdentifier"])>
	</cffunction>
	
	<cffunction access="remote" name="addApplicationToDefaultBlackList" returntype="Void" hint="Ajoute une application dans la blacklist par défaut de l'OS">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationName" type="String" required="true" hint="Valeur de la propriété name pour l'application">
		<cfargument name="applicationIdentifier" type="String" required="true" hint="Valeur de la propriété identifier pour l'application">
		<cfset var blacklistService=getManagementService().getBlacklistService()>
		<cfset blacklistService.addApplicationToDefaultBlackList(ARGUMENTS["osFamily"],ARGUMENTS["applicationName"],ARGUMENTS["applicationIdentifier"])>
	</cffunction>
	
	<cffunction access="remote" name="updateApplicationInBlackList" returntype="Void" hint="Met à jour une application dans une blacklist d'un OS">
		<cfargument name="name" type="String" required="true" hint="Nom pour la blacklist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationOldName" type="String" required="true" hint="Valeur actuelle de la propriété name de l'application">
		<cfargument name="applicationNewName" type="String" required="true" hint="Nouvelle valeur pour de la propriété name de l'application">
		<cfargument name="applicationNewIdentifier" type="String" required="true" hint="Nouvelle valeur pour de la propriété identifier de l'application">
		<cfset var blacklistService=getManagementService().getBlacklistService()>
		<cfset blacklistService.updateApplicationInBlackList(
			ARGUMENTS["name"],ARGUMENTS["osFamily"],ARGUMENTS["applicationOldName"],ARGUMENTS["applicationNewName"],ARGUMENTS["applicationNewIdentifier"]
		)>
	</cffunction>
	
	<cffunction access="remote" name="updateApplicationInDefaultBlackList" returntype="Void" hint="Met à jour une application dans la blacklist par défaut d'un OS">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationOldName" type="String" required="true" hint="Valeur actuelle de la propriété name de l'application">
		<cfargument name="applicationNewName" type="String" required="true" hint="Nouvelle valeur pour de la propriété name de l'application">
		<cfargument name="applicationNewIdentifier" type="String" required="true" hint="Nouvelle valeur pour de la propriété identifier de l'application">
		<cfset var blacklistService=getManagementService().getBlacklistService()>
		<cfset blacklistService.updateApplicationInDefaultBlackList(
			ARGUMENTS["osFamily"],ARGUMENTS["applicationOldName"],ARGUMENTS["applicationNewName"],ARGUMENTS["applicationNewIdentifier"]
		)>
	</cffunction>
	
	<cffunction access="remote" name="removeApplicationFromBlackList" returntype="Void" hint="Supprime une application d'une blacklist d'un OS">
		<cfargument name="name" type="String" required="true" hint="Nom pour la blacklist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationName" type="String" required="true" hint="Valeur de la propriété name de l'application">
		<cfset var blacklistService=getManagementService().getBlacklistService()>
		<cfset blacklistService.removeApplicationFromBlackList(ARGUMENTS["name"],ARGUMENTS["osFamily"],ARGUMENTS["applicationName"])>
	</cffunction>
	
	<cffunction access="remote" name="removeApplicationFromDefaultBlackList" returntype="Void" hint="Supprime une application de la blacklist par défaut d'un OS">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationName" type="String" required="true" hint="Valeur de la propriété name de l'application">
		<cfset var blacklistService=getManagementService().getBlacklistService()>
		<cfset blacklistService.removeApplicationFromDefaultBlackList(ARGUMENTS["osFamily"],ARGUMENTS["applicationName"])>
	</cffunction>
</cfcomponent>