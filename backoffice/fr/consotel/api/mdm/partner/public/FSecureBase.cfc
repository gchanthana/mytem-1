<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.public.FSecureBase" extends="fr.consotel.api.mdm.partner.public.PublicAPI" hint="Implémentation de base FSecure">
	<!--- fr.consotel.api.mdm.partner.public.FSecure --->
	<cffunction access="package" name="getSecurityService" returntype="fr.consotel.api.mdm.partner.private.ws.ISecurityManagement" hint="Accès aux services FSecure">
		<cfif NOT structKeyExists(VARIABLES,"SECURITY_SERVICE")>
			<cfset VARIABLES["SECURITY_SERVICE"]=getServiceProvider().getSecurityService()>
		</cfif>
		<cfreturn VARIABLES["SECURITY_SERVICE"]>
	</cffunction>
</cfcomponent>