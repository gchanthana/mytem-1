<cfcomponent displayname="fr.consotel.api.mdm.CodeFuction" hint="Composant utilisé par la page de détection pour récupérer les infos serveur MDM">
	<cffunction name="get_ListeCodeFunction_ServeurMDM" access="Remote" returntype="Struct"
	hint="Retourne une structure contenant les infos serveur MDM utilisées par la page de détection. La structure retournée contient les clés suivantes :<br>
	servermdm : Adresse ou IP du serveur MDM<br>servermdmuser : Login d'enrollment au MDM<br>servermdmpwd : Mot de passe d'enrollment au MDM<br>
	codefunction : La valeur est toujours ZP001">
		<cfargument name="infos" type="Struct" required="true" hint="Structure contenant une clé INFOS associée à une structure contenant les clés suivantes :<br>
		IDRACINE : Identifiant de la racine à laquelle sont rattachées les infos serveur MDM retournées par cette méthode"/>
		<cfset var idGroupe=VAL(ARGUMENTS["infos"]["idracine"])>
		<cfset var dataSource=(isDefined("SESSION") AND structKeyExists(SESSION,"OFFREDSN")) ? SESSION.OFFREDSN:"ROCOFFRE">
		<cfstoredproc datasource="#datasource#" procedure="PKG_MDM.get_server_racine_v2">
			<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#idGroupe#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="p_infos">
		</cfstoredproc>
		<cfif p_infos.recordcount GT 0>
			<cfset var infosServer=structNew()>
			<cfset var infosServer.servermdmuser=p_infos['ENROLL_USERNAME'][1]>
			<cfset var infosServer.servermdmpwd=p_infos['ENROLL_PASSWORD'][1]>
			<cfset var infosServer.servermdm=p_infos['MDM'][1]>
			<cfset var infosServer.useSSL=yesNoFormat(p_infos["USE_SSL"][1])>
			<cfset var infosServer.codefunction="ZP001">
			<cfreturn infosServer>
		<cfelse>
			<cflog type="error" text="[M111 CodeFunction] PKG_MDM.get_server_racine_v2 recordcount is 0">
		</cfif>
	</cffunction>
</cfcomponent>