<cfcomponent displayname="MDMOutil" output="false">
	
	<cffunction name="getCodeFuntion" returntype="string">
		<cfreturn  "">
	</cffunction>
	
	<cffunction name="getMatrice">
		
		<cfargument name="idrow" type="numeric" required="true" >
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_MDM.get_detail_ligne">
			<cfprocparam cfsqltype="CF_SQL_inTEGER"  type="in" variable="p_idrow" value="#idrow#"/>
			<cfprocresult name="p_retour"/>
		</cfstoredproc>
		
		<cfreturn p_retour>
		
	</cffunction>

</cfcomponent>