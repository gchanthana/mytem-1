<cfcomponent displayname="API SMS PUBLIC" output="false" extends="fr.consotel.api.sms.Core">

	<cfset VARIABLES.SendMail=createobject("component","fr.consotel.api.sms.model.SendMail")>
	<cfset VARIABLES.mode=WS_MODE()>
	<cfset VARIABLES.remoteHost="'#CGI.REMOTE_HOST#' (CGI) ">

	<!--- fonction qui permet de creer UUID unique --->

	<cffunction  name="createCampagne" access="remote" hint="I return a new UUID for your campaign" output="true" returntype="string" >
		<cfargument name="idracine" required="false" type="numeric"  hint="id racine client">
		<cfif not (isDefined("idracine"))>
			<cfset idracine=5252761><!--- 5252761 =  idracine de saaswedo --->
		</cfif>
		<cfset uuidCampagne="">
		<!--- creation de l'objet de type component et appelle la methode  RealCreateCampagne ()--->
		<cftry>
			<cfset uuidCampagne=createobject("component","fr.consotel.api.sms.public.ApiSmsPrivate").realCreateCampagne(idracine)>
 		<cfcatch type="any">
			<cfset body=bodyException(CFCATCH)>
			<cfset saveInMntDetail(uuidCampagne,"#exce# createCampagne-[remoteHost] : #VARIABLES.remoteHost#",body,idMntServiceIndisonible) >
			<!--- <cfset SendMail.SendMessageToAdmin(CFCATCH) > send mail  --->
		</cfcatch>
		</cftry>
		<cfreturn uuidCampagne>
	</cffunction>

	<!------------------------------------------------------------------------------------------------------------------------------------------>

	<!--- permet d’envoyer message identique à un seul et unique téléphone --->
	<cffunction  name="sendMessageToSingleNumber" access="remote" hint="I send your SMS et return FALSE ou TRUE" output="true" returntype="any" >
		<cfargument name="phoneNumber" required="false" type="any"  hint="Phone number">
		<cfargument name="contentSms"  required="false" type="any"  hint="The content of SMS">
		<cfargument name="operator"    required="false" type="any"  hint="Operator">
		<cfargument name="uuid"        required="false" type="any"  hint="UUID compaign">

		<cfset messageSend="">
	    <cftry>
			<cfset messageSend=createobject("component","fr.consotel.api.sms.public.ApiSmsPrivate").realSendMessageToSingleNumber(phoneNumber,contentSms,operator,uuid,VARIABLES.mode)>
   		<cfcatch type="any">
			<cfset body=bodyException(CFCATCH)>
			<cfset idEvent=getIdEventByErrorCode(CFCATCH.ErrorCode)>
			<cfset saveInMntDetail(uuid,"#exce# sendMessageToSingleNumber-[phoneNumber] : #phoneNumber#-[Operator] : #operator#",body,idEvent) >
			<!--- <cfset SendMail.SendMessageToAdmin(CFCATCH) > send mail  --->
		</cfcatch>
		</cftry>
		<cfreturn messageSend>
	</cffunction>

	<!----------------------------------------------------------------------------------------------------------------------------------------->

	<!--- permet d’envoyer message identique  à plusieurs  téléphone --->
	<cffunction  name="sendMessageToMultipleNumber" access="remote" hint="I send your SMS for several phone et return FALSE ou TRUE" output="true" returntype="any" >

		<cfargument name="phoneNumber" required="false" type="any"  hint="Phone number">
		<cfargument name="contentSms"  required="false" type="any"  hint="The content of SMS">
		<cfargument name="operator"    required="false" type="any"  hint="Operator">
		<cfargument name="uuid"        required="false" type="any"  hint="UUID compaign">

		<cfset messageSend="">
		<cftry>
			<cfset messageSend=createobject("component","fr.consotel.api.sms.public.ApiSmsPrivate").realSendMessageToMultipleNumber(phoneNumber,contentSms,operator,uuid,VARIABLES.mode)>
   		<cfcatch type="any">
			<cfset body=bodyException(CFCATCH)>
			<cfset numbers = ArrayToList(phoneNumber)>
			<cfset idEvent=getIdEventByErrorCode(CFCATCH.ErrorCode)>
			<cfset saveInMntDetail(uuid,"#exce# sendMessageToMultipleNumber-[phoneNumber] : #numbers#-[Operator] : #operator#",body,idEvent) >
			<!--- <cfset SendMail.SendMessageToAdmin(CFCATCH) > send mail  --->
		</cfcatch>
		</cftry>
		<cfreturn messageSend>
	</cffunction>

    <!----------------------------------------------------------------------------------------------------------------------------------------->

	<!--- permet d’envoyer plusieurs message a un seul numero téléphone avec un  intervalle de temps prédéfini --->
	<cffunction  name="sendMultipleMessageToSingleNumber" access="remote" hint="I send yours SMS for one phone et return FALSE ou TRUE" output="true" returntype="any" >

		<cfargument name="phoneNumber" required="false" type="any"  hint="Phone number">
		<cfargument name="contentSms"  required="false" type="any"  hint="The content of SMS">
		<cfargument name="operator"    required="false" type="any"  hint="Operator">
		<cfargument name="delay"       required="false" type="any"  hint="Le nombre des seconds entre les envois ">
		<cfargument name="uuid"        required="false" type="any"  hint="UUID compaign">

		<cfset messageSend="">

		 <cftry>
			<cfset messageSend=createobject("component","fr.consotel.api.sms.public.ApiSmsPrivate").realSendMultipleMessageToSingleNumber(phoneNumber,contentSms,operator,delay,uuid,VARIABLES.mode)>
 		<cfcatch type="any">
			<cfset body=bodyException(CFCATCH)>
			<cfset idEvent=getIdEventByErrorCode(CFCATCH.ErrorCode)>
			<cfset saveInMntDetail(uuid,"#exce# sendMultipleMessageToSingleNumber-[phoneNumber] : #phoneNumber#-[Operator] : #operator#",body,idEvent) >
			<!--- <cfset SendMail.SendMessageToAdmin(CFCATCH) > send mail  --->
		</cfcatch>
		</cftry>
		<cfreturn messageSend>
	</cffunction>

	<!----------------------------------------------------------------------------------------------------------------------------------------->

	<!--- permet d’envoyer plusieurs message a un seul numero téléphone avec un  intervalle de temps prédéfini --->
	<cffunction  name="sendMultipleMessageToMultipleNumber" access="remote" hint="I send yours SMS for one phone et return FALSE ou TRUE" output="true" returntype="any" >
		<cfargument name="tabSMSNumber" required="false" type="any"  hint="Object's array of (phone number + contents SMS)">
		<cfargument name="operator"     required="false" type="any"  hint="Operator">
		<cfargument name="uuid"         required="false" type="any"  hint="UUID compaign">

		<cfset messageSend="">

		 <cftry>
			<cfset messageSend=createobject("component","fr.consotel.api.sms.public.ApiSmsPrivate").realSendMultipleMessageToMultipleNumber(tabSMSNumber,operator,uuid,VARIABLES.mode)>
    	<cfcatch type="any">
			<cfset body=bodyException(CFCATCH)>
			<cfset numbers = ArrayToList(phoneNumber)>
			<cfset idEvent=getIdEventByErrorCode(CFCATCH.ErrorCode)>
			<cfset saveInMntDetail(uuid,"#exce# sendMultipleMessageToMultipleNumber-[phoneNumber] : #numbers#-[Operator] : #operator#",body,idEvent) >
			<!--- <cfset SendMail.SendMessageToAdmin(CFCATCH) > send mail  --->
		</cfcatch>
		</cftry>
		<cfreturn messageSend>
	</cffunction>

	<!----------------------------------------------------------------------------------------------------------------------------------------->

	<!--- recuperer les stats par numero --->
	<cffunction  name="stateByNumber" access="remote"  output="true" returntype="any" >
		<cfargument name="phoneNumber" required="false" type="any"  hint="Phone number">
		<cfargument name="uuid"        required="false" type="any"  hint="UUID compaign">

		<cftry>
			<cfset tabStateByNumber=createobject("component","fr.consotel.api.sms.public.ApiSmsPrivate").stateByNumber(phoneNumber,uuid)>
			<cfif  isArray(tabStateByNumber)>
				<cfset createobject("component","fr.consotel.api.sms.model.ShowStatistique").Show(tabStateByNumber)>
			</cfif>
    	<cfcatch>
				<cfset SendMail.SendMessageToAdmin(CFCATCH,uuid) ><!--- envoi des mails  --->
		</cfcatch>
		</cftry>
	</cffunction>

	<!----------------------------------------------------------------------------------------------------------------------------------------->

	<!--- recuperer les stats par statut --->
	<cffunction  name="stateByStatut" access="remote"  output="true" returntype="any" >
		<cfargument name="libelle"     required="false" type="any"  hint="libelle statut">
		<cfargument name="uuid"        required="false" type="any"  hint="UUID compaign">

		<!--- <cftry> --->
			<cfset tabStateByStatut=createobject("component","fr.consotel.api.sms.public.ApiSmsPrivate").stateByStatut(libelle,uuid)>
			<cfif  isArray(tabStateByStatut)>
				<cfset createobject("component","fr.consotel.api.sms.model.ShowStatistique").Show(tabStateByStatut)>
				<cfelse>
					<cfoutput><p style="background-color:##F9A600; font-size:24px" align="center">Il n'y a pas de SMS </p></cfoutput>
			</cfif>
  		<!--- <cfcatch>
				<cfset SendMail.SendMessageToAdmin(CFCATCH,uuid) ><!--- envoi des mails  --->
		</cfcatch>
		</cftry>  --->
	</cffunction>

	<!----------------------------------------------------------------------------------------------------------------------------------------->

	<!--- recuperer les stats par statut --->
	<cffunction  name="stateByUuid" access="remote"  output="true" returntype="any" >
		<cfargument name="uuid"   required="false" type="any"  hint="UUID compaign">

		<cftry>
			<cfset tabStateByUuid=createobject("component","fr.consotel.api.sms.public.ApiSmsPrivate").stateByUuid(uuid)>
			<cfif  isArray(tabStateByUuid)>
				<cfset createobject("component","fr.consotel.api.sms.model.ShowStatistique").Show(tabStateByUuid)>
				<cfelse>
					<cfoutput><p style="background-color:##F9A600; font-size:24px" align="center">Il n'y a pas de SMS </p></cfoutput>
			</cfif>
  		 <cfcatch>
				<cfset SendMail.SendMessageToAdmin(CFCATCH,uuid) ><!--- envoi des mails  --->
		</cfcatch>
		</cftry>
	</cffunction>

	<!----------------------------------------------------------------------------------------------------------------------------------------->

	<cffunction name="FTP_MODE" returntype="numeric" access="public">
		<cfreturn 0>
	</cffunction>

	<cffunction name="WS_MODE" returntype="numeric" access="public">
		<cfreturn 1>
	</cffunction>

	<cffunction name="set_mode" returntype="boolean" access="public">
		<cfargument name="_mode"   required="true" type="numeric">

		<!--- créer un objet de type exception --->
		<cfset errorException=createobject("component","fr.consotel.api.sms.model.ApiException")>
		<cfset exception="">

		<cfif _mode eq FTP_MODE() >
			<cfset VARIABLES.mode= FTP_MODE() >
			<cfreturn true>
		<cfelseif _mode  eq WS_MODE()>
			<cfset VARIABLES.mode= WS_MODE() >
			<cfreturn true>
		<cfelse>
			<cfset exception=errorException.throwException("apiSMS_08", "Paramètre : phoneNumber, doit être de type String ")>
		</cfif>
	</cffunction>

	<cffunction name="SFR_OPERATOR" returntype="String" access="public">
		<cfreturn "Sfr">
	</cffunction>

	<cffunction name="ORANGE_OPERATOR" returntype="String" access="public">
		<cfreturn "Orange">
	</cffunction>

	<cffunction name="TWILIO_OPERATOR" returntype="String" access="public">
		<cfreturn "Twilio">
	</cffunction>

</cfcomponent>