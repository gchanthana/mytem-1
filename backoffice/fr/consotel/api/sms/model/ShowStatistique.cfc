<cfcomponent displayname="show statistics SMS" output="true">
	
	<cffunction name="Show" returntype="void" output="true">
		
		<cfargument name="data" type="array" required="true">			
		
		 <html>
			<head>
				 <link href="/fr/consotel/api/sms/css/style.css" rel="stylesheet" type="text/css" media="screen" />
			</head>
			<body>
				<table id="tab_stat">
				    <caption> </caption>		  			   
				    <thead>
				        <tr>
				            <th id="one_th">Numéro Téléphone</th>
				            <th id="two_th">UUID</th>
				            <th id="three_th">Date d'envoi</th>
				            <th id="four_th">Statut</th>
				        </tr>
				    </thead>
 				    <tfoot>
				    	<tr>
				            <td rowspan="2">Créé par</td>
				            <td colspan="3">API SMS- Saas We Do</td>
				        </tr>
				        <tr>
				            <td colspan="3">Date :#DateFormat(now(),"dd/mm/yyyy")# A #TimeFormat(now(),"HH:mm:ss") #  </td>
				        </tr>
				    </tfoot> 
				    <tbody>
					    <cfloop from="1" to="#arraylen(data)#" index="i">
					        <tr>
					            <td>#data[i]["num_tel"]#</td>
					            <td>#data[i]["uuid"]#</td>
					            <td>#DateFormat(data[i]["date_envoi"],"dd / mm / yyyy")# A #TimeFormat(data[i]["date_envoi"],"HH:mm:ss")#</td>
					            <td><cfif data[i]["statut"] eq "">
						            <cfoutput>Statut non disponible</cfoutput>
						            <cfelse>
						            	#data[i]["statut"]#
								    </cfif></td>
					        </tr>
				        </cfloop>
				        
				    </tbody>
				</table>
			</body>
		</html>
	</cffunction>

</cfcomponent>