<cfcomponent displayname="gestionException" hint="I do exceptions" output="false">

	<!--- fonction qui permet de lancer une exeception selon le code d'erreur qui est passé en paramètre --->

	<cffunction name="throwException" output="true" access="remote" returntype="string" hint="I insert a compaign.">

		<cfargument name="errorCode" type="string" required="true" hint="Error Code">
		<cfargument name="GeneralVariable" type="any"    required="true" hint="Error Code">

		<cfswitch expression="#errorCode#">
			<cfcase value="apiSMS_01">
			    <cfthrow message="Required parameter missing" errorcode="apiSMS_01" extendedinfo="Il manque des parametres" >
			</cfcase>
			<cfcase value="apiSMS_02">
			   <cfthrow message="Invalid parameter type" errorcode="apiSMS_02" extendedinfo="#GeneralVariable#" >
			</cfcase>
			<cfcase value="apiSMS_03">
			   <cfthrow message="Invalid phoneNumber" errorcode="apiSMS_03" extendedinfo="Num phone : #GeneralVariable# [ Votre num telephone doit commencer par (+) sans (- . ; [ a-z A-Z] ) etc . Si votre num telephone est international , verifier que votre operateur est SFR ou Twillio ]">
			</cfcase>
			<cfcase value="apiSMS_04">
			  <cfthrow message="Invalid SMSContents" errorcode="apiSMS_04" extendedinfo="Content SMS : #GeneralVariable# [ Your SMS contains a ; ] " >
			</cfcase>
			<cfcase value="apiSMS_05">
			    <cfthrow message="Invalide Operator" errorcode="apiSMS_05" extendedinfo="Operateur : #GeneralVariable#" >
			</cfcase>
			<cfcase value="apiSMS_07">
			   <cfthrow message="Invalide UUID " errorcode="apiSMS_07" extendedinfo="UUID : #GeneralVariable#" >
			</cfcase>
			<cfcase value="apiSMS_08">
			   <cfthrow message="Invalide send methode (FTP OU WS)" errorcode="apiSMS_08" extendedinfo="#GeneralVariable#">
			</cfcase>
			<cfcase value="apiSMS_099">
			   <cfthrow message="service unavailable" errorcode="apiSMS_099" extendedinfo="Le service indisponible : #GeneralVariable#"  >
			</cfcase>
		</cfswitch>
	</cffunction>

</cfcomponent>