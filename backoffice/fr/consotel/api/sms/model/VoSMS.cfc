    <cfcomponent output="false"  hint="">

<------------------------------------------------------------------------------------------------------------------------------------------->

	    <cffunction  name="Init" access="public"  returntype="void" output="false" hint="I provide super-constructor">
	     
		    <cfargument name="phoneNumber" type="string" required="true" hint=" I am the phone number" />
		    <cfargument name="contentSms"  type="string" required="true" hint=" I am the SMS" />
		     
		    <cfset VARIABLES.Instance = {  phoneNumber = ARGUMENTS.phoneNumber } />
		    <cfset VARIABLES.Instance = {  contentSms =  ARGUMENTS.contentSms } />
		     
		    <cfreturn />
	    </cffunction>

<------------------------------------------------------------------------------------------------------------------------------------------->  
	
<------------------------------------------------------------------------------------------------------------------------------------------->
     	
	<!--- getter --->
	    <cffunction  name="GetPhoneNumber" access="public"  returntype="string" output="false" hint="I return the phone number.">
	    		<cfreturn VARIABLES.Instance.phoneNumber />
	    </cffunction>
	    
	    <cffunction  name="GetContentSms" access="public"  returntype="string" output="false" hint="I return the SMS's content.">
	    		<cfreturn VARIABLES.Instance.contentSms />
	    </cffunction>
<------------------------------------------------------------------------------------------------------------------------------------------->

  
    </cfcomponent>