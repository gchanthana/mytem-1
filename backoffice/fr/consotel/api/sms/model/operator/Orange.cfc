<cfcomponent displayname="Orange Operator" output="false" implements="fr.consotel.api.sms.model.operator.IOperator">

	<cffunction name="perpareSMS" access="public" output="false" returnType="any" >

		<cfargument name="phoneNumber"   required="true"  type="any">
		<cfargument name="contentSms"    required="true"  type="any">
		<cfargument name="tabSMSNumber"  required="true"  type="any">
		<cfargument name="uuid"          required="true"  type="any">
		<cfargument name="delay"         required="false" type="any">
		<cfargument name="withWebService"  required="false" type="numeric"><!--- ne sert à rien pour Orange --->

		<cfif isArray(phoneNumber) or isArray(contentSms) or isArray(tabSMSNumber) >

			<cfif isArray(tabSMSNumber)><!--- tableau d'objet des n°phone+SMS --->
				<cfloop from="1" to="#ArrayLen(tabSMSNumber)#" index="z" step="1">
					<cfset sms=createStringSMS(tabSMSNumber[z]["contentSms"])>
					 <cfif isArray(sms)>
						<cfloop from="1" to="#ArrayLen(sms)#" index="k" step="1">
							<cfset lineFile=createFile(tabSMSNumber[z]["phoneNumber"],sms[k],uuid)>
							<cfthread action="sleep" duration="4000"></cfthread>
						</cfloop>
					 <cfelse>
						<cfset lineFile=createFile(tabSMSNumber[z]["phoneNumber"],tabSMSNumber[z]["contentSms"],uuid)>
						<cfthread action="sleep" duration="4000"></cfthread>
					</cfif>
				</cfloop>
				<cfreturn true>

			<cfelseif isArray(phoneNumber)><!--- un tableau des N° phone et un seule SMS --->
				 <cfset sms=createStringSMS(contentSms)>
				 <cfif isArray(sms)>
					<cfloop from="1" to="#ArrayLen(sms)#" index="z" step="1">
						<cfset lineFile=createFile(phoneNumber,sms[z],uuid)>
						<cfthread action="sleep" duration="4000"></cfthread>
					</cfloop>
					<cfreturn true>
				  <cfelse>
				  	 <cfset resSendMail=createFile(phoneNumber,contentSms,uuid)>
				 	 <cfreturn resSendMail>
				 </cfif>

			<cfelse><!--- un tableau des SMS  et un seul n° phone --->
				<cfloop from="1" to="#ArrayLen(contentSms)#" index="z" step="1">
					 <cfset sms=createStringSMS(contentSms[z])>
					 <cfif isArray(sms)>
						<cfloop from="1" to="#ArrayLen(sms)#" index="k" step="1">
							<cfset lineFile=createFile(phoneNumber,sms[k],uuid)>
							<cfthread action="sleep" duration="4000"></cfthread>
						</cfloop>
					 <cfelse>
						<cfset lineFile=createFile(phoneNumber,contentSms[z],uuid)>
						<cfthread action="sleep" duration="4000"></cfthread>
					</cfif>
				</cfloop>
				<cfreturn true>
			</cfif>
		<cfelse><!--- un seul sms et n°tel --->
			 <cfset sms=createStringSMS(contentSms)>
			 <cfif isArray(sms)>
				<cfloop from="1" to="#ArrayLen(sms)#" index="z" step="1">
					<cfset lineFile=createFile(phoneNumber,sms[z],uuid)>
					<cfthread action="sleep" duration="4000"></cfthread>
				</cfloop>
				<cfreturn true>
			  <cfelse>
			  	 <cfset resSendMail=createFile(phoneNumber,contentSms,uuid)>
			 	 <cfreturn resSendMail>
			 </cfif>

		</cfif>
	</cffunction>

<!----------------------------------------------------------------------------------------------------------------------------------->

	<cffunction name="createStringSMS" output="false" access="private" returntype="any">

		<cfargument name="contentSMS"  required="true" type="any">
		<cfset tailleSMS=160>
		<cfif Len(contentSMS) gt #tailleSMS#>

			<cfset stringLen=Len(contentSMS)\ #tailleSMS#>
			<cfset modDiv=Len(contentSMS) mod #tailleSMS#>

			<cfset tabString=arraynew(1)>
			<cfset j=1>

			<cfloop from="1" to="#stringLen#" index="i">
				<cfset tabString[i]=Mid(contentSMS,j,#tailleSMS#)>
				<cfset j=j+160>
			</cfloop>

			<cfif modDiv gt 0>
				<cfset tabString[#stringLen#+1]=Mid(contentSMS,((#stringLen#*#tailleSMS#)+1),#modDiv#)>
			</cfif>
			<cfreturn tabString>
		<cfelse>
			<cfreturn "">
		</cfif>

	</cffunction>

<!----------------------------------- preparer le fichier à envoyer par email  -------------------------------------------------------->

	<cffunction name="createFile" access="private" returntype="any">

		<cfargument name="phoneNumber"   required="true"  type="any">
		<cfargument name="contentSms"    required="true"  type="any">
		<cfargument name="uuid"          required="true"  type="any">

		<cfset contentSms = Replace(contentSms,chr(13),"#chr(13)#T-", "ALL")><!--- retour chariot --->
		<cfset contentSms = Replace(contentSms,chr(10),"#chr(10)#T-", "ALL")><!--- saut de ligne --->

		<cfset line="T-#contentSms#">

		<cfset startDate=DateFormat(now(),"yyyymmddHHmmss")>
		<cfset nameFile="#uuid#_#startDate#.txt">

	    <cfset filePath="/fr/consotel/api/sms/fichiers/fichiersOrange/#nameFile#"><!--- le chemin du fichier --->

	     <!--- Make a new file with charset="iso-8859-1"  --->
	    <cffile mode ="777" action="Write" file="#expandpath(filePath)#"  output="#line#" charset="iso-8859-1">

		<cfif isArray(phoneNumber) >
			<cfloop from="1" to="#ArrayLen(phoneNumber)#" index="i" step="1">
					<cffile action="append" file="#expandpath(filePath)#" output="##-#phoneNumber[i]#">
			</cfloop>
		<cfelse>
			<cffile action="append" file="#expandpath(filePath)#" output="##-#phoneNumber#">
		</cfif>

		<cfset resSendMail=sendMail()>

		<cfreturn resSendMail>

	</cffunction>

<!----------------------------------------------------------------------------------------------------------------------------------->

	<!--- permet d'envoyer un mail avec comme pièce jointe le nom du fichier --->
	<cffunction name="sendMail" access="private" returntype="boolean" hint="I send your email" output="true">
	     <!--- Send out email. to="diffusion@contact-everyone.fr" --->

	     <cfmail to="diffusion@contact-everyone.fr" from="brice.miramont@consotel.fr" subject="Envoi SMS API" type="html" server="192.168.3.119:26">
			<!--- attacher une pièce jointe --->
			 <cfmailparam  file="#expandpath(filePath)#" type="text/plain">
		 </cfmail>

		 <cfreturn true>
	</cffunction>

</cfcomponent>