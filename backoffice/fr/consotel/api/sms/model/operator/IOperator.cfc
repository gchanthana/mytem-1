<cfinterface displayname="interface for prepare SMS" hint="" >
	
	<cffunction name="perpareSMS" access="public" output="false" returnType="any" >
		
		<cfargument name="phoneNumber"   required="true" type="any">
		<cfargument name="contentSms"    required="true" type="any">
		<cfargument name="tabSMSNumber"  required="true" type="any">
		<cfargument name="uuid"          required="true" type="any">
		<cfargument name="delay"         required="false" type="any">
		<cfargument name="withWebService"       required="false" type="numeric">
		
	</cffunction>
	
</cfinterface>
