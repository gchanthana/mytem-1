<cfcomponent displayname="SFR Operator" output="false" implements="fr.consotel.api.sms.model.operator.IOperator">

	<cfset VARIABLES.urlWebServiceSfr="https://www.dmc.sfr-sh.fr/DmcWS/1.5.1/MessagesUnitairesWS">
	<cfset VARIABLES.serviceId="42321001">
	<cfset VARIABLES.servicePassword="8JGdHUnG">
	<cfset VARIABLES.spaceId="548">
	<cfset VARIABLES.lang="fr_FR">
    <cfset VARIABLES.errorException=createobject("component","fr.consotel.api.sms.model.ApiException") >
	<cfset VARIABLES.sendWS=1>

	<cffunction name="perpareSMS" access="public" output="false" returnType="any">

		<cfargument name="phoneNumber"   	required="true"  type="any">
		<cfargument name="contentSms"    	required="true"  type="any">
		<cfargument name="tabSMSNumber"  	required="true"  type="any">
		<cfargument name="uuid"          	required="true"  type="any">
		<cfargument name="delay"         	required="false" type="any">
		<cfargument name="withWebService"  	required="false" type="numeric"><!--- envoyer les SMS en FTP ou Web Service --->

	    <cfset VARIABLES.phoneNumber  = ARGUMENTS.phoneNumber >
	    <cfset VARIABLES.contentSms   = ARGUMENTS.contentSms >
	    <cfset VARIABLES.tabSMSNumber = ARGUMENTS.tabSMSNumber >
	    <cfset VARIABLES.uuid = ARGUMENTS.uuid >
	    <cfset VARIABLES.withWebService = ARGUMENTS.withWebService >

		<cfset nameFile="#uuid#.txt">
		<cfset filePath="/fr/consotel/api/sms/fichiers/fichiersSfr/#nameFile#"><!--- le chemin du fichier --->

		<cfif isArray(phoneNumber) or isArray(contentSms) or isArray(tabSMSNumber) >

		    <cfif isArray(phoneNumber) or isArray(tabSMSNumber) or isArray(contentSms) >
			    <cfset createFile=createCommandFile(uuid)>
	        </cfif>

			<cfif isArray(tabSMSNumber)><!--- tableau d'objet des n°phone+SMS --->
				<cfif withWebService eq VARIABLES.sendWS >
					<cfloop from="1" to="#ArrayLen(tabSMSNumber)#" index="z" step="1">
						<cfset resultatSendSms=callWebService(tabSMSNumber[z]["phoneNumber"],tabSMSNumber[z]["contentSms"])>	<!--- appele le web service --->
						<cfif delay lt 10000>
							<cfthread action="sleep" duration="10000"></cfthread><!--- faire une pause --->
						<cfelse>
							<cfthread action="sleep" duration="#delay#"></cfthread>
						</cfif>
					 </cfloop>
					<cfreturn true>
				<cfelse>
					<cfloop from="1" to="#ArrayLen(tabSMSNumber)#" index="z" step="1">
						<cfset lineFile=createLineSMS(tabSMSNumber[z]["contentSms"])>
						<cfset line='#tabSMSNumber[z]["phoneNumber"]#;#lineFile#;#uuid#'>
						<cffile action="append" file="#expandpath(filePath)#" output="#line#">
					</cfloop>
					<cffile action="append" file="#expandpath(filePath)#" output="##END">
					<cfset resultatSendSms=sendSMS(uuid)><!--- send SMS --->
					<cfreturn resultatSendSms>
				</cfif>

			<cfelseif isArray(phoneNumber)><!--- un tableau des N° phone et un seule SMS --->

				 <cfif withWebService eq VARIABLES.sendWS >
					 <cfloop from="1" to="#ArrayLen(phoneNumber)#" index="z" step="1">
							 <cfset resultatSendSms=callWebService(phoneNumber[z],contentSms)>	<!--- appele le web service --->
							 <cfif delay lt 10000>
								<cfthread action="sleep" duration="10000"></cfthread><!--- faire une pause --->
							<cfelse>
								<cfthread action="sleep" duration="#delay#"></cfthread>
							</cfif>
					 </cfloop>
					<cfreturn true>

				 <cfelse>
					<cfset lineFile=createLineSMS(contentSms)>
					<cfloop from="1" to="#ArrayLen(phoneNumber)#" index="z" step="1">
						<cfset line='#phoneNumber[z]#;#lineFile#;#uuid#'>
						<cffile action="append" file="#expandpath(filePath)#" output="#line#">
					</cfloop>
					<cffile action="append" file="#expandpath(filePath)#" output="##END">
					<cfset resultatSendSms=sendSMS(uuid)><!--- send SMS --->
					<cfreturn resultatSendSms>

				 </cfif>

			<cfelse><!--- un tableau des SMS  et un seul n° phone --->

				 <cfif withWebService eq VARIABLES.sendWS>
					 <cfloop from="1" to="#ArrayLen(contentSms)#" index="z" step="1">
						 <cfset resultatSendSms=callWebService(phoneNumber,contentSms[z])>	<!--- appele le web service --->
						 <cfif delay lt 10000>
							<cfthread action="sleep" duration="10000"></cfthread><!--- faire une pause --->
						<cfelse>
							<cfthread action="sleep" duration="#delay#"></cfthread>
						</cfif>
					 </cfloop>
					 <cfreturn true>
				 <cfelse><!--- FTP --->
					 <cfloop from="1" to="#ArrayLen(contentSms)#" index="z" step="1">
						 <cfset lineFile=createLineSMS(contentSms[z])>
						 <cfset line='#phoneNumber#;#lineFile#;#uuid#'>
						 <cffile action="append" file="#expandpath(filePath)#" output="#line#">
					</cfloop>
					<cffile action="append" file="#expandpath(filePath)#" output="##END">
					<cfset resultatSendSms=sendSMS(uuid)><!--- send SMS --->
					<cfreturn resultatSendSms>
				</cfif>

			</cfif> <!--- <cfif isArray(tabSMSNumber)> --->

		<cfelse>

			<cfif withWebService eq VARIABLES.sendWS><!--- appele le web service --->
			 	<cfset resultatSendSms=callWebService(phoneNumber,contentSms)>
			 	<cfreturn resultatSendSms>
			 <cfelse> <!--- FTP --->
				 <cfset createFile=createCommandFile(uuid)>
				 <cfset lineFile=createLineSMS(contentSms)>
				 <cfset line='#phoneNumber#;#lineFile#;#uuid#'>
				 <cffile action="append" file="#expandpath(filePath)#" output="#line#">
				 <cffile action="append" file="#expandpath(filePath)#" output="##END">
				 <cfset resultatSendSms=sendSMS(uuid)>
				 <cfreturn resultatSendSms>
			 </cfif>

		</cfif>
	</cffunction>

	<!------------------------------------------------------------------------------------------------------------------------------------->

	<!--- make à new file and append command part --->

	<cffunction name="createCommandFile" access="private" output="false" returntype="void">

		<cfargument name="uuid"  required="true" type="any">

		<cfset todayDate = Now()>
	    <cfset nameDate=DateFormat(todayDate,"ddmmyyyy")>
	    <cfset nameTime=TimeFormat(todayDate,"HHmmss")>
	    <!---  	<cfset nameFile="#nameDate##nameTime#.txt"> --->

	 	<cfset nameFile="#uuid#.txt">
	    <cfset filePath="/fr/consotel/api/sms/fichiers/fichiersSfr/#nameFile#"><!--- le chemin du fichier --->

	     <!--- Make a new file --->
	    <cffile mode ="777" action="Write" file="#expandpath(filePath)#"  output="##COMMAND" charset="UTF-8">

	    <cfset NewLine = Chr(13) & Chr(10)><!--- retour chariot --->
	    <cfset startDate=DateFormat(todayDate,"yyyy-mm-dd HH:mm:ss")>
	    <cfset stopDate=DateFormat(todayDate+2,"yyyy-mm-dd HH:mm:ss")>

	    <!--- build a string like that
	    	#COMMAND
			serviceId:42321001
			spaceId:548
			broadcastName:CONSOTEL
			scenarioId:4091
			startDate:2011-11-30 08:05:00
			stopDate:2011-12-02 08:05:00
			callPlanningId:1205
			description:Envoi SMS
			maxSimultaneousContact:10
			priority:4
			customizableId:5956
			multimediaType:TXT
			smsLong:1
	     --->

	    <cfset command="serviceId:42321001#NewLine#spaceId:548#NewLine#broadcastName:SAAS WE DO#NewLine#scenarioId:4091#NewLine#startDate:#startDate##NewLine#stopDate:#stopDate##NewLine#callPlanningId:1205#NewLine#description:Envoi SMS#NewLine#maxSimultaneousContact:10#NewLine#priority:4#NewLine#customizableId:5956#NewLine#multimediaType:TXT#NewLine#smsLong:1#NewLine#text: |contact.info1||contact.info2||contact.info3||contact.info4|#NewLine###CONTACTS#NewLine#PHONENUMBER1;INFO1;INFO2;INFO3;INFO4;EXTERNALREFERENCE">
	    <cffile action="append" file="#expandpath(filePath)#" output="#command#">

	    <cfreturn />
	</cffunction>

	<!------------------------------------------------------------------------------------------------------------------------------------->

	<!--- déposer un fichier sur le ftp pour l'envoyer --->
	<cffunction name="sendSMS" access="private" output="false" hint=" Send SMS" returntype="any">

		<cfargument name="uuid"  required="true" type="any">
		<cfset exception ="">
	 	<cfset nameFile="#uuid#.csv">
	    <cfset filePath="/fr/consotel/api/sms/fichiers/fichiersSfr/#nameFile#"><!--- le chemin du fichier --->

	     <!--- convertir le fichier en iso en appellant le .sh  en executant un .sh--->
    	<cfset pathFileSH="/fr/consotel/api/sms/conversionUTF/sms.sh">
	    <cfexecute name="#expandpath(pathFileSH)#" ></cfexecute>

		<!---  connexion au serveur FTP --->
		<cfftp  action ="open"  connection="My_FTP" username="42321001" password="QQ938HRy" server="ftp.dmc.sfr-sh.fr" stopOnError="no">

		<cfset connexionOK=cfftp.Succeeded>

	    <cfif connexionOK eq "yes">
			 <!--- Put file --->
 			<cfftp action="putfile"
				   remotefile="./in/#uuid#.csv"
				   localfile="#expandpath(filePath)#" connection ="My_FTP"  stoponerror="Yes" >

			<!--- <cfftp action="rename"	existing="./in/#uuid#.txt" new="./in/#uuid#.csv" connection ="My_FTP" stoponerror="Yes"> --->

			<cfftp action = "close"   connection = "My_FTP"  stopOnError = "Yes"> <!--- fermer la connexion au serveur FTP --->

			<cfset fileTxt="/fr/consotel/api/sms/fichiers/fichiersSfr/#uuid#.txt">
			<cfset fileCsv="/fr/consotel/api/sms/fichiers/fichiersSfr/#uuid#.csv">
			<cffile action="delete"  file="#expandpath(fileTxt)#">
			<cffile action="delete"  file="#expandpath(fileCsv)#">

		<cfelse><!--- declancher une exception --->
			<cfset exception=VARIABLES.errorException.throwException("apiSMS_099", "Une erreur de connexion au serveur FTP s'est produit ,Veuillez essayer ultérieurement ")>
		</cfif>

		<cfif exception eq "" >
			<cfset retrunDB=insertDataBase()>
			<cfreturn retrunDB>
		</cfif>

	</cffunction>

	<!------------------------------------------------------------------------------------------------------------------------------------->

	<cffunction name="createLineSMS" output="false" access="private" returntype="String">

		<cfargument name="contentSMS"  required="true" type="any">

		<cfset contentSMS = Replace(contentSMS,chr(13),"", "ALL")><!--- retour chariot --->
		<cfset contentSMS = Replace(contentSMS,chr(10),"", "ALL")><!--- saut de ligne --->

		<cfset tab=arraynew(1)>
		<cfset n=4><!---  pour faire info1;info2;info3;info4--->

		<cfset stringLen=Len(contentSMS)\#n#> <!--- \ division entière  --->
		<cfset j=1>
		<cfset line="">

		<!--- diviser la chaine en 4  --->
		<cfif Len(contentSMS) gte 4 >
			<cfloop from="1" to="#n-1#" index="i">
				<cfset tab[i]=Mid(contentSMS,j,stringLen)>
				<cfset j=j+#stringLen#>
			</cfloop>

			<cfset tab[#n#]=Mid(contentSMS,((#n#-1)*#stringLen#)+1,Len(contentSMS))>
			<cfset line= "#tab[1]#;#tab[2]#;#tab[3]#;#tab[4]#">

		</cfif>
		<cfreturn #line#>
	</cffunction>

	<!------------------------------------------------------------------------------------------------------------------------------------->

	<cffunction  name="callWebService" access="private" returntype="any">

		<cfargument name="phoneNumber"   required="true"  type="any">
		<cfargument name="contentSms"    required="true"  type="any">

		<!--- remplacer les caracteres speciaux de XML <,>,& ,',"  --->
		<!---  Les deux caractères < et > servent à délimiter les balises --->
		<!--- & marque le début des entités xml, exemple &amp; --->
		<!--- ',"  servent également de délimiteurs, en particulier pour les valeurs des attributs --->

		<cfset contentSms = Replace(contentSms,"&","%26", "ALL")>
		<cfset contentSms = Replace(contentSms,"<","%3C", "ALL")>
		<cfset contentSms = Replace(contentSms,">","%3E", "ALL")>
		<cfset contentSms = Replace(contentSms,'"',"%22", "ALL")>
		<!--- <cfset contentSms = Replace(contentSms,"'","%60", "ALL")> --->

		<!--- <![CDATA[#contentSms#]]>  ne marche pas , le web service supprime tout ce qui est après & --->

		<cfset exception="">

		<!--- sauvegarder le XML dans une variable  --->
 		<cfsavecontent variable="soapBody">
             <cfoutput>
                 <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
						           xmlns:ser="http://servicedata.ws.dmc.sfrbt/">
					 <soapenv:Header>
					      <ser:authenticate>
					         <serviceId>#VARIABLES.serviceId#</serviceId>
					         <servicePassword>#VARIABLES.servicePassword#</servicePassword>
					         <spaceId>#VARIABLES.spaceId#</spaceId>
							 <lang>#VARIABLES.lang#</lang>
					      </ser:authenticate>
					  </soapenv:Header>
                     <soapenv:Body>
					      <ser:addSingleCall>
					         <messageUnitaire>
					            <media>SMSLong</media>
					            <textMsg>#contentSms#</textMsg>
					            <to>#phoneNumber#</to>
					         </messageUnitaire>
					      </ser:addSingleCall>
				    </soapenv:Body>
                 </soapenv:Envelope>
             </cfoutput>
         </cfsavecontent>
		 <!--- <cfoutput>
             SOAP BODY : <br>
             <cfdump var="#soapBody#">
             <br>
         </cfoutput> --->

		<!--- créer une requete http , url : l'adresse du web service' --->
		<cfhttp
             url="#VARIABLES.urlWebServiceSfr#"
             method="post"
             result="httpResponse">
             <cfhttpparam   type="xml"  value="#trim(soapBody)#"/>
		</cfhttp>

		<cfif httpResponse.StatusCode eq "200 OK"><!--- tester si ok  --->
			<cfset xmlResult=xmlparse(httpResponse.Filecontent)>
			<cfset messageid=xmlResult.xmlRoot.XmlChildren[1].XmlChildren[1].XmlChildren[1].xmlText>
			<cfset retrunDB=insertDataBase(phoneNumber,contentSms,messageid)><!--- messageid doit être stocké dans la base pour pouvir injecter le status de sms  --->
			<cfreturn retrunDB>
		<cfelse>
			<cfset exception=VARIABLES.errorException.throwException("apiSMS_099", "Le web service est momentanement indisponible,Veuillez essayer ulterieurement")>
		</cfif>

	</cffunction>

	<cffunction name="insertDataBase" access="private" returntype="any" hint="I insert your data" output="true">

	    <!---  ces arguemnts sont utilisépour le web service --->
	    <cfargument name="phoneNumber" type="any" 	required="false">
	    <cfargument name="contentSms"  type="any" 	required="false">
	    <cfargument name="messageid"   type="any"   required="false">

	    <cfif not (isDefined("messageid"))>
			<cfset messageid="">
		</cfif>

		<cfset id_operator=1> <!---  sfr operateur  --->
		<cfset newCompgane=createobject("component","fr.consotel.api.sms.model.Campagne")>
		<cfset id_uuid=newCompgane.getIdCampaign(VARIABLES.uuid)>
		<cfif withWebService neq VARIABLES.sendWS > <!---  sms sent by FTP --->

			<cfif isArray(VARIABLES.phoneNumber) or isArray(VARIABLES.contentSms) or isArray(VARIABLES.tabSMSNumber) >

				<cfif isArray(VARIABLES.tabSMSNumber)>
					<cfloop from="1" to="#ArrayLen(VARIABLES.tabSMSNumber)#" index="i" step="1">
						<cfset dataStruct={id_uuid=#id_uuid#,contentSms=VARIABLES.tabSMSNumber[i]["contentSms"],phoneNumber=VARIABLES.tabSMSNumber[i]["phoneNumber"],id_operator=#id_operator#,messageid=#messageid#}>
						<cfset insertData=newCompgane.insertSms(dataStruct)>
					</cfloop>
				<cfelseif isArray(VARIABLES.contentSms)>
					<cfloop from="1" to="#ArrayLen(VARIABLES.contentSms)#" index="i" step="1">
						<cfset dataStruct={id_uuid=#id_uuid#,contentSms=VARIABLES.contentSms[i],phoneNumber=VARIABLES.phoneNumber,id_operator=#id_operator#,messageid=#messageid#}>
						<cfset insertData=newCompgane.insertSms(dataStruct)>
					</cfloop>
				<cfelse>
					<cfloop from="1" to="#ArrayLen(VARIABLES.phoneNumber)#" index="i" step="1">
						<cfset dataStruct={id_uuid=#id_uuid#,contentSms=VARIABLES.contentSms,phoneNumber=VARIABLES.phoneNumber[i],id_operator=#id_operator#,messageid=#messageid#}>
						<cfset insertData=newCompgane.insertSms(dataStruct)>
					</cfloop>
				</cfif>
			<cfelse>
				<cfset dataStruct={id_uuid=#id_uuid#,contentSms=VARIABLES.contentSms,phoneNumber=VARIABLES.phoneNumber,id_operator=#id_operator#,messageid=#messageid#}>
				<cfset insertData=newCompgane.insertSms(dataStruct)>
			</cfif>
			<cfelse> <!--- sms sent by WS --->
				<!--- messageid est utilisé pour récuperer le status de sms envoyé par le WebService si FTP  messageid="" --->
				<cfset dataStruct={id_uuid=#id_uuid#,contentSms=#contentSms#,phoneNumber=#phoneNumber#,id_operator=#id_operator#,messageid=#messageid#}>
				<cfset insertData=newCompgane.insertSms(dataStruct)>
		</cfif>

		 <cfif insertData eq false>
			<cfset exception=VARIABLES.errorException.throwException("apiSMS_099", "Une erreur s'est produit lors de l'insertion dans la base Oracle apres l'envoi des SMS")>
			<cfelse>
				<cfreturn true>
		</cfif>

	</cffunction>

</cfcomponent>