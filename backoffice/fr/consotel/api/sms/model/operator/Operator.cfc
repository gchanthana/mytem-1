<cfcomponent displayname="Operator" output="true">

	<cfset VARIABLES.operatorStrategy="">
	<cfset VARIABLES.errorException=createobject("component","fr.consotel.api.sms.model.ApiException") >

    <cffunction  name="Init" access="public"  returntype="any" output="false" hint="I provide super-constructor">

	    <cfargument name="operatorName"   type="string"   required="true"  hint=" I am the operator's name">
	    <cfargument name="phoneNumber"    type="any"	  required="true"  hint=" phone number">
	    <cfargument name="contentSms"     type="any" 	  required="true"  hint=" SMS content">
	    <cfargument name="tabSMSNumber"   type="any" 	  required="true">
	    <cfargument name="uuid"           type="any" 	  required="true" >
	    <cfargument name="delay"          type="any" 	  required="false">
	  	<cfargument name="withWebService" type="numeric"  required="false">

	    <!--- <cfset VARIABLES.Instance = {  operatorName = ARGUMENTS.operatorName } /> --->
	    <cfset VARIABLES.operatorName = ARGUMENTS.operatorName >
	    <cfset VARIABLES.phoneNumber  = ARGUMENTS.phoneNumber >
	    <cfset VARIABLES.contentSms   = ARGUMENTS.contentSms >
	    <cfset VARIABLES.tabSMSNumber = ARGUMENTS.tabSMSNumber >
	    <cfset VARIABLES.uuid = ARGUMENTS.uuid >
	    <cfset VARIABLES.delay = ARGUMENTS.delay >
	    <cfset VARIABLES.withWebService = ARGUMENTS.withWebService >

	    <!--- instancier la class qui correspond au nom de l'operater passé en paramètre --->
	    <cfset VARIABLES.operatorStrategy=createObject("component","fr.consotel.api.sms.model.operator.#operatorName#") >

	    <cfset resultatSendSms=perpareSMS()>
	    <cfreturn resultatSendSms >

    </cffunction>

	<cffunction name="perpareSMS" access="private" output="false" returnType="any" >
		<cfset resultSend=VARIABLES.operatorStrategy.perpareSMS(VARIABLES.phoneNumber,VARIABLES.contentSms,VARIABLES.tabSMSNumber,VARIABLES.uuid,VARIABLES.delay,VARIABLES.withWebService)>
		<cfreturn resultSend>
	</cffunction>

</cfcomponent>