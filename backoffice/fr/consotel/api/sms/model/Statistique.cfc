<cfcomponent displayname="Class for statistique" output="false">
	
	/*--------------------------------------------------------------------------------------------------------------------------*/

	<cffunction name="stateByNumber" access="public" returntype="any" output="true">
		
		<cfargument name="data" type="struct" required="true" hint="This is a structure.">	
		
		<cfset var uuid=trim(arguments.data.uuid)>
		<cfset var phone=trim(arguments.data.phoneNumber)>
			
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_api_sms.getSmsDetail">		
				<cfprocparam type="in"  cfsqltype="CF_SQL_VARCHAR" value="#uuid#" variable="p_uuid">
				<cfprocparam type="in"  cfsqltype="CF_SQL_VARCHAR" value="#phone#" variable="p_ligne">
				<cfprocresult name="getStateByNumber">		
			</cfstoredproc>	
		<cfcatch>
			<cfreturn false>
		</cfcatch>
		</cftry>
		
		<cfif getStateByNumber.RecordCount gte 1 >
			<!--- creer un tableau des structures --->	
			<cfloop from="1" to="#getStateByNumber.RecordCount#" index="i">
				<cfset stats[i]=
							 {
							 	num_tel=#getStateByNumber.num_tel[i]#,
							 	date_envoi=#getStateByNumber.date_envoi[i]#,
							 	uuid=#arguments.data.uuid#,
							 	statut=#getStateByNumber.statut[i]#
							 }/>
			</cfloop>
			<cfreturn stats>
		<cfelse>
			<cfreturn false>
		</cfif>
	</cffunction>
	
	/*--------------------------------------------------------------------------------------------------------------------------*/
	
	<cffunction name="stateByStatut" access="public" returntype="any" output="true">
		
		<cfargument name="data" type="struct" required="true" hint="This is a structure.">	
		
		<cfset var uuid=trim(arguments.data.uuid)>
		<cfset var statut=trim(arguments.data.libelle)>
			
		<cftry>
			
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_api_sms.getSmsDetail">		
				<cfprocparam type="in"  cfsqltype="CF_SQL_VARCHAR" value="#uuid#" variable="p_uuid">
				<cfprocparam type="in"  cfsqltype="CF_SQL_VARCHAR" value="#statut#" variable="p_statut">
				<cfprocresult name="getStateByStatut">		
			</cfstoredproc>	
			
		<cfcatch>
			<cfreturn false>
		</cfcatch>
		</cftry>
		
		<cfif getStateByStatut.RecordCount gte 1 >
			<!--- creer un tableau des structures --->	
			<cfloop from="1" to="#getStateByStatut.RecordCount#" index="i">
				<cfset stats[i]=
							 {
							 	num_tel=#getStateByStatut.num_tel[i]#,
							 	date_envoi=#getStateByStatut.date_envoi[i]#,
							 	uuid=#arguments.data.uuid#,
							 	statut=#getStateByStatut.statut[i]#
							 }/>
			</cfloop>
			<cfreturn stats>
		<cfelse>
			<cfreturn false>
		</cfif>
		
	</cffunction>
	
	/*--------------------------------------------------------------------------------------------------------------------------*/

	<cffunction name="stateByUuid" access="public" returntype="any" output="true">
		<cfargument name="uuid" type="String" required="true">	
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_api_sms.getSmsDetailAll">		
				<cfprocparam type="in"  cfsqltype="CF_SQL_VARCHAR" value="#uuid#" variable="p_uuid">
				<cfprocresult name="getStateByIdCompagne">		
			</cfstoredproc>
					
		<cfcatch>
			<cfreturn false>
		</cfcatch>
		</cftry>
		<!--- creer un tableau des structures --->	
		<cfloop from="1" to="#getStateByIdCompagne.RecordCount#" index="i">
			<cfset stats[i]=
						 {
						 	num_tel=#getStateByIdCompagne.num_tel[i]#,
						 	date_envoi=#getStateByIdCompagne.date_envoi[i]#,
						 	uuid=#arguments.uuid#,
						 	statut=#getStateByIdCompagne.statut[i]#
						 }/>
		</cfloop>
		<cfreturn stats>
		
	</cffunction>
	
	
</cfcomponent>