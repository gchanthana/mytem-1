<cfcomponent displayname="UpdateStatSMS" output="false">
	
	<!--- info pour sfr --->
	<cfset VARIABLES.urlWebServiceSfr=    "https://www.dmc.sfr-sh.fr/DmcWS/1.5.1/MessagesUnitairesWS?wsdl">
	<cfset VARIABLES.urlWebServiceSfrJSON="https://www.dmc.sfr-sh.fr/DmcWS/1.5.1/JsonService/MessagesUnitairesWS/getSingleCallCra?authenticate=">
	<cfset VARIABLES.serviceId="42321001">
	<cfset VARIABLES.servicePassword="8JGdHUnG">
	<cfset VARIABLES.spaceId="548">
	<cfset VARIABLES.lang="fr_FR">

	<!--- info pour twilio --->
	<cfset VARIABLES.accountSid="AC5e2a48a8c4c76a0a6c8c7590adbfb89b">
	<cfset VARIABLES.authToken="c1746286055896f7ff04d5693dd8df02">
	<cfset VARIABLES.fromSid="+19782613232">
	<cfset VARIABLES.twilioSMSResource ="https://api.twilio.com/2010-04-01/Accounts/#accountSid#/Messages.json/" />
	

	<cffunction  name="getSfrMessage" access="remote" returntype="any">
		
		<cfargument name="dateDebut"   required="false"  type="any">
		<cfargument name="DateFin"     required="false"  type="any">
				
		<cfif not (isDefined("dateDebut"))>
			<cfset dateDebut= dateFormat(Now()-3,"yyyy-mm-dd")>
		</cfif>

		<cfif not (isDefined("DateFin"))>
			<cfset DateFin= dateFormat(Now(),"yyyy-mm-dd")>
		</cfif>
		
		<!--- date au format timestamp --->		
		<cfset beginDate=DateDiff("s", CreateDate(1970,1,1),#dateDebut#)>
		<cfset endDate=DateDiff("s", CreateDate(1970,1,1),#DateFin#)>
		
		<cfset beginDate='beginDate="#beginDate#000"'> <!---  on ajoute 000 sinon le web service ne prends pas en compte les dates envoyée. timestamp sur 13 chiffre selon la doc de sfr --->
		<cfset endDate='endDate="#endDate#000"'>

		<cfset authenticate='{"serviceId": "#serviceId#",
							   "servicePassword": "#servicePassword#",
							   "spaceId": "#spaceId#"
							 }'>		 
		  <cfhttp
            url="#VARIABLES.urlWebServiceSfrJSON##VARIABLES.authenticate#&#beginDate#&#endDate#"
            method="get"
            result="httpResponse">		
					
         </cfhttp>

		<cfset deserializeReponse=deserializejson(httpResponse.Filecontent) >
		<!--- <cfdump var="#deserializeReponse#"> --->


		<cfset finalJson=serializejson(deserializeReponse) >
		<!--- <cfdump var="#finalJson#">  --->

		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_api_sms.updateStateSmsSfrUnitaire">		
			<cfprocparam type="in" cfsqltype="CF_SQL_CLOB" value="#finalJson#" variable="p_response">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER"    variable="p_retour">	
		</cfstoredproc>	

	</cffunction>
	
	<!--- mettre a jour les status des sms envoyé par twilio  --->
	<cffunction  name="getTwilioMessage" access="remote" returntype="any" output="true">
			
		<cfargument name="dateSent"   required="false"  type="any">
	
		<cfif not (isDefined("dateSent"))>
			<cfset dateSent= dateFormat(Now(),"yyyy-mm-dd")>
		</cfif>
		
		 <!--- creer une requete http --->
        <cfhttp
            url="#VARIABLES.twilioSMSResource#"
            method="get"
            result="httpResponse"
		 	username="#VARIABLES.accountSid#"
	 		password="#VARIABLES.authToken#" >
		 		
			<cfhttpparam name="DateSent" value="#dateSent#" type="url">
         </cfhttp>
		
		<cfset deserializeReponse=deserializejson(httpResponse.Filecontent) >
		<!--- <cfdump var="#deserializeReponse#"> --->
		<cfset finalJson=serializejson(deserializeReponse) >
		<!--- <cfdump var="#finalJson#"> --->
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_api_sms.updateStateSmsTwilio">		
			<cfprocparam type="in" cfsqltype="CF_SQL_CLOB" value="#finalJson#" variable="p_response">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER"    variable="p_retour">	
		</cfstoredproc>	
	  
	</cffunction>
	
</cfcomponent>