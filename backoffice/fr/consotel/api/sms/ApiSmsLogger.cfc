<cfcomponent extends="fr.consotel.api.monitoring.MLogger">

	<cffunction access="public" name="logIt" returnType="void" hint="log un evenement de type Mlog">

		<cfargument name="mLog" type="fr.consotel.api.monitoring.MLog" required="true" hint="Evènement à logger">
		<cfset t_name="ApiSmsLogger_" & createUUID()>
		<cfset cfcName=getMetadata(THIS).NAME>

		<cfthread action="run" name="#t_name#" pmlog="#mLog#" cfcName="#cfcName#">

			<cftry>
				<cfset 	messageid=CGI.SERVER_NAME & CGI.SCRIPT_NAME>
				<cfset  subjectid=pmlog.getSUBJECTID()>
				<cfset  idracine=pmLog.getIDRACINE()>
				<cfset  bool_racine_null=TRUE>

				<cfif idracine GT 0>
					<cfset bool_racine_null=FALSE>
				</cfif>
				<cfif LEN(subjectid) GT 199>
					<cfset subjectid=LEFT(TRIM(subjectid),199)>
				</cfif>

				<cfif LEN(messageid) GT 199>
					<cfset messageid=LEFT(TRIM(messageid),199)>
				</cfif>

				<!---UUID API SMS --->
				<cfset ApiSmsUUID=pmlog.getUUID()>

				<!--- Ce stockage permet de requeter sur les champs les plus consultés lors du debug de Datalert --->
				<cfquery name="qInsert" dataSource="ROCOFFRE">
					INSERT into MNT_DETAIL(IDMNT_ID,IDMNT_CAT,BODY,SUBJECTID,IDRACINE,UUID,MESSAGEID)
					VALUES (
						<cfqueryparam cfsqltype="cf_sql_integer" value="#pmlog.getIDMNT_ID()#">,
					 	<cfqueryparam cfsqltype="cf_sql_integer" value="#pmlog.getIDMNT_CAT()#">,
					 	<cfqueryparam cfsqltype="cf_sql_clob" 	 value="#pmlog.getBODY()#">,
					 	<cfqueryparam cfsqltype="cf_sql_varchar" value="#subjectid#">,
						<cfqueryparam cfsqltype="cf_sql_integer" value="#idracine#" null="#bool_racine_null#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#ApiSmsUUID#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#messageid#">)
				</cfquery>
				<cfcatch type="Any">
					<cflog type="error" text="[API SMS logger] Error: #CFCATCH.message# - Detail: #CFCATCH.detail#">
				</cfcatch>
			</cftry>
		</cfthread>

		<!--- permet d'eviter une erreur coldfusion weblogic.work.ExecuteThread.run sinon il faut decocher les setting de server monitoring ( serveur cf) --->
		<cfthread action="join" name="#t_name#"/>

	</cffunction>

</cfcomponent>