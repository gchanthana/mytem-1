#! /bin/bash
# Convert all sms from txt (utf-8) to csv (iso-8859-15)
shopt -s nullglob
cd $(dirname $0)
FILES=./../fichiers/fichiersSfr/*.txt
for f in $FILES
do
	csv=`echo $f | cut -d "." -f 4`
	iconv -f utf-8 -t iso-8859-15 $f > ./../$csv.csv
done