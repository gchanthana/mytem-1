<cfinterface author="Cedric" displayname="fr.consotel.api.ibis.reporting.IReporting"
hint="Interface qui spécifie les propriétés requises pour définir un reporting à exécuter avec ReportingService<br>
En plus des propriétés du rapport à exécuter, le reporting doit spécifier les valeurs pour reportingDelivery() et reportingHandler() :<br>
Le 1er spécifie la stratégie à utiliser pour générer et diffuser le reporting<br>
Le 2ème spécifie un composant qui à exécuter après la diffusion du reporting">
<!--- *************************************************************************************************************************************** --->
	<!--- Paramètres  XDO pour BIP --->
	<cffunction name="xdoDirectory" returntype="String" hint="Répertoire de xdoName()">
	</cffunction>
	
	<cffunction name="xdoName" returntype="String" hint="Nom du XDO (Sans l'extension xdo)">
	</cffunction>
	
	<cffunction name="xdoTemplate" returntype="String" hint="Template du XDO (Sans l'extension xdo)">
	</cffunction>
	
	<cffunction name="xdoFormat" returntype="String" hint="Format du reporting. Valeurs possibles : xml,csv,html,pdf,excel,ppt">
	</cffunction>
	
	<cffunction name="xdoLocale" returntype="String" hint="Localisation au format (xx_XX)">
	</cffunction>
	
	<cffunction name="xdoParameters" returntype="Struct" hint="Paramètres du rapport BIP. Nom du paramètre associé à un tableau contenant ses valeurs (Séparateur virgule)">
	</cffunction>
	
	<!--- Paramètres spécifiques à Evènement --->
	<cffunction name="reportingId" returntype="Numeric" hint="Valeur numérique qui est restituée à la notification (Utilisé par DELIVERY et POSTPROCESS)">
	</cffunction>
	
	<cffunction name="reportingDelivery" returntype="String" hint="Retourne le suffixe du nom du CFC PostProcess (Doit implémenter IServiceHandler)">
	</cffunction>
	
	<cffunction name="reportingHandler" returntype="String" hint="Retourne le suffixe du nom du CFC pour EVENT_HANDLER">
	</cffunction>
	
	<!--- Paramètres spécifiques à la difussion CONTAINER et communs avec la diffusion MAIL --->
	<cffunction name="userId" returntype="numeric" hint="Identifiant de l'utilisateur CONTAINER">
	</cffunction>
	
	<cffunction name="codeApp" returntype="numeric" hint="CodeApp">
	</cffunction>
	
	<cffunction name="idRacine" returntype="numeric" hint="Identifiant ID_RACINE pour CONTAINER">
	</cffunction>
	
	<cffunction name="fileName" returntype="String" hint="Nom du fichier généré (Sans l'extension)">
	</cffunction>
	
	<cffunction name="fileExt" returntype="String" hint="Retourne l'extension du fichier">
	</cffunction>
	
	<!--- Paramètres spécifiques à la diffusion MAIL --->
	<cffunction name="mailFrom" returntype="String" hint="Retourne le champ MAIL_FROM">
	</cffunction>
	
	<cffunction name="mailTo" returntype="String" hint="Retourne le champ MAIL_TO">
	</cffunction>
	
	<cffunction name="mailCC" returntype="String" hint="Retourne le champ MAIL_CC">
	</cffunction>
	
	<cffunction name="mailSubject" returntype="String" hint="Retourne le champ MAIL_SUBJECT">
	</cffunction>
	
	<cffunction name="haveAttachment" returntype="boolean" hint="Retourne TRUE si le ou les rapports sont en attachement et FALSE en inline">
	</cffunction>
</cfinterface>