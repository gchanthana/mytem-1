<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.reporting.ReportingService"
hint="Implémentation qui définit un service permettant d'exécuter un reporting<br><br>
Un reporting est une implémentation de l'interface IReporting<br>
La valeur de reporting.reportingDelivery() est utilisée comme nom d'un CFC qui est instancié pour générer et diffuser le reporting<br>
Ce CFC doit vérifier les conditions suivantes :<br>
- Implémenter l'interface fr.consotel.api.ibis.publisher.delivery.IDelivery<br>
- Se trouver sous le package fr.consotel.api.ibis.publisher.delivery (e.g : test.MyDelivery, etc...)<br><br>
La valeur de reporting.reportingHandler() est utilisée comme suffixe du nom d'un CFC qui est instancié pour effectuer un traitement après la diffusion du reporting<br>
Ce CFC doit vérifier les conditions suivantes :<br>
- Implémenter l'interface fr.consotel.api.ibis.publisher.handler.IServiceHandler<br>
- Se trouver sous le package fr.consotel.api.ibis.publisher.handler.process (e.g : test2.MyPostProcessE00, etc...)">
<!--- *************************************************************************************************************************************** --->
	<!--- fr.consotel.api.ibis.reporting.ReportingService --->
	<cffunction name="execute" returntype="void"
	hint="Exécute un reporting puis son EVENT_HANDLER (POSTPROCESS)<br>
	Cette méthode ajoute une clé EVENT_ID dans le paramètre parameters qui est passé à IBIS.invokeService()<br>
	Sa valeur est restituée par IServiceEvent.getEventDispatcher() et son utilisation dépend de l'implémentation du POSTPROCESS">
		<cfargument name="reporting" type="fr.consotel.api.ibis.reporting.IReporting" required="true" hint="Reporting à exécuter">
		<cfset var reportingObj=ARGUMENTS.reporting>
		<cfset var reportingDelivery=createObject("component",VARIABLES.DELIVERY_PACKAGE & "." & reportingObj.reportingDelivery()).createInstance()>
		<!--- Structure passée en tant que paramètre parameters à fr.consotel.api.ibis.IBIS.invokeService() --->
		<cfset var ibisParameters=reportingDelivery.deliver(ARGUMENTS.reporting)>
		<!--- Exécution du reporting (Retourne le JOBID) --->
		<cfset var jobId=VARIABLES.IBIS.invokeService("IBIS","scheduleReport",ibisParameters)>
		<!--- Spécifique à Container --->
		<cfset var afterScheduledStatus=reportingDelivery.afterScheduled(ibisParameters.EVENT_TARGET,jobId)>
		<cflog text="ReportingService.execute(JOBID:#jobId#) : #ibisParameters.EVENT_TARGET#,#ibisParameters.EVENT_HANDLER#" file="#getLog()#" type="information">
	</cffunction>
	
	<cffunction name="createService" returntype="fr.consotel.api.ibis.reporting.ReportingService" hint="Constructeur de cette classe">
		<cfargument name="user" type="string" required="false" default="" hint="User du service">
		<cfargument name="pwd" type="string" required="false" default="" hint="Mdp du user du service">
		<cfset VARIABLES.SERVICE_LOG="REPORTING_SERVICE"><!--- Valeur de l'attribut file pour cflog pour cette implémentation --->
		<cfif NOT isDefined("VARIABLES.INITIALIZED")><!--- Flag d'initialisation --->
			<!--- Package pour les implémentations IDelivery --->
			<cfset VARIABLES.DELIVERY_PACKAGE="fr.consotel.api.ibis.publisher.delivery">
			<!--- Implémentation IBIS --->
			<cfset VARIABLES.IBIS_IMPL="fr.consotel.api.ibis.IBIS">
			<cfset VARIABLES.IBIS=createObject("component","fr.consotel.api.ibis.IBIS").createInstance()>
			<cflog text="ReportingService.createService() : Creation Completed" file="#getLog()#" type="information">
			<cfset VARIABLES.INITIALIZE=TRUE>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="getLog" returntype="string" hint="Valeur de l'attribut file pour cflog">
		<cfreturn VARIABLES.SERVICE_LOG>
	</cffunction>
</cfcomponent>