<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.publisher.BIP" hint="Factory pour instancier un Web Service BIP (org.apache.axis.client.*)">
	<cffunction access="public" name="getInstance" returntype="any" hint="Retourne une instance du web service BIP">
		<cfargument name="wsName" type="string" required="true" hint="Endpoint/URL du WSDL BIP (org.apache.axis.client.Stub)">
		<cftry>
			<cfreturn createObject("webservice",ARGUMENTS.wsName)>
			<cfcatch type="any">
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="BIP Instanciation Failed : #CFCATCH.Message#" detail="#CFCATCH.Detail#">
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>