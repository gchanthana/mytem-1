<cfcomponent displayname="fr.consotel.api.ibis.publisher.prototype.PublisherPrototype" hint="BI Publisher Scheduling Prototype (IBIS)">
	<!--- BIP Scheduling Prototype (Auteur : Cedric)
		NOTIFICATION TYPES : FAILED, WARNING, SUCCESS
		HTTP NOTIFICATION TARGET : IBIS
	--->

	<!--- EXPLICIT INITIALIZER --->
	<cfset initPrototype()>
	
	<cffunction access="private" name="initPrototype" returntype="void" hint="Initialize this prototype">
		<!--- SCHEDULE REQUEST --->
		<cfset VARIABLES.SCHEDULE_REQUEST=structNew()>
		<cfset VARIABLES.SCHEDULE_REQUEST.userJobName="">
		<cfset VARIABLES.SCHEDULE_REQUEST.jobLocale="fr_FR">
		<cfset VARIABLES.SCHEDULE_REQUEST.notifyWhenFailed=TRUE>
		<cfset VARIABLES.SCHEDULE_REQUEST.notifyWhenWarning=TRUE>
		<cfset VARIABLES.SCHEDULE_REQUEST.notifyWhenSuccess=TRUE>
		<cfset VARIABLES.SCHEDULE_REQUEST.schedulePublicOption=TRUE>
		<cfset VARIABLES.SCHEDULE_REQUEST.scheduleBurstringOption=FALSE>
		<cfset VARIABLES.SCHEDULE_REQUEST.saveDataOption=FALSE>
		<cfset VARIABLES.SCHEDULE_REQUEST.saveOutputOption=FALSE>
		<cfset VARIABLES.SCHEDULE_REQUEST.useUTF8Option=TRUE>
		<cfset VARIABLES.SCHEDULE_REQUEST.httpNotificationServer="IBIS">
		<!--- REPORT REQUEST --->
		<cfset VARIABLES.SCHEDULE_REQUEST.reportRequest=structNew()>
		<cfset VARIABLES.SCHEDULE_REQUEST.reportRequest.attributeFormat="xml">
		<cfset VARIABLES.SCHEDULE_REQUEST.reportRequest.byPassCache=FALSE>
		<cfset VARIABLES.SCHEDULE_REQUEST.reportRequest.attributeLocale=VARIABLES.SCHEDULE_REQUEST.jobLocale>
		<cfset VARIABLES.SCHEDULE_REQUEST.reportRequest.attributeTemplate="">
		<cfset VARIABLES.SCHEDULE_REQUEST.reportRequest.reportAbsolutePath="">
		<!--- REPORT PARAMETERS --->
		<cfset VARIABLES.SCHEDULE_REQUEST.reportRequest.parameterNameValues=arrayNew(1)>
		<!--- DELIVERY REQUEST --->
		<cfset VARIABLES.SCHEDULE_REQUEST.deliveryRequest=structNew()>
	</cffunction>
	
	<cffunction access="public" name="cloneScheduleRequest" returntype="struct" hint="Return a DEEP COPY of this instance ScheduleRequest Element">
		<cfreturn DUPLICATE(VARIABLES.SCHEDULE_REQUEST)>
	</cffunction>
</cfcomponent>