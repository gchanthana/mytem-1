<cfcomponent displayname="fr.consotel.api.ibis.publisher.BipCmdWebServiceFactory" extends="fr.consotel.api.ibis.publisher.BipWebServiceFactory" hint="BIP Commande WebService Factory">
	<!--- BIP WEBSERVICE FACTORY (Auteur : Cedric)
		- VARIABLES.WSDL_ENDPOINT : Named WSDL Url Address registered in Server Configuration
		- VARIABLES.WSDL_ENDPOINT_HELPER : CFC used to retrieve WSDL Address if fails with WSDL_ENDPOINT
		- VARIABLES.HELPER_WSDL_SUFFIX : WSDL Suffix used for Helper
	--->
	
	<!--- EXPLICIT INITIALIZATION --->
	<cfset initFactory()>
	
	<cffunction access="private" name="initFactory" returntype="void" hint="Initialize this factory instance">
		<cfset super.initFactory()>
		<cfset VARIABLES.WSDL_ENDPOINT="http://bip-commande.consotel.fr/xmlpserver/services/PublicReportService_v11?WSDL">
	</cffunction>
	
</cfcomponent>
