<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.event.FtpEvent" extends="fr.consotel.api.ibis.publisher.handler.event.JobEvent"
hint="Evènement permettant d'accéder aux infos concernant un reporting généré en FTP. Ces infos sont récupérés à partir des infos et historique du JOB
Les méthodes getContent() et getContentAsLocalFile() effectuent un téléchargement du fichier FTP en fonction de la valeur du paramètre asboluteLocalPath
Ces méthodes sont redéfinies pour permettre l'utilisation d'un répertoire temporaire prédéfini par cette implémentation pour le paramètre localDirAbsPath
Lorsque la valeur du paramètre localDirAbsPath est une chaine vide alors le fichier localFileName est stocké dans un répertoire temporaire prédéfini
La connexion FTP utilisée pour le téléchargement de fichier n'est pas mise en cache (i.e : Ne sont pas nommées) et l'encodage utilisé est UTF-8
Le téléchargement avec une connexion SFTP n'est pas supporté (Le provider Java JCE utilisé par défaut par ColdFusion n'est pas compatible)
Le séparateur de chemin utilisé pour le FTP est toujours / et celui pour les chemins locaux est la valeur retournée par getLocalPathSep()">
	<!--- fr.consotel.api.ibis.publisher.handler.event.FtpEvent --->
	<cffunction access="public" name="getFtpServer" returntype="String" hint="Retourne le DNS du serveur FTP sur lequel le reporting a été généré">
		<cfreturn getFtpOption().getFtpServerName()>
	</cffunction>
	
	<cffunction access="public" name="getRemoteFilePath" returntype="String" hint="Retourne le chemin relatif du fichier généré en FTP">
		<cfreturn getFtpOption().getRemoteFile()>
	</cffunction>
	
	<cffunction access="public" name="getFtpUserName" returntype="String" hint="Retourne le login utilisateur FTP utilisé">
		<cfreturn getFtpOption().getFtpUserName()>
	</cffunction>
	
	<cffunction access="public" name="getFtpUserPassword" returntype="String" hint="Retourne le mot de passe FTP utilisé">
		<cfreturn getFtpOption().getFtpUserPassword()>
	</cffunction>
	
	<cffunction access="public" name="isSFTP" returntype="boolean" hint="Retourne TRUE une connexion SFTP a été utilisée par BIP">
		<cfreturn getFtpOption().isSftpOption()>
	</cffunction>
	
	<cffunction access="private" name="getFtpOption" returntype="any" hint="Retourne l'implémentation BIP de l'option FTP.
	Type : com.oracle.xmlns.oxp.service.PublicReportService.FTPDeliveryOption. Une structure vide est retournée si l'info n'est pas définie">
		<cfset var ftpOption={}>
		<cfset var jobInfo=getJobInfo()>
		<cfset var deliveryRequest=jobInfo.getDeliveryParameters()>
		<cfif isDefined("deliveryRequest")>
			<cfset ftpOption=deliveryRequest.getFtpOption()>
		</cfif>
		<cfreturn ftpOption>
	</cffunction>
	
	<cffunction access="private" name="deleteLocalTempSubDir" returntype="String"
	hint="Supprime un répertoire temporaire créé avec createLocalTempSubDir() et retourne le chemin absolu du répertoire supprimé
	Le paramètre qui est fourni doit correspondre au chemin absolu du fichier téléchargé localement lorsque le répertoire temporaire prédéfini est utilisé
	Cela signifie que la valeur du paramètre localDirAbsPath passé à getContent() ou getContentAsLocalFile() est une chaine vide
	Une exception est levée si une des conditions suivantes est vérifiée :
	- Le répertoire parent du répertoire contenu le fichier asboluteLocalPath n'est pas getLocalTempDir()
	- Le répertoire parent du fichier asboluteLocalPath n'existe pas sous getLocalTempDir()">
	<cfargument name="asboluteLocalPath" type="String"  required="true"
	hint="Chemin absolu retourné par getContentAsLocalFile() lorsque le paramètre localDirAbsPath qui lui est passé est une chaine vide">
		<cfset var localFileAbsPath=ARGUMENTS["asboluteLocalPath"]>
		<!--- Nom du fichier local --->
		<cfset var localFileName=listLast(localFileAbsPath,getLocalPathSep())>
		<cfset var localDirPathLen=LEN(localFileAbsPath) - LEN(localFileName)>
		<!--- Chemin absolu du répertoire à supprimer --->
		<cfset var localDirAbsPath=LEFT(localFileAbsPath,localDirPathLen)>
		<!--- Si le répertoire à supprimer existe et que son répertoire parent est getLocalTempDir() : Effectue sa suppression--->
		<cfif (LEFT(localDirAbsPath,LEN(getLocalTempDir())) EQ getLocalTempDir()) AND directoryExists(localDirAbsPath)>
			<cfdirectory action="delete" recurse="true" directory="#localDirAbsPath#">
			<cfset getReportingService().logInfo("FtpEvent TEMP DIR DELETED : " & localDirAbsPath)>
			<cfreturn localDirAbsPath>
		<!--- Si le répertoire parent du répertoire à supprimer n'est pas getLocalTempDir() ou qu'il n'existe pas : Une exception est levée --->
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION"
				detail="Suppression du répertoire : #localDirAbsPath#" message="Le répertoire #localDirAbsPath# n'existe pas">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="createLocalTempSubDir" returntype="String"
	hint="Crée un répertoire temporaire sous getLocalTempDir() et retourne son chemin absolu (Doit inclure le séparateur de chemin en dernier caractère)
	Ce sous répertoire est utilisé par getContentAsLocalFile() pour stocker temporairement le fichier FTP qui est téléchargé localement
	Le nom donné au sous répertoire créé est un UUID unique retourné par la fonction CFMX createUUID()
	Une exception est levée si le répertoire getLocalTempDir() n'existe pas">
		<!--- Chemin absolu du répertoire parent pour celui qui est à créer --->
		<cfset var localParentDir=getLocalTempDir()>
		<!--- Si le répertoire getLocalTempDir() existe : Le sous répertoire temporaire est créé --->
		<cfif directoryExists(localParentDir)>
			<!--- Création du sous répertoire temporaire sous getLocalTempDir() --->
			<cfset localTempDir=localParentDir & createUUID() & getLocalPathSep()>
			<cfset getReportingService().logInfo("FtpEvent TEMP DIR : " & localTempDir)>
			<cfdirectory action="create" directory="#localTempDir#">
			<cfreturn localTempDir>
		<!--- Sinon le répertoire getLocalTempDir() n'existe pas et une exception est levée --->
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION"
				detail="Création du sous répertoire temporaire" message="Le répertoire temporaire local #localParentDir# n'existe pas">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="getLocalTempDir" returntype="String"
	hint="Retourne le chemin absolu du répertoire local utilisé pour stocker les fichiers FTP téléchargés (Doit inclure le séparateur de chemin en dernier caractère)
	Cette méthode retourne la valeur de getTempDirectory() et est utilisée par getContent() lorsque le chemin du fichier à créer localement n'est pas fourni">
		<cfreturn getTempDirectory()>
	</cffunction>
	
	<cffunction access="private" name="getFtpParameters" returntype="Struct"
	hint="Cette méthode est utilisée par getContenAsLocalFile() pour effectuer obtenir les paramètres FTP utilisés pour le téléchargement du reporting 
	Retourne les propriétés FTP utilisées avec CFFTP pour le téléchargement sans l'attribut localFile. La connexion utilisée n'est pas mise en cache.
	Ces propriétés sont : secure=FALSE, server=Valeur de getFtpServer(), username=Valeur de getFtpUserName(), password=Valeur de getFtpUserPassword(),
	stopOnError=TRUE, action=getFile, transfermode=AUTO, failIfExists=FALSE, remoteFile=Valeur de getRemoteFilePath()">
		<!--- ATTENTION : La balise cfftp n'a pas d'attribut charset. Une erreur se produira si cet attribut est spécifié --->
		<cfreturn {
			secure=FALSE, server=getFtpServer(), username=getFtpUserName(), password=getFtpUserPassword(),
			stoponerror=TRUE, action="getfile", transfermode="auto", failifexists=FALSE, remotefile=getRemoteFilePath()
		}>
	</cffunction>
	
	<cffunction access="private" name="downloadFTP" returntype="String"
	hint="Télécharge localement le fichier FTP du reporting et retourne le chemin absolu du fichier local. Tout fichier existant est écrasé">
		<cfargument name="asboluteLocalPath" type="String"  required="true" hint="Chemin absolu du fichier local correspondant au fichier téléchargé">
		<cfset var absLocalPath=ARGUMENTS["asboluteLocalPath"]>
		<!--- Téléchargement du fichier FTP en local en utilisant ces paramètres FTP --->
		<cfset var getParamTick=getTickCount()>
		<cfset var ftpParams=getFtpParameters()>
		<cfset ftpParams.localFile=absLocalPath>
		<cfset getReportingService().logInfo("FtpEvent FTP DOWNLOAD FROM : " & getFtpServer())>
		<cfftp attributecollection="#ftpParams#">
		<cfset getReportingService().logInfo("FtpEvent DOWNLOADED AFTER : " & ((getTickCount() - getParamTick) / 1000) & "s")>
		<cfset getReportingService().logInfo("FtpEvent FILE DOWNLOADED : " & absLocalPath)>
		<cfreturn absLocalPath>
	</cffunction>
	
	<cffunction access="private" name="getLocalFileContent" returntype="any" hint="Retourne le contenu du fichier asboluteLocalPath.
	Une lecture en mode texte est effectuée si isTextContent() retourne TRUE et une lecture binaire sinon. L'encodage utilisé est UTF-8">
		<cfargument name="asboluteLocalPath" type="String"  required="true" hint="Chemin absolu du fichier dont le contenu est à retourner">
		<cfargument name="fileFormat" type="String"  required="true" hint="Format du fichier">
		<cfset var fileContent="">
		<cfset var absLocalPath=ARGUMENTS["asboluteLocalPath"]>
		<cfset var contentFormat=ARGUMENTS["fileFormat"]>
		<!--- Propriétés et paramètres utilisés pour la lecture du fichier --->
		<cfset var fileProps={file=absLocalPath, charset="utf-8", variable="fileContent"}>
		<!--- Lecture d'un fichier texte --->
		<cfif isTextContent()>
			<cffile action="read" attributecollection="#fileProps#">
		<!--- Lecture d'un fichier binaire --->
		<cfelse>
			<cffile action="readbinary" attributecollection="#fileProps#">
		</cfif>
		<cfreturn fileContent>
	</cffunction>
	
	<!--- fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent --->
	<cffunction access="public" name="getContentType" returntype="String" hint="Retourne type MIME du contenu généré en FTP
	Sa valeur correspond au type MIME du contenu récupéré à partir du JobHistoryInfo BIP">
		<cfreturn SUPER.getReportContentType()>
	</cffunction>
	
	<cffunction access="public" name="isTextContent" returntype="boolean"
	hint="Retourne TRUE si getFormat() vaut une des valeurs suivantes : xml, csv, html">
		<cfreturn (getFormat() EQ "xml") OR (getFormat() EQ "csv") OR (getFormat() EQ "html")>
	</cffunction>
	
	<cffunction access="public" name="isBinaryContent" returntype="boolean"
	hint="Retourne TRUE si getFormat() vaut une des valeurs suivantes : excel, pdf">
		<cfreturn (getFormat() EQ "excel") OR (getFormat() EQ "pdf")>
	</cffunction>
	
	<cffunction access="public" name="isUnknownContent" returntype="boolean"
	hint="Retourne TRUE si getFormat() n'est pas égal à une des valeurs suivantes : xml, csv, html, excel, pdf">
		<cfreturn (getFormat() NEQ "xml") AND (getFormat() NEQ "csv") AND
			(getFormat() EQ "html") AND (getFormat() EQ "excel") AND (getFormat() EQ "pdf")>
	</cffunction>
	
	<cffunction access="public" name="getContent" returntype="any"
	hint="Télécharge localement le fichier FTP correspondant au reporting en utilisant getContentAsLocalFile() et retourne son contenu
	Chaque appel à cette méthode correspond à un nouveau téléchargement du fichier FTP si une des conditions est vérifiée :
	- Le fichier localFileName n'est pas présent dans localDirAbsPath
	- La valeur de localDirAbsPath est une chaine vide
	- La valeur de localFileName est une chaine vide et il n'y a pas de fichier portant le meme nom que le fichier FTP dans localDirAbsPath
	Une lecture en mode texte est effectuée si isTextContent() retourne TRUE et en mode binaire sinon
	Le répertoire temporaire local est supprimé lorsque le contenu du fichier a été récupéré (i.e : Lorsque localDirAbsPath est une chaine vide)
	Le contenu générique retourné par la méthode getContent() de la classe parent est retourné si getStatus() est égal à : FAILURE() ou EXCEPTION()">
		<cfargument name="localFileName" type="String"  required="true" hint="Chaine vide pour utiliser le nom du fichier FTP">
		<cfargument name="localDirAbsPath" type="String"  required="true" hint="Chaine vide pour utiliser le répertoire temporaire prédéfini">
		<!--- Répertoire local de téléchargement --->
		<cfset var localDir=ARGUMENTS["localDirAbsPath"]>
		<!--- Téléchargement du fichier FTP en local et Récupération de son contenu --->
		<cfset var absLocalPath=getContentAsLocalFile(ARGUMENTS["localFileName"],localDir)>
		<cfset var fileContent=getLocalFileContent(absLocalPath,getFormat())>
		<!--- Si le répertoire local n'a pas été renseigné : Supprime le répertoire temporaire qui a été créé --->
		<cfif LEN(TRIM(localDir)) EQ 0>
			<cfset deleteLocalTempSubDir(absLocalPath)>
		</cfif>
		<cfreturn fileContent>
	</cffunction>
	
	<cffunction access="public" name="getContentAsLocalFile" returntype="String"
	hint="Télécharge localement le fichier FTP correspondant au reporting et retourne le chemin absolu du fichier local
	Le fichier téléchargé a pour nom localFileName si sa valeur n'est pas une chaine vide et celui du fichier FTP sinon
	Le fichier est stocké localement dans le répertoire localDirAbsPath si sa valeur n'est une chaine vide et dans un répertoire temporaire local sinon
	Dans ce dernier cas le répertoire temporaire local est créé avec un nom unique qui est un UUID et sous un répertoire prédéfini par cette implémentation">
		<cfargument name="localFileName" type="String"  required="true" hint="Chaine vide pour utiliser le nom du fichier FTP">
		<cfargument name="localDirAbsPath" type="String"  required="true" hint="Chaine vide pour utiliser le répertoire temporaire prédéfini">
		<cfset var localFile=ARGUMENTS["localFileName"]>
		<cfset var localDirPath=ARGUMENTS["localDirAbsPath"]>
		<!--- Si le nom du fichier local est un chemin absolu ou relatif : Lève une exception --->
		<cfif arraylen(listToArray(localFile,getLocalPathSep(),TRUE)) GT 1>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION"
				message="La valeur de localFileName n'est pas un nom de fichier valide" detail="Téléchargement FTP en local avec le nom de fichier : #localFile#">
		</cfif>
		<!--- Si le nom du fichier local n'est pas renseigné : Utilise le nom du fichier FTP comme nom local --->
		<cfif LEN(TRIM(localFile)) EQ 0>
			<cfset localFile=listLast(getRemoteFilePath(),"/")>
		</cfif>
		<!--- Si le répertoire local n'est pas renseigné : Crée un sous répertoire temporaire local et télécharge le fichier --->
		<cfif LEN(TRIM(localDirPath)) EQ 0>
			<cfset localDirPath=createLocalTempSubDir()>
		</cfif>
		<!--- Si le chemin du répertoire local est un chemin absolu : Vérifie qu'il est valide par rapport à cette implémentation --->
		<cfif LEFT(localDirPath,1) EQ getLocalPathSep()>
			<!--- Si le chemin du répertoire local se termine par le séparateur de chemin : Vérifie son existence --->
			<cfif RIGHT(localDirPath,1) EQ getLocalPathSep()>
				<!--- Si le répertoire local existe : Effectue le téléchargement local du fichier FTP --->
				<cfif directoryExists(localDirPath)>
					<cfreturn downloadFTP(localDirPath & localFile)>
				 <!--- Si le répertoire local n'existe pas : Une exception est levée --->
				<cfelse>
					<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION"
						detail="Chemin du répertoire local : #localDirPath#" message="Echec du téléchargement FTP du reporting : Le répertoire local n'existe pas">
				</cfif>
			<!--- Si le chemin du répertoire local ne se termine pas par le séparateur de chemin : Lève une exception --->
			<cfelse>
				<cfset var errorDetails="Chemin du répertoire local " & localDirPath & " n'est pas un chemin absolu">
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" detail="Chemin du répertoire local : #localDirPath#"
					message="Echec du téléchargement FTP du reporting : Le chemin absolu du répertoire local doit se terminer par #getLocalPathSep()#">
			</cfif>
		<!--- Si le chemin du répertoire local n'est pas un chemin absolu : Une exception est levée --->
		<cfelse>
			<cfset var errorDetails="Chemin du répertoire local " & localDirPath & " n'est pas un chemin absolu">
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION"
				message="Echec du téléchargement FTP du reporting : Le chemin du répertoire local n'est pas un chemin absolu" detail="#errorDetails#">
		</cfif>
	</cffunction>

	<cffunction access="public" name="getDeliveryInfos" returntype="any" hint="Retourne le contenu retourné par getDeliveryInfos() incluant des infos FTP">
		<cfset var deliveryInfos="">
		<cfset var deliveryContent=SUPER.getDeliveryInfos()>
		<cfsavecontent variable="deliveryInfos">
			<cfoutput>
				#deliveryContent#<br><br>
				<b>Les propriétés de diffusion FTP provenant de l'évènement sont :</b><br>
				Serveur : #getFtpServer()#<br>
				Utilisateur : #getFtpUserName()#<br>
				Mot de passe : #getFtpUserPassword()#<br>
				Connexion SFTP : #isSFTP()#<br>
				Fichier : #getRemoteFilePath()#<br>
				Type MIME (FTP) : #getContentType()#
			</cfoutput>
		</cfsavecontent>
		<cfreturn deliveryInfos>
	</cffunction>
</cfcomponent>