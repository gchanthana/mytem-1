<cfinterface author="Cedric" displayname="fr.consotel.api.ibis.publisher.handler.event.INotificationEvent"
hint="Interface spécifiant les méthodes de l'évènement correspondant à la notification reçue par le service de reporting">
	<cffunction name="createNotificationEvent" returntype="fr.consotel.api.ibis.publisher.handler.event.INotificationEvent" hint="Crée et retourne une instance">
		<cfargument name="notificationInfos" type="Struct" required="true" hint="Contient les infos de notification pour créer l'évènement">
	</cffunction>

	<cffunction name="getJob" returntype="Struct" hint="Infos concernant l'exécution ou la planification (i.e : Le résultat de ce qui a été exécuté ou planifié)">
	</cffunction>

	<cffunction name="getReporting" returntype="Struct" hint="Infos concernant le reporting (i.e : Ce qui a été exécuté ou planifié)">
	</cffunction>

	<cffunction name="getDelivery" returntype="Struct" hint="Infos et propriétés concernant la stratégie de diffusion">
	</cffunction>
	
	<cffunction name="getPostProcess" returntype="Struct" hint="Infos et propriétés concernant la stratégie de traitement de fin">
	</cffunction>
</cfinterface>