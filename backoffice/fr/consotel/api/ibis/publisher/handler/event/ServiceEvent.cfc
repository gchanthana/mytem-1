<cfcomponent displayName="fr.consotel.api.ibis.publisher.handler.event.ServiceEvent"
		implements="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" hint="Service EVENT Implementation">
	<!--- 
		EVENT DATA key list :
			- EVENT_TARGET_VALUE (STRING) : EVENT_TARGET value. MAX LENGTH : 3
			- EVENT_TYPE (STRING) : EVENT_TYPE value. MAX LENGTH : 3
	 --->
	<cffunction access="public" name="createInstance" returntype="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" hint="Initializes and returns this instance. May throw an exception.">
		<cfargument name="eventData" type="struct" required="true" hint="Structure containing parameters to initialize and return this instance. A deep copy of this parameter will be stored in this instance">
		<cfset var eventTargetValueKey="EVENT_TARGET_VALUE">
		<cfset var eventTypeKey="EVENT_TYPE">
		<cfset var EVENT_TYPE_MAX_LENGTH=3>
		<cfset VARIABLES["IS_INITIALIZED"]=FALSE>
		<cfif structKeyExists(ARGUMENTS["eventData"],eventTypeKey)>
			<cfif structKeyExists(ARGUMENTS["eventData"],eventTargetValueKey)>
				<cfset VARIABLES["EVENT_TARGET"]=
						createObject("component","fr.consotel.api.ibis.publisher.handler.EventTarget").createInstance(ARGUMENTS["eventData"][eventTargetValueKey])>
					<cfset VARIABLES["EVENT_TYPE"]=ARGUMENTS["eventData"][eventTypeKey]>
					<cfset VARIABLES["EVENT_DATA"]=DUPLICATE(eventData)>
					<cfset VARIABLES["IS_INITIALIZED"]=TRUE>
					<cfreturn THIS>
			<cfelse>
				<cfthrow type="Custom" errorcode="ILLEGAL_ARGUMENT_EXCEPTION" message="Missing #eventTargetValueKey# key in eventData parameter">
			</cfif>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_ARGUMENT_EXCEPTION" message="#eventTypeKey# is missing OR is not a valid String OR has an invalid LENGTH">
		</cfif>
	</cffunction>
			
	<cffunction access="public" name="getEventTarget" returntype="fr.consotel.api.ibis.publisher.handler.IEventTarget" hint="Returns EVENT TARGET. May throws an exception">
		<cfset var eventTargetKey="EVENT_TARGET">
		<cfif isInitialized() EQ TRUE>
			<cfreturn VARIABLES[eventTargetKey]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_STATE_EXCEPTION" message="THIS instance was not created using createInstance()">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getEventType" returntype="string" hint="Returns EVENT_TYPE">
		<cfset var eventTypeKey="EVENT_TYPE">
		<cfif isInitialized() EQ TRUE>
			<cfreturn VARIABLES[eventTypeKey]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_STATE_EXCEPTION" message="THIS instance was not created using createInstance()">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="isInitialized" returntype="boolean" hint="Returns TRUE if this instance was initialized using createInstance and FALSE otherwise">
		<cfset var isInitKey="IS_INITIALIZED">
		<cfif structKeyExists(VARIABLES,isInitKey)>
			<cfreturn VARIABLES[isInitKey]>
		<cfelse>
			<cfreturn FALSE>
		</cfif>
	</cffunction>
</cfcomponent>
