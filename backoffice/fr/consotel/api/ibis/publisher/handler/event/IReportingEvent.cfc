<cfinterface author="Cedric" displayname="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent"
hint="Interface qui décrit l'évènement correspondant à l'exécution d'un reporting et qui est généré puis dispatché par une implémentation IReportingService">
	<cffunction name="getStatus" returntype="String" hint="Retourne le statut du reporting">
	</cffunction>

	<cffunction name="getReportingName" returntype="String" hint="Retourne le nom du reporting">
	</cffunction>
	
	<cffunction name="getDeliveryType" returntype="String" hint="Retourne le type de la stratégie de diffusion spécifiée par le reporting">
	</cffunction>
	
	<cffunction name="getReportingService" returntype="fr.consotel.api.ibis.publisher.reporting.IReportingService"
	hint="Retourne une instance de l'implémentation IReportingService qui a généré et dispatché cet évènement
	L'instance retournée utilise les memes credentials que ceux utilisés lors de l'exécution du reporting">
	</cffunction>
	
	<cffunction name="getBipDNS" returntype="String" hint="Retourne le DNS du serveur BIP qui a été utilisé pour l'exécution du reporting">
	</cffunction>
	
	<cffunction name="getBipSessionInterval" returntype="Numeric" hint="Retourne la durée Session Interval du serveur BIP en minutes">
	</cffunction>
	
	<cffunction name="getBackOffice" returntype="String" hint="Retourne le nom du BackOffice utilisé">
	</cffunction>
	
	<cffunction name="getJob" returntype="Struct" hint="Retourne une structure contenant les propriétés relatives au JOB">
	</cffunction>

	<cffunction name="getReporting" returntype="Struct" hint="Retourne une structure contenant les propriétés relatives au reporting">
	</cffunction>

	<cffunction name="getDelivery" returntype="Struct"
	hint="Retourne une structure contenant les propriétés provenant de la méthode getDeliveryProperties() de la stratégie de diffusion utilisée">
	</cffunction>
	
	<cffunction name="getXDO" returntype="Struct" hint="Retourne une structure contenant les paramètres spécifiques au XDO">
	</cffunction>
	
	<cffunction name="RUNNING" returntype="String" hint="Retourne la valeur d'un statut correspondant à un reporting en cours d'exécution">
	</cffunction>

	<cffunction name="FAILURE" returntype="String" hint="Retourne la valeur d'un statut correspondant à un reporting en Erreur">
	</cffunction>
	
	<cffunction name="WARNING" returntype="String" hint="Retourne la valeur d'un statut correspondant à un reporting en Warning">
	</cffunction>
	
	<cffunction name="SUCCESS" returntype="String" hint="Retourne la valeur d'un statut correspondant à un reporting en Succès">
	</cffunction>
	
	<cffunction name="EXCEPTION" returntype="String" hint="Retourne la valeur d'un statut correspondant à un reporting avec une exception capturée">
	</cffunction>
</cfinterface>