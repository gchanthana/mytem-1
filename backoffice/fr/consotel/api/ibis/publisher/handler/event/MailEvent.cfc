<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.event.MailEvent" extends="fr.consotel.api.ibis.publisher.handler.event.EmailDiffusionEvent"
hint="Evènement qui est dispatché par la stratégie fr.consotel.api.ibis.publisher.delivery.Mail">
	<cffunction access="public" name="getDeliveryType" returntype="String">
		<cfreturn "Evenement">
	</cffunction>
</cfcomponent>