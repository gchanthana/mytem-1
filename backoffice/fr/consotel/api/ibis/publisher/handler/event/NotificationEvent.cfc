<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.event.NotificationEvent"
implements="fr.consotel.api.ibis.publisher.handler.event.INotificationEvent"
hint="Implémentation par défaut de INotificationEvent créée par l'API et utilisée par le service reporting
Les clés INFOS et HISTORY ne sont présentes que si les infos et historique du JOB sont définies et/ou ont pu être récupérées
Les méthodes retournent une structure vide si l'infos correspondante n'est pas définie. Ex : getJob(), getReporting()">
	<cffunction access="public" name="createNotificationEvent" returntype="fr.consotel.api.ibis.publisher.handler.event.INotificationEvent">
		<cfargument name="notificationInfos" type="Struct" required="true" hint="Contient les infos de notification pour créer l'évènement">
		<cfif NOT isDefined("VARIABLES.EVENT_DATA")>
			<cfset VARIABLES.EVENT_DATA=ARGUMENTS.notificationInfos>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="public" name="getJob" returntype="Struct" hint="Infos concernant l'exécution ou la planification (i.e : Le résultat de ce qui a été exécuté ou planifié)">
		<cfif structKeyExists(getData(),"JOB")>
			<cfreturn getData().JOB>
		<cfelse>
			<cfreturn {}>
		</cfif>
	</cffunction>

	<cffunction access="public" name="getReporting" returntype="Struct" hint="Infos concernant le reporting (i.e : Ce qui a été exécuté ou planifié)">
		<cfif structKeyExists(getData(),"REPORTING")>
			<cfreturn getData().REPORTING>
		<cfelse>
			<cfreturn {}>
		</cfif>
	</cffunction>

	<cffunction access="public" name="getDelivery" returntype="Struct" hint="Infos et propriétés concernant la stratégie de diffusion">
		<cfif structKeyExists(getData(),"DELIVERY")>
			<cfreturn getData().DELIVERY>
		<cfelse>
			<cfreturn {}>
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getPostProcess" returntype="Struct" hint="Infos et propriétés concernant la stratégie de traitement de fin">
		<cfif structKeyExists(getData(),"POST_PROCESS")>
			<cfreturn getData().POST_PROCESS>
		<cfelse>
			<cfreturn {}>
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="SCHEDULED" returntype="String" hint="Retourne le type correspondant à un rapport planifié (Le JOBID est disponible)">
		<cfreturn "C">
	</cffunction>

	<cffunction access="public" name="FAILURE" returntype="String" hint="Retourne le type correspondant à un rapport généré avec un statut en Erreur">
		<cfreturn "F">
	</cffunction>
	
	<cffunction access="public" name="WARNING" returntype="String" hint="Retourne le type correspondant à un rapport généré avec un statut en Warning">
		<cfreturn "W">
	</cffunction>
	
	<cffunction access="public" name="SUCCESS" returntype="String" hint="Retourne le type correspondant à un rapport généré avec un statut en Succès">
		<cfreturn "S">
	</cffunction>
	
	<cffunction access="private" name="getData" returntype="Struct" hint="Retourne les propriétés de l'évènement en tant que structure
	Une structure vide est retournée si les propriétés ne sont pas définies. Méthode utilisée par les classes dérivées pour étendre les méthodes de l'évènement">
		<cfif isDefined("VARIABLES.EVENT_DATA")>
			<cfreturn VARIABLES.EVENT_DATA>
		<cfelse>
			<cfreturn {}>
		</cfif>
	</cffunction>
</cfcomponent>