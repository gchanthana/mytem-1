<cfcomponent displayName="fr.consotel.api.ibis.publisher.handler.event.ReportEvent"
		implements="fr.consotel.api.ibis.publisher.handler.event.IReportEvent" hint="Reporting EVENT Implementation">
	<!--- (BIP) REPORT EVENT INTERFACE (Auteur : Cedric)
		EVENT DATA key list :
			- EVENT_DISPATCHER : Value of CGI.HTTP_HOST in the received HTTP REQUEST (POST)
			- EVENT_TARGET : Event Target (STRING). MAX LENGTH : 35
			- EVENT_TYPE : Event Type (STRING). MAX LENGTH : 3
			- EVENT_HANDLER : Event Handler (STRING). MAX LENGTH : 3
			- BIP_SERVER : BIP Server DNS from DNS_MAPPING
			- JOB_ID : QUARTZ JOB_ID
			- REPORT_STATUS : QUARTZ REPORT_STATUS
			- REPORT_URL : QUARTZ REPORT_URL
	 --->
	
	<cffunction access="public" name="createInstance" returntype="fr.consotel.api.ibis.publisher.handler.event.IReportEvent"
											hint="Initializes and returns a new updated instance. Throws an exception if instanciation fails">
		<cfargument name="eventData" type="struct" required="true" hint="Structure containing parameters to initialize and return this instance">
		<cfset VARIABLES.EVENT_DATA=ARGUMENTS.eventData>
		<cfreturn THIS>
	</cffunction>

	<cffunction access="public" name="getEventTarget" returntype="String" hint="Returns EVENT_TARGET as STRING value. Check returned TYPE as this returntype can change">
		<cfreturn VARIABLES.EVENT_DATA.EVENT_TARGET>
	</cffunction>
	
	<cffunction access="public" name="getEventType" returntype="string" hint="Returns EVENT_TYPE as STRING value">
		<cfreturn VARIABLES.EVENT_DATA.EVENT_TYPE>
	</cffunction>
	
	<cffunction access="public" name="getJobId" returntype="numeric" hint="Returns BI Publisher JOB_ID">
		<cfreturn VARIABLES.EVENT_DATA.JOB_ID>
	</cffunction>
	
	<cffunction access="public" name="getReportStatus" returntype="string" hint="Returns BI Publisher REPORT_STATUS">
		<cfreturn VARIABLES.EVENT_DATA.REPORT_STATUS>
	</cffunction>

	<cffunction access="public" name="getReportPath" returntype="string" hint="Returns BI Publisher REPORT_URL">
		<cfreturn VARIABLES.EVENT_DATA.REPORT_URL>
	</cffunction>
	
	<cffunction access="public" name="getBipServer" returntype="string" hint="Returns BI Publisher SERVER_NAME">
		<cfreturn VARIABLES.EVENT_DATA.BIP_SERVER>
	</cffunction>
	
	<cffunction access="public" name="getEventDispatcher" returntype="string" hint="(Subject to modifications) Returns HTTP_HOST that sends (HTTP NOTIFICATION TARGET)">
		<cfreturn VARIABLES.EVENT_DATA.EVENT_DISPATCHER>
	</cffunction>
</cfcomponent>
