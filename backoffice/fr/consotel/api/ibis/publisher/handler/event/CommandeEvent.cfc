<cfcomponent displayName="fr.consotel.api.ibis.publisher.handler.event.CommandeEvent" implements="fr.consotel.api.ibis.publisher.handler.event.IReportEvent" hint="Reporting EVENT Implementation">

	<cfset statusList = ''>


	<cffunction access="public" name="comandeEventInit" returntype="void" hint="Constructeur">
		<cfargument name="evtTarget" type="fr.consotel.api.ibis.publisher.handler.event.IReportEvent" required="true" hint="Event recu par la methode run()">
			
			<cfset VARIABLES.eventOrigine = evtTarget>
		
	</cffunction>

	<cffunction access="public" name="getJobId" returntype="numeric" hint="Returns BI Publisher JOB_ID">
		<cfreturn VARIABLES.eventOrigine.getJobId()>
	</cffunction>
	
	<cffunction access="public" name="getReportStatus" returntype="string" hint="Returns BI Publisher REPORT_STATUS : S (SUCCESS), F (FAILED), C (RUNNING)">
			
			<cfset myreportstatus = 'F'>
			
			<cfif VARIABLES.eventOrigine.getReportStatus() EQ 'S'>
				<cfif respositoryExist() EQ TRUE>
					<cfset myreportstatus = 'S'>
				<cfelse>	
					<cfset myreportstatus = 'R'>
				</cfif>
			<cfelse>
				<cfset myreportstatus = 'F'>
			</cfif>
		
		<cfreturn myreportstatus>
	</cffunction>

	<cffunction access="public" name="getReportPath" returntype="string" hint="Returns BI Publisher REPORT_URL">
		<cfreturn VARIABLES.eventOrigine.getReportPath()>
	</cffunction>
	
	<cffunction access="public" name="getBipServer" returntype="string" hint="Returns BI Publisher SERVER_NAME">
		<cfreturn VARIABLES.eventOrigine.getBipServer()>
	</cffunction>
	
	<cffunction access="public" name="getEventTarget" returntype="String" hint="Returns EVENT_TARGET as STRING value. Check returned TYPE as this returntype can change">
		<cfreturn VARIABLES.eventOrigine.getEventTarget()>
	</cffunction>
	
	<cffunction access="public" name="getEventType" returntype="string" hint="Returns EVENT_TYPE as STRING value">
		<cfreturn VARIABLES.eventOrigine.getEventType()>
	</cffunction>
	
	<cffunction access="public" name="getEventDispatcher" returntype="string" hint="(Subject to modifications) Returns HTTP_HOST that sends (HTTP NOTIFICATION TARGET)">
		<cfreturn VARIABLES.eventOrigine.getEventDispatcher()>
	</cffunction>

	<cffunction access="private" name="respositoryExist" returntype="boolean" hint="(Subject to modifications) Returns HTTP_HOST that sends (HTTP NOTIFICATION TARGET)">

			<cfset stringValueRtrn 	= ''>
			<cfset uuidlog			= #VARIABLES.eventOrigine.getEventTarget()#>
			<cfset ImportTmp		= "/container/M16/">

			<!--- RECUPERATION MAIL INSERE CONTENANT TOUS LES INFOS --->
			<cfset mailObject  = createObject('component',"fr.consotel.consoview.M16.LogsCommande")>
			<cfset rsltMailObj = mailObject.getlogsCommandeuuid(#uuidlog#)>

			<!--- RECUPERATION DES PEICES JOINTES A JOINDRE AU MAIL OU NON --->
			<cfset attachObject = createObject('component',"fr.consotel.consoview.M16.v2.AttachementService")>
			<cfset rsltAttachOb = attachObject.fournirAttachements(#rsltMailObj.IDCOMMANDE#)>

			<cfset fileAttached = ArrayNew(1)>

			<cfloop query="rsltAttachOb">
				<cfset ArrayAppend(fileAttached, rsltAttachOb.PATH)>
			</cfloop>

			<!--- LE REPERTOIRE EXISTE OU NON --->
			<cfif ArrayLen(fileAttached) GT 0>
				<cfset srcPath = fileAttached[1]>

				<cfif DirectoryExists('#ImportTmp##srcPath#')>
					<cfset stringValueRtrn = TRUE>
				<cfelse>
					<cfset stringValueRtrn = FALSE>
				</cfif>
			<cfelse>
				<cfset stringValueRtrn = FALSE>
			</cfif>
		
		<cfreturn stringValueRtrn>
	</cffunction>

</cfcomponent>