<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.event.EmailEvent" extends="fr.consotel.api.ibis.publisher.handler.event.DelegateEvent"
hint="Evènement contenant les infos liées à un reporting à diffuser par mail et les propriétés mail à utiliser
Lorsque cet évènement est dispatché avec la méthode IReportingService.dispatchReportingEvent() la stratégie de diffusion utilisée sera Email
La stratégie Email diffuse alors par mail le contenu retournée par getContent() ou getContentAsLocalFile() provenant de l'évènement IDeliveryEvent 
Le paramètres de diffusion mail sont récupérés indirectement à partir des propriétés retournée par les méthodes de l'évènement IReportingEvent">
	<!--- fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent --->
	<cffunction access="public" name="getDeliveryInfos" returntype="any"
	hint="Retourne la valeur retournée par la méthode getDeliveryInfos() de l'évènement IDeliveryEvent utilisé pour créer cette instance">
		<cfset var deliveryInfos="">
		<cfset var deliveryContent=getDeliveryEvent().getDeliveryInfos()>
		<cfsavecontent variable="deliveryInfos">
			<cfoutput>
				#deliveryContent#<br><br><br>
				<b>Les propriétés de diffusion mail provenant de l'évènement sont :</b><br>
				Serveur Mail : #getMailServer()#<br>
				Utilisateur : #getMailUserName()#<br>
				Mot de passe : #getMailUserPassword()#<br>
				Expéditeur : #getFrom()#<br>
				Destinataire : #getTo()#<br>
				Sujet : #getSubject()#<br>
				Adresse de réponse : #getReplyTo()#<br>
				Adresse en copie : #getCC()#<br>
				Adresse en copie cachée : #getBCC()#
			</cfoutput>
		</cfsavecontent>
		<cfreturn deliveryInfos>
	</cffunction>

	<!--- fr.consotel.api.ibis.publisher.handler.event.EmailEvent --->
	<cffunction access="public" name="getMailServer" returntype="String" hint="Retourne le DNS du serveur mail">
		<cfreturn getDeliveryKeyValue("MAIL_SERVER")>
	</cffunction>
	
	<cffunction access="public" name="getMailUserName" returntype="String" hint="Retourne le login utilisateur pour le serveur mail">
		<cfreturn getDeliveryKeyValue("MAIL_USER")>
	</cffunction>
	
	<cffunction access="public" name="getMailUserPassword" returntype="String" hint="Retourne le mot de passe pour le serveur mail">
		<cfreturn getDeliveryKeyValue("MAIL_PWD")>
	</cffunction>

	<cffunction access="public" name="getFrom" returntype="String" hint="Retourne l'adresse expéditeur">
		<cfreturn getDeliveryKeyValue("FROM")>
	</cffunction>
	
	<cffunction access="public" name="getTo" returntype="String" hint="Retourne l'adresse destinataire">
		<cfreturn getDeliveryKeyValue("TO")>
	</cffunction>

	<cffunction access="public" name="getSubject" returntype="String" hint="Retourne le sujet du mail">
		<cfreturn getDeliveryKeyValue("SUBJECT")>
	</cffunction>

	<cffunction access="public" name="getReplyTo" returntype="String" hint="Retourne l'adresse de réponse">
		<cfreturn getDeliveryKeyValue("CC")>
	</cffunction>
	
	<cffunction access="public" name="getCC" returntype="String" hint="Retourne l'adresse en copie">
		<cfreturn getDeliveryKeyValue("CC")>
	</cffunction>
	
	<cffunction access="public" name="getBCC" returntype="String" hint="Retourne l'adresse en copie cachée">
		<cfreturn getDeliveryKeyValue("BCC")>
	</cffunction>
	
	<cffunction access="public" name="getAttachment" returntype="String"
	hint="Retourne le nom à donner à la pièce jointe lorsque le contenu est à envoyer en attachement mail ou une chaine vide s'il n'est pas défini">
		<cfreturn getDeliveryKeyValue("ATTACHMENT")>
	</cffunction>
	
	<cffunction access="public" name="INVALID_CONTENT" returntype="String" hint="Statut correspond à un type de contenu invalide">
		<cfreturn "INVALID_CONTENT">
	</cffunction>
	
	<cffunction access="public" name="INVALID_ADDRESS" returntype="String" hint="Statut correspond à une ou plusieurs adresses mail invalides">
		<cfreturn "INVALID_ADDRESS">
	</cffunction>
</cfcomponent>