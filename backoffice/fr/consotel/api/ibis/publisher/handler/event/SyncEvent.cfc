<!--- TODO : Support et implémentation de la méthode getContentAsLocalFile() --->
<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.event.SyncEvent" extends="fr.consotel.api.ibis.publisher.handler.event.JobEvent"
hint="Evènement contenant les infos liées à la tache correspondant à l'exécution d'un reporting : JOBID, JOBNAME, REPORT_URL, etc...
Cette classe est spécifique à un mode synchrone de l'exécution du reporting et du traitement correspondant par la stratégie de diffusion utilisée
La méthode getContentAsLocalFile() retourne une chaine vide car la récupération du contenu en tant que fichier local n'est pas implémentée">
	<!--- fr.consotel.api.ibis.publisher.handler.event.SyncEvent --->
	<cffunction access="public" name="getContentImpl" returntype="any" hint="Retourne l'implémentation BIP du contenu qui a été généré
	Le type de l'implémentation BIP est : com.oracle.xmlns.oxp.service.PublicReportService.ReportResponse">
		<cfset var jobProperties=getReportingEvent().getJob()>
		<cfif structKeyExists(jobProperties,"CONTENT")>
			<cfreturn jobProperties["CONTENT"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'implémentation du contenu n'est pas défini dans l'évènement">
		</cfif>
	</cffunction>
	
	<!--- fr.consotel.api.ibis.publisher.handler.event.JobEvent --->
	<cffunction access="public" name="getReportContentType" returntype="String"
	hint="Retourne le type MIME du contenu généré à partir de getContentImpl()">
		<cfreturn getContentImpl().getReportContentType()>
	</cffunction>
	
	<cffunction access="public" name="getOutputId" returntype="Numeric" hint="Retourne toujours la valeur 0">
		<cfreturn 0>
	</cffunction>
	
	<cffunction access="public" name="getBurstOption" returntype="String" hint="Retourne toujours la valeur FALSE">
		<cfreturn FALSE>
	</cffunction>
	
	<cffunction access="public" name="getJobInfo" returntype="any"
	hint="Lève une exception. Cette méthode n'est pas supportée par une execution en mode synchrone">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas supportée par une execution en mode synchrone">
	</cffunction>
	
	<cffunction access="public" name="getJobHistory" returntype="any"
	hint="Lève une exception. Cette méthode n'est pas supportée par une execution en mode synchrone">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas supportée par une execution en mode synchrone">
	</cffunction>
	
	<!--- fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent --->
	<cffunction access="public" name="getContent" returntype="any" hint="Retourne le contenu qui a été généré
	Si isTextContent() retourne FALSE : Le contenu est retourné en tant que tableau de bytes
	Sinon ce tableau de bytes est passé à la fonction toString() avec l'encodage UTF-8 et le résultat obtenu est retourné">
		<cfargument name="localFileName" type="String"  required="true" hint="La valeur de ce paramètre n'est pas utilisée par cette implémentation">
		<cfargument name="localDirAbsPath" type="String"  required="true" hint="La valeur de ce paramètre n'est pas utilisée par cette implémentation">
		<cfset var contentBytes=getContentImpl().getReportBytes()>
		<cfif isTextContent()>
			<cfreturn toString(contentBytes,"UTF-8")>
		<cfelse>
			<cfreturn contentBytes>
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getContentAsLocalFile" returntype="String"
	hint="La récupération du contenu en tant que fichier local n'est pas implémentée. Retourne une chaine vide">
		<cfargument name="localFileName" type="String"  required="true" hint="La valeur de ce paramètre n'est pas utilisée par cette implémentation">
		<cfargument name="localDirAbsPath" type="String"  required="true" hint="La valeur de ce paramètre n'est pas utilisée par cette implémentation">
		<cfreturn "">
	</cffunction>
	
	<cffunction access="public" name="getContentType" returntype="String" hint="Retourne le type MIME du contenu généré à partir de getContentImpl()">
		<cfreturn getReportContentType()>
	</cffunction>
	
	<cffunction access="public" name="isTextContent" returntype="boolean"
	hint="Retourne TRUE si getFormat() vaut une des valeurs suivantes : xml, csv, html">
		<cfreturn (getFormat() EQ "xml") OR (getFormat() EQ "csv") OR (getFormat() EQ "html")>
	</cffunction>
	
	<cffunction access="public" name="isBinaryContent" returntype="boolean"
	hint="Retourne TRUE si getFormat() vaut une des valeurs suivantes : excel, pdf">
		<cfreturn (getFormat() EQ "excel") OR (getFormat() EQ "pdf")>
	</cffunction>
	
	<cffunction access="public" name="isUnknownContent" returntype="boolean"
	hint="Retourne TRUE si getFormat() n'est pas égal à une des valeurs suivantes : xml, csv, html, excel, pdf">
		<cfreturn (getFormat() NEQ "xml") AND (getFormat() NEQ "csv") AND
			(getFormat() EQ "html") AND (getFormat() EQ "excel") AND (getFormat() EQ "pdf")>
	</cffunction>
</cfcomponent>