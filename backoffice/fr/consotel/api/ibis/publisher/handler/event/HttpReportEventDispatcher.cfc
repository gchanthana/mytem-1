<cfcomponent displayname="fr.consotel.api.ibis.publisher.handler.event.HttpReportEventDispatcher" hint="HTTP ReportEvent Dispatcher Interface">
	<cffunction access="public" name="initInstance" returntype="fr.consotel.api.ibis.publisher.handler.event.HttpReportEventDispatcher" hint="Returns an initialized instance">
		<cfset VARIABLES["JOB_NAME_SEPARATOR"]="|">
		<cfset VARIABLES["EVENT_SEPARATOR"]=".">
		<!--- URL variables key list for BIP HTTP Notification --->
		<cfset VARIABLES["BIP_SERVER"]="BIP">
		<cfset VARIABLES["EVENT"]="EVENT">
		<cfset VARIABLES["TRIGGER_INSTR"]="TRIGGER_INSTR_CODE">
		<!--- Returned Event Data Keylist --->
		<cfset VARIABLES["CLASS_KEY"]="CLASS">
		<cfset VARIABLES["BIP_USER"]="BIP_USER">
		<cfset VARIABLES["JOB_ID"]="JOB_ID">
		<!--- Event Types and Classes --->
		<cfset VARIABLES["DEFAULT_EVENT_CLASS"]="fr.consotel.api.ibis.publisher.handler.event.ServiceEvent">
		<cfset VARIABLES["EVENT_CLASS"]="fr.consotel.api.ibis.publisher.handler.event.ServiceEvent">
		<!--- BI Publisher WebService related infos --->
		<cfset VARIABLES["CV_BIP_USER"]="consoview">
		<cfset VARIABLES["CV_BIP_PWD"]="public">
		<cfset VARIABLES["BIP_WS_FACTORY"]="fr.consotel.api.ibis.publisher.AbstractWebServiceFactory">
		<cfset VARIABLES["IS_INITIALIZED"]=TRUE>
	</cffunction>

	<!---
		* BIP QUARTZ infos keylist appended to event data :
			- JOB_NAME (STRING) : QUARTZ JOB_NAME. MAX LENGTH : 100
			- REPORT_STATUS (STRING) : QUARTZ REPORT_STATUS. MAX LENGTH : 1
			- REPORT_ABS_PATH (STRING) : BI Publisher XDO absolute path (XDO)
		* TODO (VARIABLES) :
			- USER_INFOS (STRUCT) : Integrate key referencing CV User infos in Event data (Not Updated for current version)
			- DEVELOPMENT : Integrate BI Publisher WebService API
		* DEPENDENCIES :
			- BI Publisher (Version, Patch) : BIP_WEBSERVICE._getService().getPublicReportService_v11Address()
			- JDK (Version, Patch) : BIP_WEBSERVICE._getService().getPublicReportService_v11Address()
	 --->
	<cffunction access="public" name="onURLEvent" returntype="void" hint="Handles received HTTP Events (BI Publisher)">
		<cfargument name="httpParameters" type="struct" required="true" hint="URL variables for received HTTP Request by this Event Dispatcher">
		<!--- This UDF checks if this instance is correctly initialized of not --->
		<cfset var eventData=getEventData(ARGUMENTS["httpParameters"])>
		<cfset var eventObject="">
		<cfset var bipUser="">
		<cfset var jobName="">
		<cfset var bipService="">
		<cfset var wsdlString="">
		<cfset var bipFactory=createObject("component",VARIABLES["BIP_WS_FACTORY"])>
		<cfif bipFactory.initFactory() EQ FALSE>
			<cfthrow type="Custom" errorcode="ILLEGAL_STATE_EXCEPTION" message="Unable to initialize BIP Factory : #VARIABLES.BIP_WS_FACTORY#">
		</cfif>
		<!--- Get BI Publisher WebService and Check WSDL --->
		<cfset bipService=bipFactory.getInstance()>
		<cfset wsdlString=bipService._getService().getPublicReportService_v11Address()>
		<cfif eventData[VARIABLES["BIP_SERVER"]] EQ wsdlString>
			<!--- RETRIEVE QUARTZ JOB_NAME --->
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_STATE_EXCEPTION" message="Incompatible BIP DNS [EVENT,CONFIG] : [#eventData[VARIABLES.BIP_SERVER]#,#wsdlString#]">
		</cfif>
		<cfset eventObject=dispatchEvent(createObject("component",eventData["CLASS_KEY"]).createInstance(eventData))>
	</cffunction>
	
	<cffunction access="public" name="dispatchEvent" returntype="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" hint="Dispatch event">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true" hint="Event to dispatch">
		<cfreturn ARGUMENTS["eventObject"]>
	</cffunction>

	<!---
		* Event data common keylist :
			- EVENT_TARGET_VALUE (STRING) : EVENT_TARGET extracted from JOB_NAME value. Default is empty string. MAX LENGTH : 35
			- EVENT_TYPE (STRING) : EVENT_TYPE value. Default is empty string. MAX LENGTH : 3
			- EVENT_HANDLER (STRING) : EVENT_HANDLER value. Default is empty string. MAX LENGTH : 3
		* BIP HTTP Notification URL and Returned Event data key list :
			- VARIABLES.BIP_SERVER (STRING) : BIP DNS
			- VARIABLES.EVENT (STRING.NUMERIC) : Concatenation USERNAME.JOB_ID (Username,JobId)
			- VARIABLES.TRIGGER_INSTR_CODE (NUMERIC) : Used to identify QUARTZ HTTP Notification (TriggerListener, PluginListener, Etc...)
			- VARIABLES.CLASS_KEY (STRING) : Event Class
			- VARIABLES.BIP_USER (STRING) : BIP USERNAME
			- VARIABLES.JOB_ID (NUMERIC) : QUARTZ JOB_ID
		* DEPENDENCIES :
			- fr.consotel.api.ibis.publisher.handler.event.ServiceEvent.createInstance() : For event data keylist
			- fr.consotel.api.ibis.publisher.handler.event.ReportEvent.createInstance() : For event data keylist
	 --->
	<cffunction access="private" name="getEventData" returntype="struct" hint="Returns data structure used to create IReportEvent">
		<cfargument name="dataStruct" type="struct" required="true" hint="Parameters used to create data structure">
		<cfset var eventData=ARGUMENTS["dataStruct"]>
		<cfset var returnedEventData=structNew()>
		<!--- TRIGGER_INSTR : QUARTZ HTTP Notification KEY (PluginListener, TriggerListener, etc...) --->
		<cfset var httpUrlType=VARIABLES["TRIGGER_INSTR"]>
		<!--- Other temporary variables --->
		<cfset var jobIdIndex=0>
		<cfset var eventInfosList="">
		<cfif isInitialized() EQ TRUE>
			<!--- DEFAULT VALUES --->
			<cfset returnedEventData["EVENT_TARGET_VALUE"]="">
			<cfset returnedEventData["EVENT_TYPE"]="">
			<cfset returnedEventData["EVENT_HANDLER"]="">
			<cfset returnedEventData[VARIABLES["CLASS_KEY"]]=VARIABLES["DEFAULT_EVENT_CLASS"]>
			<!--- *** HTTP NOTIFICATION TYPES PROCESSING *** --->
			<cfif structKeyExists(eventData,VARIABLES[httpUrlType]) EQ TRUE>
				<!--- HTTP NOTIFICATION TYPE : QUARTZ --->
				<cfset returnedEventData[VARIABLES["CLASS_KEY"]]=VARIABLES["EVENT_CLASS"]>
				<cfif (structKeyExists(eventData,VARIABLES["BIP_SERVER"]) EQ FALSE) OR (structKeyExists(eventData,VARIABLES["EVENT"]) EQ FALSE)>
					<cfthrow type="Custom" errorcode="ILLEGAL_ARGUMENT_EXCEPTION" message="Missing keys in event data keylist [#structKeyList(eventData)#]">
				</cfif>
				<!--- Extracting USERNAME and JOB_ID --->
				<cfset eventInfosList=eventData[VARIABLES["EVENT"]]>
				<cfset jobIdIndex=listLen(eventInfosList,VARIABLES["EVENT_SEPARATOR"])>
				<cfif jobIdIndex LTE 1>
					<cfthrow type="Custom" errorcode="ILLEGAL_ARGUMENT_EXCEPTION" message="Invalid length for EVENT value : #eventData.VARIABLES.EVENT#">
				</cfif>
				<!--- Populating returnedEventData from eventData --->
				<cfset returnedEventData[VARIABLES["JOB_ID"]]=listGetAt(eventInfosList,jobIdIndex,VARIABLES["EVENT_SEPARATOR"])>
				<cfset returnedEventData[VARIABLES["BIP_USER"]]=listDeleteAt(eventInfosList,jobIdIndex,VARIABLES["EVENT_SEPARATOR"])>
				<cfset returnedEventData[VARIABLES["BIP_SERVER"]]=eventData[VARIABLES["BIP_SERVER"]]>
			</cfif>
			<!--- *** END OF HTTP NOTIFICATION TYPES PROCESSING *** --->
			<cfreturn returnedEventData>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_STATE_EXCEPTION" message="This instance was not initialized">
		</cfif>
	</cffunction>

	<cffunction access="private" name="isInitialized" returntype="boolean" hint="Returns TRUE if this instance was created with getFactory()">
		<cfset var isInitialized=structKeyExists(VARIABLES,"IS_INITIALIZED")>
		<cfif isInitialized EQ TRUE>
			<cfreturn VARIABLES["IS_INITIALIZED"]>
		<cfelse>
			<cfreturn FALSE>
		</cfif>
	</cffunction>
</cfcomponent>