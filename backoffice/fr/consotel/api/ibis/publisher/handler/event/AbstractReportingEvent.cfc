<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.event.AbstractReportingEvent" implements="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent"
hint="Implémentation abstraite qui sert de classe parent pour les évènements de type IReportingEvent
Seules les méthodes suivantes sont implémentées : getReportingService()">
	<!--- fr.consotel.api.ibis.publisher.handler.event.AbstractReportingEvent --->
	<cffunction access="public" name="createReportingEvent" returntype="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent" hint="Constructeur">
		<cfargument name="eventData" type="Struct" required="true" hint="Contient les infos permettant de créer l'évènement">
		<cfif NOT isDefined("VARIABLES.EVENT_DATA")>
			<cfset VARIABLES.EVENT_DATA=ARGUMENTS.eventData>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="getEventData" returntype="Struct" hint="Retourne les propriétés de l'évènement en tant que structure
	Une structure vide est retournée si les propriétés ne sont pas définies. Méthode utilisée par les classes dérivées pour étendre les méthodes de l'évènement">
		<cfif isDefined("VARIABLES.EVENT_DATA")>
			<cfreturn VARIABLES.EVENT_DATA>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Les infos de l'évènement IReportingEvent ne sont pas correctement définis">
		</cfif>
	</cffunction>
	
	<!--- fr.consotel.api.ibis.publisher.handler.event.IReportingEvent --->
	<cffunction access="public" name="getReportingService" returntype="fr.consotel.api.ibis.publisher.reporting.IReportingService"
	hint="Retourne une instance IReportingService utilisant les mêmes credentials que l'instance qui a exécuté et généré le reporting
	Une exception est levée la clé correspondante n'est pas définie ou si la valeur associée n'implémente pas IReportingService">
		<cfif structKeyExists(getEventData(),"SERVICE") AND isInstanceOf(getEventData().SERVICE,"fr.consotel.api.ibis.publisher.reporting.IReportingService")>
			<cfreturn getEventData().SERVICE>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'implémentation IReportingService n'est pas correctement définie">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="RUNNING" returntype="String">
		<cfreturn "C">
	</cffunction>

	<cffunction access="public" name="FAILURE" returntype="String">
		<cfreturn "F">
	</cffunction>
	
	<cffunction access="public" name="WARNING" returntype="String">
		<cfreturn "W">
	</cffunction>
	
	<cffunction access="public" name="SUCCESS" returntype="String">
		<cfreturn "S">
	</cffunction>
	
	<cffunction access="public" name="EXCEPTION" returntype="String">
		<cfreturn "X">
	</cffunction>
	
	<cffunction access="public" name="getStatus" returntype="String" hint="N'est pas implémentée. Lève une exception">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée">
	</cffunction>

	<cffunction access="public" name="getReportingName" returntype="String" hint="N'est pas implémentée. Lève une exception">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée">
	</cffunction>
	
	<cffunction access="public" name="getDeliveryType" returntype="String" hint="N'est pas implémentée. Lève une exception">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée">
	</cffunction>
	
	<cffunction access="public" name="getBipDNS" returntype="String" hint="N'est pas implémentée. Lève une exception">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée">
	</cffunction>
	
	<cffunction access="public" name="getBipSessionInterval" returntype="Numeric" hint="N'est pas implémentée. Lève une exception">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée">
	</cffunction>
	
	<cffunction access="public" name="getBackOffice" returntype="String" hint="N'est pas implémentée. Lève une exception">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée">
	</cffunction>
	
	<cffunction access="public" name="getJob" returntype="Struct" hint="N'est pas implémentée. Lève une exception">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée">
	</cffunction>

	<cffunction access="public" name="getReporting" returntype="Struct" hint="N'est pas implémentée. Lève une exception">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée">
	</cffunction>

	<cffunction access="public" name="getDelivery" returntype="Struct" hint="N'est pas implémentée. Lève une exception">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée">
	</cffunction>
	
	<cffunction access="public" name="getXDO" returntype="Struct" hint="N'est pas implémentée. Lève une exception">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée">
	</cffunction>
</cfcomponent>