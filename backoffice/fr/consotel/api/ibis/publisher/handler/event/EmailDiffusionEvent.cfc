<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.event.EmailDiffusionEvent" extends="fr.consotel.api.ibis.publisher.handler.event.EmailEvent"
hint="Evènement contenant les infos concernant le statut d'une diffusion par mail du contenu d'un reporting.
La méthode getStatus() est redéfinie pour retourner d'autres valeurs possibles qui sont :
- EmailEvent.INVALID_CONTENT() : Signifie que le type du contenu ne permet pas de le diffuser dans le corps du mail
- EmailEvent.INVALID_ADDRESS() : Signifie qu'au moins une des adresses destinataire ou expéditeur n'est pas une adresse mail valide">
	<!--- fr.consotel.api.ibis.publisher.handler.event.EmailDiffusionEvent --->
	<cffunction access="public" name="createDeliveryEvent" returntype="fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent"
	hint="Appelle le constructeur de la classe parent avec eventSource comme paramètre et défini diffusionStatus comme valeur de retour de getStatus()">
		<cfargument name="eventSource" type="fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent"  required="true"
		hint="Implémentation de l'évènement à partir duquel les infos et le contenu du reporting sont extraites">
		<cfargument name="diffusionStatus" type="String" required="true" hint="Valeur du statut de diffusion du mail">
		<cfset var deliveryEvent=ARGUMENTS["eventSource"]>
		<cfset SUPER.createDeliveryEvent(deliveryEvent)>
		<cfif NOT isDefined("VARIABLES.DIFFUSION_STATUS")>
			<cfset VARIABLES.DIFFUSION_STATUS=ARGUMENTS["diffusionStatus"]>
		</cfif>
		<cfreturn THIS>
	</cffunction>

	<!--- fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent --->
	<cffunction access="public" name="getDeliveryInfos" returntype="any" hint="Retourne getDeliveryEvent().getDeliveryInfos()">
		<cfreturn getDeliveryEvent().getDeliveryInfos()>
	</cffunction>

	<!--- fr.consotel.api.ibis.publisher.handler.event.AbstractReportingEvent --->
	<cffunction access="public" name="getStatus" returntype="String" hint="Retourne la valeur du statut de diffusion. Lève une exception s'il n'est pas défini">
		<cfif isDefined("VARIABLES.DIFFUSION_STATUS")>
			<cfreturn VARIABLES.DIFFUSION_STATUS>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le statut de diffusion n'est pas correctement défini">
		</cfif>
	</cffunction>
</cfcomponent>