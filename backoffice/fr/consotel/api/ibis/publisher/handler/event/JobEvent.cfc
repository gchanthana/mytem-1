<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.event.JobEvent" extends="fr.consotel.api.ibis.publisher.handler.event.DeliveryEvent"
hint="Evènement contenant les infos liées à la tache correspondant à l'exécution d'un reporting : JOBID, JOBNAME, REPORT_URL, etc...
Cette classe est spécifique à un mode asynchrone de l'exécution du reporting et du traitement correspondant par la stratégie de diffusion utilisée
La méthode getContentAsLocalFile() n'est pas redéfinie">
	<cffunction access="public" name="getReportContentType" returntype="String" hint="Retourne le type MIME du contenu généré à partir du JobHistoryInfo BIP">
		<cfset var jobHistory=getJobHistory()>
		<cfreturn jobHistory.getDocumentDataContentType()>
	</cffunction>
	
	<cffunction access="public" name="getJobId" returntype="Numeric" hint="Retourne le JOBID du JOB correspond ou 0 s'il n'est pas défini">
		<cfset var jobProperties=getReportingEvent().getJob()>
		<cfif structKeyExists(jobProperties,"JOBID")>
			<cfreturn jobProperties["JOBID"]>
		<cfelse>
			<cfreturn 0>
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getJobName" returntype="String" hint="Retourne le nom du JOB ou une chaine vide s'il n'est pas défini">
		<cfset var jobProperties=getReportingEvent().getJob()>
		<cfif structKeyExists(jobProperties,"JOBNAME")>
			<cfreturn jobProperties["JOBNAME"]>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>

	<cffunction access="public" name="getReportUrl" returntype="String" hint="Retourne le chemin absolu du XDO ou une chaine vide s'il n'est pas défini">
		<cfset var jobProperties=getReportingEvent().getJob()>
		<cfif structKeyExists(jobProperties,"REPORT_URL")>
			<cfreturn jobProperties["REPORT_URL"]>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getOutputId" returntype="Numeric" hint="Récupère et retourne l'OUTPUTID du JOB à partir du JobHistoryInfo BIP">
		<cfreturn getJobHistory().getOutputID()>
	</cffunction>

	<cffunction access="public" name="getUserName" returntype="String" hint="Retourne le login d'utilisateur utilisé par l'implémentation IReportingService">
		<cfset var jobUsername="">
		<cfset var jobProperties=getReportingEvent().getJob()>
		<cfif structKeyExists(jobProperties,"USER")>
			<cfset jobUsername=jobProperties["USER"]>
		</cfif>
		<cfreturn jobUsername>
	</cffunction>
	
	<cffunction access="public" name="getBurstOption" returntype="String" hint="Récupère et retourne l'option de ventilation du JOB à partir du JobInfo BIP">
		<cfreturn getJobInfo().isBurstingOption()>
	</cffunction>

	<cffunction access="public" name="getMessage" returntype="String" hint="Retourne le message du JOB">
		<cfset var jobMessage="">
		<cfset var jobProperties=getReportingEvent().getJob()>
		<cfif structKeyExists(jobProperties,"MESSAGE")>
			<cfset jobMessage=jobProperties["MESSAGE"]>
		</cfif>
		<cfreturn jobMessage>
	</cffunction>
	
	<cffunction access="public" name="getJobInfo" returntype="any" hint="Retourne l'implémentation BIP des infos du JOB.
	Type : com.oracle.xmlns.oxp.service.v11.PublicReportService.JobInfo. Une structure vide est retournée si elle n'est pas définie">
		<cfset var jobInfo={}>
		<cfset var jobProperties=getReportingEvent().getJob()>
		<cfif structKeyExists(jobProperties,"INFOS")>
			<cfset jobInfo=jobProperties["INFOS"]>
		</cfif>
		<cfreturn jobInfo>
	</cffunction>
	
	<cffunction access="public" name="getJobHistory" returntype="any" hint="Retourne l'implémentation BIP de l'historique du JOB.
	Type : com.oracle.xmlns.oxp.service.PublicReportService.JobHistoryInfo. Une structure vide est retournée si elle n'est pas définie">
		<cfset var jobInfo={}>
		<cfset var jobProperties=getReportingEvent().getJob()>
		<cfif structKeyExists(jobProperties,"HISTORY")>
			<cfset jobInfo=jobProperties["HISTORY"]>
		</cfif>
		<cfreturn jobInfo>
	</cffunction>
	
	<!--- fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent --->
	<cffunction access="public" name="getContent" returntype="any"
	hint="Retourne un contenu générique de type text/html retourné par getDeliveryInfos()">
		<cfargument name="localFileName" type="String"  required="true" hint="La valeur de ce paramètre n'est pas utilisée par cette implémentation">
		<cfargument name="localDirAbsPath" type="String"  required="true" hint="La valeur de ce paramètre n'est pas utilisée par cette implémentation">
		<cfreturn getDeliveryInfos()>
	</cffunction>
	
	<cffunction access="public" name="getContentType" returntype="String"
	hint="Retourne la valeur text/html qui correspond au type MIME du contenu retourné par getContent()">
		<cfreturn "text/html">
	</cffunction>
	
	<cffunction access="public" name="getDeliveryInfos" returntype="any"
	hint="Retourne un contenu générique de type text/html contenant des infos concernant le JOB">
		<cfset var deliveryInfos="">
		<cfset var reportingEvent=getReportingEvent()>
		<!--- Paramètres du rapport BIP (XDO) --->
		<cfset var xdoParams=getReportParameters()>
		<cfsavecontent variable="deliveryInfos">
			<cfoutput>
				<center><i>Contenu générique généré par l'API Reporting</i></center><br><br>
				<b>Contexte de l'API :</b><br>
				BackOffice : #reportingEvent.getBackOffice()#<br>
				Implémentation du service IReportingService : #getMetadata(getReportingService()).NAME#<br>
				Stratégie utilisée pour l'exécution du reporting : #reportingEvent.getDeliveryType()#<br>
				Implémentation IReportingEvent : #getMetadata(reportingEvent).NAME#<br>
				Implémentation IDeliveryEvent : #getMetadata(THIS).NAME#<br>
				Stratégie utilisée pour la diffusion du reporting : #getDeliveryType()#<br><br>
				<b>Rapport d'exécution du reporting (IReportingEvent) :</b><br>
				Serveur BIP : #reportingEvent.getBipDNS()#<br>
				Utilisateur BIP : #getUserName()#<br>
				ID du JOB (OUTPUTID) : #getJobId()# (#getOutputId()#)<br>
				Statut d'exécution : #getStatus()#<br>
				Nom du JOB : #getJobName()#<br>
				Reporting : #getReportingName()#<br>
				Chemin du XDO : #getReportUrl()#<br>
				Template : #getTemplateId()#<br>
				Format : #getFormat()#<br>
				Type MIME du contenu généré (JOB) : #getReportContentType()#<br>
				Localisation : #getLocalization()#<br>
				Time Zone : #getTimeZone()#<br>
				Option de ventilation : #getBurstOption()#<br><br>
				<TABLE border="1" bordercolor="black">
					<TR bordercolor="black"><TH colspan="2" bordercolor="black"><b>Paramètres du rapport BIP</b></TH></TR>
					<cfloop item="paramKey" collection="#xdoParams#">
						<TR bordercolor="black">
							<TH bordercolor="black"><b>Paramètre</b></TH><TH bordercolor="black"><b>Valeur(s)</b></TH>
						</TR>
						<TR bordercolor="black">
							<TD bordercolor="black">#xdoParams[paramKey]["NAME"]#</TD>
							<TD bordercolor="black">#arrayToList(xdoParams[paramKey]["VALUES"])#</TD>
						</TR>
					</cfloop>
				</TABLE><br>
				<TABLE border="1" bordercolor="black">
					<TR bordercolor="black"><TH bordercolor="black"><b>Message du JOB</b></TH></TR>
					<TR bordercolor="black"><TD bordercolor="black">#getMessage()#</TD></TR>
				</TABLE>
			</cfoutput>
		</cfsavecontent>
		<cfreturn deliveryInfos>
	</cffunction>
	
	<cffunction access="public" name="isTextContent" returntype="boolean" hint="Retourne TRUE">
		<cfreturn TRUE>
	</cffunction>
	
	<cffunction access="public" name="isBinaryContent" returntype="boolean" hint="Retourne FALSE">
		<cfreturn FALSE>
	</cffunction>
	
	<cffunction access="public" name="isUnknownContent" returntype="boolean" hint="Retourne FALSE">
		<cfreturn FALSE>
	</cffunction>
</cfcomponent>