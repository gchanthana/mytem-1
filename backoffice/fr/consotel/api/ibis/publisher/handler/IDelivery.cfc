<cfinterface author="Cedric" displayname="fr.consotel.api.ibis.publisher.handler.IDelivery"
hint="Interface qui spécifie les méthodes à implémenter pour définir une stratégie de diffusion
Cette stratégie est utilisée par une implémentation de type IReportingService pour lui permettre d'obtenir dans l'ordre les infos suivantes :
- Propriétés d'exécution ou de planification du rapport du rapport BI Publisher correspondant au reporting (ScheduleRequest)
- Paramètres spécifiques et nécessaires au rapport BI Publisher correspondant au reporting (XDO)
- Propriétés d'exécution de la stratégie de diffusion permettant la génération et la diffusion du reporting (DELIVERY)
Ces infos sont obtenues à partir des propriétés fournies par le reporting (IReporting) à la stratégie de diffusion
La stratégie de diffusion qui est utilisée pour l'exécution du reporting est indiqué par son type dont la valeur est à fournir dans la propriété TYPE
Un évènement est généré et dispatché par l'implémentation IReportingService lorsque l'exécution du reporting est terminée
Ce dispatch conduit à l'appel de la méthode handleReportingEvent() de la stratégie de diffusion dont le type est indiquée dans la propriété TYPE">
	<cffunction name="createInstance" returntype="fr.consotel.api.ibis.publisher.handler.IDelivery" hint="Méthode appelée pour instancier la stratégie">
	</cffunction>

	<cffunction name="getDeliveryRequest" returntype="Struct" hint="Méthode appelée par l'implémentation IReportingService qui exécute le reporting
	Retourne une structure de type ScheduleRequest contenant les propriétés d'exécution ou de planification du rapport BIP correspondant au reporting
	Les paramètres qui sont fournis au rapport BIP dans la structure retournée sont remplacées par l'implémentation IReportingService
	Cette méthode doit etre appelée exactement une seule fois par l'implémentation IReportingService">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true" hint="Reporting à exécuter">
	</cffunction>

	<cffunction name="getReportingParameters" returntype="Struct" hint="Méthode appelée par l'implémentation IReportingService qui exécute le reporting
	Retourne une structure contenant les paramètres spécifiques et nécessaires au rapport BIP correspondant au reporting (XDO)
	Chaque clé identifie le nom du paramètre correspondant et est associée à un tableau contenant la liste de ses valeurs (Chaine ou Numérique)
	Ces paramètres sont ajoutés par l'implémentation IReportingService dans la structure utilisée pour exécuter ou planifier le rapport BIP correspondant">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true" hint="Reporting à exécuter">
	</cffunction>

	<cffunction name="getDeliveryProperties" returntype="Struct" hint="Méthode appelée par l'implémentation IReportingService qui exécute le reporting
	Retourne une structure contenant les propriétés d'exécution de la stratégie. Chaque clé identifie le nom de la propriété associée à sa valeur (Chaine ou Numérique)">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true" hint="Reporting à exécuter">
	</cffunction>

	<cffunction name="handleJobId" returntype="void"
	hint="Appelée par l'implémentation IReportingService après le déclenchement de l'exécution du reporting si celle-ci s'est déroulée sans erreur
	Lorsque le rapport BIP correspondant est planifié jobId a une valeur strictement positive et la valeur 0 s'il est directement exécuté sans planification">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true" hint="Reporting qui a été planifié">
		<cfargument name="jobId" type="Numeric"  required="true" hint="JOBID de planification ou 0 si le rapport n'est pas généré par planification">
	</cffunction>
	
	<cffunction name="handleReportingEvent" returntype="void"
	hint="Appelée par l'implémentation IReportingService si les conditions suivantes sont vérifiées :
	- Le type de cette stratégie est indiquée dans la propriété TYPE qui est provient de getDeliveryProperties() et généralement fournie par le reporting
	- L'exécution du reporting est terminée : La méthode getStatus() de event indique le statut de cette exécution (e.g : En succès, En erreur, etc...)
	- Un évènement de type IReportingEvent a été créé dispatché par une implémentation IReportingService
	- La méthode getDeliveryType() de cet évènement retourne la valeur du type de cette stratégie (i.e : Valeur de la propriété TYPE)">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent"  required="true"
		hint="Evènement dispatché par la méthode dispatchReportingEvent() d'une implémentation IReportingService
		La méthode getDeliveryType() de cet évènement retourne alors le type de cette stratégie de diffusion">
	</cffunction>
</cfinterface>