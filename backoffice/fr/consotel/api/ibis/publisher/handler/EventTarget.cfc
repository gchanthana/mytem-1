<cfcomponent displayName="fr.consotel.api.ibis.publisher.handler.EventTarget"
			implements="fr.consotel.api.ibis.publisher.handler.IEventTarget" hint="EVENT TARGET Implementation">
	<cffunction access="public" name="createInstance" returntype="fr.consotel.api.ibis.publisher.handler.IEventTarget" hint="Initializes and returns this instance. May throw an exception.">
		<cfargument name="targetValue" type="string" required="true" hint="EVENT TARGET value">
		<cfset var EVENT_TARGET_VALUE_MAX_LENGTH=35>
		<cfif isValid("String",ARGUMENTS["targetValue"])>
			<cfif (LEN(ARGUMENTS["targetValue"]) GT 0) AND (LEN(ARGUMENTS["targetValue"]) LTE EVENT_TARGET_VALUE_MAX_LENGTH)>
				<cfset VARIABLES["TARGET_VALUE"]=ARGUMENTS["targetValue"]>
				<cfreturn THIS>
			<cfelse>
				<cfthrow type="Custom" errorcode="ILLEGAL_ARGUMENT_EXCEPTION" message="targetValue LENGTH must be GT 0 and LTE #EVENT_TARGET_VALUE_MAX_LENGTH#">
			</cfif>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_ARGUMENT_EXCEPTION" message="targetValue is not a valid STRING">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getValue" returntype="string" hint="Returns value of EVENT_HANDLER. Throws an Exception if this instance was not created using createInstance()">
		<cfset var targetValueKey="TARGET_VALUE">
		<cfif structKeyExists(VARIABLES,targetValueKey)>
			<cfreturn VARIABLES[targetValueKey]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_STATE_EXCEPTION" message="THIS instance was not created using createInstance()">
		</cfif>
	</cffunction>
</cfcomponent>