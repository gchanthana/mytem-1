<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement"
hint="Un évènement est une liste d'exécution de reporting à effectuer et qui est représentée par une requête ayant des colonnes prédéfinies qui dépendent de l'évènement
Cette implémentation est caractérisé par les règles suivantes :
- La liste des paramètres est la même pour chaque exécution et elle correspond à toutes les colonnes qui ne sont pas des paramètres métiers
- Par défaut tous les paramètres ne sont pas visibles dans le module container
- Les valeurs des paramètres sont toujours fournis dans la requête contenant la liste d'exécution">
	<!--- fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement --->
	<cffunction access="public" name="getName" returntype="String" hint="Retourne le nom de l'évènement">
		<cfreturn "Evènement " & listLast(getMetadata(THIS).NAME,".")>
	</cffunction>

	<cffunction access="public" name="getServiceName" returntype="String"
	hint="Retourne le nom complet de la classe implémentant fr.consotel.api.ibis.publisher.reporting.IReportingService qui va exécuter le reporting
	Cette implémentation retourne : fr.consotel.api.ibis.publisher.reporting.impl.ReportingService">
		<cfreturn "fr.consotel.api.ibis.publisher.reporting.impl.ReportingService">
	</cffunction>

	<cffunction access="public" name="getRequiredProperties" returntype="struct" hint="Retourne la liste des propriétés requises">
		<cfif NOT structKeyExists(VARIABLES,"REQUIRED_PROPERTIES")>
			<cfset VARIABLES.REQUIRED_PROPERTIES=requiredProperties()>
		</cfif>
		<cfreturn VARIABLES.REQUIRED_PROPERTIES>
	</cffunction>
	
	<cffunction access="public" name="getRequiredColumns" returntype="struct" hint="Retourne la liste des colonnes correpondants aux paramètres métiers">
		<cfif NOT structKeyExists(VARIABLES,"REQUIRED_COLUMNS")>
			<cfset VARIABLES.REQUIRED_COLUMNS=requiredColumns()>
		</cfif>
		<cfreturn VARIABLES.REQUIRED_COLUMNS>
	</cffunction>

	<cffunction access="public" name="getParameterColumns" returntype="struct" hint="Retourne la liste des colonnes correspondants aux paramètres du rapport">
		<cfif NOT structKeyExists(VARIABLES,"XDO_PARAMS")>
			<cfset VARIABLES.XDO_PARAMS=parameterColumns()>
		</cfif>
		<cfreturn VARIABLES.XDO_PARAMS>
	</cffunction>

	<cffunction access="public" name="getExecutionList" returntype="query" hint="Retourne la liste des exécutions à effectuer">
		<cfreturn VARIABLES.REPORTING_QUERY>
	</cffunction>
	
	<cffunction access="public" name="getSize" returntype="numeric" hint="Retourne le nombre d'exécutions à effectuer">
		<cfreturn getExecutionList().recordcount>
	</cffunction>

	<cffunction access="public" name="currentIndex" returntype="Numeric" hint="Retourne l'index de la dernière exécution effectuée">
		<cfreturn VARIABLES.CURRENT_ROW_IDX>
	</cffunction>
	
	<cffunction access="public" name="haveNext" returntype="boolean" hint="Retourne TRUE s'il reste au moins une exécution à effectuer">
		<cfreturn (currentIndex() LT getSize())>
	</cffunction>
	
	<cffunction access="public" name="nextReporting" returntype="Struct" hint="Retourne les propriétés représentants l'exécution suivante à effectuer
	Un appel à cette cette méthode incrémente de 1 la valeur retournée par currentIndex() avant de récupérer les propriétés. Une exception est levée si haveNext() vaut FALSE">
		<cfif haveNext()>
			<!--- Incréation de l'index de dernier enregistrement traité --->
			<cfset VARIABLES.CURRENT_ROW_IDX=currentIndex() + 1>
			<cfreturn currentReporting()>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="La valeur de haveNext() indique qu'il n'y a plus de rapport à exécuter"
			detail="Valeur de haveNext() : #haveNext()#. Valeur de currentIndex()#currentIndex()#">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="createReporting" returntype="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement"
	hint="Constructeur de l'évènement. Lève une exécution dans l'un des cas suivants :
	- Si initProperties ne contient pas toutes les propriétés requises par l'évènement
	- Si la liste d'exécution ne contient pas les colonnes requises par l'évènement">
		<cfargument name="initProperties" type="Struct" required="true" hint="Contient les propriétés d'initialisation pour l'évènement Les propriétés suivantes sont obligatoires :
		- RUN_PROPERTIES : Structure contenant les propriétés d'initialisation pour l'évènement provenant soit du scope URL soit fournis au constructeur getInstance()
		- MAIL_PROPERTIES : Structure contenant les propriétés mail (Fournis par l'application Evènements)
		- LOG : Valeur utilisée pour l'attribut log de cflog (Fournie par l'application Evènements)">
		<cfset var initProps=ARGUMENTS["initProperties"]>
		<!--- Propriétés de l'évènement --->
		<cfset VARIABLES.CURRENT_ROW_IDX=0>
		<cfset VARIABLES.INIT_PROPERTIES=initProps>
		<!--- Validation des propriétés d'exécution de l'évènement --->
		<cfset validateProperties(getRunProperties())>
		<!--- Exécution de la requete ramenant la liste des rapports à exécuter et qui constituent l'évènement --->
		<cfset var startTick=getTickCount()>
		<cfset VARIABLES.REPORTING_QUERY=getReportList()>
		<cfset var queryDuration=((getTickCount() - startTick) / 1000)>
		<cflog text="Evenement report list loaded after : #queryDuration#s (Recordcount : #getSize()#)" file="#getLog()#" type="information">
		<!--- Validation des colonnes requises par la requête de l'évènement --->
		<cfset validateReportList()>
		<!--- Notification de l'exécution de l'évènement --->
		<cfmail attributecollection="#getMailProperties().NOTIFICATION#" subject="Création d'un reporting Evènement : #getName()#">
			<cfoutput>
				Evènement lancé à partir de : #CGI.REMOTE_HOST# (#CGI.HTTP_USER_AGENT#)<br>
				BackOffice de traitement de l'évènement : #CGI.SERVER_NAME#<br>
				Page d'exécution : #CGI.SCRIPT_NAME#<br>
				Implémentation du service de reporting utilisée : #getServiceName()#<br><br>
				Nom de l'évènement : #getName()#<br>
				Implémentation de l'évènement : #getMetadata(THIS).NAME#<br>
				<cfdump var="#getRunProperties()#" label="Propriétés d'exécution fournies à l'évènement" format="html"><br>
				<cfif getSize() GT 0>
					<cfdump var="#getExecutionList()#" label="Contenu de la liste de diffusion (Durée : #queryDuration#s)">
				<cfelse>
					Liste des colonnes de la requête de l'évènement : #getExecutionList().columnList#<br>
					Pas d'enregistrement ramené par la requête de l'évènement<br><br>
					<cfdump var="#getRequiredProperties()#" label="Propriétés requises par l'évènement"><br>
					<cfdump var="#getRequiredColumns()#" label="Colonnes requises par l'évènement">
				</cfif>
			</cfoutput>
		</cfmail>
		<cfreturn THIS>
	</cffunction>

	<cffunction access="private" name="currentReporting" returntype="Struct" hint="Retourne les propriétés du reporting correspondant à currentIndex()">
		<!--- Propriétés à fournir pour définir un reporting (API Reporting - Sept 2011) --->
		<cfset var i=currentIndex()>
		<cfset var RLIST=getExecutionList()>
		<cfset var reportingProperties = {
			bipReport = {
				reporting=getName() & " (ID : " & RLIST.ID[i] & ")", reportParameters=xdoParameters(), xdoAbsolutePath=xdoAbsolutePath(),
				xdoTemplateId=RLIST.TEMPLATE[i], outputFormat=RLIST.FORMAT[i], localization=RLIST.LOCALISATION[i], timeZone="Europe/Paris"
			},
			DELIVERY = {
				DIFFUSION="Evenement", TYPE=RLIST.DELIVERY[i], FILE_NAME=reportFileName(), FILE_EXT=RLIST.FILE_EXT[i],
				USERID=VAL(RLIST.USERID[i]), IDRACINE=VAL(RLIST.IDRACINE[i]), CODE_APP=VAL(RLIST.CODE_APPLI[i]),
				FROM=RLIST.EMAIL_FROM[i], TO=RLIST.EMAIL_TO[i], CC=RLIST.EMAIL_CC[i], SUBJECT=RLIST.EMAIL_SUBJECT[i], REPLYTO="",
				FICHIER=reportLongName(), SHORT_NAME=reportShortName(), POST_PROCESS=RLIST.POST_PROCESS[i], ID=RLIST.ID[i], ATTACHMENT=""
			}
		}>
		<!--- Mise à jour de la valeur de la propriété ATTACHMENT --->
		<cfif RLIST.HAVE_ATTACHEMENT[i]>
			<cfset reportingProperties.DELIVERY["ATTACHMENT"]=reportFileName() & "." & RLIST.FILE_EXT[i]>
		</cfif>
		<cfreturn reportingProperties>
	</cffunction>

	<cffunction access="private" name="reportLongName" returntype="String"
	hint="Retourne le nom long du fichier utilisé pour l'exécution dont l'index correspond à currentIndex() et utilisé pour le container
	Cette implémentation retourne la valeur retournée par la méthode reportFileName()">
		<cfreturn reportFileName()>
	</cffunction>

	<cffunction access="private" name="reportShortName" returntype="String"
	hint="Retourne le nom du fichier utilisé pour l'exécution dont l'index correspond à currentIndex() et utilisé pour le container
	Cette implémentation retourne reportFileName() en remplaçant les espaces par le caractère _">
		<cfreturn replace(TRIM(reportFileName())," ","_")>
	</cffunction>

	<cffunction access="private" name="reportFileName" returntype="String"
	hint="Retourne le nom du fichier utilisé pour l'exécution dont l'index correspond à currentIndex() et utilisé pour le container et le mail">
		<cfset var RLIST=getExecutionList()>
		<cfreturn RLIST.FILE_NAME[currentIndex()]>
	</cffunction>

	<cffunction access="private" name="xdoAbsolutePath" returntype="String"
	hint="Retourne le chemin absolu du XDO utilisé pour l'exécution dont l'index correspond à currentIndex()">
		<cfset var RLIST=getExecutionList()>
		<cfreturn RLIST.DOSSIER[currentIndex()] & "/" & RLIST.XDO[currentIndex()] & "/" & RLIST.XDO[currentIndex()] & ".xdo">
	</cffunction>

	<cffunction access="private" name="xdoParameters" returntype="Struct" hint="Retourne la structure contenant les paramètres du rapport avec leurs valeurs
	Une exception est levée si la colonne correspondant à un paramètre n'est pas présente dans getExecutionList()">
		<cfset var xdoParams={}>
		<cfset var RLIST=getExecutionList()>
		<cfset var parameterColumns=getParameterColumns()>
		<cfloop item="key" collection="#parameterColumns#">
			<cfif structKeyExists(RLIST,key)>
				<!--- Récupération des valeurs du paramètre à partir de la liste d'exécution de l'évènement : getExecutionList() --->
				<cfset var xdoParamValue=RLIST[key][currentIndex()]>
				<!--- Récupération des propriétés du paramètre à partir de la liste des paramètres du rapport : getParameterColumns() --->
				<cfset xdoParams[key] = {
					LABEL=parameterColumns[key]["LABEL"], VALUES=listToArray(xdoParamValue,"|"), VISIBLE=parameterColumns[key]["VISIBLE"]
				}>
			<cfelse>
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION"
					message="La colonne correspondant au paramètre #key# du rapport nest pas présente dans la requete de l'évènement">
			</cfif>
		</cfloop>
		<cfreturn xdoParams>
	</cffunction>

	<cffunction access="private" name="parameterColumns" returntype="struct" hint="Retourne la liste des colonnes qui sera définie comme paramètres du rapports
	Chaque clé de la structure retournée est le nom d'un paramètre du rapport et associée une structure contenant les clés suivantes :
	- LABEL : Libellé du paramètre dont la valeur est retournée par parameterLabel(). La valeur est toujours le nom du paramètre
	- VISIBLE : TRUE si le paramètre est visible dans le module container et FALSE sinon. La valeur est toujours FALSE">
		<cfset var paramColumns={}>
		<cfloop index="columnName" list="#getExecutionList().columnList#">
			<!--- Une colonne est un paramètre du rapport si elle n'est pas un paramètre métier --->
			<cfif NOT structKeyExists(getRequiredColumns(),columnName)>
				<!--- Un paramètre ne peut etre ajouté qu'une et une seule fois (Une requete pouvant avoir plusieurs colonnes de meme nom) --->
				<cfif NOT structKeyExists(paramColumns,columnName)>
					<cfset structInsert(paramColumns,columnName,{LABEL=columnName, VISIBLE=FALSE},FALSE)>
				</cfif>
			</cfif>
		</cfloop>
		<cfreturn paramColumns>
	</cffunction>

	<cffunction access="private" name="validateReportList" returntype="void"
	hint="Lève une exception si une des colonnes de la liste retournée par getRequiredColumns() n'est pas présente dans la requete de l'évènement">
		<cfloop item="columnName" collection="#getRequiredColumns()#">
			<cfif NOT structKeyExists(getExecutionList(),columnName)>
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="La colonne #columnName# est obligatoire et n'a pas été fournie">
			</cfif>
		</cfloop>
	</cffunction>

	<cffunction access="private" name="requiredColumns" returntype="struct" hint="Retourne la liste des colonnes qui sera définie comme paramètres métiers">
		<cfreturn {
			ID="ID de MAJ", DELIVERY="Stratégie de diffusion", POST_PROCESS="Post Process Evènement", USERID="UserId container", CODE_APPLI="CodeApp container",
			IDRACINE="IdRacine container", EMAIL_FROM="Expéditeur mail", EMAIL_TO="Destinataire mail", EMAIL_CC="Copie mail", EMAIL_SUBJECT="Sujet mail",
			FILE_NAME="Nom du fichier container et mail", FILE_EXT="Extension du fichier container et mail", HAVE_ATTACHEMENT="1 Si attachment mail et 0 sinon",
			DOSSIER="Chemin du dossier du XDO", XDO="Nom du XDO", TEMPLATE="Template du XDO", FORMAT="Format de sortie", LOCALISATION="Localisation"
		}>
	</cffunction>
	
	<cffunction access="private" name="validateProperties" returntype="void"
	hint="Lève une exception une des propriétés de la liste retournée par requiredProperties() n'est pas présente dans initProperties">
		<cfargument name="initProperties" type="Struct" required="true" hint="Propriétés à valider">
		<cfset var initProps=ARGUMENTS["initProperties"]>
		<cfloop item="requiredProperty" collection="#requiredProperties()#">
			<cfif NOT structKeyExists(initProps,requiredProperty)>
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="La propriété #requiredProperty# est obligatoire et n'a pas été fournie">
			</cfif>
		</cfloop>
	</cffunction>
	
	<cffunction access="private" name="requiredProperties" returntype="Struct" hint="Retourne la liste des propriétés qui sera définie comme propriétés requises
	Ces propriétés sont à fournir soit dans le scope URL soit à partir du constructeur getInstance()">
		<cfreturn {ID="Valeur permettant d'identifier l'évènement à exécuter"}>
	</cffunction>
	
	<cffunction access="private" name="getRunProperties" returntype="Struct" hint="Retourne les propriétés d'exécution pour l'évènement">
		<cfreturn getInitProperties().RUN_PROPERTIES>
	</cffunction>

	<cffunction access="private" name="getMailProperties" returntype="Struct" hint="Retourne les propriétés mail">
		<cfreturn getInitProperties().MAIL_PROPERTIES>
	</cffunction>
	
	<cffunction access="private" name="getLog" returntype="string" hint="Retourne la valeur de l'attribut file pour cflog">
		<cfreturn getInitProperties().LOG>
	</cffunction>
	
	<cffunction access="private" name="getInitProperties" returntype="Struct"
	hint="Retourne les propriétés d'initialisation de l'évènement ou une structure vide s'ils ne sont pas définis">
		<cfif structKeyExists(VARIABLES,"INIT_PROPERTIES")>
			<cfreturn VARIABLES.INIT_PROPERTIES>
		<cfelse>
			<cfreturn {}>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="getReportList" returntype="query" hint="Exécute et retourne la requete ramenant la liste des rapports à exécuter">
		<cfquery name="qEvenement" datasource="ROCOFFRE">
			select	/* Champs métiers */
						null as ID, null as DELIVERY, null as POST_PROCESS, null as USERID, null as CODE_APPLI, null as IDRACINE,
						null as EMAIL_FROM, null as EMAIL_TO, null as EMAIL_CC, null as EMAIL_SUBJECT,
						null as FILE_NAME, null as FILE_EXT, null as HAVE_ATTACHEMENT,
						/* Champs pour exécution du rapport BIP */
						null as DOSSIER, null as XDO, null as TEMPLATE, null as FORMAT, null as LOCALISATION
						/* (Tout le reste est considéré comme) Champs Paramètres du rapport BIP */
			from		DUAL
			where	1=0
		</cfquery>
		<cfreturn qEvenement>
	</cffunction>
</cfcomponent>