<cfcomponent displayName="fr.consotel.api.ibis.publisher.handler.process.ProcessM16" implements="fr.consotel.api.ibis.publisher.handler.IServiceHandler" hint="Exemple de handler de service">
	
	<!--- PROCESS DEMO (Auteur : Cedric --->
	
	<cfset explicitInit()>
	
	<cffunction access="public" name="run" returntype="void" hint="Called with the event that triggered this handler">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true" hint="Event object that triggered this handler">
		
		<cftry>
			<cfset VARIABLES.MY_CODE.run(ARGUMENTS.eventObject)>
		<cfcatch>
			<cfset onError(eventObject,"M16",cfcatch)>
		</cfcatch>			
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="onError" returntype="void" hint="Called when an error has occurred either during creation or execution of IServiceHandler">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true" hint="Event object that triggered run() handler">
		<cfargument name="eventHandler" type="string" required="true" hint="The corresponding EVENT_HANDLER supposed to handle eventObject">
		<cfargument name="exceptionObject" type="any" required="false" hint="Optionally the corresponding catched exception (Same structure as CFCATCH scope)">
		
		<cfset VARIABLES.MY_CODE.onError(ARGUMENTS.eventObject, ARGUMENTS.eventHandler, ARGUMENTS.exceptionObject)>
	</cffunction>
	
	<cffunction access="private" name="explicitInit" returntype="void" hint="Initialization">
		
		<cfset VARIABLES.MY_CODE=createObject("component","fr.consotel.api.ibis.publisher.handler.process.ServiceHandlerM16")>
	
	</cffunction>
	
</cfcomponent>