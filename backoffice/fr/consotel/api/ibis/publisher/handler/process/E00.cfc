<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.process.E00"
extends="fr.consotel.api.ibis.publisher.reporting.impl.ReportingLogging" implements="fr.consotel.api.ibis.publisher.handler.IPostProcess"
hint="Implémentation servant de classe parent et/ou d'exemple de traitement de mise à jour exécuté par la stratégie de diffusion Evenement
La stratégie Evenement n'effectue ce type de traitement que lorsque les conditions suivantes sont vérifiées :
- La méthode getStatus() de l'évènement passé à handleUpdateEvent() retourne UpdateEvent.SUCCESS() ou UpdateEvent.WARNING()
- La valeur retournée par UpdateEvent.getPostProcess() n'est pas une chaine vide
Un traitement de mise à jour Evenement est identifié par la valeur de la propriété POST_PROCESS qui est fournie par le reporting lors de son exécution
Le traitement doit correspondre à un composant qui se trouve sous le package : fr.consotel.api.ibis.publisher.handler.process
La valeur POST_PROCESS doit correspondre au nom restant du composant sans les caractères correspondant à fr.consotel.api.ibis.publisher.handler.process
Dans cet exemple ce traitement envoi un mail générique au destinataire par défaut de l'API Reporting et est identifié par la valeur POST_PROCESS : E00">
	<!--- fr.consotel.api.ibis.publisher.handler.IPostProcess --->
	<cffunction access="public" name="handleUpdateEvent" returntype="void"
	hint="Effectue un exemple de traitement de mise à jour à partir des infos contenues dans l'évènement updateEvent. Le traitement implémenté effectue :
	- L'envoi d'un mail générique au destinataire par défaut de l'API contenant entre autres les infos retournés par event.getDeliveryInfos()
	- Un exemple d'affichage de message de log en utilisant la méthode héritée de l'API Reporting correspondant aux méthodes : logInfo(), logWarning(), logError()
	Le mail générique est diffusé en utilisant les propriétés Mail héritées de l'API Reporting et retournés par : getMailProperties()">
		<cfargument name="updateEvent" type="fr.consotel.api.ibis.publisher.handler.event.UpdateEvent" required="true"
		hint="Evènement contenant les infos concernant le reporting et la mise à jour effectuer">
		<cfset var eventImpl=ARGUMENTS["updateEvent"]>
		<cfset var thisEventType=listLast(getMetadata(THIS).NAME,".")>
		<cfset updateId(eventImpl)>
		<cfset logInfo(thisEventType & " UpdateEvent COMPLETED (ID : " & eventImpl.getId() & ")")>
	</cffunction>

	<!--- fr.consotel.api.ibis.publisher.handler.process.E00 --->
	<cffunction access="public" name="getInstance" returntype="fr.consotel.api.ibis.publisher.handler.IPostProcess" hint="Constructeur de cette implémentation">
		<!--- Propriétés minimales permettant : Log, Notifications mails de l'API (ReportingLogging) --->
		<cfset SUPER.getService("","")>
		<cfreturn THIS>
	</cffunction>

	<cffunction access="private" name="updateId" returntype="void" hint="Exemple d'implémentation d'un traitement de mise à jour de la valeur UpdateEvent.getId()
	Envoi un mail générique au destinataire par défaut de l'API Reporting contenant :
	- Les infos concernant l'évènement qui sont retournés par event.getDeliveryInfos()
	- Un dump de la requete SELECT event.getId() as ID, event.getPostProcess() as PROCESS from DUAL en utilisant la source de donnée retournée par getDataSource()">
		<cfargument name="updateEvent" type="fr.consotel.api.ibis.publisher.handler.event.UpdateEvent" required="true" hint="Provient de handleUpdateEvent()">
		<cfset eventImpl=ARGUMENTS["updateEvent"]>
		<!--- Requete affichant la valeur de l'ID de MAJ UpdateEvent.getId() et le type du traitement de MAJ UpdateEvent.getPostProcess() --->
		<cfquery name="qTestQuery" datasource="#getDataSource()#">
			SELECT	#eventImpl.getId()# as ID, '#eventImpl.getPostProcess()#' as PROCESS
			FROM		DUAL
		</cfquery>
		<!--- Récupèration des propriétés de diffusion mail héritées de l'API Reporting (ReportingLogging) --->
		<cfset var MAIL_API=getMailProperties()>
		<!--- Récupère les propriétés mail de diffusion d'une notification (NOTIFICATION). Les autres correspondant à un avertissement (WARNING) et une erreur (ERROR) --->
		<cfset var notificationMail=MAIL_API["NOTIFICATION"]>
		<cfset var warningMail=MAIL_API["WARNING"]>
		<cfset var errorMail=MAIL_API["ERROR"]>
		<cfmail attributecollection="#notificationMail#" subject="Exemple de traitement de mise à jour pour la stratégie de diffusion Evenement">
			<cfoutput>
				Implémentation du traitement de mise à jour : #getMetadata(THIS).NAME#<br>
				<b>Dump d'une requete test effectuée avec la source de données :</b> #getDataSource()#<br>
				<cfdump var="#qTestQuery#"><br>
				#eventImpl.getDeliveryInfos()#
			</cfoutput>
		</cfmail>
	</cffunction>
	
	<cffunction access="private" name="getDataSource" returntype="String" hint="Retourne le nom du data source utilisé par updateId()">
		<cfreturn "ROCOFFRE">
	</cffunction>
</cfcomponent>