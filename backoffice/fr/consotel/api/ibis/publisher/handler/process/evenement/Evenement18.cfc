<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement18"
extends="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement"
hint="Impl�mentation IReporting pour Ev�nement. Fichier de Log : [CFUSION]/logs/EVENEMENT.log">
<!--- *********************************************************************************************************************** --->
	<cffunction access="private" name="getReportList" returntype="query" hint="Retourne la liste des rapports � ex�cuter et d�termine les param�tres du rapport BIP">
		<cfset var startTick=getTickCount()><!--- Tracking de la dur�e --->
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_pfgp_suivi_chargement.type_18_liste">
			<cfprocresult name="qEvenement">
		</cfstoredproc>
		<cflog text="Evenement.getReportList() : COMPLETED in #(getTickCount() - startTick)# ms" file="#getLog()#" type="information">
		<cfreturn qEvenement>
	</cffunction>
</cfcomponent>