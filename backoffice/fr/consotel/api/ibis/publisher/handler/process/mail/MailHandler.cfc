<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.process.mail.MailHandler"
extends="fr.consotel.api.ibis.publisher.handler.process.PostProcess"
hint="Extension de PostProcess pour l'implémentation du HANDLER d'envoi de mail<br>
Les rapports sont générés dans le FTP Pelican (container) et sont toujours téléchargés dans un répertoire temporaire local avant leur diffusion<br>
Les valeurs possibles pour EVENT_TYPE sont :<br>
- INLINE : Le rapport est diffusé par mail en disposition INLINE. Le rapport ne sera pas correctement affiché s'il n'est pas au format HTML<br>
- ATTACHMENT : Le rapport est diffusé par mail en disposition ATTACHMENT">
<!--- *********************************************************************************************************************** --->
	<!--- fr.consotel.api.ibis.publisher.handler.process.PostProcess --->
	<cffunction access="public" name="run" returntype="void"
	hint="Envoi le reporting par mail. En ATTACHMENT si eventObject.getEventType() vaut 1 et en INLINE si la valeur est 0<br>
	Le rapport n'est diffusé que si son statut est en Succès. Une fois l'envoi du mail effectué, la méthode updateId() est appelée">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true">
		<cfset var eventTargetInfos=SUPER.getTargetInfos(ARGUMENTS.eventObject)>
		<cfif eventTargetInfos.recordcount GT 0>
			<cfxml variable="reportingParams">
				<cfoutput>#TRIM(eventTargetInfos.SYS_PARAM)#</cfoutput>
			</cfxml>
			<cflog text="MailHandler.run(ID : #ARGUMENTS.eventObject.getId()#) - Report Status : #ARGUMENTS.eventObject.getReportStatus()#" file="#getLog()#" type="information">
			<cfif UCase(ARGUMENTS.eventObject.getReportStatus()) NEQ "F">
				<!--- Vérification de l'extension pour la diffusion en attachement mail --->
				<cfif structKeyExists(VARIABLES.FILE_EXT,TRIM(reportingParams.ROWSET.ROW.FILE_EXT.xmlText)) OR
						(UCase(TRIM(reportingParams.ROWSET.ROW.EVENT_TYPE.xmlText)) EQ VARIABLES.INLINE_EVENT)>
					<!--- Diffusion par mail du reporting --->
					<cfset sendReporting(ARGUMENTS.eventObject.getEventTarget(), reportingParams,eventTargetInfos.CODE_APPLI)>
					<!--- En cas d'exception la méthode onError() est appelée par l'entité qui a exécuté cet HANDLER --->
					<cfset SUPER.run(ARGUMENTS.eventObject)><!--- Inclus MAJ ID --->
				<cfelse>
					<!--- Propriétés mails --->
					<cfset var mailAttributes = {from="api@consotel.fr", to=VARIABLES.HANDLER_ADMIN, type="html"}>
					<cflog text="MailHandler.sendReporting() : INVALID EMAIL ATTACHMENT EXTENSION #TRIM(reportingParams.ROWSET.ROW.FILE_EXT.xmlText)#" file="#getLog()#" type="warning">
					<cfmail attributecollection="#mailAttributes#" subject="Warning : #TRIM(reportingParams.ROWSET.ROW.MAIL_SUBJECT.xmlText)#">
						<cfoutput>
							Référence : #TRIM(reportingParams.ROWSET.ROW.FILE_NAME.xmlText)#<br>
							Le rapport ne sera pas diffusé par mail car l'extension fournie <b>#TRIM(reportingParams.ROWSET.ROW.FILE_EXT.xmlText)#</b> n'est pas supportée par le serveur Mail
							La MAJ de l'ID de MAJ ne sera pas effectuée (ID : #ARGUMENTS.eventObject.getId()#)
						</cfoutput>
					</cfmail>
				</cfif>
			<cfelse>
				<cfset sendReportingFailed(ARGUMENTS.eventObject.getEventTarget(), reportingParams, eventTargetInfos.CODE_APPLI)>
			</cfif>
			<cflog text="MailHandler.run() - Event Target Infos Size #eventTargetInfos.recordcount#" file="#getLog()#" type="information">
		<cfelse>
			<cflog text="MailHandler.run() : TARGET INFOS NOT FOUND" file="#getLog()#" type="error">
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="TARGET INFOS NOT FOUND">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="onError" returntype="void" hint="Envoi 2 mails de notification d'erreur et affiche le log d'erreur">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true">
		<cfargument name="eventHandler" type="string" required="true">
		<cfargument name="exceptionObject" type="any" required="false" default="">
		<cfset var errorMsg="AN EXCEPTION OCCURED">
		<!--- Propriétés mails --->
		<cfset var mailAttributes = {from="api@consotel.fr", to=VARIABLES.HANDLER_ADMIN, type="html"}>
		<cfif isDefined("ARGUMENTS.exceptionObject.MESSAGE")>
			<cfset var errorMsg=ARGUMENTS.exceptionObject.MESSAGE>
		</cfif>
		<cflog text="MailHandler.onError() : #errorMsg#" file="#getLog()#" type="error">
		<cfset var eventTargetInfos=SUPER.getTargetInfos(ARGUMENTS.eventObject)>
		<cfmail attributecollection="#mailAttributes#" subject="[*Error*] MailHandler.onError() : #errorMsg#">
			<cfoutput>
				Evènement :<br>
					- EVENT_ID : #ARGUMENTS.eventObject.getId()#<br>
					- EVENT_TARGET : #ARGUMENTS.eventObject.getEventTarget()#<br>
					- EVENT_TYPE : #ARGUMENTS.eventObject.getEventType()#<br>
					- EVENT_HANDLER : #ARGUMENTS.eventObject.getEventHandler()#<br>
					- JOB_ID : #ARGUMENTS.eventObject.getJobId()#<br>
					- JOB_STATUS : #ARGUMENTS.eventObject.getReportStatus()#<br>
					- REPORT_URL : #ARGUMENTS.eventObject.getReportPath()#<br>
				Autres :<br>
					- BIP_SERVER : #ARGUMENTS.eventObject.getBipServer()#<br>
					- EVENT_DISPATCHER : #ARGUMENTS.eventObject.getEventDispatcher()#<br><br>
			</cfoutput>
			<cfdump var="#eventTargetInfos#" label="EventTarget Infos">
			<cfdump var="#ARGUMENTS.exceptionObject#" label="Exception Rencontrée">
		</cfmail>
	</cffunction>

	<cffunction access="public" name="createInstance" returntype="fr.consotel.api.ibis.publisher.handler.IServiceHandler" hint="Appelée en tant que constructeur">
		<cfset SUPER.createInstance()><!--- Constructeur de la classe parent --->
		<!--- fr.consotel.api.ibis.publisher.handler.process.mail.MailHandler --->
		<cfset VARIABLES.ATTACHMENT_EVENT="ATTACHMENT">
		<cfset VARIABLES.INLINE_EVENT="INLINE">
		<cfset VARIABLES.MAIL_SERVER="mail-cv.consotel.fr">
		<cfset VARIABLES.MAIL_SERVER_PORT=25>
		<cfset VARIABLES.MAIL_TYPE="html">
		<cfset VARIABLES.HANDLER_ADMIN="monitoring@saaswedo.com">
		<cfset VARIABLES.FILE_EXT={xml="xml",txt="txt",csv="csv",html="html",rtf="rtf",pdf="pdf",xls="xls",ppt="ppt"}>
		<!--- Propriétés FTP --->
		<cfset VARIABLES.FTP = {
			server="pelican.consotel.fr", username="container", password="container", action="getfile", transfermode="auto", failifexists=FALSE, secure=FALSE, stoponerror=TRUE
		}>
		<cflog text="MailHandler.createInstance() : COMPLETED" file="#getLog()#" type="information">
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="sendReporting" returntype="void"
	hint="Appelée pour envoyer le reporting par mail. Les propriétés du serveur mail sont retournées par getEventMailAttributes()">
		<cfargument name="remoteFile" type="String" required="true" hint="Chemin relatif du fichier FTP">
		<cfargument name="reportingParams" type="XML" required="true" hint="Paramètres du reporting (SYS_PARAM)">
		<cfargument name="codeApp" type="numeric" required="false" default="0" hint="Code App">
		<cfset var xmlParams=ARGUMENTS["reportingParams"]>
		<!--- Propriétés mails --->
		<cfset var mailAttributes = {
			from=TRIM(xmlParams.ROWSET.ROW.MAIL_FROM.xmlText), to=TRIM(xmlParams.ROWSET.ROW.MAIL_TO.xmlText),
			subject=TRIM(xmlParams.ROWSET.ROW.MAIL_SUBJECT.xmlText)
		}>
		<!--- Ajout de l'adresse de copie --->
		<cfif LEN(TRIM(xmlParams.ROWSET.ROW.MAIL_CC.xmlText)) GT 0>
			<cfset mailAttributes.cc=TRIM(xmlParams.ROWSET.ROW.MAIL_CC.xmlText)>
		</cfif>
		<!--- Ajout des propriétés du serveur Mail --->
		<cfset structAppend(mailAttributes,getEventMailAttributes(ARGUMENTS.codeApp),TRUE)>
		<!--- Lecture du contenu du fichier pour l'envoyer par mail (INLINE ou ATTACHEMENT) --->
		<cffile file="/container/#ARGUMENTS.remoteFile#" action="readBinary" charset="utf-8" variable="REPORT_OUTPUT_FILE">
		<!--- Diffusion du rapport --->
		<cfif UCase(TRIM(xmlParams.ROWSET.ROW.EVENT_TYPE.xmlText)) EQ VARIABLES.ATTACHMENT_EVENT><!--- Disposition : ATTACHMENT --->
			<!--- Nom du fichier (Attachement) : ARGUMENTS.reportingId pour unicité du fichier temporaire (Pièce jointe Mail) --->
			<cfset var attachmentName=TRIM(xmlParams.ROWSET.ROW.FILE_NAME.xmlText) & "." & TRIM(xmlParams.ROWSET.ROW.FILE_EXT.xmlText)>
			<!--- Envoi du mail (Récupération du fichier via le partage réseau PELICAN/container) --->
			<cfmail attributecollection="#mailAttributes#">
				<cfmailparam file="#attachmentName#" content="#REPORT_OUTPUT_FILE#">
			</cfmail>
			<cflog text="MailHandler.sendReporting() : Mail Sent (#TRIM(xmlParams.ROWSET.ROW.EVENT_TYPE.xmlText)#)" file="#getLog()#" type="information">
		<cfelseif UCase(TRIM(xmlParams.ROWSET.ROW.EVENT_TYPE.xmlText)) EQ VARIABLES.INLINE_EVENT><!--- Disposition : INLINE --->
			<cfmail attributecollection="#mailAttributes#">
				<html>
					<head>
						<style type="text/css">
						body {background-color: ##FFFFFF;}
						</style>
					</head>
				<cfoutput>
					<center>#toString(REPORT_OUTPUT_FILE,"utf-8")#</center>
				</cfoutput>
				</html>
			</cfmail>
			<cflog text="MailHandler.sendReporting() : Mail Sent (#TRIM(xmlParams.ROWSET.ROW.EVENT_TYPE.xmlText)#)" file="#getLog()#" type="information">
		<cfelse>
			<cflog text="MailHandler.sendReporting() - UNKNOWN EVENT_TYPE : #TRIM(xmlParams.ROWSET.ROW.EVENT_TYPE.xmlText)#" file="#getLog()#" type="information">
		</cfif>
	</cffunction>

	<cffunction access="private" name="sendReportingFailed" returntype="void"
	hint="Appelée pour envoyer une notification dans le cas où le rapport n'est pas généré en succès">
		<cfargument name="remoteFile" type="String" required="true" hint="Chemin relatif du fichier FTP">
		<cfargument name="reportingParams" type="XML" required="true" hint="Paramètres du reporting (SYS_PARAM)">
		<cfargument name="codeApp" type="numeric" required="false" default="0" hint="Code App">
		<cfset var xmlParams=ARGUMENTS["reportingParams"]>
		<!--- Propriétés mails --->
		<cfset var mailAttributes = {
			from="api@consotel.fr", to=VARIABLES.HANDLER_ADMIN,
			subject="Erreur : " & TRIM(xmlParams.ROWSET.ROW.MAIL_SUBJECT.xmlText), type="html"
		}>
		<cfmail attributecollection="#mailAttributes#">
			<cfoutput>
				Référence : #ARGUMENTS.remoteFile#<br>
				Le statut du rapport est en erreur
			</cfoutput>
		</cfmail>
		<cflog text="MailHandler.sendReportingFailed() : Mail Sent" file="#getLog()#" type="information">
	</cffunction>
	
	<cffunction access="private" name="getEventMailAttributes" returntype="Struct" hint="Retourne les propriétés du serveur MAIL à utiliser pour sendReporting()">
		<cfargument name="codeApp" type="numeric" required="false" default="0" hint="Code App">
		<cfreturn {server=VARIABLES.MAIL_SERVER, port=VARIABLES.MAIL_SERVER_PORT, type=VARIABLES.MAIL_TYPE}>
	</cffunction>
</cfcomponent>