<cfcomponent displayName="fr.consotel.api.ibis.publisher.handler.process.ServiceHandlerM11"
			extends="fr.consotel.api.ibis.publisher.handler.ServiceHandler" hint="Exemple de handler de service">
	
	<cffunction access="public" name="run" returntype="void" hint="Called with the event that triggered this handler">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true" hint="Event object that triggered this handler">
			
			<cfset SUPER.run(ARGUMENTS.eventObject)>

			<cfset mailObject = createObject('component',"fr.consotel.consoview.M111.SendMail")>
			<cfset mailObject.sendMail(eventObject)>
			
	</cffunction>
	
	<cffunction access="public" name="onError" returntype="void" hint="Called when an error has occurred either during creation or execution of IServiceHandler">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true" hint="Event object that triggered run() handler">
		<cfargument name="eventHandler" type="string" required="true" hint="The corresponding EVENT_HANDLER supposed to handle eventObject">
		<cfargument name="exceptionObject" type="any" required="false" hint="Optionally the corresponding catched exception (Same structure as CFCATCH scope)">
		
			<cfmail from="fromM11@consotel.fr" to="monitoring@saaswedo.com" subject="MAIL SAV M11" type="html" server="mail-cv.consotel.fr" >
				<cfoutput>
					JOB_ID : #ARGUMENTS.eventObject.getJobId()#<br />
					REPORT_STATUS : #ARGUMENTS.eventObject.getReportStatus()#<br />
					REPORT_URL : #ARGUMENTS.eventObject.getReportPath()#<br />
					BIP_SERVER : #ARGUMENTS.eventObject.getBipServer()#<br />
					EVENT_TARGET : #ARGUMENTS.eventObject.getEventTarget()#<br />
					EVENT_TYPE : #ARGUMENTS.eventObject.getEventType()#<br />
					EVENT_DISPATCHER : #ARGUMENTS.eventObject.getEventDispatcher()#<br />
					EVENT_HANDLER : #ARGUMENTS.eventHandler#
				</cfoutput>
				<cfdump var="#ARGUMENTS.exceptionObject#" label="EXCEPTION">
			</cfmail>
		
	</cffunction>
	
</cfcomponent>
