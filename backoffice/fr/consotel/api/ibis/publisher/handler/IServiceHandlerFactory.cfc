<cfinterface displayName="fr.consotel.api.ibis.publisher.handler.IServiceHandlerFactory" hint="Factory Interface for IServiceHandler">
	<cffunction access="public" name="getFactory" returntype="fr.consotel.api.ibis.publisher.handler.IServiceHandlerFactory" hint="Returns an initialized instance of IServiceHandlerFactory">
	</cffunction>
	
	<cffunction access="public" name="createServiceHandler" returntype="fr.consotel.api.ibis.publisher.handler.IServiceHandler" hint="Returns an instance of IServiceHandler">
		<cfargument name="eventHandler" type="string" required="true" hint="Event Handler used to instanciate coresponding IServiceHandler">
	</cffunction>
</cfinterface>