<cfcomponent displayname="fr.consotel.api.ibis.publisher.handler.NotificationServlet"
hint="Implémentation qui traite la réception d'une notification HTTP (BIP) envoyée vers le serveur de notification : REPORTING<br>
Page : /fr/consotel/api/ibis/notification/notification.cfm
Fichier de Log : [CFUSION]/logs/CFMX_NOTIFICATION_SERVLET.log
Implémentation IServiceHandler : Ne fonctionne qu'avec une classe qui dérive de fr.consotel.api.ibis.publisher.handler.process.PostProcess<br>
La méthode PostProcess.createInstance() est appelée en 1er en tant que constructeur de l'implémentation IServiceHandler">
<!--- ************************************************************************************************************************* --->
	<!--- fr.consotel.api.ibis.publisher.handler.NotificationServlet --->
	<cffunction access="public" name="createInstance" returntype="fr.consotel.api.ibis.publisher.handler.NotificationServlet" hint="Constructeur de cette classe">
		<cfargument name="httpNotification" type="string" required="true" hint="Nom du serveur de notification HTTP prédéfini dans BIP">
		<cfset VARIABLES.NOTIFICATION_LOG="CFMX_NOTIFICATION_SERVLET"><!--- Valeur file pour cflog --->
		<cfif NOT isDefined("VARIABLES.INITIALIZED")><!--- Flag d'initialisation --->
			<!--- DebugMode + Gateway ID --->
			<cfset VARIABLES.TRACING=isDebugMode()><!--- TODO : Inutilisé --->
			<!--- Nom par défaut du serveur de notification HTTP qui doit être prédéfini dans BIP --->
			<cfset VARIABLES.DEFAULT_NOTIFICATION="REPORTING"><!--- TODO : Inutilisé --->
			<cfset VARIABLES.GATEWAY_ID="BIP">
			<cfset VARIABLES.DATASOURCE="ROCOFFRE">
			<!--- fr.consotel.api.ibis.publisher.handler.NotificationServlet --->
			<cfset VARIABLES.DOMAIN="consotel.fr"><!--- Valeur file pour cflog --->
			<cfset VARIABLES.NOTIFICATION=ARGUMENTS.httpNotification><!--- Nom par défaut du serveur de notification HTTP reçu en paramètre à la création --->
			<!--- Propriétés pour l'instanciation de l'entité EVENT_HANDLER (TTH) --->
			<cfset VARIABLES.HANDLER_INTERFACE="fr.consotel.api.ibis.publisher.handler.IServiceHandler">
			<cfset VARIABLES.HANDLER_PACKAGE="fr.consotel.api.ibis.publisher.handler.process"><!--- Package pour les implémentation IServiceHandler --->
			<cfset VARIABLES.REPORTING_EVENT_IMPL="fr.consotel.api.ibis.publisher.handler.event.ReportingEvent">
			<cfset VARIABLES.PERSISTENCE="pkg_m00.getmail"><!--- Persistance : Procédure Stockée (ROCOFFRE) --->
			<!--- Mail --->
			<cfset VARIABLES.MAIL={from="api@" & VARIABLES.DOMAIN, to="monitoring@saaswedo.com", type="html"}>
			<cflog text="NotificationServlet : CREATION COMPLETED" file="#getLog()#" type="information">
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="getMailAttributes" returntype="Struct" hint="Retourne la collection d'attribut pour CFMAIL (Pour les logs)">
		<cfreturn VARIABLES.MAIL>
	</cffunction>
	
	<cffunction access="private" name="getDataSource" returntype="String" hint="Retourne le nom du data source pour la valeur de getPersistence()">
		<cfreturn VARIABLES.DATASOURCE>
	</cffunction>

	<cffunction access="private" name="getPersistence" returntype="String" hint="Retourne le nom de la procédure stockée qui est utilisée pour la persistance">
		<cfreturn VARIABLES.PERSISTENCE>
	</cffunction>
	
	<cffunction access="private" name="getLog" returntype="string" hint="Valeur de l'attribut file pour cflog">
		<cfreturn VARIABLES.NOTIFICATION_LOG>
	</cffunction>

	<!--- fr.consotel.api.ibis.publisher.handler.HttpReportEventHandler --->
	<cffunction access="public" name="onHttpNotification" returntype="void"
	hint="Méthode appelée à la reception d'une notification HTTP (BIP)<br>
	La valeur de IServiceEvent.getEventDispatcher() est : le chemin de la page de notification<br>
	La valeur de IServiceEvent.getBipServer() est : BIP'">
		<cfargument name="httpParameters" type="struct" required="true" hint="Formulaire HTTP/POST provenant de la notification (BIP)">
		<cfset var startTick=getTickCount()><!--- Tracking de la durée  --->
		<cftry>
			<cfset var EVENT_TARGET=ARGUMENTS.httpParameters.JOBNAME>
			<cfstoredproc datasource="#getDataSource()#" procedure="#getPersistence()#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  value="#EVENT_TARGET#">
				<cfprocresult name="eventTargetInfos">
			</cfstoredproc>
			<!--- Instanciation de l'implémentation IServiceEvent et Dispatch de l'evènement --->
			<cfif eventTargetInfos.recordcount GT 0><!--- Un seul enregistrement ramené --->
				<cfxml variable="reportingParams">
					<cfoutput>#eventTargetInfos.SYS_PARAM#</cfoutput>
				</cfxml>
				<cfxml variable="xdoParams">
					<cfoutput>#eventTargetInfos.TECH_PARAM#</cfoutput>
				</cfxml>
				<cfset var EVENT_ID=VAL(TRIM(reportingParams.ROWSET.ROW.EVENT_ID.xmlText))>
				<cfset var EVENT_TYPE=TRIM(reportingParams.ROWSET.ROW.EVENT_TYPE.xmlText)>
				<cfset var EVENT_HANDLER=TRIM(reportingParams.ROWSET.ROW.EVENT_HANDLER.xmlText)>
				<cfset var httpEvent = {
					EVENT_ID=EVENT_ID, EVENT_TARGET=EVENT_TARGET, EVENT_TYPE=EVENT_TYPE, EVENT_HANDLER=EVENT_HANDLER, JOB_ID=ARGUMENTS.httpParameters.JOBID,
					REPORT_STATUS=ARGUMENTS.httpParameters.STATUS, REPORT_URL=ARGUMENTS.httpParameters.REPORT_URL,
					BIP_SERVER="BIP", EVENT_DISPATCHER=CGI.SCRIPT_NAME
				}>
				<!--- SendGatewayMessage (ASYNC : Non Bloquant) --->
				<cftry>
					<cfset sendGatewayMessage(VARIABLES.GATEWAY_ID,ARGUMENTS.httpParameters)>
					<cflog file="#getLog()#" type="information" text="NotificationServlet.onHttpNotification() - Gateway Message Sent :  #VARIABLES.GATEWAY_ID#">
					<cfcatch type="any">
						<cflog file="#getLog()#" type="warning" text="NotificationServlet.onHttpNotification() - SendGatewayMessage :  #CFCATCH.message#">
					</cfcatch>
				</cftry>
				<!--- Exécute le POSTPROCESS --->
				<cflog file="#getLog()#" type="information"
						text="NotificationServlet.onHttpNotification(#(getTickCount() - startTick)# ms) - TTH :  #EVENT_TARGET#,#EVENT_TYPE#,#EVENT_HANDLER#">
				<cfset dispatchEvent(httpEvent,EVENT_HANDLER)>
			<cfelse>
				<cflog text="NotificationServlet.onHttpNotification() : EMPTY PERSISTENCE.getMailToDatabase(#EVENT_TARGET#)" file="#getLog()#" type="warning">
			</cfif>
			<cfcatch type="any">
				<cflog text="NotificationServlet.onHttpNotification() : #CFCATCH.message#" file="#getLog()#" type="error">
				<cfmail attributecollection="#getMailAttributes()#" subject="[*Error*] NotificationServlet.onHttpNotification() : #CFCATCH.MESSAGE#">
					Exception : #CFCATCH.message#<br>
					Details : #CFCATCH.Detail#<br><br>
					<cfdump var="#ARGUMENTS.httpParameters#" label="Notification HTTP (BIP)">
					<cfdump var="#CFCATCH.TagContext#" label="Tag Context">
				</cfmail>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="private" name="dispatchEvent" returntype="void"
	hint="Instancie l'entitié EVENT_HANDLER (IServiceHandler) et execute sa méthode run()<br>
	Si l'exécution de run() lève une exception alors la méthode IServiceHandler.onError() est exécutée<br>
	Si onError() lève une exception alors la méthode onHandlerError() un mail d'erreur est envoyé'">
		<cfargument name="eventInfos" type="struct" required="true" hint="Structure contenant les infos de l'évènement">
		<cfargument name="eventHandler" type="string" required="true" hint="Valeur de l'entitié EVENT_HANDLER">
		<cfset var startTick=getTickCount()><!--- Tracking de la durée  --->
		<!--- Instanciation de l'évènement --->
		<cfset var eventObject=createObject("component",VARIABLES.REPORTING_EVENT_IMPL).createInstance(ARGUMENTS.eventInfos)>
		<cftry>
			<!--- Instanciation du HANDLER (Pas de constructeur) --->
			<cfset var serviceHandler=createObject("component",VARIABLES.HANDLER_PACKAGE & "." & ARGUMENTS.eventHandler).createInstance()>
			<!--- Exécution du HANDLER avec l'ID de MAJ et les autres propriétés dans le paramètre IServiceEvent --->
			<cfif isInstanceOf(serviceHandler,VARIABLES.HANDLER_INTERFACE)>
				<cftry>
					<cfset serviceHandler.run(eventObject)>
					<cfcatch type="any"><!--- Gestion des exceptions : IServiceHandler.run() --->
						<cfif VARIABLES.TRACING>
							<cflog text="NotificationServlet.dispatchEvent() - IServiceHandler.run() Failed : #CFCATCH.message#" file="#getLog()#" type="warning">
						</cfif>
						<cftry>
							<!--- Exécution du HANDLER d'erreur de l'implémentation IServiceHandler --->
							<cfset serviceHandler.onError(eventObject,ARGUMENTS.eventHandler,CFCATCH)>
							<cfcatch type="any"><!---  Gestion des exceptions : IServiceHandler.onError() --->
								<cfif VARIABLES.TRACING>
									<cflog text="NotificationServlet.dispatchEvent() - IServiceHandler.onError() Failed : #CFCATCH.message#" file="#getLog()#" type="error">
								</cfif>
								<cfmail attributecollection="#getMailAttributes()#" subject="[*Error*] NotificationServlet.dispatchEvent()  - Execution IServiceHandler.onError() : #CFCATCH.message#">
									<cfoutput>
										Implémentation IServiceHandler : #getMetaData(serviceHandler).NAME#<br>
										Exception levée à l'exécution de IServiceHandler.onError() : #CFCATCH.message#<br>
										Details : #CFCATCH.Detail#<br><br>
									</cfoutput>
									<cfdump var="#ARGUMENTS.eventInfos#" label="Propriétés d'instanciation de l'implémentation IServiceEvent">
									<cfdump var="#CFCATCH.TagContext#" label="Tag Context">
								</cfmail>
							</cfcatch>
						</cftry>
					</cfcatch>
				</cftry>
			<cfelse><!--- Implémentation IServiceHandler Invalide --->
				<cfmail subject="[*Warning*] NotificationServlet.dispatchEvent() - INVALID HANDLER : #ARGUMENTS.eventHandler#" attributecollection="#getMailAttributes()#">
					<cfoutput>
						L'entité EVENT_HANDLER #ARGUMENTS.eventHandler# n'est pas du type #VARIABLES.HANDLER_INTERFACE#<br><br>
					</cfoutput>
					<cfdump var="#ARGUMENTS.eventInfos#" label="Propriétés d'instanciation de l'implémentation IServiceEvent">
				</cfmail>
				<cfif VARIABLES.TRACING>
					<cflog text="NotificationServlet.dispatchEvent() - Handler Invalid Interface : #ARGUMENTS.eventHandler#" file="#getLog()#" type="warning">
				</cfif>
			</cfif>
			<cfcatch type="any"><!--- Gestion des exceptions : Instanciation de l'implémentation IServiceHandler --->
				<cfif VARIABLES.TRACING>
					<cflog text="NotificationServlet.dispatchEvent() - IServiceHandler Instantiation : #CFCATCH.message#" file="#getLog()#" type="error">
				</cfif>
				<cfmail attributecollection="#getMailAttributes()#" subject="[*Error*] NotificationServlet.dispatchEvent() - Instanciation IServiceHandler : #CFCATCH.message#">
					<cfoutput>
						Exception levée à l'instantiation de IServiceHandler : #CFCATCH.message#<br>
						Details : #CFCATCH.Detail#<br><br>
					</cfoutput>
					<cfdump var="#ARGUMENTS.eventInfos#" label="Propriétés d'instanciation de l'implémentation IServiceEvent">
					<cfdump var="#CFCATCH.TagContext#" label="Tag Context">
				</cfmail>
			</cfcatch>
		</cftry>
		<cflog text="NotificationServlet.dispatchEvent() : COMPLETED in #(getTickCount() - startTick)# ms" file="#getLog()#" type="information">
	</cffunction>
</cfcomponent>