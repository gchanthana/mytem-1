<cfinterface displayName="fr.consotel.api.ibis.publisher.PublisherRequest">
	<cffunction access="public" name="selfFactoryMethod" returntype="fr.consotel.api.ibis.publisher.PublisherRequest" hint="Returns an initialized instance">
		<cfargument name="properties" type="struct" required="true" hint="Minimum properties required to build ReportRequest">
	</cffunction>
	
	<cffunction access="public" name="addParameter" returntype="boolean" hint="Adds a parameter to ReportRequest">
		<cfargument name="parameterName" type="string" required="true" hint="Parameter name">
		<cfargument name="parameterValues" type="array" required="true" hint="Parameter list of values">
	</cffunction>
	
	<cffunction access="public" name="getParameters" returntype="array" hint="Return ReportRequest parameter list">
	</cffunction>
	
	<cffunction access="public" name="getReportRequest" returntype="struct" hint="Returns ReportRequest">
	</cffunction>
	
	<cffunction access="public" name="getScheduleRequest" returntype="struct" hint="Returns ScheduleRequest">
	</cffunction>
	
	<cffunction access="public" name="getDeliveryRequest" returntype="struct" hint="Returns DeliveryRequest">
	</cffunction>

	<cffunction access="public" name="getOutputType" returntype="string" hint="Returns OUTPUT properties TYPE. Default is UNKNOWN">
	</cffunction>

	<cffunction access="public" name="getDataType" returntype="string" hint="Returns DATA properties TYPE. Default is UNKNOWN">
	</cffunction>

	<cffunction access="public" name="setReportProperties" returntype="boolean" hint="Sets report properties. TRUE if success FALSE otherwise">
		<cfargument name="properties" type="struct" required="true" hint="Report properties">
	</cffunction>

	<cffunction access="public" name="setDataProperties" returntype="boolean" hint="Sets data properties. TRUE if success FALSE otherwise">
		<cfargument name="properties" type="struct" required="true" hint="Data properties">
	</cffunction>
	
	<cffunction access="public" name="setOutputProperties" returntype="boolean" hint="Sets output properties and TYPE. TRUE if success FALSE otherwise">
		<cfargument name="properties" type="struct" required="true" hint="Output properties">
	</cffunction>
	
	<cffunction access="public" name="setExecutionProperties" returntype="boolean" hint="Sets report execution properties. TRUE if success FALSE otherwise">
		<cfargument name="properties" type="struct" required="true" hint="Execution properties">
	</cffunction>
	
	<cffunction access="public" name="setNotificationProperties" returntype="boolean" hint="Sets Notification Properties. TRUE if success FALSE otherwise">
		<cfargument name="properties" type="struct" required="true" hint="Notification properties">
	</cffunction>
	
	<cffunction access="public" name="setHistoryProperties" returntype="boolean" hint="Sets data properties. TRUE if success FALSE otherwise">
		<cfargument name="properties" type="struct" required="true" hint="Data properties">
	</cffunction>
</cfinterface>
