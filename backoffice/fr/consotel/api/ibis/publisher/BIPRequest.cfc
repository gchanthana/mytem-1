<cfcomponent displayname="fr.consotel.api.ibis.publisher.BIPRequest" extends="fr.consotel.api.ibis.publisher.RequestDecorator"
			hint="BIP Request">
	<cffunction access="public" name="selfFactoryMethod" returntype="fr.consotel.api.ibis.publisher.PublisherRequest" hint="Returns an initialized instance">
		<cfargument name="properties" type="struct" required="true" hint="Minimum properties required to build ReportRequest">
		<cfset SUPER.selfFactoryMethod(ARGUMENTS["properties"])>
		<cfreturn THIS>
	</cffunction>
</cfcomponent>