<cfcomponent displayname="fr.consotel.api.ibis.publisher.RequestDecorator" extends="fr.consotel.api.ibis.publisher.AbstractRequest">
	<cffunction access="public" name="setReportProperties" returntype="boolean" hint="Sets report properties. TRUE if success FALSE otherwise">
		<cfargument name="properties" type="struct" required="true" hint="Report properties">
		<cfset var tmpProperties=arguments["properties"]>
		<cfset var isValid=SUPER.setReportProperties(tmpProperties)>
		<cfset var reportRequest=SUPER.getReportRequest()>
		<cfif isValid EQ TRUE>
			<cfloop item="keyName" collection="#tmpProperties#">
				<cfset reportRequest[keyName]=tmpProperties[keyName]>
			</cfloop>
		</cfif>
		<cfreturn isValid>
	</cffunction>

	<cffunction access="public" name="setDataProperties" returntype="boolean" hint="Sets data properties. TRUE if success FALSE otherwise">
		<cfargument name="properties" type="struct" required="true" hint="Data properties">
		<cfset var tmpProperties=arguments["properties"]>
		<cfset var isValid=SUPER.setDataProperties(tmpProperties)>
		<!--- DEFAULT STRUCTURE TO FILL IS REPORT REQUEST --->
		<cfset var tmpStructure=SUPER.getReportRequest()>
		<cfset var dataType=getDataType()>
		<!--- FILL STRUCTURE DEPENDING ON DATA TYPE --->
		<cfif isValid EQ TRUE>
			<cfif (dataType EQ THIS["dynamicFile"]) OR (dataType EQ THIS["dynamicJdbc"])>
				<cfset tmpStructure["dynamicDataSource"]=structNew()>
				<cfset tmpStructure=tmpStructure["dynamicDataSource"]>
				<cfif dataType EQ THIS["dynamicFile"]>
					<cfset tmpStructure["fileDataSource"]=structNew()>
					<cfset tmpStructure=tmpStructure["fileDataSource"]>
				<cfelseif dataType EQ THIS["dynamicJdbc"]>
					<cfset tmpStructure["JDBCDataSource"]=structNew()>
					<cfset tmpStructure=tmpStructure["JDBCDataSource"]>
				</cfif>
			</cfif>
			<cfloop item="keyName" collection="#tmpProperties#">
				<cfset tmpStructure[keyName]=tmpProperties[keyName]>
			</cfloop>
		</cfif>
		<cfreturn isValid>
	</cffunction>
	
	<cffunction access="public" name="setOutputProperties" returntype="boolean" hint="Sets output properties. TRUE if success FALSE otherwise">
		<cfargument name="properties" type="struct" required="true" hint="Output properties">
		<!--- DUPLICATE BECAUSE GLOBAL OPTIONAL KEY ARE DELETED DURING OUTPUT PROPERTIES PROCESSING --->
		<cfset var tmpProperties=DUPLICATE(arguments["properties"])>
		<cfset var isValid=SUPER.setOutputProperties(tmpProperties)>
		<!--- DEFAULT STRUCTURE TO FILL IS DELIVERY REQUEST --->
		<cfset var tmpStructure=SUPER.getDeliveryRequest()>
		<cfset var outputType=getOutputType()>
		<!--- FILL STRUCTURE DEPENDING ON OUTPUT TYPE --->
		<cfif isValid EQ TRUE>
			<cfloop index="optionalKey" list="#THIS["OUTPUT_KEYS_OPT"]#">
				<cfif structKeyExists(tmpProperties,optionalKey)>
					<cfif optionalKey EQ "attributeFormat">
						<cfset structInsert(SUPER.getReportRequest(),optionalKey,tmpProperties[optionalKey],true)>
					<cfelseif optionalKey EQ "scheduleBurstringOption">
						<cfset structInsert(SUPER.getScheduleRequest(),optionalKey,tmpProperties[optionalKey],true)>
					<cfelseif optionalKey EQ "contentType">
						<cfset structInsert(SUPER.getDeliveryRequest(),optionalKey,tmpProperties[optionalKey],true)>
					</cfif>
					<!--- DELETE ALL GLOBAL OUTPUT OPTIONAL KEYS : Rest are SPECIFIC OUTPUT KEYS (REQ and OPT) --->
					<cfset structDelete(tmpProperties,optionalKey)>
				</cfif>
			</cfloop>
			<!--- FIRST CASE IS FOR REPORT REQUEST, THE REST FOR DELIVERY REQUEST (Default value for tmpStructure) --->
			<cfif outputType EQ THIS["runLocalType"]>
				<cfset tmpStructure=SUPER.getReportRequest()>
			<cfelseif outputType EQ THIS["localType"]>
				<cfset tmpStructure["localOption"]=structNew()>
				<cfset tmpStructure=tmpStructure["localOption"]>
			<cfelseif outputType EQ THIS["ftpType"]>
				<cfset tmpStructure["ftpOption"]=structNew()>
				<cfset tmpStructure=tmpStructure["ftpOption"]>
			<cfelseif outputType EQ THIS["emailType"]>
				<cfset tmpStructure["emailOption"]=structNew()>
				<cfset tmpStructure=tmpStructure["emailOption"]>
			</cfif>
			<cfloop item="keyName" collection="#tmpProperties#">
				<cfset tmpStructure[keyName]=tmpProperties[keyName]>
			</cfloop>
		</cfif>
		<cfreturn isValid>
	</cffunction>
	
	<cffunction access="public" name="setExecutionProperties" returntype="boolean" hint="Sets report execution properties. TRUE if success FALSE otherwise">
		<cfargument name="properties" type="struct" required="true" hint="Execution properties">
		<!--- DUPLICATE BECAUSE GLOBAL OPTIONAL KEY ARE DELETED DURING OUTPUT PROPERTIES PROCESSING --->
		<cfset var tmpProperties=DUPLICATE(arguments["properties"])>
		<cfset var isValid=SUPER.setExecutionProperties(tmpProperties)>
		<!--- DEFAULT STRUCTURE TO FILL IS REPORT REQUEST --->
		<cfset var tmpStructure=SUPER.getReportRequest()>
		<cfif structKeyExists(tmpProperties,"jobLocale")>
			<cfif structKeyExists(tmpProperties,"attributeLocale")>
				<cfif tmpProperties["attributeLocale"] NEQ arguments["properties"]["jobLocale"]>
					<cftrace category="IBIS" type="warning" text="setExecutionProperties() - jobLocale is not equal to attributeLocale">
				</cfif>
			</cfif>
			<cfset structInsert(getScheduleRequest(),"jobLocale",tmpProperties["jobLocale"],true)>
			<cfset structDelete(tmpProperties,"jobLocale")>
		</cfif>
		<cfif structKeyExists(tmpProperties,"jobCalendar")>
			<cfif structKeyExists(tmpProperties,"attributeCalendar")>
				<cfif tmpProperties["attributeCalendar"] NEQ arguments["properties"]["jobCalendar"]>
					<cftrace category="IBIS" type="warning" text="setExecutionProperties() - jobCalendar is not equal to attributeCalendar">
				</cfif>
			</cfif>
			<cfset structInsert(getScheduleRequest(),"jobCalendar",tmpProperties["jobCalendar"],true)>
			<cfset structDelete(tmpProperties,"jobCalendar")>
		</cfif>
		<cfif isValid EQ TRUE>
			<cfloop item="keyName" collection="#tmpProperties#">
				<cfset tmpStructure[keyName]=tmpProperties[keyName]>
			</cfloop>
		</cfif>
		<cfreturn isValid>
	</cffunction>
	
	<cffunction access="public" name="setNotificationProperties" returntype="boolean" hint="Sets Notification Properties. TRUE if success FALSE otherwise">
		<cfargument name="properties" type="struct" required="true" hint="Notification properties">
		<cfset var tmpProperties=arguments["properties"]>
		<cfset var isValid=SUPER.setNotificationProperties(tmpProperties)>
		<!--- DEFAULT STRUCTURE TO FILL IS SCHEDULE REQUEST --->
		<cfset var tmpStructure=SUPER.getScheduleRequest()>
		<cfif isValid EQ TRUE>
			<cfloop item="keyName" collection="#tmpProperties#">
				<cfset tmpStructure[keyName]=tmpProperties[keyName]>
			</cfloop>
		</cfif>
		<cfreturn isValid>
	</cffunction>
	
	<cffunction access="public" name="setHistoryProperties" returntype="boolean" hint="Sets data properties. TRUE if success FALSE otherwise">
		<cfargument name="properties" type="struct" required="true" hint="Data properties">
		<cfset var tmpProperties=arguments["properties"]>
		<cfset var isValid=SUPER.setHistoryProperties(tmpProperties)>
		<!--- DEFAULT STRUCTURE TO FILL IS SCHEDULE REQUEST --->
		<cfset var tmpStructure=SUPER.getScheduleRequest()>
		<cfif isValid EQ TRUE>
			<cfloop item="keyName" collection="#tmpProperties#">
				<cfset tmpStructure[keyName]=tmpProperties[keyName]>
			</cfloop>
		</cfif>
		<cfreturn isValid>
	</cffunction>
</cfcomponent>
