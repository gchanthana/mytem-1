<!--- TODO : Modifier les paramètres FTP par défaut : Serveur, Login, Pwd, Répertoire, etc... (Container) --->
<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.publisher.delivery.Mail" extends="fr.consotel.api.ibis.publisher.delivery.Email"
hint="Stratégie identifiée par le type Mail permettant d'effectuer les opérations suivantes :
- Générer un reporting dans le répertoire FTP de l'utilisateur container sur pelican.consotel.fr
- Diffuser le reporting généré par mail en dispatchant un évènement de type : fr.consotel.api.ibis.publisher.handler.event.EmailEvent
Lorsque la propriété TYPE est Mail alors :
- Les propriétés requises par la stratégie FTP doivent etre retournées en plus par la méthode getDeliveryProperties()
- Les propriétés suivantes sont ignorées : FTP_SERVER, FTP_USER, FTP_PWD, SFTP, FTP_DIR
- Le serveur FTP utilisé est celui qui est utilisé par la classe parent (container:pelican.consotel.fr)
Lorsque la propriété DIFFUSION est Mail alors :
- Les propriétés requises par la stratégie Email doivent etre retournées en plus par la méthode getDeliveryProperties()
- Les propriétés suivantes sont ignorées : MAIL_SERVER, MAIL_USER, MAIL_PWD
- Le serveur Mail utilisé par celui qui est configuré par défaut avec le BackOffice
Cette implémentation ne vérifie pas la cohérence entre le nom de domaine du serveur mail et ceux utilisés pour les adresses mail
L'implémentation IDeliveryEvent dispatché par cette implémentation est :
- fr.consotel.api.ibis.publisher.handler.event.MailEvent si l'évènement passé à handleReportingEvent() est de type IDeliveryEvent
- fr.consotel.api.ibis.publisher.handler.event.FtpEvent si l'évènement passé à handleReportingEvent() est de type IDeliveryEvent">
	<cffunction access="public" name="createInstance" returntype="fr.consotel.api.ibis.publisher.handler.IDelivery">
		<!--- Constructeur parent --->
		<cfset SUPER.createInstance()>
		<!--- Propriétés FTP spécifiques à cette implémentation (Propriétés par défaut de la classe parent) --->
		
		<cfset structAppend(VARIABLES.FTP,{USER="container", PWD="container", SFTP=FALSE, DIR=""},TRUE)>
		
		<!---
		<cfset structAppend(VARIABLES.FTP,{
			SERVER="sun-bi.consotel.fr", USER="oracle", PWD="tred78", SFTP=FALSE, PATH_SEP="/", EXT_SEP=".", DIR=""
		},TRUE)>
		--->
		<cfreturn THIS>
	</cffunction>

	<cffunction access="public" name="getDeliveryProperties" returntype="Struct" hint="Effectue dans l'ordre les opérations suivantes :
	Récupère la structure retournée par la méthode de la classe parent et ajoute les propriétés suivantes avant de la retourner :
	- FILE_NAME : Valeur retournée par CONTAINER.GETUuid()
	- CODE_APP : La valeur fournie par le reporting. Par défaut la valeur retournée par getCodeApp() si elle n'est pas fournie
	- PROCESS_ID : La valeur retournée par getProcessId()
	Une exception est levée si la propriété CONTAINER n'est pas présente dans la structure retournée par reporting.getProperties()">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting" required="true">
		<cfset var reportingImpl=ARGUMENTS["reporting"]>
		<cfset var reportingProps=reportingImpl.getProperties()>
		<cfset var deliveryProperties=SUPER.getDeliveryProperties(reportingImpl)>
		<!--- Remplace la valeur de la propriété FILE_NAME par la valeur retournée par l'UUID provenant de getDeliveryRequest() --->
		<cfset deliveryProperties["FILE_NAME"]=reportingProps["UUID"]>
		<cfreturn deliveryProperties>
	</cffunction>
	
	<cffunction access="public" name="getDeliveryRequest" returntype="Struct" hint="Effectue dans l'ordre les opérations suivantes :
	- Insère une propriété CONTAINER dans reporting.getProperties()
	- Effectue les traitements container requis avant l'exécution du reporting avec CONTAINER et les propriétés getDeliveryProperties()
	- Stocke les propriétés et paramètres du reporting qui sont liés au module container en utilisant storeContainerProperties()
	- Récupère la structure retournée par la méthode de la classe parent avec le paramètre reporting
	- Remplace le nom du fichier FTP contenu dans cette structure par la valeur retournée par ExtensionApiContainerV1.GETUuid()
	- Remplace le JOBNAME par la concaténation Container : UUID avec UUID la valeur retournée par ExtensionApiContainerV1.GETUuid()
	- Retourne la structure ainsi modifiée
	La valeur associée à CONTAINER est créée à l'appel de cette méthode et sont type est retourné par la méthode getContainerClass()
	L'implémentation de ce type qui est utilisée par cette classe est retournée par getContainerClass()
	Une exception est levée si une propriété CONTAINER est présente dans la structure retournée par reporting.getProperties()">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting" required="true">
		<cfset var reportingImpl=ARGUMENTS["reporting"]>
		<cfset var reportingProperties=reportingImpl.getProperties()>
		<!--- Ajout d'un UUID unique dans les propriétés du reporting --->
		<cfset reportingProperties["UUID"]=createUUID()>
		<cfset var scheduleRequest=SUPER.getDeliveryRequest(reportingImpl)>
		<!--- MAJ de la structure ScheduleRequest : Remplace le JOBNAME et le nom du fichier FTP --->
		<cfset scheduleRequest.deliveryRequest.ftpOption.remoteFile=reportingProperties["UUID"]>
		<cfset logInfo("Mail Remote FileName : " & scheduleRequest.deliveryRequest.ftpOption.remoteFile)>
		<cfreturn scheduleRequest>
	</cffunction>

</cfcomponent>