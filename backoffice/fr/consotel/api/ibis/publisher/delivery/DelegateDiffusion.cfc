<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.publisher.delivery.DelegateDiffusion" extends="fr.consotel.api.ibis.publisher.delivery.Delivery"
hint="Stratégie identifiée par le type DelegateDiffusion permettant de déléguer le traitement handleReportingEvent() à une autre stratégie
Cette délégation est effectué par un appel à la méthode dispatchReportingEvent() de l'implémentation retournée par event.getReportingService()
L'évènement qui est dispatché par cette implémentation doit etre de type fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent
Il est créé à partir de celui qui est fourni à handleReportingEvent() et son  dispatch n'est effectué que si les conditions suivantes sont vérifiées :
- La valeur retournée par la méthode getDeliveryType() de l'évènement qui est créé n'est pas une chaine vide
- La valeur retournée par la méthode getDeliveryType() de l'évènement qui est créé est différente de celle de event.getDeliveryType()
Ces conditions permettent respectivement de définir un état final et de prévenir un référencement CIRCULAIRE sur la meme stratégie
Lorsque les conditions du dispatch sont vérifées alors une propriété DIFFUSION fournie par le reporting doit indiquer le type de la stratégie de délégation
La méthode getDeliveryProperties() de cette classe retourne les propriétés de la méthode parent et celles retournées par reporting.getProperties()
L'implémentation IDeliveryEvent dispatché par cette implémentation est : fr.consotel.api.ibis.publisher.handler.event.JobEvent">
	<!--- fr.consotel.api.ibis.publisher.handler.IDelivery --->
	<cffunction access="public" name="getDeliveryProperties" returntype="Struct"
	hint="Récupère les propriétés retournées par la méthode de la classe parent et la modifie avant de la retourner
	Ajoute une copie de toutes les propriétés fournies par le reporting à la stratégie de diffusion : reporting.getProperties().DELIVERY
	Les valeurs des propriétés provenant de la méthode de la classe parent ne sont pas écrasées">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting" required="true" hint="Reporting à exécuter">
		<cfset var reportingImpl=ARGUMENTS["reporting"]>
		<cfset var deliveryProperties=SUPER.getDeliveryProperties(reportingImpl)>
		<!--- Ajout des propriétés fournies par le reporting à la stratégie de diffusion (i.e : Contenu de la clé DELIVERY) --->
		<cfset structAppend(deliveryProperties,DUPLICATE(reportingImpl.getProperties().DELIVERY),FALSE)>
		<cfreturn deliveryProperties>
	</cffunction>
	
	<cffunction access="public" name="handleReportingEvent" returntype="void"
	hint="Appelle la méthode de la classe parent et dispatche un évènement de type : fr.consotel.api.ibis.publisher.handler.event.JobEvent
	L'évènement est dispatché que si les conditions suivantes sont vérifiées :
	- La valeur retournée par JobEvent.getDeliveryType() n'est pas une chaine vide
	- La valeur retournée par JobEvent.getDeliveryType() est différente de celle de event.getDeliveryType()
	Un handler interne non implémenté est appelé avec l'évènement JobEvent en paramètre lorsqu'une de ces conditions n'est pas vérifiée">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent" required="true">
		<cfset var reportingEvent=ARGUMENTS["event"]>
		<cfset SUPER.handleReportingEvent(reportingEvent)>
		<!--- Dispatche un évènement de type IDeliveryEvent --->
		<cfset var deliveryEvent=getDeliveryEvent(reportingEvent)>
		<cfset dispatchDeliveryEvent(deliveryEvent)>
	</cffunction>

	<!--- fr.consotel.api.ibis.publisher.delivery.DelegateDiffusion --->
	<cffunction access="public" name="dispatchEventType" returntype="String" hint="Retourne le type IDeliveryEvent dispatché par cette stratégie
	Le type retourné par cette implémentation est : fr.consotel.api.ibis.publisher.handler.event.JobEvent">
		<cfreturn "fr.consotel.api.ibis.publisher.handler.event.JobEvent">
	</cffunction>
	
	<cffunction access="private" name="deliveryEventType" returntype="String" hint="Retourne le type IDeliveryEvent retourné par getDeliveryEvent()
	Le type retourné par cette implémentation est : fr.consotel.api.ibis.publisher.handler.event.JobEvent">
		<cfreturn "fr.consotel.api.ibis.publisher.handler.event.JobEvent">
	</cffunction>
	
	<cffunction access="private" name="getDeliveryEvent" returntype="fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent"
	hint="Retourne une instance IDeliveryEvent event permettant de récupérer les informations concernant le contenu du reporting
	Le type de l'instance retourné par cette méthode est la valeur retournée par la méthode deliveryEventType()">
		<cfargument name="sourceEvent" type="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent"
		required="true" hint="Evènement qui a été passé en paramètre à la méthode handleReportingEvent()">
		<cfreturn createObject("component",deliveryEventType()).createDeliveryEvent(ARGUMENTS["sourceEvent"])>
	</cffunction>
	
	<cffunction access="private" name="dispatchDeliveryEvent" returntype="void" hint="Dispatche l'évènement event
	Cet évènement est dispatché avec la méthode dispatchReportingEvent() de l'instance retournée par event.getReportingService()
	Le dispatch passe indirectement event en paramètre à la méthode handleReportingEvent() de la stratégie dont le type est event.getDeliveryType() 
	Il est effectué si les conditions suivantes sont vérifiées :
	- La valeur retournée par event.getDeliveryType() n'est pas une chaine vide
	- La valeur retournée par event.getDeliveryType() est différente du type identifiant cette stratégie (i.e : DelegateDiffusion pour cette classe)
	Un handler interne non implémenté est appelé avec l'évènement JobEvent en paramètre lorsqu'une de ces conditions n'est pas vérifiée">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent" required="true"
		hint="Evènement à dispatcher dont le type est égal à la valeur retournée par la méthode dispatchEventType()">
		<cfset var deliveryEvent=ARGUMENTS["event"]>
		<cfset var eventClassShortName=listLast(getMetadata(deliveryEvent).NAME,".")>
		<cfset var dispatcher=listLast(getMetadata(THIS).NAME,".")>
		<cfset var target=deliveryEvent.getDeliveryType()>
		<!--- Vérifie les conditions requises pour le dispatch de l'évènement IDeliveryEvent (targetEvent) --->
		<cfif (TRIM(dispatcher) NEQ "") AND compareNoCase(dispatcher,target)>
			<!--- Dispatch de l'évènement IDeliveryEvent vers l'implémentation IReportingService --->
			<cfset var reportingService=deliveryEvent.getReportingService()>
			<cfset logInfo("Strategy " & dispatcher & " dispatch " & eventClassShortName & " to " & target)>
			<cfset reportingService.dispatchReportingEvent(deliveryEvent)>
		<!--- Si les conditions du dispatch de l'évènement de type IDeliveryEvent ne sont pas vérifiées --->
		<cfelse>
			<cfset logInfo("Strategy " & dispatcher & " no dispatch " & eventClassShortName & " to " & target)>
			<cfset onDispatchEventMissed(deliveryEvent)>
		</cfif>
	</cffunction>

	<cffunction access="private" name="onDispatchEventMissed" returntype="void"
	hint="Handler appelé par dispatchDelegateEvent() lorsque les conditions de dispatch ne sont pas vérifiées
	L'évènement event est l'évènement de type fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent créé et vérifie une des conditions suivantes :
	- La valeur retournée par IDeliveryEvent.getDeliveryType() est une chaine vide
	- La valeur retournée par IDeliveryEvent.getDeliveryType() est égale de celle de event.getDeliveryType()
	Cette méthode n'est pas implémentée et afiche un message de log contenant le type de cette stratégie">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent" required="true"
		hint="Evènement DelegateEvent qui a été créé dans handleReportingEvent() et qui ne satisfait pas les conditions requises pour son dispatch">
		<cfset logInfo(listLast(getMetadata(THIS).NAME,".") & ".onDispatchEventMissed() : NOTHING TO DO (End of handling)")>
	</cffunction>
</cfcomponent>