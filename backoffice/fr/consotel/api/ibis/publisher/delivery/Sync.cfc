<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.publisher.delivery.Sync" extends="fr.consotel.api.ibis.publisher.delivery.DelegateDiffusion"
hint="Stratégie identifiée par le type Sync permettant de rendre synchrone l'exécution du reporting et le traitement correspondant par cette stratégie de diffusion
Cela signifie que la méthode execute() de l'implémentation IReportingService ne rend la main qu'à la fin du traitement de la stratégie de diffusion de diffusion
Cette stratégie ne peut pas etre utilisée pour effectuer le traitement d'un reporting généré par une autre
L'implémentation IDeliveryEvent dispatché par cette stratégie est : fr.consotel.api.ibis.publisher.handler.event.SyncEvent">
	<!--- fr.consotel.api.ibis.publisher.handler.IDelivery --->
	<cffunction access="public" name="getDeliveryRequest" returntype="Struct"
	hint="Récupère la structure retournée par la méthode de la classe parent et supprime la clé deliveryRequest. Retourne la structure ainsi modifiée">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true">
		<cfset var reportingImpl=ARGUMENTS["reporting"]>
		<cfset var scheduleRequest=SUPER.getDeliveryRequest(reportingImpl)>
		<cfif structKeyExists(scheduleRequest,"deliveryRequest")>
			<cfset structDelete(scheduleRequest,"deliveryRequest")>
		</cfif>
		<cfreturn scheduleRequest>
	</cffunction>
	
	<cffunction access="public" name="handleReportingEvent" returntype="void"
	hint="Si event est de type IDeliveryEvent : Une exception est lévée car cette stratégie ne peut pas traiter les reporting générés avec d'autres
	Sinon event est passé à la méthode de la classe parent pour dispatcher un évènement de type fr.consotel.api.ibis.publisher.handler.event.SyncEvent">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent"  required="true">
		<cfset var reportingEvent=ARGUMENTS["event"]>
		<!--- Validation du type de l'évènement. Une exception est levée si son type n'est pas validé --->
		<cfif isInstanceOf(reportingEvent,"fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent")>
			<cfthrow attributecollection="#getErrorProperties()#" detail="Implémentation de la stratégie : #getMetadata(THIS).NAME#"
				message="Cette stratégie ne peut pas etre utilisée pour effectuer le traitement d'un reporting généré par une autre">
		<cfelse>
			<cfset SUPER.handleReportingEvent(reportingEvent)>
		</cfif>
	</cffunction>

	<!--- fr.consotel.api.ibis.publisher.delivery.DelegateDiffusion --->
	<cffunction access="public" name="dispatchEventType" returntype="String" hint="fr.consotel.api.ibis.publisher.handler.event.SyncEvent">
		<cfreturn "fr.consotel.api.ibis.publisher.handler.event.SyncEvent">
	</cffunction>
	
	<cffunction access="private" name="deliveryEventType" returntype="String" hint="fr.consotel.api.ibis.publisher.handler.event.SyncEvent">
		<cfreturn "fr.consotel.api.ibis.publisher.handler.event.SyncEvent">
	</cffunction>
</cfcomponent>