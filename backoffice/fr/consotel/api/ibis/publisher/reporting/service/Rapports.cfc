<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.publisher.reporting.service.Rapports" extends="fr.consotel.api.ibis.publisher.reporting.impl.ReportingService"
hint="Implémentation permettant d'exécuter un reporting pour etre généré dans le container d'un utilisateur ConsoView
Le reporting utilisé doit etre obtenu à partir de la méthode createReporting() ou fournir les memes propriétés que celui qu'elle retourne
Les propriétés suivantes doivent etre fournies par à la méthode createReporting() :
- RAPPORT : Structure contenant les propriétés liées au rapport
- RAPPORT.ID : Un identifiant permettant d'identifier le rapport BIP correspondant
- RAPPORT.USER_ID : Identifiant d'un utilisateur ConsoView qui dispose d'un accès au module container
- RAPPORT.IDRACINE : Identifiant d'un groupe utilisé pour les procédures stockées container
Les propriétés suivantes sont facultatives pour la méthode createReporting() :
- DESTINATION : Nom du serveur de notification HTTP prédéfini dans BIP qui sera utilisé pour la planification
(Par défaut : Celui qui est utilisé par la stratégie de diffusion)
- RAPPORT.TEMPLATE : Nom du template du rapport BIP (Par défaut : Chaine vide)
- RAPPORT.FORMAT : Valeur du format de sortie pour le rapport (Par défaut : xml)
- RAPPORT.LOCALIZATION : Localisation utilisée pour l'exécution du rapport (Par défaut : fr_FR)
- RAPPORT.CODE_APPLICATION : CodeApp utilisé pour les procédures stockées container (Par défaut : 1)
Le format de la localisation est xx_XX avec xx le code langue et XX le code pays (e.g : en_US)
- PARAMETRE : Structure contenant les paramètres spécifiques au rapport BIP correspondant à RAPPORT.ID
Chaque clé correspond au nom du paramètre asscocié à une structure contenant les clés suivantes :
- VALEUR : Tableau contenant les valeurs du paramètre (Doivent etre de type simple : String, Numeric, Boolean)
- IS_SYSTEM : Boolean qui vaut TRUE si le paramètre est visible dans le module container et FALSE sinon
- LIBELLE : Libellé sous lequel le paramètre sera visible dans le module container
Tous les paramètres sont envoyés à l'api container pour etre stockés en base (Soit avec IS_SYSTEM à TRUE ou FALSE)
Le type de la stratégie de diffusion utilisée par cette implémentation est : Container">
	<!--- fr.consotel.api.ibis.publisher.reporting.IReportingService --->
	<cffunction access="public" name="execute" returntype="void" hint="Exécute le reporting">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true" hint="Reporting à exécuter">
		<cfreturn SUPER.execute(ARGUMENTS["reporting"])>
	</cffunction>
	
	<cffunction access="public" name="createReporting" returntype="fr.consotel.api.ibis.publisher.reporting.IReporting"
	hint="Retourne une instance de l'implémentation de IReporting adaptée à cette implémentation en utilisant createReportingProperties()
	La structure retournée contient les propriétés et valeurs suivantes :
	- bipReport : Structure contenant les propriétés du reporting
	- bipReport.reporting : Rapports Container
	- bipReport.xdoTemplateId : Valeur de la propriété props.RAPPORT.TEMPLATE ou une chaine vide si elle n'est pas fournie
	- bipReport.outputFormat : Valeur de la propriété props.RAPPORT.FORMAT ou xml si elle n'est pas fournie
	- bipReport.localization : Cette propriété est ajoutée avec la valeur de la clé props.RAPPORT.LOCALIZATION si elle est fournie
	- DELIVERY : Structure contenant les propriétés de la stratégie de diffusion Container
	- DELIVERY.TYPE : Valeur retournée par getServiceDeliveryType()
	- DELIVERY.USERID : Valeur de la propriété props.RAPPORT.USER_ID
	- DELIVERY.IDRACINE : Valeur de la propriété props.RAPPORT.IDRACINE
	- DELIVERY.CODE_APP : Cette propriété est ajoutée avec la valeur de la clé props.RAPPORT.CODE_APPLICATION si elle est fournie
	- DELIVERY.FILE_EXT : Valeur de la propriété props.RAPPORT.USER_ID
	- DELIVERY.RAPPORT_ID : Valeur de la propriété props.RAPPORT.ID
	Une exception est levée si les propriétés fournies dans props ne sont pas validées (e.g : Une propriété requise est manquante)">
		<cfargument name="props" type="Struct"  required="true" hint="Propriétés du reporting">
		<cfset var clientProperties=ARGUMENTS["props"]>
		<cfset var reportingProperties={}>
		<cftry>
			<cfset validateReportingProperties(clientProperties)>
			<cfset reportingProperties=createReportingProperties(clientProperties)>
			<!--- Propriétés du reporting--->
			<cfset reportingProperties.bipReport.reporting="Rapports Container">
			<cfif structKeyExists(clientProperties.RAPPORT,"TEMPLATE")>
				<cfif isValid("String",clientProperties.RAPPORT.TEMPLATE)>
					<cfset reportingProperties.bipReport.xdoTemplateId=clientProperties.RAPPORT["TEMPLATE"]>
				<cfelse>
					<cfthrow attributecollection="#getErrorProperties()#"
						message="La valeur associée à la propriété RAPPORT.TEMPLATE doit etre de type STRING">
				</cfif>
			</cfif>
			<cfif structKeyExists(clientProperties.RAPPORT,"FORMAT")>
				<cfif isValid("String",clientProperties.RAPPORT.FORMAT)>
					<cfset reportingProperties.bipReport.outputFormat=clientProperties.RAPPORT["FORMAT"]>
				<cfelse>
					<cfthrow attributecollection="#getErrorProperties()#"
						message="La valeur associée à la propriété RAPPORT.FORMAT doit etre de type STRING">
				</cfif>
			</cfif>
			<cfif structKeyExists(clientProperties.RAPPORT,"LOCALIZATION")>
				<cfif isValid("String",clientProperties.RAPPORT.LOCALIZATION)>
					<cfset reportingProperties.bipReport.localization=clientProperties.RAPPORT["LOCALIZATION"]>
				<cfelse>
					<cfthrow attributecollection="#getErrorProperties()#"
						message="La valeur associée à la propriété RAPPORT.LOCALIZATION doit etre de type STRING">
				</cfif>
			</cfif>
			<!--- Propriétés de la stratégie de diffusion --->
			<cfset structAppend(reportingProperties.DELIVERY,{
				TYPE=getServiceDeliveryType(), USERID=clientProperties.RAPPORT["USER_ID"], RAPPORT_ID=clientProperties.RAPPORT["ID"],
				IDRACINE=clientProperties.RAPPORT["IDRACINE"], FILE_EXT=clientProperties.RAPPORT["FILE_EXT"]
			},TRUE)>
			<cfif structKeyExists(clientProperties.RAPPORT,"CODE_APPLICATION")>
				<cfset reportingProperties.DELIVERY.CODE_APP=clientProperties.RAPPORT["CODE_APPLICATION"]>
			</cfif>
			<!--- Propriétés d'exécution de l'implémentation IReportingService --->
			<cfif structKeyExists(clientProperties,"DESTINATION")>
				<cfif isValid("String",clientProperties.DESTINATION)>
					<cfset reportingProperties.DESTINATION=clientProperties["DESTINATION"]>
				<cfelse>
					<cfthrow attributecollection="#getErrorProperties()#"
						message="La valeur associée à la propriété DESTINATION doit etre de type STRING">
				</cfif>
			</cfif>
			<cfreturn SUPER.createReporting(reportingProperties)>
			<cfcatch type="any">
				<cfset logError("Reporting Instanciation FAILED : " & CFCATCH.message)>
				<cfset handleException(CFCATCH,"Instanciation du reporting",reportingProperties)>
				<!--- Redéclenche l'exception capturée --->
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!--- fr.consotel.api.ibis.publisher.reporting.service.Rapports --->
	<cffunction access="public" name="getServiceDeliveryType" returntype="String"
	hint="Retourne le type de la stratégie de diffusion qui est utilisée par défaut par cette implémentation (Container)">
		<cfreturn "Container">
	</cffunction>

	<cffunction access="private" name="extractReportingDefinition" returntype="Query"
	hint="Retourne une requete qui identifie le XDO correspondant au reporting
	La requete retournée contient un enregistrement avec les colonnes suivantes :
	- DOSSIER : Chemin absolu du dossier contenant le XDO correspondant au reporting
	- XDO : Nom du XDO correspondant au reporting
	- FILE_NAME : Nom sous lequel le fichier généré sera visible dans le module container
	Une exception est levée si la requete ne retourne pas exactement un et un seul enregistrement
	Cette méthode n'est pas implémentée et ne ramène aucun enregistrement">
		<cfargument name="props" type="Struct"  required="true" hint="Propriétés provenant de createReportingProperties()">
		<cfset var clientProperties=ARGUMENTS["props"]>
		<cfset var reportId=clientProperties["RAPPORT"]["ID"]>
		<cfset var code_langue=clientProperties["RAPPORT"]["LOCALIZATION"]>
		<cfset var periode="N.D">
		<cfset var perimetre="N.D">
		<!--- Source de données par défaut --->
		<cfset var datasource="ROCOFFRE">
		<cfif structKeyExists(clientProperties["RAPPORT"],"DATASOURCE")>
			<cfset datasource=clientProperties["RAPPORT"]["DATASOURCE"]>
		</cfif>
		<!--- Paramètres obligatoires (Ticket 5698) : Libellé de la période (L_PERIMETRE) et Libellé du périmètre (L_PERIODE) --->
		<cfif structKeyExists(clientProperties["PARAMETRE"],"L_PERIODE")>
			<cfset periode=clientProperties["PARAMETRE"]["L_PERIODE"]["VALEUR"][1]>
		<cfelse>
			<cfthrow attributecollection="#getErrorProperties()#"
				message="Le paramètre L_PERIODE est obligatoire et n'a pas été fourni"
				detail="Le paramètre L_PERIODE est obligatoire et est utilisé pour le libellé dans le module container">
		</cfif>
		<cfif structKeyExists(clientProperties["PARAMETRE"],"L_PERIMETRE")>
			<cfset perimetre=clientProperties["PARAMETRE"]["L_PERIMETRE"]["VALEUR"][1]>
		<cfelse>
			<cfthrow attributecollection="#getErrorProperties()#"
				message="Le paramètre L_PERIMETRE est obligatoire et n'a pas été fourni"
				detail="Le paramètre L_PERIMETRE est obligatoire et est utilisé pour le libellé dans le module container">
		</cfif>
		<!--- Colonnes : DOSSIER, XDO, FILE_NAME (Ticket 5698), LIBELLE_IHM (Ticket 5698) --->
		<cfstoredproc datasource="#datasource#" procedure="PKG_M331.INFO_RAPPORT">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#reportId#" 	null="false" variable="p_idrapport">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#periode#" 	null="false" variable="p_L_PERIODE">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#perimetre#" 	null="false" variable="p_L_PERIMETRE">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#code_langue#" 	null="false" variable="p_code_langue">
			<cfprocresult name="qReportingDefinition">
		</cfstoredproc>
		<cfif qReportingDefinition.RECORDCOUNT NEQ 1>
			<cfthrow attributecollection="#getErrorProperties()#"
				message="La définition du rapport correspond à l'identifiant #reportId# est introuvable ou est invalide"
				detail="La définition du rapport est introuvable ou invalide (e.g : Plusieurs enregistrements trouvés)">
		</cfif>
		<cfreturn qReportingDefinition>
	</cffunction>
	
	<cffunction access="private" name="extractReportingParameters" returntype="Query"
	hint="Retourne une requete qui identifie les paramètres supplémentaires spécifiques au XDO correspondant au reporting
	Ces paramètres correspondent à ceux dont la valeur est prédéfinie et est spécifique au reporting
	La requete retournée contient autant d'enregistrement que de paramètres supplémentaires avec les colonnes suivantes :
	- NOM : Nom du paramètre tel qu'il est défini dans le XDO
	- VALEURS : Liste des valeurs du paramètre
	Cette méthode n'est pas implémentée et ne ramène aucun enregistrement">
		<cfargument name="props" type="Struct"  required="true" hint="Propriétés provenant de createReportingProperties()">
		<cfreturn queryNew("NOM,VALEURS","VARCHAR,VARCHAR")>
	</cffunction>

	<cffunction access="private" name="createReportingProperties" returntype="Struct"
	hint="Retourne une structure adaptée à la méthode createReporting() de la classe parent à partir des propriétés props
	La valeur propriétés et des paramètres sont récupérées à partir des méthodes : extractReportingDefinition() et extractReportingParameters()
	La structure retournée contient les propriétés et valeurs suivantes :
	- bipReport : Structure contenant les propriétés du reporting
	- bipReport.xdoAbsolutePath : Chemin absolu du XDO construit à partir des propriétés DOSSIER et XDO
	- bipReport.xdoTemplateId : Chaine vide
	- bipReport.outputFormat : La valeur xml
	- bipReport.reportParameters : Paramètres fournis dans la propriété props.PARAMETRE et par extractReportingParameters()
	- DELIVERY : Structure contenant les propriétés de la stratégie de diffusion Container
	- DELIVERY.FICHIER : Valeur de la colonne XXX (Ticket 5698) provenant de la requete retournée par extractReportingDefinition()
	- DELIVERY.FILE_EXT : Valeur de la colonne FILE_EXT provenant de la requete retournée par extractReportingDefinition() 
	Cette méthode n'est pas implémentée. Des valeurs par défaut sont utilisée pour les propriétés suivantes retournées par extractReportingDefinition() :
	DOSSIER, XDO, FILE_NAME, FILE_EXT">
		<cfargument name="props" type="Struct"  required="true" hint="Propriétés provenant de createReporting()">
		<cfset var clientProperties=ARGUMENTS["props"]>
		<cfset var clientParams=clientProperties["PARAMETRE"]>
		<cfset var reportingDefinition=extractReportingDefinition(clientProperties)>
		<cfset var reportingParameters=extractReportingParameters(clientProperties)>
		<!--- Propriétés du reporting --->
		<cfset var reportDir=reportingDefinition["DOSSIER"][1]>
		<cfset var reportXdo=reportingDefinition["XDO"][1]>
		<cfset var reportingProperties = {
			bipReport = {
				xdoAbsolutePath=reportDir & "/" & reportXdo & "/" & reportXdo & ".xdo", xdoTemplateId="", outputFormat="xml",
				reportParameters = {}
			},
			DELIVERY = {FICHIER="Libellé container"}
		}>
		<!--- Libellé visible dans le module container (Ticket 5698) --->
		<cfset reportingProperties.DELIVERY["FICHIER"]=reportingDefinition["LIBELLE_IHM"][1]>
		<!--- SHORT_NAME container (Ticket 5698) --->
		<cfset reportingProperties.DELIVERY["SHORT_NAME"]=reportingDefinition["FILE_NAME"][1]>
		<!--- Ajout des paramètres du reporting --->
		<cfloop item="paramItem" collection="#clientParams#">
			<cfset var paramValues=clientParams[paramItem]["VALEUR"]>
			<cfif isValid("Array",paramValues)>
				<cfset structInsert(reportingProperties.bipReport.reportParameters,paramItem,{VALUES=paramValues},TRUE)>
				<cfif structKeyExists(clientParams[paramItem],"LIBELLE") AND structKeyExists(clientParams[paramItem],"IS_SYSTEM")>
					<cfset var paramLabel=clientParams[paramItem]["LIBELLE"]>
					<cfset var paramIsSystem=yesNoFormat(clientParams[paramItem]["IS_SYSTEM"])>
					<cfset reportingProperties.bipReport.reportParameters[paramItem]["LABEL"]=paramLabel>
					<cfset reportingProperties.bipReport.reportParameters[paramItem]["VISIBLE"]=(NOT paramIsSystem)>
				</cfif>
			<cfelse>
				<cfthrow attributecollection="#getErrorProperties()#"
					message="La valeur associée à la propriété VALEUR du paramètre #paramItem# doit etre de type ARRAY">
			</cfif>
		</cfloop>
		<!--- Ajout des paramètres supplémentaires --->
		<cfloop index="i" from="1" to="#reportingParameters.RECORDCOUNT#">
			<cfset var paramValues=listToArray(clientParams[paramItem]["VALEURS"],"|",TRUE)>
			<cfset structInsert(reportingProperties.bipReport.reportParameters,reportingParameters["NOM"][i],{VALUES=paramValues},TRUE)>
		</cfloop>
		<cfreturn reportingProperties>
	</cffunction>
	
	<cffunction access="private" name="validateReportingProperties" returntype="void"
	hint="Valide les propriétés fournies pour créer une implémentation IReporting avec la méthode createReporting()
	Une exception est levée si une des conditions suivantes est vérifiée :
	- Une des propriétés suivantes n'est pas présente dans props : RAPPORT, PARAMETRE
	- Une des propriétés suivantes n'est pas présente dans props.RAPPORT : ID, USER_ID, IDRACINE, FILE_EXT">
		<cfargument name="props" type="Struct"  required="true" hint="Propriétés du reporting">
		<cfset var clientProperties=ARGUMENTS["props"]>
		<cfset var clientRequiredKeys="RAPPORT,PARAMETRE">
		<cfset var rapportRequiredKeys="ID,USER_ID,IDRACINE,FILE_EXT">
		<cfloop index="key" list="#clientRequiredKeys#">
			<cfif NOT structKeyExists(clientProperties,key)>
				<cfthrow attributecollection="#getErrorProperties()#"
					message="La propriété obligatoire #key# n'a pas été fournie dans les propriétés de création du reporting">
			</cfif>
		</cfloop>
		<cfloop index="key" list="#rapportRequiredKeys#">
			<cfif NOT structKeyExists(clientProperties["RAPPORT"],key)>
				<cfthrow attributecollection="#getErrorProperties()#"
					message="La propriété obligatoire #key# n'a pas été fournie sous la propriété RAPPORT">
			</cfif>
		</cfloop>
	</cffunction>
</cfcomponent>