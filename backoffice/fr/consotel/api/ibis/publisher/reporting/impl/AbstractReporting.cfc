<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.publisher.reporting.impl.AbstractReporting"
implements="fr.consotel.api.ibis.publisher.reporting.IReporting" hint="Implémentation par défaut de l'interface IReporting">
	<cffunction access="public" name="getReportingName" returntype="String" hint="Retourne la valeur de la clé bipReport.reporting fournie dans le propriétés">
		<cfreturn getProperties().bipReport.reporting>
	</cffunction>

	<cffunction access="public" name="getDeliveryType" returntype="String"
	hint="Retourne la valeur de la clé DELIVERY.TYPE si elle est fournie dans le propriétés du reporting et une chaine vide sinon">
		<cfset var reportingProps=getProperties()>
		<cfif structKeyExists(reportingProps,"DELIVERY") AND structKeyExists(reportingProps.DELIVERY,"TYPE")>
			<cfreturn reportingProps.DELIVERY.TYPE>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>

	<cffunction access="public" name="getProperties" returntype="Struct" hint="Retourne la structure contenant les valeurs des propriétés et paramètres (Eventuellement modifiées)
	Une structure vide est retournée si les propriétés et paramètres ne sont pas définis">
		<cfif isDefined("VARIABLES.PROPS")>
			<cfreturn VARIABLES.PROPS>
		<cfelse>
			<cfreturn {}>
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="createReporting" returntype="fr.consotel.api.ibis.publisher.reporting.IReporting" hint="Retourne une nouvelle instance">
		<cfargument name="props" type="Struct" required="true" hint="Le contenu de cette structure est fonction de l'implémentation">
		<cfif NOT isDefined("VARIABLES.PROPS")>
			<cfset VARIABLES.PROPS=ARGUMENTS.props>
		</cfif>
		<cfreturn THIS>
	</cffunction>
</cfcomponent>