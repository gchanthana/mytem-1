<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.publisher.reporting.impl.RequestService"
extends="fr.consotel.api.ibis.publisher.reporting.impl.ReportingLogging" hint="Implémentation contenant les méthodes permettant :
- L'accès au Web Service BI Publisher : Définition des propriétés et instantiation
- Construire l'objet ScheduleRequest pour exécuter ou planifier le rapport à partir de la méthode getDeliveryRequest() de la stratégie diffusion">
	<cffunction access="public" name="getService" returntype="fr.consotel.api.ibis.publisher.reporting.IReportingService"
	hint="Appele la méthode de la classe parent et met à jour les credentials">
		<cfargument name="user" type="string"  required="true" hint="Login utilisateur du service">
		<cfargument name="pwd" type="string"  required="true" hint="Mot de passe utilisateur du service">
		<cfset SUPER.getService(ARGUMENTS.user,ARGUMENTS.pwd)>
		<cfset logInfo("Reporting Service : " & listLast(getMetadata(THIS).NAME,".") & " [" & getBipProperties().DNS & "]")>
		<!--- Défini les propriétés du service puis les credentials --->
		<cfset setServiceCredentials(ARGUMENTS.user,ARGUMENTS.pwd)>
		<cfreturn THIS>
	</cffunction>

	<cffunction access="private" name="buildScheduleRequest" returntype="Struct" hint="Construit et retourne la structure ScheduleRequest correspondante.
	Récupère la structure ScheduleRequest retournée par delivery.getDeliveryRequest() et la modifie comme suit :
		- Avec la méthode setParametersValues() y ajoute les paramètres du XDO avec les valeurs provenant de deliveryStrategy.getReportingParameters()
		- Avec la méthode addProperties() y ajoute les propriétés retournées par deliveryStrategy.getDeliveryProperties() 
		- Avec la méthode addProperties() y ajoute les propriétés ScheduleRequest : attributeTemplate, attributeFormat, attributeLocale, attributeTimeZone">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting" required="true" hint="Reporting à exécuter">
		<cfargument name="deliveryStrategy" type="fr.consotel.api.ibis.publisher.handler.IDelivery" required="true" hint="Instance de la stratégie de diffusion">
		<cfset var reportingImpl=ARGUMENTS["reporting"]>
		<cfset var delivery=ARGUMENTS["deliveryStrategy"]>
		<cfset var xdoRequest=delivery.getDeliveryRequest(reportingImpl)>
		<cfset var reportingParameters=delivery.getReportingParameters(reportingImpl)>
		<cfset var deliveryProperties=delivery.getDeliveryProperties(reportingImpl)>
		<!--- Ajout des paramètres du XDO avec les valeurs fournies par delivery.getReportingParameters() --->
		<cfset xdoRequest=setParametersValues(xdoRequest,reportingParameters)>
		<!--- Ajout des propriétés retournées par delivery.getDeliveryProperties() avec le préfixe DPREFIX() --->
		<cfset xdoRequest=addProperties(xdoRequest,deliveryProperties,DPREFIX())>
		<!--- Ajout des propriétés attributeTemplate,attributeFormat,attributeLocale,attributeTimeZone avec le préfixe RPREFIX() --->
		<cfset xdoRequest=addProperties(xdoRequest,{
			xdoTemplateId=xdoRequest.reportRequest["attributeTemplate"], outputFormat=xdoRequest.reportRequest["attributeFormat"],
			localization=xdoRequest.reportRequest["attributeLocale"], timeZone=xdoRequest.reportRequest["attributeTimeZone"]
		},RPREFIX())>
		<cfreturn xdoRequest>
	</cffunction>
	
	<cffunction access="private" name="splitParameters" returntype="Struct" hint="Retourne une structure avec les paramètres de arrayParameters identifiés selon leur type.
	Chaque clé correspondant à un type donné et est associée à une structure dont chaque clé identifie le nom du paramètre correspondant associé à un tableau de ses valeurs.
	- XDO : Structure contenant les paramètres provenant de IDelivery.getReportingParameters() ajoutés avec setParametersValues(). Elle contient les clés : 
		- NAME : Nom du paramètre
		- VALUES : Tableau contenant les valeurs du paramètre
	Paramètres correspondants à des propriétés ajoutés avec la méthode addProprerties() identifiés à partir de leur préfixe qui est supprimé leur nom dans la structure retournée :
	- DELIVERY :  Propriétés retournées par IDelivery.getDeliveryProperties() et préfixés avec DPREFIX()
	- REPORTING : Propriétés ScheduleRequest préfixés avec RPREFIX() : xdoTemplateId, outputFormat, localization, timeZone">
		<cfargument name="arrayParameters" type="Array" required="true" hint="Tableau contenant les paramètres qui seront présents dans la structure retournée.
		Chaque élément du tableau peut etre :
			- Soit une structure réprésentant un paramètre d'une structure ScheduleRequest (Clés : name, values, multiValuesAllowed)
			- Soit une implémentation BIP de type com.oracle.xmlns.oxp.service.v11.PublicReportService.ParamNameValue
		Le paramètre BIP _cache qui est spécifique à une planification de rapport est ajouté dans la structure REPORTING s'il est défini
		Les méthodes getParamName() et getParamValues() sont redéfinies par les classes dérivées pour retourner respectivement le nom et les valeurs d'un paramètre">
		<cfset var parametersToSplit=ARGUMENTS["arrayParameters"]>
		<!--- Structure retournée contenant les paramètres de arrayParameters identifiés selon leur type --->
		<cfset var splitParameters={XDO={}, DELIVERY={}, REPORTING={}}>
		<cfset var nbParams=arrayLen(parametersToSplit)>
		<!--- Identification des paramètres en fonction de leur type --->
		<cfloop index="i" from="1" to="#nbParams#">
			<!--- Récupération du nom et des valeurs du paramètre (Rédéfinies par les classes dérivées) --->
			<cfset var paramName=getParamName(parametersToSplit[i])>
			<cfset var paramValues=getParamValues(parametersToSplit[i])>
			<!--- Type de paramètre correspondant à une propriétés provenant de IDelivery.getDeliveryProperties() --->
			<cfif NOT compare(left(paramName,LEN(DPREFIX())),DPREFIX())>
				<cfset paramName=removeChars(paramName,1,LEN(DPREFIX()))>
				<cfset structAppend(splitParameters.DELIVERY,{#paramName#=paramValues},TRUE)>
			<!--- Type de paramètre correspondant à une propriétés ScheduleRequest --->
			<cfelseif NOT compare(left(paramName,LEN(RPREFIX())),RPREFIX())>
				<cfset paramName=removeChars(paramName,1,LEN(RPREFIX()))>
				<cfset structAppend(splitParameters.REPORTING,{#paramName#=paramValues},TRUE)>
			<!--- Pour tout autre type de paramètre --->
			<cfelse>
				<cfif NOT compare(paramName,"_cache")><!--- Paramètre spécifique au Scheduler BIP --->
					<cfset structAppend(splitParameters.REPORTING,{_cache=paramValues},TRUE)>	
				<cfelse>
					<!--- Type de paramètre correspondant à un paramètre provenant de IDelivery.getReportingParameters() --->
					<cfset structAppend(splitParameters.XDO,{#paramName# = {name=paramName, values=paramValues}},TRUE)>
				</cfif>
			</cfif>
		</cfloop>
		<cfreturn splitParameters>
	</cffunction>
	
	<cffunction access="private" name="getParamName" returntype="String" hint="Retourne le nom du paramètre (Méthode rédéfinie par les classes dérivées)">
		<cfargument name="bipReportParameter" type="any"  required="true" hint="Paramètre d'un rapport BIP (Exécution directe d'un rapport) : ReportRequest">
		<cfreturn ARGUMENTS.bipReportParameter.NAME>
	</cffunction>
	
	<cffunction access="private" name="getParamValues" returntype="Array" hint="Retourne les valeurs du paramètre (Méthode rédéfinie par les classes dérivées)">
		<cfargument name="bipReportParameter" type="any"  required="true" hint="Paramètre d'un rapport BIP (Exécution directe d'un rapport) : ReportRequest">
		<cfreturn ARGUMENTS.bipReportParameter.VALUES>
	</cffunction>
	
	<cffunction access="private" name="setParametersValues" returntype="Struct"
	hint="Remplace l'ensemble des paramètres présents dans scheduleRequest par leur définition provenant du XDO.
	La structure reportingParameters contient les valeurs des paramètres. Chaque clé est le nom du paramètre associé à un tableau contenant ses valeurs
	Les valeurs fournies dans ce tableau doivent etre de type simple : String, Numeric, Date
	Chaque paramètre est ajouté avec sa valeur par défaut ou celle fournie dans reportingParameters si la clé correspondante existe
	Cette implémentation permet de contourner la contrainte CASE SENSITIVE du nom des paramètres car ils sont spécifiés en tant que clés de structure dans reportingParameters
	Tout paramètre présent dans reportingParameters et dont la valeur n'est pas fournie (i.e : Tableau vide) équivaut à sa valeur par défaut">
		<cfargument name="scheduleRequest" type="Struct" required="true" hint="ScheduleRequest BIP provenant d'une stratégie de diffusion">
		<cfargument name="reportingParameters" type="Struct" required="true" hint="Paramètres spécifiques au rapport fournis par le reporting à la stratégie de diffusion">
		<cfset var xdoRequest=ARGUMENTS["scheduleRequest"]>
		<cfset var parametersValues=ARGUMENTS["reportingParameters"]>
		<!--- Récupération de la définition des paramètres du XDO --->
		<cfset var xdoParameters=getParametersDefinition(xdoRequest)>
		<cfset var nbParams=arrayLen(xdoParameters)>
		<cfset var arrayOfParamNameValue=xdoRequest.reportRequest["parameterNameValues"]>
		<!--- Ajout de chaque paramètre soit avec sa valeur par défaut soit avec celles fournies dans reportingParameters :
		Si la valeur n'est pas fournie (i.e : Tableau vide) le paramètre ne pourra pas etre récupéré si le rapport est planifié
		Il faut alors le remplacer par une chaine vide (i.e : Insérer la chaine vide dans le tableau vide) --->
		<cfloop index="i" from="1" to="#nbParams#">
			<cfset var paramDefinition=xdoParameters[i]>
			<cfset var paramName=paramDefinition.getName()>
			<cfset var paramNameValue = {
				name=paramName, values=paramDefinition.getValues(), multiValuesAllowed=paramDefinition.isMultiValuesAllowed()
			}>
			<!--- Définition des valeurs du paramètre --->
			<cfif structKeyExists(parametersValues,paramName)>
				<cfset paramNameValue.values=parametersValues[paramName]["VALUES"]>
			</cfif>
			<cfset arrayAppend(arrayOfParamNameValue,paramNameValue)>
		</cfloop>
		<!--- CFMX : La référence du tableau doit etre réaffectée dans scheduleRequest --->
		<cfset xdoRequest.reportRequest.parameterNameValues=arrayOfParamNameValue>
		<cfreturn xdoRequest>
	</cffunction>
	
	<cffunction access="private" name="getParametersDefinition" returntype="Array"
	hint="Récupère et retourne les définition des paramètres du XDO. La durée de la récupération est affichée en secondes dans le log.
	Lorsque le XDO n'a pas de paramètres, le tableau récupéré est indéfini. Un tableau vide est retourné (i.e : Rapport sans paramètres) et un message de log est affiché">
		<cfargument name="scheduleRequest" type="Struct" required="true" hint="ScheduleRequest BIP provenant d'une stratégie de diffusion">
		<cfset var xdoRequest=ARGUMENTS["scheduleRequest"]>
		<cfset var getParamTick=getTickCount()>
		<!--- Récupération de la définition des paramètres du XDO (Valeurs par défaut) : Array of com.oracle.xmlns.oxp.service.v11.PublicReportService.ParamNameValue --->
		<cfset var xdoParameters=getBipService().getReportParameters(xdoRequest.reportRequest,getServiceProperties().USER,getServiceProperties().PWD)>
		<cfset logInfo("Parameters Definition retrieved in : " & ((getTickCount() - getParamTick) / 1000) & "s")><!--- Durée en secondes --->
		<!--- Si la définition des paramètres est définie --->
		<cfif isDefined("xdoParameters")>
			<cfreturn xdoParameters>
		<cfelse>
			<cfset logInfo("XDO : " & xdoRequest.reportRequest.reportAbsolutePath & " has no parameters definition (Will use empty parameters)")>
			<cfreturn []>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="addProperties" returntype="Struct" hint="Ajoute des propriétés en tant que paramètres supplémentaires à scheduleRequest.
	La valeur associée à chaque clé de structProperties doit etre de type simple. La clé est préfixée par paramPrefix et est utilisée comme nom du paramètre">
		<cfargument name="scheduleRequest" type="Struct" required="true" hint="ScheduleRequest BIP provenant d'une stratégie de diffusion">
		<cfargument name="structProperties" type="Struct" required="true" hint="Propriétés à ajouter en tant que paramètres à scheduleRequest">
		<cfargument name="paramPrefix" type="String" required="false" default="" hint="Préfixe ajouté à chaque clé en tant que nom du paramètre correspondant">
		<cfset var xdoRequest=ARGUMENTS["scheduleRequest"]>
		<cfset var parametersToAdd=ARGUMENTS["structProperties"]>
		<!--- Paramètres actuels de scheduleRequest --->
		<cfset var arrayOfParamNameValue=xdoRequest.reportRequest["parameterNameValues"]>
		<!--- Ajout des propriétés en tant que paramètres supplémentaires --->
		<cfloop item="param" collection="#parametersToAdd#">
			<cfset arrayAppend(arrayOfParamNameValue,{
				name=ARGUMENTS["paramPrefix"] & param, values=[parametersToAdd[param]], multiValuesAllowed=FALSE
			})>
		</cfloop>
		<!--- CFMX : La référence du tableau doit etre réaffectée dans scheduleRequest --->
		<cfset xdoRequest.reportRequest.parameterNameValues=arrayOfParamNameValue>
		<cfreturn xdoRequest>
	</cffunction>

	<cffunction access="private" name="RPREFIX" returntype="String" hint="Préfixe utilisé pour ajouter les propriétés ScheduleRequest">
		<cfreturn "_R_">
	</cffunction>
	
	<cffunction access="private" name="DPREFIX" returntype="String" hint="Préfixe utilisé pour ajouter les propriétés de IDelivery.getDeliveryProperties()">
		<cfreturn "_D_">
	</cffunction>

	<cffunction access="private" name="wsdlUrl" returntype="String" hint="Retourne l'URL du WSDL BIP (Invocation par introspection Java)">
		<cfset var bipWebService=getBipService()><!--- Instance du WebService BIP --->
		<cfset var localPortName=bipWebService.getPortName()><!--- PortName  --->
		<cfset var stubService=bipWebService._getService()><!--- Service --->
		<cfset var serviceClass=stubService.getClass()><!--- Classe du service (Introspection Java) --->
		<cfset var getEndpointMethod=serviceClass.getMethod("get" & localPortName & "Address",[])>
		<cfset var wsdlAddr=getEndpointMethod.invoke(stubService,[])><!--- URL du WSDL --->
		<cfreturn wsdlAddr>
	</cffunction>

	<cffunction access="private" name="setBipProperties" returntype="void" hint="Défini les propriétés relatives au WebService BIP et l'instancie">
		<!--- Propriétés du Web Service BIP --->
		<cfset VARIABLES.BIP={
			ENDPOINT="BIP", NOTIFICATION="NOTIFICATION", USER_AGENT="Oracle BI Publisher 10.1.3.4.1",
			WS="UNDEFINED", WSDL_URL="UNDEFINED", DNS="BIP"
		}>
		<!--- Instanciation du Web Service BIP --->
		<cfset VARIABLES.BIP.WS=getServiceProperties().WS_FACTORY.getInstance(VARIABLES.BIP.ENDPOINT)>
		<!--- URL du WSDL du Web Service BIP de la forme : http(s)://DNS.DOMAIN/WSDL_CONTEXT_PATH --->
		<cfset VARIABLES.BIP.WSDL_URL=wsdlUrl()>
		<!--- Ajout de la durée d'une session BIP en minutes --->
		<cfset VARIABLES.BIP.SESSION_INTERVAL=120>
		<!--- Ajout du DNS du serveur BIP. Par défaut : BIP si WSDL_URL n'est pas valide --->
		<cfset var wsdlInfos=listToArray(VARIABLES.BIP.WSDL_URL,"/",FALSE)>
		<!--- Vérification de la validité de wsdlInfos --->
		<cfif (arrayLen(wsdlInfos) GTE 2) AND isValid("URL",wsdlInfos[1] & "//" & wsdlInfos[2])>
			<cfset VARIABLES.BIP.DNS=wsdlInfos[2]>
		<cfelse>
			<cfset logWarning("BIP WSDL Infos and DNS URL Validation FAILED for URI : " &  VARIABLES.BIP.WSDL_URL)>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="setServiceProperties" returntype="void"
	hint="Défini les propriétés relatives au service (Sans renseigner les credentials). Redéclenche les exceptions capturées">
		<!--- Propriétés minimales permettant : Log, Notifications mails de l'API --->
		<cfset SUPER.setServiceProperties()>
		<!--- Propriétés du Service de Reporting --->
		<cfset structAppend(VARIABLES.SERVICE,{
			USER="", PWD="", SCHED_USER="", SCHED_PWD="",
			WS_FACTORY=createObject("component","fr.consotel.api.ibis.publisher.BIP"),
			FORMATS={xml=TRUE, csv=TRUE, html=TRUE, pdf=TRUE, excel=TRUE, rtf=TRUE, ppt=TRUE}
		},TRUE)>
		<!--- Défini les propriétés relatives au WebService BIP --->
		<cfset setBipProperties()>
	</cffunction>
	
	<cffunction access="private" name="setServiceCredentials" returntype="void" hint="MAJ les credentials du service utilisés avec le Web Service BIP">
		<cfargument name="user" type="string"  required="true" hint="Login utilisateur du service">
		<cfargument name="pwd" type="string"  required="true" hint="Mot de passe utilisateur du service">
		<!--- Credentials du service --->
		<cfset VARIABLES.SERVICE.USER=ARGUMENTS.user>
		<cfset VARIABLES.SERVICE.PWD=ARGUMENTS.pwd>
		<cfset logInfo("CREDENTIALS UPDATED for reporting service : " & listLast(getMetadata(THIS).NAME,"."))>
	</cffunction>
	
	<cffunction access="private" name="getBipService" returntype="any" hint="Retourne le web service BIP (org.apache.axis.client.Stub)">
		<cfreturn getBipProperties().WS>
	</cffunction>
	
	<cffunction access="private" name="getBipProperties" returntype="Struct" hint="Retourne les propriétés du Web Service BIP">
		<cfreturn VARIABLES.BIP>
	</cffunction>
</cfcomponent>