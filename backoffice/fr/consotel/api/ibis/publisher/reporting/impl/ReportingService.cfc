<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.publisher.reporting.impl.ReportingService"
extends="fr.consotel.api.ibis.publisher.reporting.impl.HandlingService"
hint="Classe contenant l'implémentation des méthodes permettant :
- Le traitement des notifications des rapports exécutés (i.e : Non planifiés)
- Le traitement des notifications d'exceptions durant l'exécution ou la planification des rapports
Les rapports sont soit exécutés soit planifiés en fonction de la structure retournée par la méthode getDeliveryRequest() de la stratégie de diffusion du reporting
Les paramètres fournis mais qui ne sont pas présents dans la définition du XDO sont ignorés.
Dans le cas d'un rapport planifié, les paramètres dont la valeur par défaut est NULL dans la définition du XDO ne peuvent pas etre récupéré lorsque :
La valeur du paramètre n'est pas fournie par le reporting ou la valeur fournie n'est pas renseignée (i.e : Tableau vide associé à la clé correspondante)
Le contournement n'est pas implémenté et consiste à :
- Gérer ces paramètres comme les propriétés de la stratégie de diffusion
- Fournir une chaine vide comme valeur lorsque celle-ci n'est pas fournie par le reporting et que la valeur par défaut est NULL dans la définition du XDO">
	<cffunction access="public" name="getService" returntype="fr.consotel.api.ibis.publisher.reporting.IReportingService">
		<cfargument name="user" type="string"  required="true" hint="Login utilisateur du service">
		<cfargument name="pwd" type="string"  required="true" hint="Mot de passe utilisateur du service">
		<cftry>
			<cfset SUPER.getService(ARGUMENTS.user,ARGUMENTS.pwd)>
			<cfreturn THIS>
			<cfcatch type="any">
				<cfset logError("Reporting Service " & listLast(getMetadata(THIS).NAME,".") & " properties definition FAILED : " & CFCATCH.message)>
				<cfset handleException(CFCATCH,"Exception durant la création du service de reporting : " & listLast(getMetadata(THIS).NAME,"."))>
				<!--- Redéclenche l'exception capturée --->
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
		
	<cffunction access="public" name="createReporting" returntype="fr.consotel.api.ibis.publisher.reporting.IReporting"
	hint="Retourne une instance de l'implémentation de IReporting. Redéclenche les exceptions capturées">
		<cfargument name="props" type="Struct"  required="true" hint="Propriétés du reporting">
		<cfset var reportingProperties=ARGUMENTS["props"]>
		<cftry>
			<cfreturn createObject("component",getReportingType()).createReporting(reportingProperties)>
			<cfcatch type="any">
				<cfset logError("Reporting Instanciation FAILED : " & CFCATCH.message)>
				<cfset handleException(CFCATCH,"Instanciation du reporting",reportingProperties)>
				<!--- Redéclenche l'exception capturée --->
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="execute" returntype="void" hint="Exécute ou planifie le reporting en fonction de la stratégie de diffusion">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true" hint="Reporting à exécuter">
		<cfset var reportingImpl=ARGUMENTS["reporting"]>
		<cftry>
			<!--- Instanciation de la stratégie de diffusion --->
			<cfset var deliveryType=reportingImpl.getDeliveryType()>
			<cfset var deliveryStrategy=createDelivery(deliveryType)>
			<cfset var getParamTick=getTickCount()>
			<!--- Requete ScheduleRequest BIP utilisée pour l'exécution ou la planification du rapport --->
			<cfset var scheduleRequest=buildScheduleRequest(reportingImpl,deliveryStrategy)>
			<cfset logInfo("ScheduleRequest BUILT in : " & ((getTickCount() - getParamTick) / 1000) & "s")>
			<!--- Si la clé deliveryRequest est présente dans la structure ScheduleRequest : Planification du rapport --->
			<cfif structKeyExists(scheduleRequest,"deliveryRequest")>
				<cfset scheduleReporting(scheduleRequest,deliveryStrategy,reportingImpl)>
			<!--- Si la clé deliveryRequest n'est pas présente dans la structure ScheduleRequest : Exécution du rapport --->
			<cfelse>
				<cfset runReporting(scheduleRequest,deliveryStrategy,reportingImpl)>
			</cfif>
			<!--- Traitement des exceptions capturées --->
			<cfcatch type="any">
				<cfset var customDesc="Exception durant le processus d'exécution du reporting : IReportingService.execute()">
				<cfif isDefined("reportingRequest")>
					<cfif structKeyExists(scheduleRequest,"deliveryRequest")>
						<cfset customDesc="Exception durant la planification du rapport (scheduleReport)">
					<cfelse>
						<cfset customDesc="Exception durant l'exécution du rapport (runReport)">
					</cfif>
				</cfif>
				<cfset logError("ReportingService.execute() FAILED : " & CFCATCH.message)>
				<!--- Traitement de l'exception avec les propriétés du reporting --->
				<cfset handleException(CFCATCH,customDesc)>
				<!--- Relance l'exception capturée --->
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="private" name="addNotificationTarget" returntype="Struct"
	hint="L'implémentation associée à la clé SERVICE est une référence pointant sur l'instance IReportingService utilisée pour exécuter et générer le rapport
	Cette implémentation est spécifique à un rapport qui est exécuté. Les propriétés qui ont été fournies par getAuthProperties() ne sont ni nécessaires ni utilisées ici">
		<cfargument name="notification" type="Struct" required="true">
		<cfset var jobNotification=ARGUMENTS["notification"]>
		<!--- Ajoute la clé SERVICE (Event Target) dans la notification : Meme instance que celle utilisée pour exécuter le rapport --->
		<cfset structAppend(jobNotification,{SERVICE=THIS},TRUE)>
		<cfreturn jobNotification>
	</cffunction>
	
	<cffunction access="private" name="scheduleReporting" returntype="void" hint="Exécute un reporting par planification du rapport
	La méthode createExceptionNotification() est appelée en cas d'exception durant la planification du rapport
	La notification obtenue est passée à handleNotification() puis l'exception capturée est redéclenchée">
		<cfargument name="scheduleRequest" type="Struct" required="true" hint="ScheduleRequest BIP correspondant">
		<cfargument name="deliveryStrategy" type="fr.consotel.api.ibis.publisher.handler.IDelivery" required="true" hint="Stratégie de diffusion">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true" hint="Reporting à exécuter">
		<cfset var jobId = -1>
		<cfset var delivery=ARGUMENTS["deliveryStrategy"]>
		<cfset var xdoRequest=ARGUMENTS["scheduleRequest"]>
		<cfset var getParamTick=getTickCount()> 
		<cftry>
			<cfset logInfo("SCHEDULE HTTP DESTINATION : " & xdoRequest.httpNotificationServer)>
			<cfset jobId=getBipService().scheduleReport(xdoRequest,getServiceProperties().USER,getServiceProperties().PWD)>
			<cfset logInfo("SCHEDULE SUCCESS after " & ((getTickCount() - getParamTick) / 1000) & "s for job [#jobId#] : " & xdoRequest.userJobName)>
			<!--- Traitement des exceptions capturées durant la planification du rapport --->
			<cfcatch type="any">
				<cfset logInfo("SCHEDULE FAILED after " & ((getTickCount() - getParamTick) / 1000) & "s for job : " & xdoRequest.userJobName)>
				<!--- Création et traitement de la notification correspondante --->
				<cfset var exceptionNotification=createExceptionNotification({ScheduleRequest=xdoRequest, Exception=CFCATCH})>
				<cfset handleNotification(exceptionNotification)>
				<!--- Relance l'exception capturée --->
				<cfrethrow>
			</cfcatch>
		</cftry>
		<!--- Traitement du JOBID par la stratégie de diffusion --->
		<cfset getParamTick=getTickCount()>
		<cfset delivery.handleJobId(ARGUMENTS["reporting"],jobId)>
		<cfset logInfo("HANDLE JOBID " & jobId & " COMPLETED in " & ((getTickCount() - getParamTick) / 1000) & "s for job : " & xdoRequest.userJobName)>
	</cffunction>
	
	<cffunction access="private" name="runReporting" returntype="void" hint="Exécute le rapport puis génère et traite la notification correspondante
	Lorsque le rapport est généré avec succès une notification contenant les clés suivantes est créée et passée à handleNotification() :
	- JOBID : Valeur 0
	- JOBNAME : Nom du JOB provenant de la structure ScheduleRequest utilisée pour exécuter le rapport
	- STATUS : Valeur de getEvent().SUCCESS()
	- MESSAGE : Generated Successfully
	- CONTENT : Implémentation du contenu du rapport retourné par le Web Service BIP (com.oracle.xmlns.oxp.service.PublicReportService.ReportResponse)
	- ScheduleRequest : Structure ScheduleRequest utilisée pour l'exécution ou la planification du rapport et provenant de notification
	La méthode createExceptionNotification() est appelée en cas d'exception durant l'exécution du rapport
	La notification obtenue est passée à handleNotification() puis l'exception capturée est redéclenchée">
		<cfargument name="scheduleRequest" type="Struct" required="true" hint="ScheduleRequest BIP correspondant">
		<cfargument name="deliveryStrategy" type="fr.consotel.api.ibis.publisher.handler.IDelivery" required="true" hint="Stratégie de diffusion">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true" hint="Reporting à exécuter">
		<cfset var jobId=0>
		<cfset var delivery=ARGUMENTS["deliveryStrategy"]>
		<cfset var xdoRequest=ARGUMENTS["scheduleRequest"]>
		<!--- Contenu du rapport --->
		<cfset var reportContent="">
		<cfset var getParamTick=getTickCount()> 
		<cftry>
			<cfset reportContent=getBipService().runReport(xdoRequest.reportRequest,getServiceProperties().USER,getServiceProperties().PWD)>
			<cfset logInfo("EXECUTION SUCCESS after " & ((getTickCount() - getParamTick) / 1000) & "s for job : " & xdoRequest.userJobName)>
			<!--- Toute exception captuée pendant la planification est dispatchée en tant qu'évènement à dispatchReportingEvent() --->
			<cfcatch type="any">
				<cfset logError("RUN FAILED after " & ((getTickCount() - getParamTick) / 1000) & "s for job : " & xdoRequest.userJobName)>
				<!--- Création et traitement de la notification correspondante --->
				<cfset var exceptionNotification=createExceptionNotification({ScheduleRequest=xdoRequest, Exception=CFCATCH})>
				<cfset handleNotification(exceptionNotification)>
				<!--- Relance l'exception capturée --->
				<cfrethrow>
			</cfcatch>
		</cftry>
		<!--- Traitement du JOBID par la stratégie de diffusion --->
		<cfset getParamTick=getTickCount()>
		<cfset delivery.handleJobId(ARGUMENTS["reporting"],jobId)>
		<cfset logInfo("HANDLE JOBID " & jobId & " COMPLETED in " & ((getTickCount() - getParamTick) / 1000) & "s for job : " & xdoRequest.userJobName)>
		<!--- Création et traitement de la notification correspondante : Statut getEvent().SUCCESS() --->
		<cfset getParamTick=getTickCount()>
		<cfset handleNotification({
			JOBID=jobId, JOBNAME=xdoRequest["userJobName"], STATUS=getEvent().SUCCESS(), MESSAGE="Generated Successfully",
			CONTENT=reportContent, ScheduleRequest=xdoRequest
		})>
	</cffunction>

	<cffunction access="private" name="getJob" returntype="Struct" hint="Les valeurs des clés de la notification retournée sont :
	- STATUS : Valeur de la clé contenue dans notification
	- REPORT_URL : Chemin absolu du XDO provenant de la structure ScheduleRequest utilisée pour exécuter ou planifier le rapport
	- MESSAGE : Valeur de la clé contenue dans notification
	- CONTENT : Valeur de la clé contenue dans notification. La clé n'est pas ajoutée dans la notification retournée si elle n'existe pas (e.g : Echec de l'exécution) 
	- ScheduleRequest : Valeur de la clé contenue dans notification
	Ajoute une clé ScheduleRequest : Structure ScheduleRequest utilisée pour l'exécution ou la planification du rapport et provenant de notification
	La structure retournée est construite à partir de l'implémentation getJobNotification() de HandlingService">
		<cfargument name="notification" type="Struct" required="true" hint="Provient de buildReportingNotification() et validée avec isValidNotification()">
		<cfset var notificationInfos=ARGUMENTS["notification"]>
		<cfset var scheduleRequest=notificationInfos["ScheduleRequest"]>
		<!--- Structure contenant les clés : JOBID (Valeur du JOBID provenant de notification), JOBNAME, STATUS --->
		<cfset var jobProperties=getJobNotification(notificationInfos)>
		<!--- Met à jour et ajoute des infos à la structure retournée --->
		<cfset structAppend(jobProperties,{
			STATUS=notificationInfos["STATUS"], MESSAGE=notificationInfos["MESSAGE"],
			REPORT_URL=scheduleRequest.reportRequest["reportAbsolutePath"], ScheduleRequest=scheduleRequest
		},TRUE)>
		<!--- Ajout du contenu du rapport s'il existe (i.e : La rapport a été exécuté directement et avec succès) --->
		<cfif structKeyExists(notificationInfos,"CONTENT")>
			<cfset jobProperties.CONTENT=notificationInfos["CONTENT"]>
		</cfif>
		<cfreturn jobProperties>
	</cffunction>
	
	<cffunction access="private" name="getJobParameters" returntype="Array"
	hint="Retourne les paramètres du rapport spécifiés dans la structure ScheduleRequest contenue dans notification">
		<cfargument name="notification" type="Struct" required="true">
		<cfset var scheduleRequest=ARGUMENTS.notification["ScheduleRequest"]>
		<cfreturn scheduleRequest.reportRequest["parameterNameValues"]>
	</cffunction>
	
	<cffunction access="private" name="createExceptionNotification" returntype="Struct"
	hint="Appelée par scheduleReporting() et runReporting() pour toute exception capturée durant l'exécution ou la planification d'un rapport
	Les valeurs des clés de la notification retournée sont :
	- JOBID : Valeur -1 ou celle de la clé contenue dans notification
	- JOBNAME : Nom du JOB provenant de la structure ScheduleRequest contenue dans notification 
	- STATUS : Valeur de getEvent().EXCEPTION() ou celle de la clé contenue dans notification
	- MESSAGE : MESSAGE de l'exception suivi du DETAIL de l'exception s'il est défini séparés par ::
	- ScheduleRequest : Valeur de la clé contenue dans notification
	La notification retournée est passée par scheduleReporting() et runReporting() à handleNotification()">
		<cfargument name="notification" type="any" required="true" hint="Structure représentant la notification correspondante contenant au moins les clés :
		Exception (Exception CFCATCH qui a été capturée), ScheduleRequest (Structure ScheduleRequest utilisée pour l'exécution ou la planification du rapport)">
		<cfset var notificationInfos=ARGUMENTS["notification"]>
		<cfset var xdoRequest=notificationInfos["scheduleRequest"]>
		<cfset var exceptionInfos=notificationInfos["Exception"]>
		<cfset var jobId = -1>
		<cfset var jobStatus=getEvent().EXCEPTION()>
		<cfset var jobMessage=exceptionInfos["MESSAGE"]>
		<!--- Valeur du JOBID : -1 ou la valeur de la clé provenant de notification --->
		<cfif structKeyExists(notificationInfos,"JOBID")>
			<cfset jobId=notificationInfos["JOBID"]>
		</cfif>
		<!--- Valeur du statut du JOB : getEvent().EXCEPTION() ou la valeur de la clé contenue dans notification --->
		<cfif structKeyExists(notificationInfos,"STATUS")>
			<cfset jobStatus=notificationInfos["STATUS"]>
		</cfif>
		<!--- Construction du message du JOB --->
		<cfif structKeyExists(exceptionInfos,"DETAIL") AND (LEN(TRIM(exceptionInfos["DETAIL"])) GT 0)>
			<cfset jobMessage=jobMessage & "::" & exceptionInfos["DETAIL"]>
		</cfif>
		<!--- Création de la notification correspondante --->
		<cfreturn {ScheduleRequest=xdoRequest, JOBID=jobId, JOBNAME=xdoRequest["userJobName"], STATUS=jobStatus, MESSAGE=jobMessage}>
	</cffunction>
	
	<cffunction access="private" name="isValidNotification" returntype="boolean"
	hint="Retourne TRUE si la notification est validée par l'implémentation isValidJobNotification() de HandlingService et si elle contient les clés :
	STATUS (Statut du JOB), MESSAGE (Message du JOB), ScheduleRequest (Structure ScheduleRequest utilisée pour l'exécution ou la planification du rapport)">
		<cfargument name="notification" type="Struct"  required="true" hint="Notification à valider">
		<cfset var notificationInfos=ARGUMENTS["notification"]>
		<!--- Vérification de la validité et du type de la notification --->
		<cfset var validNotification=structKeyExists(notificationInfos,"STATUS") AND
			structKeyExists(notificationInfos,"MESSAGE") AND structKeyExists(notificationInfos,"ScheduleRequest")>
		<cfreturn isValidJobNotification(notificationInfos) AND validNotification>
	</cffunction>
	
	<cffunction access="private" name="getReportingType" returntype="String" hint="Retourne le type par défaut de l'implémentation IReportingEvent">
		<cfreturn "fr.consotel.api.ibis.publisher.reporting.impl.AbstractReporting">
	</cffunction>
</cfcomponent>