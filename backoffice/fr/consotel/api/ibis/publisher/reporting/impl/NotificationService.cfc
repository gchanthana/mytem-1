<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.publisher.reporting.impl.NotificationService"
extends="fr.consotel.api.ibis.publisher.reporting.impl.ReportingService"
hint="Cette classe contient l'implémentation du traitement des notifications des rapports planifiés (Notification HTTP POST provenant de BIP).
Le traitement de ce type de notification comprend la récupération des infos et historique du JOB correspondant (Sans les supprimer de l'historique BIP).
Ces données sont récupérées en utilisant le Web Service BIP avec un login utilisateur spécifique à cette implémentation (scheduler)
Cet utilisateur doit avoir accès aux à l'historique du Scheduler BIP (Role : Gestionnaire de planification dans BI Publisher)
Cette classe récupère à partir de ces données les credentials utilisés par l'implémentation IReportingService qui a exécuté le reporting
Cette opération est effectuée après le traitement de la notification provenant de BIP et au moment de la création de l'évènement IReportingEvent
Une exception est levée avec le message MISSING CREDENTIALS in notification lorsque les credentials n'ont pas pu etre récupérés">
	<cffunction access="public" name="handleNotification" returntype="void" hint="Traitement des notifications HTTP provenant de BIP (Rapports planifiés)
	Récupère et copie la notification provenant de BIP puis la passe à la méthode de la classe parent. Une structure vide est passée si cette notification n'est pas définie
	La notification récupérée contient les clés : JOBID (JOBID du JOB), JOBNAME (Nom du JOB), STATUS (Statut du JOB), REPORT_URL : Chemin absolu du XDO
	Si la notification récupérée est invalide par rapport à cette implémentation : Un log d'erreur est affiché puis une exception est levée et traitée">
		<cfargument name="notification" type="Struct"  required="true" hint="La valeur de ce paramètre n'est pas utilisée par cette implémentation">
		<cfset var notificationInfos={}>
		<cftry>
			<cfset var getParamTick=getTickCount()>
			<cfif isDefined("FORM")>
				<cfset notificationInfos=DUPLICATE(FORM)>
			</cfif>
			<!--- Délègue le traitement à l'implémentation de la classe parent --->
			<cfset SUPER.handleNotification(notificationInfos)>
			<!--- Traitement des exceptions capturées --->
			<cfcatch type="any">
				<cfset onNotificationHandlingException(notificationInfos,CFCATCH,getParamTick)>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="private" name="addNotificationTarget" returntype="Struct"
	hint="L'instance associée à la clé SERVICE est créée au moment du traitement de la notification HTTP provenant de BIP et correspond à cette implémentation
	Cette implémentation est spécifique à un rapport qui est planifié. Les propriétés qui ont été fournies par getAuthProperties() sont utilisées comme suit :
	- La valeur héxadécimale associée à la clé AUTH_TOKEN est convertie vers sa d'origine
	- Les credentials fournis à l'instance associée à la clé SERVICE sont la valeur de la clé JOB.USER comme login et la valeur convertie de AUTH_TOKEN.
	La credentials sont fournis à l'instance IReportingService avec la méthode setServiceCredentials() de la classe parent
	La conversion de la valeur héxadécimal asssociée à la clé AUTH_TOKEN est effectuée comme suit :
	- La valeur héxadécimale est convertie au format binaire avec binaryDecode()
	- La valeur convertie correspondante est obtenue en utilisant toString() avec l'encodage UTF-8
	Une exception est levée si JOB.USER ou AUTH_TOKEN ne sont pas définis ou ne sont pas dans le format attendu">
		<cfargument name="notification" type="Struct" required="true" hint="Contient une clé AUTH_TOKEN : Valeur hexadécimale de getServiceProperties().PWD">
		<cfset var jobNotification=ARGUMENTS["notification"]>
		<cfif structKeyExists(jobNotification,"REPORTING") AND structKeyExists(jobNotification.REPORTING,"AUTH_TOKEN") AND
			isValid("Array",jobNotification.REPORTING.AUTH_TOKEN) AND (arrayLen(jobNotification.REPORTING.AUTH_TOKEN) EQ 1) AND
			structKeyExists(jobNotification,"JOB") AND structKeyExists(jobNotification.JOB,"USER")>
			<cfset var pwdBinary=binaryDecode(jobNotification.REPORTING.AUTH_TOKEN[1],"HEX")>
			<cfset var pwdString=toString(pwdBinary,"utf-8")><!--- Valeur littérale de AUTH_TOKEN --->
			<!--- Ajoute la clé SERVICE (Event Target) dans la notification : Nouvelle instance avec les memes credentials utilisés pour la planification du rapport --->
			<cfset SUPER.setServiceCredentials(jobNotification.JOB.USER,pwdString)>
			<cfset structAppend(jobNotification,{SERVICE=THIS},TRUE)>
			<cfreturn jobNotification>
		<cfelse>
			<cfset logError("MISSING CREDENTIALS in notification keys : " & structKeyList(jobNotification,","))>
			<cfthrow attributecollection="#getErrorProperties()#" message="Les credentials pour le reporting sont manquants dans la notification">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="getJob" returntype="Struct" hint="Les valeurs des clés de la notification retournée sont :
	- STATUS : Valeur de la clé contenue dans notification
	- JOBNAME : Valeur de la clé provenant de notification
	- REPORT_URL : Valeur de la clé provenant de notification
	- MESSAGE : Message de l'historique du JOB
	- USER : Login utilisateur BIP utilisé pour la planification du rapport
	Ajoute les clés suivantes dans la notification retournée :
	- INFOS : Implémentation BIP des infos du JOB (com.oracle.xmlns.oxp.service.PublicReportService.JobInfo)
	- HISTORY : Implémentation BIP de l'historique du JOB (com.oracle.xmlns.oxp.service.PublicReportService.JobHistoryInfo)
	La structure retournée est construite à partir de l'implémentation getJobNotification() de HandlingService
	L'historique du JOBID n'est pas supprimé de l'historique BIP une fois ces infos récupérées
	Une exception est levée si les valeurs des clés INFOS et HISTORY n'ont pas pu etre récupérées">
		<cfargument name="notification" type="Struct" required="true">
		<cfset var notificationInfos=ARGUMENTS["notification"]>
		<cfset var jobId=notificationInfos["JOBID"]>
		<!--- Infos du JOB : Array of com.oracle.xmlns.oxp.service.PublicReportService.JobInfo --->
		<cfset var jobInfo=getBipService().getScheduledReportInfo(jobId,getServiceProperties().SCHED_USER,getServiceProperties().SCHED_PWD,"All")>
		<!--- Historique du JOB : Array of com.oracle.xmlns.oxp.service.PublicReportService.JobHistoryInfo --->
		<cfset var jobHistoryInfo=getBipService().getScheduledReportHistoryInfo(jobId,getServiceProperties().SCHED_USER,getServiceProperties().SCHED_PWD,"All",FALSE)>
		<cfif isDefined("jobInfo") AND isValid("Array",jobInfo) AND isDefined("jobHistoryInfo") AND isValid("Array",jobHistoryInfo)>
			<cfset var jobInfosImpl=jobInfo[1]>
			<cfset var jobHistoryInfosImpl=jobHistoryInfo[1]>
			<!--- Implémentation de la classe parent --->
			<cfset var jobProperties=getJobNotification(notificationInfos)>
			<!--- Met à jour et ajoute des infos à la structure retournée --->
			<cfset structAppend(jobProperties,{
				STATUS=notificationInfos["STATUS"], REPORT_URL=notificationInfos["REPORT_URL"], MESSAGE=jobHistoryInfosImpl.getJobMessage(),
				USER=jobInfosImpl.getUsername(), INFOS=jobInfosImpl, HISTORY=jobHistoryInfosImpl
			},TRUE)>
			<cfreturn jobProperties>
		<!--- Levée d'une exception si les infos et historique du JOB n'ont pas pu etre récupérées à partir du Web Service --->
		<cfelse>
			<cfset var errorMessage="Unable to retrieve JOB Infos or History">
			<cfset var detailMessage="JOBID : #jobId#. BIP DNS : #getBipProperties().DNS#">
			<cfthrow attributecollection="#getErrorProperties()#" message="#errorMessage#" detail="#detailMessage#">
		</cfif>
	</cffunction>

	<cffunction access="private" name="getJobParameters" returntype="Array"
	hint="Récupère et retourne les paramètres du rapport à partir des infos du JOB. Un tableau vide est retourné si les paramètres ne sont pas définis
	Tout paramètre dont la valeur fournie n'est pas fournie (i.e : Tableau vide) équivaut à sa valeur par défaut et ne peut pas etre récupéré
	Tout paramètre dont la valeur par défaut est NULL dans la définition du XDO ne peut pas etre récupéré
	Une exception est levée si aucun paramètre n'est récupéré à partir des infos du JOB (Doit au moins contenir les propriétés de la stratégie de diffusion)">
		<cfargument name="notification" type="Struct" required="true" hint="Résultat de getJob() obtenu par buildReportingNotification()">
		<cfset var notificationInfos=ARGUMENTS["notification"]>
		<cfset var jobId=notificationInfos["JOBID"]>
		<cfset var jobInfos=notificationInfos["INFOS"]>
		<!--- Récupération des paramètres du rapport planifié : Array of com.oracle.xmlns.oxp.service.v11.PublicReportService.ParamNameValue --->
		<cfset var jobParameters=jobInfos.getReportParameters()>
		<cfif isDefined("jobParameters")>
			<cfreturn jobParameters>
		<cfelse>
			<cfreturn []>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="isValidNotification" returntype="boolean"
	hint="Retourne TRUE si la notification vérifie les conditions suivantes : (L'implémentation de la classe parent est redéfinie)
	- Valide par rapport à l'implémentation isValidJobNotification() de HandlingService
	- Elle contient les clés : JOBID (JOBID du JOB), JOBNAME (Nom du JOB), STATUS (Statut du JOB), REPORT_URL (Chemin absolu du XDO du rapport planifié)
	- La méthode de la requete HTTP doit etre de type POST">
		<cfargument name="notification" type="Struct"  required="true">
		<cfset var notificationInfos=ARGUMENTS["notification"]>
		<cfset var validNotification=(CGI.REQUEST_METHOD EQ "POST")>
		<cfif validNotification>
			<cfset validNotification=validNotification AND isValidJobNotification(notificationInfos) AND structKeyExists(notificationInfos,"JOBID") AND
				structKeyExists(notificationInfos,"JOBNAME") AND structKeyExists(notificationInfos,"STATUS") AND structKeyExists(notificationInfos,"REPORT_URL")>
		</cfif>
		<cfreturn validNotification>
	</cffunction>
	
	<cffunction access="private" name="getParamName" returntype="String" hint="Retourne le nom du paramètre">
		<cfargument name="bipReportParameter" type="any"  required="true" hint="Paramètre : com.oracle.xmlns.oxp.service.v11.PublicReportService.ParamNameValue">
		<!--- Equivaut à bipReportParameter.NAME (Mappage des types JAVA/CFMX) --->
		<cfreturn ARGUMENTS.bipReportParameter.getName()>
	</cffunction>
	
	<cffunction access="private" name="getParamValues" returntype="Array" hint="Retourne les valeurs du paramètre">
		<cfargument name="bipReportParameter" type="any"  required="true" hint="Paramètre : com.oracle.xmlns.oxp.service.v11.PublicReportService.ParamNameValue">
		<!--- Equivaut à bipReportParameter.VALUES (Mappage des types JAVA/CFMX) --->
		<cfreturn ARGUMENTS.bipReportParameter.getValues()>
	</cffunction>	

	<cffunction access="private" name="setServiceCredentials" returntype="void"
	hint="MAJ les credentials du service avec une implémentation d'une classe parent et définis ceux utilisés pour la récupérer les infos et historique du JOB">
		<cfargument name="user" type="string"  required="true" hint="Login utilisateur du service">
		<cfargument name="pwd" type="string"  required="true" hint="Mot de passe utilisateur du service">
		<cfset SUPER.setServiceCredentials(ARGUMENTS["user"],ARGUMENTS["pwd"])>
		<!--- Credentials utilisés par le service pour la récupération des infos et historique d'un JOB avec le Web Service BIP --->
		<cfset VARIABLES.SERVICE.SCHED_USER="scheduler">
		<cfset VARIABLES.SERVICE.SCHED_PWD="public">
	</cffunction>
</cfcomponent>