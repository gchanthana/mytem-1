<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.publisher.reporting.impl.ReportingHandler"
implements="fr.consotel.api.ibis.publisher.reporting.IPostProcess"
hint="Implementation par défaut de IDelivery. Les classes dérivées peuvent redéfinir les propriétés :
- NOTIFICATION_SERVER : Serveur de notification HTTP (Prédéfini dans BIP)
- NOTIFICATION_TO : Destinataire des notifications mails de BIP
La notification par défaut (NOTIFICATION) lorsque la clé postProcess.type n'est pas renseignée">
<!--- *************************************************************************************************************************************** --->
	<cffunction access="public" name="createPostProcess" returntype="fr.consotel.api.ibis.publisher.reporting.IPostProcess" hint="Méthode appelée en tant que constructeur de l'implémentation">
		<!--- Propriétés communes pour la notification (HANDLER) --->
		<cfset VARIABLES.HANDLER = {
			PACKAGE="fr.consotel.api.ibis.publisher.handler.process", LOG="HANDLER", DOMAIN="consotel.fr",
			NOTIFICATION_TO="", NOTIFICATION_SERVER="NOTIFICATION"
		}>
		<cfreturn THIS>
	</cffunction>

	<cffunction access="public" name="notificationRequest" returntype="Struct" hint="Factory reservée pour l'usage de l'API. Lève une exception si elle est appelée par les classes dérivées">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true" hint="Reporting à exécuter">
		<cfset var callerClass=getMetadata(THIS)["name"]><!--- Classe appelante --->
		<cfif NOT compareNoCase(callerClass,"fr.consotel.api.ibis.publisher.reporting.impl.ReportingHandler")><!--- Cette méthode est reservée à l'API --->
			<cfset var reportingProps=ARGUMENTS.reporting.getProperties()><!--- Propriétés du reporting --->
			<!--- Instanciation de l'implémentation de la notification --->
			<cfif structKeyExists(reportingProps,"postProcess") AND (LEN(TRIM(reportingProps["postProcess"]["type"])) GT 0)><!--- Si le PostProcess est renseigné --->
				<cfset var handlerType=reportingProps["postProcess"]["type"]><!--- Propriétés du PostProcess --->
				<cfset var postProcess=createObject("component",VARIABLES.HANDLER.PACKAGE & "." & handlerType).createPostProcess()><!--- Implémentation de IPostProcess --->
				<cfreturn postProcess.notificationRequest(ARGUMENTS.reporting)><!--- Structure contenant les propriétés de notification --->
			<cfelse><!--- Si le PostProcess n'est pas renseigné --->
				<cfset logWarning("PostProcess Type not specified for reporting (Using Default Notification Mechanism : QUARTZ Java API")>
				<cfreturn {}><!--- Structure retournée par défaut pour les paramètres de notification dans l'objet ScheduleRequest --->
			</cfif>
		<cfelse>
			<cfset errorInfos={type="Custom", errorcode="ILLEGAL_OPERATION", message="PRIVATE METHOD RESERVED FOR API"}>
			<cfset logError("ReportingHandler.createRequest() : #errorInfos.message#")><!--- Message de log de l'erreur --->
			<cfthrow attributecollection="#errorInfos#" detail="ILLEGAL_OPERATION : #callerClass#">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="run" returntype="void" hint="Méthode qui est exécutée à la fin du processus complet de génération d'un reporting (Traitement de fin)">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.reporting.IReportingEvent"  required="true" hint="Evènement correspondant à cette étape">
	</cffunction>
	
	<cffunction access="private" name="getMailAttributes" returntype="Struct" hint="Retourne les valeurs utilisées par cette API pour les champs expéditeur et type (html) avec CFMAIL">
		<cfreturn {from="api@" & VARIABLES.HANDLER.DOMAIN, type="html"}>
	</cffunction>
	
	<cffunction access="private" name="logInfo" returntype="void" hint="Affiche le message dans les logs avec le level INFORMATION. La valeur de l'attribut file est getLog()">
		<cfargument name="message" type="String" required="true" hint="Message à logger">
		<cflog file="#getLog()#" type="information" text="#ARGUMENTS.message#">
	</cffunction>

	<cffunction access="private" name="logWarning" returntype="void" hint="Affiche le message dans les logs avec le level WARNING. La valeur de l'attribut file est getLog()">
		<cfargument name="message" type="String" required="true" hint="Message à logger">
		<cflog file="#getLog()#" type="warning" text="#ARGUMENTS.message#">
	</cffunction>
	
	<cffunction access="private" name="logError" returntype="void" hint="Affiche le message dans les logs avec le level ERROR. La valeur de l'attribut file est getLog()">
		<cfargument name="message" type="String" required="true" hint="Message à logger">
		<cflog file="#getLog()#" type="error" text="#ARGUMENTS.message#">
	</cffunction>

	<cffunction access="private" name="getLog" returntype="String" hint="Retourne la valeur utilisée pour l'attribut file de CFLOG">
		<cfreturn VARIABLES.HANDLER.LOG>
	</cffunction>
</cfcomponent>