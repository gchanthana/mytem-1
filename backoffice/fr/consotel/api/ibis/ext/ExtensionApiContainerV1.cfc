<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.ext.ExtensionApiContainerV1" extends="fr.consotel.consoview.api.container.ApiContainerV1"
hint="Extension de ApiContainerV1 utilisée par l'API Reporting dans laquelle les méthodes suivantes ont été redéfinies :
- setCodeProcessForJOBID() : La valeur du 3ème paramètre de la procédure stockée qui est appelée est remplacée getBipServerParamValue()
- getCodeProcess() : Le paramètre BI_SERVER est ignoré et est remplacé par getBipServerParamValue()
La méthode checkProperties() est appelée à l'instanciation et vérifie la présence des propriétés qui sont spécifiées à la l'extérieur des méthodes de la classe parent
Cette méthode est spécifique à cette implémentation et n'est pas présente dans la classe parent
La méthode getBipServerParamValue() retourne la valeur à fournie au paramètre indiquant le nom du serveur BIP pour les méthodes de la classe parent">
	<cfset checkProperties()>
	
	<cffunction access="remote" name="setCodeProcessForJOBID" returntype="numeric" hint="Reprend la meme implémentation que la classe parent
	La valeur du 3ème paramètre de la procédure stockée qui est appelée est remplacée par la valeur retournée par getBipServerParamValue()
	Retourne la valeur retournée par la procédure stockée qui est appelée : pkg_m61.addJobDetail_V2">
		<cfargument name="UUID" type="String" hint="UUID placé dans le champ SERVER_BIP de la table CONTAINER_JOB_DETAIL">
		<cfargument name="JOBID" type="Numeric" required="true" hint="JOBID du JOB">				
		<cfargument name="PROCESSID" type="Numeric" hint="PROCESSID">
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m61.addJobDetail_V2">
			<cfprocparam value="#ARGUMENTS.JOBID#" cfsqltype="CF_SQL_INTEGER" type="in">			
			<cfprocparam value="#ARGUMENTS.PROCESSID#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#ARGUMENTS.UUID#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam variable="p_retour" cfsqltype="CF_SQL_INTEGER" type="out">			
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction access="remote" name="getCodeProcess" returntype="String"
	hint="Reprend la meme implémentation que la classe parent. Le paramètre BI_SERVER est ignoré et est remplacé par la chaine : BI_SERVER
	- Si la procédure stockée qui est appelée retourne exactement 1 enregistrement :
	Retourne la valeur du champ LIBELLE qui est retournée par la procédure stockée qui est appelée : pkg_m61.getCodeProcess_V2
	- Sinon retourne la valeur : -1 (En tant que chaine de caractères)">
		<cfargument name="JOBID" type="Numeric" required="true" hint="JOBID du JOB">	
		<cfargument name="UUID" type="String" required="true" hint="UUID recherché dans le champ SERVER_BIP de la table CONTAINER_JOB_DETAIL">
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m61.getCodeProcess_V2">
			<cfprocparam value="#ARGUMENTS.JOBID#" cfsqltype="CF_SQL_INTEGER" type="in">			
			<cfprocparam value="#ARGUMENTS.UUID#"	cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfif p_retour.RecordCount eq 1>
			<cfreturn p_retour.LIBELLE>
		<cfelse>
			<cfreturn "-1">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="checkProperties" returntype="void"
	hint="Vérifie la présence des propriétés qui sont définies à l'extérieur des méthodes de la classe parent. Les valeurs de ces propriétés sont définie à l'instanciation
	Une exception est levée si les propriétés suivantes ne sont pas définies : FTPserver, FTPport, FTPusername, FTPpassword, OFFREDSN, DEV_SUPPORT_ADDR, uuid">
		<cfset var requiredKeys="FTPserver,FTPport,FTPusername,FTPpassword,OFFREDSN,DEV_SUPPORT_ADDR,uuid">
		<cfloop index="key" list="#requiredKeys#" delimiters=",">
			<cfif NOT isDefined(key)>
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION"
					message="La propriété #key# n'est pas définie à l'instanciation (container)" detail="CFC container : #getMetadata(THIS).NAME#">
			</cfif>
		</cfloop>
	</cffunction>
	
	<cffunction access="public" name="getBipServerParamValue" returntype="String"
	hint="Retourne la valeur qui doit etre utilisée au paramètre indiquant le nom du serveur BIP pour les méthodes de la classe parent">
		<cfreturn "N.D (Valeur par défaut)">
	</cffunction>
	
	<cffunction access="public" name="getContainerXmlDefinition" returntype="xml"
	hint="Retourne la définition XML qui est stockée par l'API Reporting avec l'API Container">
		<cfxml variable="containerXmlDefinition">
			<TASK>
				<UUID></UUID>
				<PARAMETERS>
					<!-- PARAMETER TAG DEFINITION :
					<PARAMETER>
						<NAME>PARAM_NAME</NAME>
						<VALUES>
							<VALUE>0</VALUE>
						</VALUES>
						<TYPE>{cv,client}</TYPE>
					</PARAMETER>
					-->
				</PARAMETERS>
			</TASK>
		</cfxml>
		<cfreturn containerXmlDefinition>
	</cffunction>
</cfcomponent>
