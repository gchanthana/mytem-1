<cfcomponent displayname="fr.consotel.api.AbstractApplication" hint="Abstract CFMX Application">
	<!--- COMMON APPLICATION REFERENCE (Auteur : Cedric) :
		PACKAGE METHODS DEPENDENCIES :
			- getAppLogger() : fr.consotel.api.AppProxy
			- getAppTimeTimeOut() : fr.consotel.api.AppProxy
		
		DEFAULT APPLICATION PROPERTIES :
			- NAME : Cannot be an empty string and should be specified with onApplicationInit()
				Otherwise an error is thrown (INVALID_ARGUMENT) and application is not initialized
			- TIMEOUT : 5 secs
			- CLIENT MANAGEMENT : FALSE
			- SESSION MANAGEMENT : FALSE
			- CLIENT COOKIES : FALSE
			- DOMAIN COOKIES : FALSE
			- APPLICATION LOGGER NAME : Application NAME value
			- APPLICATION LOGGER CLASS : fr.consotel.api.utils.logging.Logger
			- APPLICATION PROXY CLASS : fr.consotel.api.AppProxy
			- onError LOGGER NAME : SERVER
			- onError LOGGER CLASS : fr.consotel.api.utils.logging.Logger
			
		PUBLIC APPLICATION KEY :
			- APPLICATION.APP_PROXY_KEY : Value of application key to access its proxy (See fr.consotel.api.AppProxy)
	--->
	
	<cffunction access="public" name="onApplicationStart" returntype="boolean" hint="ON_APPLICATION_START handler">
		<cfset startTick=getTickCount()>
		<cftrace category="DEBUG" type="information" text="Starting application #THIS.NAME#. Initiated by remote address : #CGI.REMOTE_ADDR#">
		<!--- APPLICATION PROXY ACCESS KEY : Value of application key to access its proxy (See fr.consotel.api.AppProxy) --->
		<cfset APPLICATION.APP_PROXY_KEY="APPLICATION_PROXY">
		<!--- LOGGING --->
		<cfset setApplicationLogger()>
		<!--- APPLICATION PROXY --->
		<cfset setApplicationProxy()>
		<!--- SERVER TEST INFOS : Test server capabilities --->
		<cfset logServerInfos()>
		<!--- STARTING SUCCESS : Display application starting duration in ms --->
		<cfset startTick=getTickCount() - startTick>
		<cfset getAppLogger().logInfo(logger.FINEST,"Application #THIS.NAME# : STARTED (#startTick# ms)")>
		<cfreturn TRUE>
	</cffunction>
	
	<cffunction access="public" name="onApplicationEnd" returntype="void" hint="ON_APPLICATION_END handler">
	    <cfargument name="applicationScope" type="any" required="true"/>
	    <cfset endTick=getTickCount()>
	    <cfset endMsg="">
		<cfset logger=createObject("component","fr.consotel.api.utils.logging.Logger").getLogger("SERVER")>
	    <cfif isDefined("arguments.applicationscope.applicationName")>
		    <cfset endMsg="Application #arguments.applicationscope.applicationName#">
		<cfelse>
		    <cfset endMsg="Application of type fr.consotel.api.Application">
		</cfif>
	    <cfset endTick=getTickCount() - endTick>
		<cfset logger.logInfo(logger.INFO,endMsg & " : END (#endTick# ms)")>
	</cffunction>
	
	<cffunction access="public" name="onError" returntype="void" hint="ON_ERROR handler" output="true">
		<cfargument name="exceptionObject" type="any" required="true"/>
		<cfargument name="eventName" type="string" required="false"/>
		<cfoutput>Une erreur s'est produite. Le détails a été envoyé au Support IT</cfoutput>
	</cffunction>
	
	<cffunction name="onRequestStart" returntype="boolean" hint="ON_REQUEST_START handler">
	    <cfargument name="targetPage" type="string" required="true"/>
		<cfreturn TRUE>
	</cffunction>

	<cffunction access="public" name="onRequestEnd" returntype="void" hint="ON_REQUEST_END handler">
	    <cfargument name="targetPage" type="string" required="true"/>
	</cffunction>
	
	<cffunction access="package" name="getAppLogger" returntype="fr.consotel.api.utils.logging.ILogger" hint="Returns this instance logger">
		<cfreturn VARIABLES.LOGGER>
	</cffunction>
	
	<cffunction access="package" name="getAppTimeOut" returntype="date" hint="Returns application timeout">
		<cfreturn THIS.applicationTimeout>
	</cffunction>
	
	<cffunction access="private" name="onApplicationInit" returntype="void"
								hint="Initializes this APPLICATION instance properties. Throws ILLEGAL_ARGUMENT if appName is an empty. Should be called before any other method">
		<cfargument name="appName" type="string" required="true" default="fr.consotel.api.AbstractApplication" displayname="APPLICATION.applicationName" hint="Application NAME">
		<cfargument name="appTimeout" type="date" required="false" displayname="applicationTimeout" hint="Application TIMEOUT. Default is SERVER configuration value">
		<cfargument name="clientManagement" type="boolean" required="false" default="false" displayname="clientManagement" hint="Client Management. Default is FALSE">
		<cfargument name="sessionManagement" type="boolean" required="false" default="false" displayname="sessionManagement" hint="Session Management. Default is FALSE">
		<cfargument name="setClientCookies" type="boolean" required="false" default="false" displayname="setClientCookies" hint="Set Client Cookies. Default is FALSE">
		<cfargument name="setDomainCookies" type="boolean" required="false" default="false" displayname="setDomainCookies" hint="Set Domain Cookies. Default is FALSE">
		<cfset initStartTick=getTickCount()>
		<!--- APPLICATION NAMING --->
		<cfif LEN(TRIM(ARGUMENTS.appName)) GT 0>
			<cfset THIS.name=arguments.appName>
		<cfelse>
			<cfthrow type="Custom" errorcode="INVALID_ARGUMENT" message="Invalid Application Name : #ARGUMENTS.appName#" detail="Application name cannot be an empty string">
		</cfif>
		<!--- CFMX APPLICATION PROPERTIES --->
		<cfif isDefined("ARGUMENTS.appTimeout")>
			<cfset THIS.applicationTimeout=ARGUMENTS.appTimeout>
		</cfif>
		<cfset THIS.clientManagement=ARGUMENTS.clientManagement>
		<cfset THIS.sessionManagement=ARGUMENTS.sessionManagement>
		<cfset THIS.setClientCookies=ARGUMENTS.setClientCookies>
		<cfset THIS.setDomainCookies=ARGUMENTS.setDomainCookies>
		<!--- APPLICATION PROXY CLASS --->
		<cfset VARIABLES.PROXY_CLASS="fr.consotel.api.AppProxy">
		<!--- LOGGING --->
		<cfset VARIABLES.LOGGER_CLASS="fr.consotel.api.utils.logging.Logger">
		<cfset initStartTick=getTickCount() - initStartTick>
		<cftrace category="DEBUG" type="information" text="Application #THIS.NAME# : INITIALIZED (#initStartTick# ms)">
	</cffunction>

	<cffunction access="private" name="setApplicationLogger" returntype="void"
			hint="Set this instance AbstractApplication LOGGING and is called by onApplicationStart. Throws INSTANTIATION_FAILED">
		<cfargument name="loggerClass" type="string" required="false" displayname="LOGGER_CLASS" hint="Logger Class. Default value is used if not specified">
		<cfset isDefault="DEFAULT">
		<cfif isDefined("ARGUMENTS.loggerClass") AND (LEN(TRIM(ARGUMENTS.loggerClass)) GT 0)>
			<cfset VARIABLES.LOGGER_CLASS=ARGUMENTS.loggerClass>
			<cfset isDefault=VARIABLES.LOGGER_CLASS>
		</cfif>
		<cftry>
			<cfset VARIABLES.LOGGER=createObject("component",VARIABLES.LOGGER_CLASS).getLogger(THIS.NAME)>
			<cfcatch type="any">
				<cftrace type="error" category="INSTANTIATION_FAILED" text="Unable to instantiate logger #VARIABLES.LOGGER_CLASS# for #THIS.NAME#">
				<cfrethrow/>
			</cfcatch>
		</cftry>
		<cfset getAppLogger().logInfo(logger.FINEST,"Logger Class set to #isDefault# for #THIS.NAME#")>
	</cffunction>

	<cffunction access="private" name="setApplicationProxy" returntype="void"
			hint="Set this instance AbstractApplication AppProxy and is called by onApplicationStart. Throws INSTANTIATION_FAILED">
		<cfargument name="proxyClass" type="string" required="false" displayname="PROXY_CLASS" hint="Proxy Class. Default value is used if not specified">
		<cfset isDefault="DEFAULT">
		<cfif isDefined("ARGUMENTS.proxyClass") AND (LEN(TRIM(ARGUMENTS.proxyClass)) GT 0)>
			<cfset VARIABLES.PROXY_CLASS=ARGUMENTS.proxyClass>
			<cfset isDefault=VARIABLES.PROXY_CLASS>
		</cfif>
		<cftry>
			<cfset APPLICATION[APPLICATION.APP_PROXY_KEY]=createObject("component",VARIABLES.PROXY_CLASS).setApplicationTarget(THIS)>
			<cfcatch type="any">
				<cftrace type="error" category="INSTANTIATION_FAILED" text="Unable to instantiate proxy #VARIABLES.PROXY_CLASS# for #THIS.NAME#">
				<cfrethrow/>
			</cfcatch>
		</cftry>
		<cfset getAppLogger().logInfo(logger.FINEST,"Proxy Class set to #isDefault# for #THIS.NAME#")>
	</cffunction>
	
	<!--- TODO : REFACTORING in BackOffice.cfc --->
	<cffunction access="private" name="logServerInfos" returntype="void" hint="Log Server Infos">
		<cfset javaLogging=FALSE>
		<cfset internalCFMX=FALSE>
		<!--- TESTING java.util.logging.LogManager --->
		<cftry>
			<cfset tmpObject=createObject("java","java.util.logging.LogManager")>
			<cfset javaLogging=isDefined("tmpObject")>
			<cfcatch type="any">
				<cftrace type="warning" category="WARNING" text="Testing java.util.logging.LogManager produced an exception : NOT SUPPORTED">
			</cfcatch>
		</cftry>
		<!--- TESTING java.util.logging.Log --->
		<cftry>
			<cfset tmpObject=createObject("java","java.util.logging.Logger")>
			<cfset javaLogging=javaLogging AND isDefined("tmpObject")>
			<cfcatch type="any">
				<cftrace type="warning" category="WARNING" text="Testing java.util.logging.Logger produced an exception : NOT SUPPORTED">
			</cfcatch>
		</cftry>
		<!--- TESTING coldfusion.runtime.SessionTracker --->
		<cftry>
			<cfset tmpObject=createObject("java","coldfusion.runtime.SessionTracker")>
			<cfset internalCFMX=isDefined("tmpObject")>
			<cfcatch type="any">
				<cftrace type="warning" category="WARNING" text="Testing coldfusion.runtime.SessionTracker produced an exception : NOT SUPPORTED">
			</cfcatch>
		</cftry>
		<!--- LOG SERVER TEST INFOS --->
		<cftry>
			<cftrace type="information" category="DEBUG"
				text="SERVER INFOS : [DEBUG MODE:#isDebugMode()#] [LOCALE:#getLocale()#] [SUPPORT JAVA LOGGING:#javaLogging#] [SUPPORT CFMX INTERNAL JAVA:#internalCFMX#]">
			<cfcatch type="any">
				<cftrace type="error" category="ERROR" text="Unable to log server test infos">
				<cfrethrow />
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>