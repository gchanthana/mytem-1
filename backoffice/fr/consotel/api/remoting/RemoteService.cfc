<cfcomponent author="Cedric" displayname="fr.consotel.api.remoting.RemoteService" extends="fr.consotel.api.remoting.AxisUtils"
hint="Implémentation d'instanciation et d'invocation d'un Web Service. Les credentials fournis à l'instanciation sont utilisés pour tous les services qui sont instanciés par cette implémentation<br>
Les services sont instanciés par concatenation de l'URI du contexte commun avec le nom du service ciblé (e.g : protocol://dns/context-path/ServiceName?WSDL)">
	<cffunction access="public" name="getFactory" returntype="fr.consotel.api.remoting.RemoteService"
	hint="Retourne une instance initialisée de cette implémentation. La valeur de getDefaultServiceEndpoint() est utilisée si serviceEndpoint n'est pas renseignée">
		<cfargument name="userLogin" type="String" required="true" hint="Login utilisateur à utiliser pour tous les services du Web Service">
		<cfargument name="userPassword" type="String" required="true" hint="Mot de passe utilisateur à utiliser pour tous les services du Web Service">
		<cfargument name="serviceEndpoint" type="String" required="false" default="" hint="Endpoint du Web Service du Web Service">
		<!--- Credentials pour le Web Service --->
		<cfset setServiceCredentials(ARGUMENTS["userLogin"],ARGUMENTS["userPassword"])>
		<!--- Endpoint du Web Service : Valeur fournie en paramètre ou Valeur par défaut --->
		<cfif NOT structKeyExists(VARIABLES,"WS_ENDPOINT")>
			<cfset var webServiceEndpoint=TRIM(ARGUMENTS["serviceEndpoint"])>
			<cfif webServiceEndpoint EQ "">
				<cfset webServiceEndpoint=getDefaultServiceEndpoint()>
			</cfif>
			<cfset VARIABLES["WS_ENDPOINT"]=webServiceEndpoint>
		</cfif>
		<cfreturn THIS>
	</cffunction>

	<cffunction access="public" name="getServiceEndpoint" returntype="String" hint="Retourne la valeur du Endpoint fourni à l'instanciation">
		<cfif structKeyExists(VARIABLES,"WS_ENDPOINT")>
			<cfreturn VARIABLES["WS_ENDPOINT"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le Endpoint pour le Web Service n'est pas défini">
		</cfif>
	</cffunction>

	<cffunction access="public" name="getDefaultServiceEndpoint" returntype="String"
	hint="Retourne le Endpoint utilisé par défaut. Cette méthode n'est pas implémentée et lève une exception">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée par cette classe">
	</cffunction>

	<cffunction access="public" name="getServiceEndpointDNS" returntype="String" hint="Retourne le DNS du serveur dans l'URI correspondant au Endpoint du Web Service">
		<cfif NOT structKeyExists(VARIABLES,"WS_DNS")>
			<cfset var contextURI=getServiceContextURI()>
			<cfset var wsdlURL=createObject("java","java.net.URL").init(contextURI)>
			<cfset VARIABLES["WS_DNS"]=wsdlURL.getHost() & ":" & wsdlURL.getPort()>
		</cfif>
		<cfreturn VARIABLES["WS_DNS"]>
	</cffunction>

	<cffunction access="private" name="getServiceInstance" returntype="Any" hint="Retourne une instance du Web Service par défaut (org.apache.axis.client.Stub)">
		<cfif NOT structKeyExists(VARIABLES,"WS_INSTANCE")>
			<cfset VARIABLES["WS_INSTANCE"]=createServiceInstance(getServiceEndpoint())>
		</cfif>
		<cfreturn VARIABLES["WS_INSTANCE"]>
	</cffunction>
	
	<cffunction access="private" name="createServiceInstance" returntype="Any" hint="Instancie et retourne une instance du Web Service indiqué">
		<cfargument name="serviceEndpoint" type="String" required="true" hint="Endpoint du WebService à instancier">
		<cfreturn createObject("webservice",ARGUMENTS["serviceEndpoint"])>
	</cffunction>
	
	<cffunction access="private" name="getServiceContextURI" returntype="String"
	hint="Retourne l'URI du context du WSDL correspondant à getServiceEndpoint() et sans le nom du service (e.g: protocol://dns/context-path)">
		<cfif NOT structKeyExists(VARIABLES,"WS_CONTEXT_URI")>
			<cfset var wsStub=getServiceInstance()>
			<cfset var localPortName=wsStub.getPortName()><!--- PortName  --->
			<cfset var stubService=wsStub._getService()><!--- Service --->
			<cfset var serviceClass=stubService.getClass()><!--- Classe du service (Introspection Java) --->
			<cfset var getEndpointMethod=serviceClass.getMethod("get" & localPortName & "Address",[])><!--- Invocation par instrospection Java --->
			<cfset var wsdlAddr=getEndpointMethod.invoke(stubService,[])><!--- URL HTTP(S) du WSDL --->
			<cfset var urlLen=listLen(wsdlAddr,"/")>
			<cfset VARIABLES["WS_CONTEXT_URI"]=listDeleteAt(DUPLICATE(wsdlAddr),urlLen,"/")>
		</cfif>
		<cfreturn VARIABLES["WS_CONTEXT_URI"]>
	</cffunction>
	
	<cffunction access="private" name="getServiceCredentials" returntype="Struct" hint="Retourne une structure contenant les clés suivantes :
	- USER : Login utilisateur utilisé pour tous les services du Web Service
	- PWD : Mot de passe utilisateur utilisé pour tous les services du Web Service">
		<cflock scope="REQUEST" type="readonly" timeout="300" throwontimeout="TRUE">
			<cfif structKeyExists(VARIABLES,"WS_CREDENTIALS")>
				<cfreturn VARIABLES["WS_CREDENTIALS"]>
			<cfelse>
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Les credentials pour le Web Service ne sont pas définis">
			</cfif>
		</cflock>
	</cffunction>
	
	<cffunction access="private" name="setServiceCredentials" returntype="void" hint="Remplace la valeur des credentials d'authentification">
		<cfargument name="userLogin" type="String" required="true" hint="Login utilisateur à utiliser pour tous les services du Web Service">
		<cfargument name="userPassword" type="String" required="true" hint="Mot de passe utilisateur à utiliser pour tous les services du Web Service">
		<!--- Credentials pour le Web Service --->
		<cflock scope="REQUEST" type="exclusive" timeout="300" throwontimeout="TRUE">
			<cfset VARIABLES["WS_CREDENTIALS"]={
				USER=ARGUMENTS["userLogin"], PWD=ARGUMENTS["userPassword"]
			}>
		</cflock>
	</cffunction>
</cfcomponent>