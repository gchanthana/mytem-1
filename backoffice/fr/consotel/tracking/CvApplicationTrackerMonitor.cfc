<cfcomponent name="CvApplicationTrackerMonitor">
	<cffunction name="getServerDirectory" access="remote" returntype="xml" output="true">
		<cfset serverXmlDirectory = XmlNew()>
		<cfset serverXmlDirectory.xmlRoot = XmlElemNew(serverXmlDirectory,"node")>
		<cfset rootNode = serverXmlDirectory.node>
		<cfset rootNode.XmlAttributes.LABEL = "Applications ConsoTel">
		<cfset rootNode.XmlAttributes.NODE_ID = 0>
		<!--- Serveur CRAPIERA --->
		<cfset rootNode.XmlChildren[1] = XmlElemNew(serverXmlDirectory,"node")>
		<cfset rootNode.XmlChildren[1].XmlAttributes.LABEL = "Serveur CRAPIERA">
		<cfset rootNode.XmlChildren[1].XmlAttributes.NODE_ID = 1>
		<cfset rootNode.XmlChildren[1].XmlAttributes.ADMIN_EMAIL = "brice.miramont@consotel.fr">
		<cfset rootNode.XmlChildren[1].XmlAttributes.TRACKING_ENABLED = 1>
		<cfset rootNode.XmlChildren[1].XmlAttributes.TRACKING_SERVICE_KEY = "CV_TRACKING_SERVICE_CRAPIERA">
		<cfset rootNode.XmlChildren[1].XmlAttributes.BACK_OFFICE_SERVER_ADDR = "crapiera.consotel.fr:8080">
		<!--- Serveur DEV --->
		<cfset rootNode.XmlChildren[2] = XmlElemNew(serverXmlDirectory,"node")>
		<cfset rootNode.XmlChildren[2].XmlAttributes.LABEL = "Serveur de Dev">
		<cfset rootNode.XmlChildren[2].XmlAttributes.NODE_ID = 2>
		<cfset rootNode.XmlChildren[2].XmlAttributes.ADMIN_EMAIL = "brice.miramont@consotel.fr">
		<cfset rootNode.XmlChildren[2].XmlAttributes.TRACKING_ENABLED = 1>
		<cfset rootNode.XmlChildren[2].XmlAttributes.TRACKING_SERVICE_KEY = "CV_TRACKING_SERVICE_DEV">
		<cfset rootNode.XmlChildren[2].XmlAttributes.BACK_OFFICE_SERVER_ADDR = "backoffice-dev.consotel.fr">
		<!--- Serveur KEASH --->
		<cfset rootNode.XmlChildren[3] = XmlElemNew(serverXmlDirectory,"node")>
		<cfset rootNode.XmlChildren[3].XmlAttributes.LABEL = "Serveur KEASH">
		<cfset rootNode.XmlChildren[3].XmlAttributes.NODE_ID = 3>
		<cfset rootNode.XmlChildren[3].XmlAttributes.ADMIN_EMAIL = "brice.miramont@consotel.fr">
		<cfset rootNode.XmlChildren[3].XmlAttributes.TRACKING_ENABLED = 1>
		<cfset rootNode.XmlChildren[3].XmlAttributes.TRACKING_SERVICE_KEY = "CV_TRACKING_SERVICE_KEASH">
		<cfset rootNode.XmlChildren[3].XmlAttributes.BACK_OFFICE_SERVER_ADDR = "192.168.3.25">
		<!--- Serveur PROD --->
		<cfset rootNode.XmlChildren[4] = XmlElemNew(serverXmlDirectory,"node")>
		<cfset rootNode.XmlChildren[4].XmlAttributes.LABEL = "Serveur de Prod">
		<cfset rootNode.XmlChildren[4].XmlAttributes.NODE_ID = 4>
		<cfset rootNode.XmlChildren[4].XmlAttributes.ADMIN_EMAIL = "brice.miramont@consotel.fr">
		<cfset rootNode.XmlChildren[4].XmlAttributes.TRACKING_ENABLED = 1>
		<cfset rootNode.XmlChildren[4].XmlAttributes.TRACKING_SERVICE_KEY = "CV_TRACKING_SERVICE_PROD">
		<cfset rootNode.XmlChildren[4].XmlAttributes.BACK_OFFICE_SERVER_ADDR = "backoffice.consotel.fr">
		<cfreturn serverXmlDirectory>
	</cffunction>
	
	<cffunction name="getGlobalDirectory" access="remote" returntype="struct" output="true">
		<cfset appKeyStruct = structNew()>
		<cfset webAppStruct = structNew()>
		<!---============= CONSOVIEW V3.0 =============--->
		<cfset webAppStruct.CONSOVIEW_V3 = structNew()>
		<cfset webAppStruct.CONSOVIEW_V3.APP_INFOS = structNew()>
		<cfset webAppStruct.CONSOVIEW_V3.APP_INFOS.APP_KEY = "ConsoTel ConsoViewBackOffice V3.0">
		<cfset webAppStruct.CONSOVIEW_V3.IT_INFOS = structNew()>
		<cfset webAppStruct.CONSOVIEW_V3.IT_INFOS.RESP_IT = "Brice Miramont">
		<cfset webAppStruct.CONSOVIEW_V3.IT_INFOS.RESP_IT_EMAIL = "brice.miramont@consotel.fr">
		<!--- LISTE DES SERVEURS --->
		<cfset webAppStruct.CONSOVIEW_V3.APP_INFOS.SERVERS = structNew()>
		<!--- SERVEUR CRAPIERA --->
		<cfset structInsert(webAppStruct.CONSOVIEW_V3.APP_INFOS.SERVERS,"Serveur CRAPIERA",structNew())>
		<cfset structInsert(webAppStruct.CONSOVIEW_V3.APP_INFOS.SERVERS['Serveur CRAPIERA'],"FRONT_OFFICE","crapiera.consotel.fr")>
		<cfset structInsert(webAppStruct.CONSOVIEW_V3.APP_INFOS.SERVERS['Serveur CRAPIERA'],"BACK_OFFICE","crapiera.consotel.fr:8080")>
		<cfset structInsert(webAppStruct.CONSOVIEW_V3.APP_INFOS.SERVERS['Serveur CRAPIERA'],
							"TRACKING_WEB_SERVICE","CV_TRACKING_SERVICE_CRAPIERA")>
		<!--- SERVEUR KEASH--->
		<cfset structInsert(webAppStruct.CONSOVIEW_V3.APP_INFOS.SERVERS,"Serveur KEASH",structNew())>
		<cfset structInsert(webAppStruct.CONSOVIEW_V3.APP_INFOS.SERVERS['Serveur KEASH'],"FRONT_OFFICE","192.168.3.25:8080")>
		<cfset structInsert(webAppStruct.CONSOVIEW_V3.APP_INFOS.SERVERS['Serveur KEASH'],"BACK_OFFICE","192.168.3.25")>
		<cfset structInsert(webAppStruct.CONSOVIEW_V3.APP_INFOS.SERVERS['Serveur KEASH'],
							"TRACKING_WEB_SERVICE","CV_TRACKING_SERVICE_KEASH")>
		<!--- SERVEUR DEV --->
		<cfset structInsert(webAppStruct.CONSOVIEW_V3.APP_INFOS.SERVERS,"Serveur DEV",structNew())>
		<cfset structInsert(webAppStruct.CONSOVIEW_V3.APP_INFOS.SERVERS['Serveur DEV'],"FRONT_OFFICE","192.168.3.128")>
		<cfset structInsert(webAppStruct.CONSOVIEW_V3.APP_INFOS.SERVERS['Serveur DEV'],"BACK_OFFICE","backoffice-dev.consotel.fr")>
		<cfset structInsert(webAppStruct.CONSOVIEW_V3.APP_INFOS.SERVERS['Serveur DEV'],
							"TRACKING_WEB_SERVICE","CV_TRACKING_SERVICE_DEV")>
		<!--- SERVEUR PROD --->
		<cfset structInsert(webAppStruct.CONSOVIEW_V3.APP_INFOS.SERVERS,"Serveur PROD",structNew())>
		<cfset structInsert(webAppStruct.CONSOVIEW_V3.APP_INFOS.SERVERS['Serveur PROD'],"FRONT_OFFICE","N.D")>
		<cfset structInsert(webAppStruct.CONSOVIEW_V3.APP_INFOS.SERVERS['Serveur PROD'],"BACK_OFFICE","backoffice.consotel.fr")>
		<cfset structInsert(webAppStruct.CONSOVIEW_V3.APP_INFOS.SERVERS['Serveur PROD'],
							"TRACKING_WEB_SERVICE","CV_TRACKING_SERVICE_PROD")>
		
		<!---============= MMC =============--->
		<cfset webAppStruct.MOBILE_MANAGER_V1 = structNew()>
		<cfset webAppStruct.MOBILE_MANAGER_V1.APP_INFOS = structNew()>
		<cfset webAppStruct.MOBILE_MANAGER_V1.APP_INFOS.APP_KEY = "MMCV1">
		<cfset webAppStruct.MOBILE_MANAGER_V1.IT_INFOS = structNew()>
		<cfset webAppStruct.MOBILE_MANAGER_V1.IT_INFOS.RESP_IT = "Brice Miramont">
		<cfset webAppStruct.MOBILE_MANAGER_V1.IT_INFOS.RESP_IT_EMAIL = "brice.miramont@consotel.fr">
		<!--- LISTE DES SERVEURS --->
		<cfset webAppStruct.MOBILE_MANAGER_V1.APP_INFOS.SERVERS = structNew()>
		<!--- SERVEUR DEV --->
		<cfset structInsert(webAppStruct.MOBILE_MANAGER_V1.APP_INFOS.SERVERS,"Serveur DEV",structNew())>
		<cfset structInsert(webAppStruct.MOBILE_MANAGER_V1.APP_INFOS.SERVERS['Serveur DEV'],"FRONT_OFFICE","N.D")>
		<cfset structInsert(webAppStruct.MOBILE_MANAGER_V1.APP_INFOS.SERVERS['Serveur DEV'],"BACK_OFFICE","N.D")>
		<cfset structInsert(webAppStruct.MOBILE_MANAGER_V1.APP_INFOS.SERVERS['Serveur DEV'],
							"TRACKING_WEB_SERVICE","N.D")>
		
		<!--- Annuaire des appli web ConsoTel --->
		<cfset appKeyStruct.WEB_APPS = webAppStruct>
  		<cfreturn appKeyStruct>
	</cffunction>
	
	<cffunction name="getApplicationsFrom" access="remote" returntype="query" output="true">
		<cfargument name="WEB_SERVICE_KEY" required="true" type="string"/>
		<cfobject webservice="#WEB_SERVICE_KEY#" name="webServiceObject">
  		<cfset appArray = webServiceObject.getApplications()>
		<cfset appQuery =
				queryNew("APPLICATION_KEY,ELAPSED_TIME,SERVER_NAME,SERVER_SOFTWARE",
						"VARCHAR,INTEGER,VARCHAR,VARCHAR")>
		<cfif arrayLen(appArray) GT 0>
			<cfset queryAddRow(appQuery,arrayLen(appArray))>
		</cfif>
		<cfloop index="i" from="1" to="#arrayLen(appArray)#">
			<cfset querySetCell(appQuery,"APPLICATION_KEY",appArray[i].APPLICATION_KEY,i)>
			<cfset querySetCell(appQuery,"ELAPSED_TIME",(appArray[i].ELAPSED_TIME_SECONDS/60),i)>
			<cfset querySetCell(appQuery,"SERVER_NAME",appArray[i].SERVER_NAME,i)>
			<cfset querySetCell(appQuery,"SERVER_SOFTWARE",appArray[i].SERVER_SOFTWARE,i)>
			<cfset i = i + 1>
		</cfloop>
		<cfreturn appQuery>
	</cffunction>
	
	<cffunction name="getSessionsFrom" access="remote" returntype="any" output="true">
		<cfargument name="WEB_SERVICE_KEY" required="true" type="string"/>
		<cfargument name="APPLICATION_KEY_STRING" required="true" type="string"/>
		<cfobject webservice="#WEB_SERVICE_KEY#" name="webServiceObject">
  		<cfreturn webServiceObject.getSessions(APPLICATION_KEY_STRING)>
	</cffunction>

	<cffunction name="getSessionsByKeyFrom" access="remote" returntype="struct" output="true">
		<cfargument name="WEB_SERVICE_KEY" required="true" type="string"/>
		<cfargument name="APPLICATION_KEY_STRING" required="true" type="string"/>
 		<cfargument name="SESSION_KEY" required="true" type="string"/>
		<cfobject webservice="#WEB_SERVICE_KEY#" name="webServiceObject">
  		<cfreturn webServiceObject.getSessionsByKey(APPLICATION_KEY_STRING,SESSION_KEY)>
	</cffunction>
	
 	<cffunction name="getSessionsFromAppTypeFrom" access="remote" returntype="query" output="true">
		<cfargument name="WEB_SERVICE_KEY" required="true" type="string"/>
 		<cfargument name="APPLICATION_KEY_STRING" required="true" type="string"/>
 		<cfargument name="APPLICATION_STRATEGY_TYPE" required="true" type="string"/>
		<cfobject webservice="#WEB_SERVICE_KEY#" name="webServiceObject">
  		<cfset sessionArray = webServiceObject.getSessionsFromAppType(APPLICATION_KEY_STRING,APPLICATION_STRATEGY_TYPE)>
		<cfset sessionQuery =
				queryNew("APPLICATION_KEY,CLIENTACCESSID,ELAPSED_TIME,EMAIL,NOM,PRENOM,REMOTE_HOST,SESSION_KEY,STATUS",
						"VARCHAR,INTEGER,INTEGER,VARCHAR,VARCHAR,VARCHAR,VARCHAR,VARCHAR,INTEGER")>
		<cfif arrayLen(sessionArray) GT 0>
			<cfset queryAddRow(sessionQuery,arrayLen(sessionArray))>
		</cfif>
		<cfloop index="i" from="1" to="#arrayLen(sessionArray)#">
			<cfset querySetCell(sessionQuery,"APPLICATION_KEY",sessionArray[i].APPLICATION_KEY,i)>
			<cfset querySetCell(sessionQuery,"CLIENTACCESSID",sessionArray[i].CLIENTACCESSID,i)>
			<cfset querySetCell(sessionQuery,"ELAPSED_TIME",(sessionArray[i].ELAPSED_TIME_SECONDS / 60),i)>
			<cfset querySetCell(sessionQuery,"EMAIL",sessionArray[i].EMAIL,i)>
			<cfset querySetCell(sessionQuery,"NOM",sessionArray[i].NOM,i)>
			<cfset querySetCell(sessionQuery,"PRENOM",sessionArray[i].PRENOM,i)>
			<cfset querySetCell(sessionQuery,"REMOTE_HOST",sessionArray[i].REMOTE_HOST,i)>
			<cfset querySetCell(sessionQuery,"SESSION_KEY",sessionArray[i].SESSION_KEY,i)>
			<cfset querySetCell(sessionQuery,"STATUS",sessionArray[i].STATUS,i)>
			<cfset i = i + 1>
		</cfloop>
		<cfreturn sessionQuery>
	</cffunction>
</cfcomponent>
