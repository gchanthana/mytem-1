<cfcomponent name="SessionTracker" alias="fr.consotel.util.consoview.tracking.SessionTracker">
	<!--- Clé de l'application CFMX de ConsoView v3 --->
			
	<cfif structKeyExists(COOKIE,"CODEAPPLICATION")>
		<cfswitch expression="#COOKIE.CODEAPPLICATION#">
					
				<cfcase value="1"> 
					<cfset appliName = "CONSOVIEW_V4.0.1">
				</cfcase>
	
				<cfcase value="101"> 
					<cfset appliName = "MYTEM360_V4.0.1">
				</cfcase>
				
				<cfcase value="51"> 
					<cfset appliName = "PILOTAGE">
				</cfcase>
				
				<cfdefaultcase>
					<cfset appliName = "AUTRE">
				</cfdefaultcase>
				
			</cfswitch>
	<cfelse>
		<cfset appliName = "PRE-LOGIN">
	</cfif>	
		
	

	
	<cfset DSN = "ROCOFFRE">
	
	<cfset appliName = appliName & "-PROD">
	
	<cfset consoviewKey = CGI.SERVER_NAME & " : " & appliName>
	<cfset logDirectory=expandPath("/") & "fr/consotel/util/consoview/tracking/log/">
	<cfset logFile=logDirectory & appliName & "-" & lsDateFormat(NOW(),"YYYY-MM-DD") & "-2.html">

	<cffunction name="remotingLog" access="remote" returntype="numeric" hint="Infos issus du remoting">
		<cfargument name="eventCode" type="string" required="true">
		<cfargument name="errorCode" type="string" required="true">
		<cfargument name="emailString" type="string" required="true">
		<cfargument name="componentName" type="string" required="true">
		<cfargument name="methodName" type="string" required="true">
		<cfargument name="LOG_STRING_VAR" type="string" required="true">
		<cfargument name="LOG_DESC_VAR" type="string" required="true">
		<cfargument name="paramArray" type="any">
		
		<cfset paramNb=0>
		<cfset paramString = "()">
		<cfset finalErrorCode=0>
<cftry>
		<cfif isDefined("arguments.paramArray")>
			<cfset paramNb=arrayLen(paramArray)>
			<cfset paramString = "(">
			<cfif paramNb GT 0>
				<cfloop index="i" from="1" to="#(paramNb - 1)#">
					<cfset paramString = paramString & paramArray[i] & ",">
				</cfloop>
				<cfset paramString = paramString & paramArray[i] & ")">
			<cfelse>
				<cfset paramString = paramString & ")">
			</cfif>
		</cfif>
		<cfset nowString = lsTimeFormat(NOW(),"HH:mm:ss") & " - " & lsDateFormat(NOW(),"DD/MM/YYYY")>
		<!--- MODIFICATION DE LOG DES ERREURS --->
		<cfset isFault=-1>
		<cfset finalErrorCode = errorCode>
		<cfif finalErrorCode EQ 0>
			<cfset isFault=FIND("FAULT",eventCode,1)>
			<cfif isFault GT 0>
				<cfset finalErrorCode=1>
			</cfif>
		</cfif>
		<cfset clientIDADDR = COOKIE.IP_ORIGINE>
		<cfset initInfos=nowString & ";" &
						consoviewKey & ";" &
						eventCode & ";" &
						clientIDADDR & ";" &
						emailString & ";" &
						componentName & ";" &
						methodName & ";" &
						paramNb & ";" &
						paramString & ";" &
						finalErrorCode>
		<!--- ******************************** --->

		<cfif (LEN(LOG_STRING_VAR) + LEN(LOG_DESC_VAR)) EQ 0>
			<cfset DETAILLED_LOG= ''>
		<cfelse>
			<cfset DETAILLED_LOG= "[" & LOG_STRING_VAR & "] - " & LOG_DESC_VAR>
		</cfif>
		<cfquery name="qInsertLogEvent" datasource="#DSN#" >
			INSERT INTO LOG_REPORT(date_log,app_log,code_erreur,event_log,ipaddr,login_log,classe_log,methode_log,DESCRIPTION_LOG,DESCRIPTION_LOG_LONG)
			VALUES (sysdate,'#consoviewKey#',#VAL(finalErrorCode)#,'#eventCode#','#LEFT(clientIDADDR,30)#','#emailString#','#LEFT(componentName,200)#','#methodName#','#LEFT(TRIM(DETAILLED_LOG),1000)#','#LEFT(TRIM(DETAILLED_LOG),1000)#')
		</cfquery>

		<cffile action="append" charset="utf-8" file="#logFile#" output="#initInfos#" addnewline="true">
		<!--- 
		<cflog type="Information" text="TRACKING : #componentName# - #methodName# - #paramNb# - #paramString#">
		 --->
	<cfcatch>
		<cfif (LEN(LOG_STRING_VAR) + LEN(LOG_DESC_VAR)) EQ 0>
			<cfset DETAILLED_LOG= ''>
		<cfelse>
			<cfset DETAILLED_LOG= "[" & LOG_STRING_VAR & "] - " & LOG_DESC_VAR>
		</cfif>
	<cfquery name="qInsertLogEvent" datasource="#DSN#" >
			INSERT INTO LOG_REPORT(date_log,app_log,code_erreur,event_log,ipaddr,login_log,classe_log,methode_log,DESCRIPTION_LOG,DESCRIPTION_LOG_LONG)
			VALUES (sysdate,'#consoviewKey#',#errorCode#,'#eventCode#','#LEFT(COOKIE.IP_ORIGINE,30)#','#emailString#','#LEFT(componentName,200)#','#methodName#','#LEFT(TRIM(DETAILLED_LOG),1000)#','#LEFT(TRIM(DETAILLED_LOG),1000)#')
	</cfquery>	
	<cfset finalErrorCode=0>
		<cfreturn finalErrorCode>
	</cfcatch>
	</cftry>
	<cfreturn finalErrorCode>
</cffunction>
	

</cfcomponent>
