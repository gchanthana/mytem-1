<cfcomponent name="MailUtil" alias="fr.consotel.util.consoview.mail.MailUtil">
	<cffunction name="sendSingleMail" access="remote" returntype="void" output="true">
		<cfargument name="arr" type="array" required="true">
		<cfif arr[8] eq "YES">
			<cfset arr[3]=arr[3] & "," & arr[1]>
		</cfif>
		<cfmail from="support@consotel.fr"
				to="#arr[2]#"
				subject="#arr[5]# : #arr[6]#"
				type="html"
				charset="utf-8"
				wraptext="72"
				bcc="#arr[4]#"
				cc="#arr[3]#"
				replyto="#arr[1]#"
				server="192.168.3.122">
				#arr[7]#
		</cfmail>
	 </cffunction>
</cfcomponent>
