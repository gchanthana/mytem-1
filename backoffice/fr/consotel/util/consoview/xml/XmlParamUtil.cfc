<!--- pour surround --->
<cfcomponent name="XmlParamUtil" alias="fr.consotel.util.consoview.xml.XmlParamUtil">
	<cffunction name="getTestXmlParams" access="remote" returntype="XML">
		<cfxml variable="xmlParamObject">
			<PARAMS>
			  <PARAM>
			    <KEY>IDGROUPE_CLIENT</KEY>
			    <DESC>Identifiant du groupe racine</DESC>
			    <VALUES>
			      <VALUE>1463</VALUE>
			    </VALUES>
			  </PARAM>
			  <PARAM>
			    <KEY>LIBELLE_ORGA</KEY>
			    <DESC>Libellé de l'orga</DESC>
			    <VALUES>
			      <VALUE>Orga Test Géo</VALUE>
			    </VALUES>
			  </PARAM>
			  <PARAM>
			    <KEY>DATE_AFFECT</KEY>
			    <DESC>Date d'affectation</DESC>
			    <VALUES>
			      <VALUE>2008/02/28</VALUE>
			    </VALUES>
			  </PARAM>
			  <PARAM>
			    <KEY>LIST_ORGA</KEY>
			    <DESC>Orga sur lesquelles on affecte les lignes</DESC>
			    <VALUES/>
			    <!--- 
			    <VALUES>
			      <VALUE>102</VALUE>
			    </VALUES>
			     --->
			  </PARAM>
			  <PARAM>
			    <KEY>IDSOUS_TETE</KEY>
			    <DESC>Identifiant des lignes à affecter</DESC>
			    <VALUES>
			      <VALUE>1001</VALUE>
			      <VALUE>1002</VALUE>
			      <VALUE>1003</VALUE>
			    </VALUES>
			  </PARAM>
			  <PARAM>
			    <KEY>USAGE</KEY>
			    <DESC>Usage des lignes</DESC>
			    <VALUES>
			      <VALUE>Ligne Secours</VALUE>
			      <VALUE/>
			      <VALUE>Standard Téléphonique</VALUE>
			    </VALUES>
			  </PARAM>
			</PARAMS>
		</cfxml>
		<cfreturn xmlParamObject>
	</cffunction>
	
	<cffunction name="testMethod" access="remote" returntype="Array">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfset keyValues=getParamValuesByKey(xmlParamObject,"MY_KEY")>
		<cfset tmpParamKey=keyValues[1].xmlText>
		<cfreturn getParamValuesByKey(xmlParamObject,tmpParamKey)>
	</cffunction>
	
	<cffunction name="getParamValuesByKey" access="remote" returntype="array">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfargument name="paramKey" required="true" type="String">
		<cfset keyValues = XMLSearch(xmlParamObject,"//PARAM[child::KEY='#paramKey#']/VALUES")>
		<cfif arrayLen(keyValues) LTE 0>
			<cfthrow type="NO_SUCH_PARAM_KEY" message="XmlParamUtil - No such param key : #paramKey#" detail="1008">
		<cfelse>
			<cfreturn keyValues[1].xmlChildren>
		</cfif>
	</cffunction>
	
	<cffunction name="getLogParamValuesByKey" access="remote" returntype="array">
		<cfargument name="xmlParamObject" required="true" type="XML">
		<cfargument name="paramKey" required="true" type="String">
		<cfset keyValues = XMLSearch(xmlParamObject,"//PARAM[child::KEY='#paramKey#' and child::USE!='METHOD']/VALUES")>
		<cfif arrayLen(keyValues) LTE 0>
			<cfthrow type="NO_SUCH_LOG_PARAM_KEY" message="XmlParamUtil - No such LOG or ALL param key : #paramKey#" detail="1008">
		<cfelse>
			<cfreturn keyValues[1].xmlChildren>
		</cfif>
	</cffunction>
</cfcomponent>
