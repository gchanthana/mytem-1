<cfcomponent output="false">

<!--- Procédure --->

	<cffunction name="zQueryCheckLog" access="remote" returntype="Numeric">
		<cfargument name="id" type="string" required="true">
		<cfargument name="pwd" type="string" required="true">
	
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_tellcost.validatelogin">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_login" value="#id#">   
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_pwd" value="#pwd#">                                                
	           	<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_idsous_tete">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_idperiode_mois">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_idtheme"> 
				<cfprocparam type="Out" cfsqltype="CF_SQL_VARCHAR" variable="p_libelle_mois">   
			</cfstoredproc>

			<cfif p_idsous_tete EQ 0>

				<cfset retour = 0>
			
			<cfelse>
				
				<cfset SESSION.IDSOUS_TETE = p_idsous_tete>
				<cfset SESSION.IDRACINE_MASTER = p_idracine_master>
				<cfset SESSION.IDPERIODE = p_idperiode_mois> 
				<cfset SESSION.IDTHEME = p_idtheme>
				<cfset SESSION.LIBELLE_MOIS = p_libelle_mois>
				<cfset retour = 1>
				
			</cfif>
			

		<cfreturn retour> 
	</cffunction> 

	<cffunction name="zQueryConso" access="remote" returntype="Query">
		<cfargument name="affichageID" type="Numeric" required="true">

		 	<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_tellcost.getConso">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" variable="p_idsous_tete" value="#SESSION.IDSOUS_TETE#">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" variable="p_idracine_master" value="#SESSION.IDRACINE_MASTER#">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" variable="p_idperiode" value="#SESSION.IDPERIODE#">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" variable="p_idtype_affichage" value="#affichageID#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<cffunction name="zQueryAbo" access="remote" returntype="Query">
		
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_tellcost.getabo">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" variable="p_idsous_tete" value="#SESSION.IDSOUS_TETE#">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" variable="p_idracine_master" value="#SESSION.IDRACINE_MASTER#">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" variable="p_idperiode" value="#SESSION.IDPERIODE#">  
				<cfprocresult name="p_retour">
			</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction> 

	<cffunction name="zQueryGetConseil" access="remote" returntype="Query">

		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_tellcost.getConseilTheme">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idtheme" value="#SESSION.IDTHEME#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		
		<cfset SESSION.themeID = p_retour['THEMEID']>
		<cfset SESSION.SHORT_THEME = p_retour['SHORT_DESC']>
		<cfset SESSION.LONG_THEME = p_retour['LONG_DESC']>
		<cfreturn p_retour>
	</cffunction> 
	
<!--- Fin procédure --->

</cfcomponent>