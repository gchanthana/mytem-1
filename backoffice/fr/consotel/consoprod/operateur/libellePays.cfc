<cfcomponent name="libellePays">

	<cffunction name="getOperateur" returntype="query">
		<cfquery name="qoperateur" datasource="#session.OFFREDSN#">
			select distinct lpo.operateurid, op.nom 
			from libelle_pays_operateur lpo, operateur op
			where op.operateurid=lpo.operateurid
			order by lower(op.nom)
		</cfquery>
		<cfquery dbtype="query" name="results">
			SELECT nom AS label, operateurid AS data
			FROM qoperateur
			ORDER BY label
		</cfquery>
		<cfreturn results>
	</cffunction>
	
	<cffunction name="getPays" returntype="query">
		<cfargument name="data" type="array" required="false">
		<cfquery name="Liste_pays_consotel" datasource="#session.OFFREDSN#">
			SELECT paysconsotelid, paysid,  TRIM(pays)  AS pays , decode(a_calculer,0,'non','oui') as a_calculer
			FROM (
					SELECT paysconsotelid, 0 AS paysid, decode(paysconsotelid, 4, ' Autres GSM', pays) AS pays, a_calculer
					FROM pays_consotel 
					WHERE lower(pays) NOT LIKE '% zone%'
							AND lower(decode(paysconsotelid, 4, ' Autres GSM', pays)) like lower('%#data[1]#%')
					UNION
					SELECT pc.paysconsotelid, p.paysid, trim(p.pays) as pays, pc.a_calculer
					FROM pays_consotel pc, pays p
					WHERE pc.paysconsotelid=p.paysconsotelid AND pc.paysconsotelid=4
							AND lower(p.pays) like lower('%#data[1]#%')
					union all
					select -1 as paysconsotelid,-1 as paysid, '[A calculer]' AS pays, 1 as a_calculer
					from dual
					where lower('[A calculer]') like lower('%#data[1]#%')
					ORDER BY pays
					)	
		</cfquery>
		<cfquery dbtype="query" name="results">
			SELECT pays AS label, paysconsotelid AS data, paysid, a_calculer
			FROM Liste_pays_consotel
			ORDER BY label
		</cfquery>
		<cfreturn results>
	</cffunction>
	
	<cffunction name="getTousPaysAffectes" returntype="query">
		<cfargument name="data" type="array" required="false">
		<cfquery name="Liste_pays" datasource="#session.OFFREDSN#">
			select lpo1.rowid, lpo1.paysconsotelid,lpo1.paysid,lpo1.lib_pays1, lpo1.lib_pays2,  '-' as nom_pays 
			from libelle_pays_operateur lpo1
			where lpo1.operateurid=#data[1]# AND lpo1.paysconsotelid=0
					AND (lower(lpo1.lib_pays1) like lower('%#data[2]#%') OR lower(lpo1.lib_pays2) like lower('%#data[2]#%'))
			union
			select lpo2.rowid, lpo2.paysconsotelid,lpo2.paysid,lpo2.lib_pays1, lpo2.lib_pays2,  lpc.pays 
			from libelle_pays_operateur lpo2, 
					(
						SELECT paysconsotelid, paysid,  TRIM(pays)  AS pays 
						FROM (
								SELECT paysconsotelid, 0 AS paysid, decode(paysconsotelid, 4, ' Autres GSM', pays) AS pays
								FROM pays_consotel 
								WHERE lower(pays) NOT LIKE '% zone%'
								UNION
								SELECT pc.paysconsotelid, p.paysid, p.pays
								FROM pays_consotel pc, pays p
								WHERE pc.paysconsotelid=p.paysconsotelid AND pc.paysconsotelid=4
								union all
								select -1 AS paysconsotelid,-1 AS paysid, '[A calculer]' AS pays
								from dual
								)	
					) lpc
			where lpo2.paysconsotelid=lpc.paysconsotelid 
					and lpo2.paysid=lpc.paysid 
					and lpo2.operateurid=#data[1]#
					and lpo2.paysconsotelid<>0
					AND (lower(lpo2.lib_pays1) like lower('%#data[2]#%') OR lower(lpo2.lib_pays2) like lower('%#data[2]#%')
					OR lower(lpc.pays) like lower('%#data[2]#%'))
			order by lib_pays1, lib_pays2
		</cfquery>
		<cfreturn Liste_pays>
	</cffunction>
	
	<cffunction name="getPaysAffectes" returntype="query">
		<cfargument name="data" type="array" required="false">
		<cfquery name="Liste_pays" datasource="#session.OFFREDSN#">
			select rowid, lpo.*,  '-' as nom_pays 
			from libelle_pays_operateur lpo
			where operateurid=#data[1]# AND lpo.paysconsotelid=0
			AND (lower(lpo.lib_pays1) like lower('%#data[2]#%') OR lower(lpo.lib_pays2) like lower('%#data[2]#%'))
			order by lib_pays1, lib_pays2
		</cfquery>
		<cfreturn Liste_pays>
	</cffunction>
	
	<cffunction name="createLigne" returntype="string">
		<cfargument name="data" type="array" required="false">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_ADMIN.CREATESOUSTETE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idgroupe_client" value="#data[1]#">		      
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idsous_compte" value="#data[2]#">		      
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_sous_tete" value="#data[3]#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="P_idtype_ligne" value="#data[4]#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="out" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="updateLibelle" returntype="void">
		<cfargument name="data" type="array" required="false">
		<cfquery name="qupdate" datasource="#session.OFFREDSN#">
			update libelle_pays_operateur 
			set paysconsotelid=#data[1]# , paysid=#data[2]#, date_modif=sysdate
			where rowid='#data[3]#'
		</cfquery>
	</cffunction>
</cfcomponent>