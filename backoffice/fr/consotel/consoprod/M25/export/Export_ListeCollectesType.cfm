<!--- Export via BI la liste des template au format (csv,xls...) spécifié
      en fonction du filtre passé en param
	  param : FORM.OPERATEURID : l'id de l'operateur
	          FORM.DATATYPEID : l'id du type de données
	          FORM.RECUPMODEID : l'id du mode de récup --->

<cfset biServer="http://bip-prod.consotel.fr/xmlpserver/services/PublicReportService?WSDL">
	
	<cfset ArrayOfParamNameValues=ArrayNew(1)>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_OPERATEURID">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=FORM.OPERATEURID>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[1]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_IDCOLLECT_TYPE">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=FORM.DATATYPEID>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[2]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_IDCOLLECT_MODE">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=FORM.RECUPMODEID>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[3]=ParamNameValue>
	
	<cfset myParamReportRequest=structNew()>
	
	<cfset myParamReportRequest.reportAbsolutePath="/consoview/M25/ListeCollectes_Type/ListeCollectes_Type.xdo">	
	
	<cfset myParamReportRequest.attributeLocale="fr-FR">
	<cfset myParamReportRequest.attributeFormat=LCase(FORM.FORMAT)>
	<cfset myParamReportRequest.sizeOfDataChunkDownload=-1>
	<cfset myParamReportRequest.parameterNameValues=ArrayOfParamNameValues>
	<cfset myParamReportParameters=structNew()>
	<cfset structInsert(myParamReportParameters,"reportRequest",myParamReportRequest)>
	
	<cfset myParamReportParameters.userID="consoview">
	<cfset myParamReportParameters.password="public">
	<cfinvoke webservice="#biServer#" returnvariable="resultRunReport" method="runReport" argumentCollection="#myParamReportParameters#"/>
	<cfset reporting=createObject("component","fr.consotel.consoview.api.Reporting")>
	<cfset filename = "Liste_Templates"   & reporting.getFormatFileExtension(FORM.FORMAT)>
			
	<cfheader name="Content-Disposition" value="attachment;filename=#filename#" charset="utf-8">	
	<cfif compareNoCase(trim(FORM.FORMAT),"csv") eq 0>
		<cfcontent type="text/csv; charset=utf-8" variable="#resultRunReport.getReportBytes()#">
		<cfelse>
		<cfcontent type="#resultRunReport.getReportContentType()#" variable="#resultRunReport.getReportBytes()#">		
	</cfif>

