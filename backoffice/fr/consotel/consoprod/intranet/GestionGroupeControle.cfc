<cfcomponent displayname="Gestion des Imports" output="false">
	
	<cffunction name="updateProduitQte" access="remote" output="false" returntype="void">
		<cfargument name="id" type="array">
		<cfargument name="data" type="array">
		<cfloop from="1" to="#ArrayLen(id)#" index="i">
			<cfquery name="result" datasource="#SESSION.OffreDSN#">
				update produit_catalogue
				set bool_qte=#data[i]#
				where idproduit_catalogue=#id[i]#
			</cfquery>
		</cfloop>
		<cfreturn />
	</cffunction>
	
	<cffunction name="affectProduits" access="remote" output="false" returntype="void">
		<cfargument name="idgrp_ctl" type="string">
		<cfargument name="data" type="array">
		<cfloop from="1" to="#ArrayLen(data)#" index="i">
			<cfquery name="result" datasource="#SESSION.OffreDSN#">
				update produit_catalogue
				set idgrp_ctl=#idgrp_ctl#
				where idproduit_catalogue=#data[i]#
			</cfquery>
		</cfloop>
		<cfreturn />
	</cffunction>
	
	<cffunction name="desaffectProduits" access="remote" output="false" returntype="void">
		<cfargument name="idgrp_ctl" type="string">
		<cfargument name="data" type="array">
		<cfloop from="1" to="#ArrayLen(data)#" index="i">
			<cfquery name="result" datasource="#SESSION.OffreDSN#">
				update produit_catalogue
				set idgrp_ctl=null
				where idproduit_catalogue=#data[i]#
			</cfquery>
		</cfloop>
		<cfreturn />
	</cffunction>
	
	<cffunction name="getGroupes" access="remote" output="false" returntype="Query">
		<cfargument name="data" type="array">
		<cfquery name="result" datasource="#SESSION.OffreDSN#">
			SELECT *
			FROM grp_ctl
			WHERE operateurid=#data[1]#
			AND grp_segment=#data[2]#
			AND type_grp_ctl=#data[3]#
			order by lower(grp_ctl)
		</cfquery>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getVersionsGroupe" access="remote" output="false" returntype="Query">
		<cfargument name="data" type="array">
		<cfquery name="result" datasource="#SESSION.OffreDSN#">
			SELECT gc.libelle_groupe_client AS libelle, a.nbr
			FROM grp_ctl g, groupe_client gc,
			     (SELECT COUNT(*) AS nbr, idgrp_ctl, idracine
				   FROM version_grp_ctl
					WHERE idgrp_ctl=#data[1]#
					GROUP BY idgrp_ctl, idracine) a
			WHERE g.idgrp_ctl=a.idgrp_ctl
			    AND gc.idgroupe_client=a.idracine
				AND g.idgrp_ctl=#data[1]#
			order by lower(libelle_groupe_client)
		</cfquery>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="AddGroupe" access="remote" output="false" returntype="Query">
		<cfargument name="data" type="array">
		<cfquery name="q1" datasource="#SESSION.OffreDSN#">
			insert into grp_ctl(grp_ctl,operateurid,grp_segment,type_grp_ctl)
			VALUES('#data[1]#',#data[2]#,#data[3]#,#data[4]#)
		</cfquery>
		<cfquery name="result" datasource="#SESSION.OffreDSN#">
			SELECT *
			FROM grp_ctl
			WHERE operateurid=#data[2]#
			AND grp_segment=#data[3]#
			AND type_grp_ctl=#data[4]#
			order by lower(grp_ctl)
		</cfquery>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="DeleteGroupe" access="remote" output="false" returntype="Query">
		<cfargument name="data" type="array">
		<cfquery name="q1" datasource="#SESSION.OffreDSN#">
			update produit_catalogue
			set idgrp_ctl=null
			where idgrp_ctl=#data[1]#
		</cfquery>
		<cfquery name="q1" datasource="#SESSION.OffreDSN#">
			delete from exception_grp_ctl
			where idgrp_ctl=#data[1]#
		</cfquery>
		<cfquery name="q1" datasource="#SESSION.OffreDSN#">
			delete from etat_grp_ctl
			where idgrp_ctl=#data[1]#
		</cfquery>
		<cfquery name="q2" datasource="#SESSION.OffreDSN#">
			delete from grp_ctl
			where idgrp_ctl=#data[1]#
		</cfquery>
		<cfquery name="result" datasource="#SESSION.OffreDSN#">
			SELECT *
			FROM grp_ctl
			WHERE operateurid=#data[2]#
			AND grp_segment=#data[3]#
			AND type_grp_ctl=#data[4]#
			order by lower(grp_ctl)
		</cfquery>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getProduitCat" access="remote" output="false" returntype="Query">
		<cfargument name="data" type="array">
		<cfquery name="result" datasource="#SESSION.OffreDSN#">
			SELECT pca.idproduit_catalogue, pca.code_article, pca.libelle_produit, decode(pca.bool_qte,1,'OUI','NON') as bool_qte, 
				cp.libelle as libelle_cat
			FROM produit_catalogue pca, theme_produit_catalogue tpc, type_produit tp, categorie_produit cp
			WHERE tpc.idtheme_produit=#data[1]#
			AND pca.operateurID=#data[2]#
			AND pca.idproduit_catalogue = tpc.idproduit_catalogue
			AND pca.idtype_produit=tp.idtype_produit
			AND tp.idcategorie_produit=cp.idcategorie_produit
			AND pca.idgrp_ctl is null
			order by lower(pca.libelle_produit)
		</cfquery>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getProduitGroupe" access="remote" output="false" returntype="Query">
		<cfargument name="data" type="array">
		<cfquery name="result" datasource="#SESSION.OffreDSN#">
			SELECT pca.idproduit_catalogue, pca.code_article, pca.libelle_produit, decode(pca.bool_qte,1,'OUI','NON') as bool_qte,
				cp.libelle as libelle_cat
			FROM produit_catalogue pca, theme_produit_catalogue tpc, type_produit tp, categorie_produit cp
			WHERE pca.idproduit_catalogue = tpc.idproduit_catalogue
			AND pca.idgrp_ctl=#data[1]#
			AND pca.idtype_produit=tp.idtype_produit
			AND tp.idcategorie_produit=cp.idcategorie_produit
			order by lower(pca.libelle_produit)
		</cfquery>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getThemes" access="remote" output="false" returntype="Query">
		<cfargument name="operateurID" type="string">
		<cfargument name="typeTheme" type="string">
		<cfif typeTheme eq "1">
			<cfset typeTheme="Abonnements">
		<cfelseif typeTheme eq "2">
			<cfset typeTheme="Consommations">
		</cfif>
		<cfquery name="q1" datasource="#SESSION.OffreDSN#">
			SELECT tp.idtheme_produit, tp.segment_theme || ' / ' || tp.theme_libelle AS theme, ordre_affichage
			FROM theme_produit tp, THEME_PRODUIT_CATALOGUE tpc, produit_catalogue pca
			WHERE idprofil_theme = 1
			      AND tp.idtheme_produit=tpc.idtheme_produit
					AND tpc.idproduit_catalogue=pca.idproduit_catalogue
					AND pca.operateurid=#operateurID#
					and tp.type_theme='#typeTheme#'
			GROUP BY tp.idtheme_produit, tp.segment_theme || ' / ' || tp.theme_libelle, ordre_affichage
			ORDER BY tp.ordre_affichage
		</cfquery>
		<cfquery name="result" dbtype="query">
			select idtheme_produit as data, theme as label
			from q1
		</cfquery>
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="getThemesAndQte" access="remote" output="false" returntype="Query">
		<cfargument name="operateurID" type="string">
		<cfargument name="typeTheme" type="string">
		<cfif typeTheme eq "1">
			<cfset typeTheme="Abonnements">
		<cfelseif typeTheme eq "2">
			<cfset typeTheme="Consommations">
		</cfif>
		<cfquery name="q1" datasource="#SESSION.OffreDSN#">
			
			SELECT tp.idtheme_produit as idtheme,tp.segment_theme || ' / ' || tp.theme_libelle AS theme, tp. ordre_affichage,
			      SUM(decode(nvl(pca.idgrp_ctl,0),0,1,0)) AS prdt_naf,
			      SUM(decode(nvl(pca.idgrp_ctl,0),0,0,1)) AS prdt_aff, tp.type_theme,tp.sur_theme,pca.operateurid			      
			FROM produit_catalogue pca, Theme_Produit_Catalogue tpc, Theme_Produit tp
			WHERE tpc.idproduit_catalogue=pca.idproduit_catalogue
			            AND tp.idtheme_produit=tpc.idtheme_produit(+)
			            AND tp.idprofil_theme=1
			            AND pca.operateurid=#operateurID#
			        	AND tp.type_theme='#typeTheme#'
			GROUP BY  tp.idtheme_produit,tp.segment_theme || ' / ' || tp.theme_libelle, tp. ordre_affichage, tp.type_theme,tp.sur_theme,pca.operateurid
			ORDER BY tp.ordre_affichage        
		</cfquery>
		
		<cfreturn q1>
	</cffunction>
	
	<cffunction name="getListeOperateur" access="remote" returntype="Query" output="false" >
		<cfquery name="getListe" datasource="#SESSION.OffreDSN#">
			select nom , o.operateurID
			from operateur o, compte_facturation cf 
			WHERE o.operateurid=cf.operateurid
			GROUP BY nom , o.operateurID
			order by lower(nom)
		</cfquery>
		<cfquery name="qGetListe" dbtype="query">
			select nom as label, operateurID as data
			from getListe
		</cfquery>
		<cfreturn qGetListe>
	</cffunction>
	
	<cffunction name="addCF" access="remote" output="false" returntype="void">
		<cfargument name="idrefclient" type="string" default="0">
		<cfargument name="operateurID" type="string" default="0">
		<cfargument name="CF" type="string" default="0">
		<cfquery name="result" datasource="#SESSION.OffreDSN#">
			insert into ref_client_operateur(operateurid,idref_client,numero_correspond)
			VALUES(#operateurID#,#idrefclient#,'#CF#')
		</cfquery>
	</cffunction>
	
	<cffunction name="deleteCF" access="remote" output="false" returntype="void">
		<cfargument name="operateurID" type="string" default="">
		<cfargument name="idrefclient" type="string" default="">
		<cfargument name="CF" type="string" default="0">
		<cfquery name="result" datasource="#SESSION.OffreDSN#">
			delete from ref_client_operateur
			where operateurid=#operateurID#
			and idref_client=#idrefclient#
			and numero_correspond='#CF#'
		</cfquery>
	</cffunction>
</cfcomponent>