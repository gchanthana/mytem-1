<cfcomponent output="false" alias="fr.consotel.consoprod.intranet.cataloguepublic.Ressource">

	<cfproperty name="libelleProduit" type="string"/>
	<cfproperty name="produitClientId" type="numeric"/>
	
	<cfproperty name="sousTeteId" type="numeric"/>
	<cfproperty name="sousTete" type="string"/>
	
	<cfproperty name="compteFacturation" type="string"/>
	<cfproperty name="sousCompte" type="string"/>
	
	<cfproperty name="prixUnitaire" type="numeric"/>
	<cfproperty name="dateFin" type="date"/>
	<cfproperty name="dateDebut" type="date"/>
	
	<cfproperty name="dateEntree" type="date"/>
	<cfproperty name="dateSortie" type="date"/>	
	
	<cfproperty name="typeTheme" type="string" />
	<cfproperty name="quantite" type="numeric" />
	<cfproperty name="montant" type="numeric" />
	<cfproperty name="volume" type="numeric" />
	
	<cfscript>
		variables.libelleProduit = "";
		variables.produitClientId = 0;
		variables.sousTeteId = 0;
		variables.sousTete = "";
		variables.prixUnitaire = 0;
		variables.dateDebut = "";
		variables.dateFin = "";
		variables.dateEntree = "";
		variables.dateSortie = "";
		variables.typeTheme = "";
		variables.quantite = 0;
		variables.montant = 0;
		variables.volume = 0;
		variables.compteFacturation = "";
		variables.sousCompte = "";	
	</cfscript>
	
	
    <!--- Usage: GetcompteFacturation / SetcompteFacturation methods for compteFacturation value --->
    <cffunction name="getcompteFacturation" access="public" output="false" returntype="string">
       <cfreturn variables.compteFacturation />
    </cffunction>

    <cffunction name="setcompteFacturation" access="public" output="false" returntype="void">
       <cfargument name="compteFacturation" type="string" required="true" />
       <cfset variables.compteFacturation = arguments.compteFacturation />
    </cffunction>
	
	
    <!--- Usage: GetsousCompte / SetsousCompte methods for sousCompte value --->
    <cffunction name="getsousCompte" access="public" output="false" returntype="string">
       <cfreturn variables.sousCompte />
    </cffunction>

    <cffunction name="setsousCompte" access="public" output="false" returntype="void">
       <cfargument name="sousCompte" type="string" required="true" />
       <cfset variables.sousCompte = arguments.sousCompte />
    </cffunction>

    <!--- Usage: GetlibelleProduit / SetlibelleProduit methods for libelleProduit value --->
    <cffunction name="getlibelleProduit" access="public" output="false" returntype="string">
       <cfreturn variables.libelleProduit />
    </cffunction>

    <cffunction name="setlibelleProduit" access="public" output="false" returntype="void">
       <cfargument name="libelleProduit" type="string" required="true" />
       <cfset variables.libelleProduit = arguments.libelleProduit />
    </cffunction>


    
    <!--- Usage: GetproduitClientId / SetproduitClientId methods for produitClientId value --->
    <cffunction name="getproduitClientId" access="public" output="false" returntype="numeric">
       <cfreturn variables.produitClientId />
    </cffunction>

    <cffunction name="setproduitClientId" access="public" output="false" returntype="void">
       <cfargument name="produitClientId" required="true" />
       <cfif (IsNumeric(arguments.produitClientId)) OR (arguments.produitClientId EQ "")>	
           <cfset variables.produitClientId = arguments.produitClientId />
       <cfelse>
           <cfthrow message="'#arguments.produitClientId#' is not a valid numeric"/>
        </cfif>
    </cffunction>
    
    <!--- Usage: GetsousTeteId / SetsousTeteId methods for sousTeteId value --->
    <cffunction name="getsousTeteId" access="public" output="false" returntype="numeric">
       <cfreturn variables.sousTeteId />
    </cffunction>

    <cffunction name="setsousTeteId" access="public" output="false" returntype="void">
       <cfargument name="sousTeteId" required="true" />
       <cfif (IsNumeric(arguments.sousTeteId)) OR (arguments.sousTeteId EQ "")>	
           <cfset variables.sousTeteId = arguments.sousTeteId />
       <cfelse>
           <cfthrow message="'#arguments.sousTeteId#' is not a valid numeric"/>
        </cfif>
    </cffunction>

    <!--- Usage: GetsousTete / SetsousTete methods for sousTete value --->
    <cffunction name="getsousTete" access="public" output="false" returntype="string">
       <cfreturn variables.sousTete />
    </cffunction>

    <cffunction name="setsousTete" access="public" output="false" returntype="void">
       <cfargument name="sousTete" type="string" required="true" />
       <cfset variables.sousTete = arguments.sousTete />
    </cffunction>

 
    <!--- Usage: GetprixUnitaire / SetprixUnitaire methods for prixUnitaire value --->
    <cffunction name="getprixUnitaire" access="public" output="false" returntype="numeric">
       <cfreturn variables.prixUnitaire />
    </cffunction>

    <cffunction name="setprixUnitaire" access="public" output="false" returntype="void">
       <cfargument name="prixUnitaire" required="true" />
       <cfif (IsNumeric(arguments.prixUnitaire)) OR (arguments.prixUnitaire EQ "")>	
           <cfset variables.prixUnitaire = arguments.prixUnitaire />
       <cfelse>
           <cfthrow message="'#arguments.prixUnitaire#' is not a valid numeric"/>
        </cfif>
    </cffunction>


    <!--- Usage: GetdateDebut / SetdateDebut methods for dateDebut value --->
    <cffunction name="getdateDebut" access="public" output="false" returntype="any">
       <cfreturn variables.dateDebut />
    </cffunction>

    <cffunction name="setdateDebut" access="public" output="false" returntype="void">
       <cfargument name="dateDebut" required="true" />
       <cfif (IsDate(arguments.dateDebut)) OR (arguments.dateDebut EQ "")>	
           <cfset variables.dateDebut = arguments.dateDebut />
       <cfelse>
           <cfthrow message="'#arguments.dateDebut#' is not a valid date"/>
        </cfif>
    </cffunction>



    <!--- Usage: GettypeTheme / SettypeTheme methods for typeTheme value --->
    <cffunction name="gettypeTheme" access="public" output="false" returntype="string">
       <cfreturn variables.typeTheme />
    </cffunction>

    <cffunction name="settypeTheme" access="public" output="false" returntype="void">
       <cfargument name="typeTheme" type="string" required="true" />
       <cfset variables.typeTheme = arguments.typeTheme />
    </cffunction>


    <!--- Usage: GetdateFin / SetdateFin methods for dateFin value --->
    <cffunction name="getdateFin" access="public" output="false" returntype="any">
       <cfreturn variables.dateFin />
    </cffunction>

    <cffunction name="setdateFin" access="public" output="false" returntype="void">
       <cfargument name="dateFin" required="true" />
       <cfif (IsDate(arguments.dateFin)) OR (arguments.dateFin EQ "")>	
           <cfset variables.dateFin = arguments.dateFin />
       <cfelse>
           <cfthrow message="'#arguments.dateFin#' is not a valid date"/>
        </cfif>
    </cffunction>



  <!--- Usage: GetdateEntree / SetdateEntree methods for dateEntree value --->
    <cffunction name="getdateEntree" access="public" output="false" returntype="any">
       <cfreturn variables.dateEntree />
    </cffunction>

   <cffunction name="setdateEntree" access="public" output="false" returntype="void">
       <cfargument name="dateEntree" required="true" />
       <cfif (IsDate(arguments.dateEntree)) OR (arguments.dateEntree EQ "")>	
           <cfset variables.dateEntree = arguments.dateEntree />
       <cfelse>
           <cfthrow message="'#arguments.dateFin#' is not a valid date"/>
        </cfif>
    </cffunction>
	

  <!--- Usage: GetdateSortie / SetdateSortie methods for dateSortie value --->
    <cffunction name="getdateSortie" access="public" output="false" returntype="any">
       <cfreturn variables.dateSortie />
    </cffunction>

   <cffunction name="setdateSortie" access="public" output="false" returntype="void">
       <cfargument name="dateSortie" required="true" />
       <cfif (IsDate(arguments.dateSortie)) OR (arguments.dateSortie EQ "")>	
           <cfset variables.dateSortie = arguments.dateSortie />
       <cfelse>
           <cfthrow message="'#arguments.dateFin#' is not a valid date"/>
        </cfif>
    </cffunction>	
	
	
	
    
    <!--- Usage: Getquantite / Setquantite methods for quantite value --->
    <cffunction name="getquantite" access="public" output="false" returntype="numeric">
       <cfreturn variables.quantite />
    </cffunction>

    <cffunction name="setquantite" access="public" output="false" returntype="void">
       <cfargument name="quantite" required="true" />
       <cfif (IsNumeric(arguments.quantite)) OR (arguments.quantite EQ "")>	
           <cfset variables.quantite = arguments.quantite />
       <cfelse>
           <cfthrow message="'#arguments.quantite#' is not a valid numeric"/>
        </cfif>
    </cffunction>

    
    <!--- Usage: Getmontant / Setmontant methods for montant value --->
    <cffunction name="getmontant" access="public" output="false" returntype="numeric">
       <cfreturn variables.montant />
    </cffunction>

    <cffunction name="setmontant" access="public" output="false" returntype="void">
       <cfargument name="montant" required="true" />
       <cfif (IsNumeric(arguments.montant)) OR (arguments.montant EQ "")>	
           <cfset variables.montant = arguments.montant />
       <cfelse>
           <cfthrow message="'#arguments.montant#' is not a valid numeric"/>
        </cfif>
    </cffunction>
    
    <!--- Usage: Getvolume / Setvolume methods for volume value --->
    <cffunction name="getvolume" access="public" output="false" returntype="numeric">
       <cfreturn variables.volume />
    </cffunction>

    <cffunction name="setvolume" access="public" output="false" returntype="void">
       <cfargument name="volume" required="true" />
       <cfif (IsNumeric(arguments.volume)) OR (arguments.volume EQ "")>	
           <cfset variables.volume = arguments.volume />
       <cfelse>
           <cfthrow message="'#arguments.volume#' is not a valid numeric"/>
        </cfif>
    </cffunction>
 </cfcomponent>