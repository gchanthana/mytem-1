<cfcomponent output="false" alias="fr.consotel.consoprod.intranet.cataloguepublic.LigneFacturation">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->	
	<cfproperty name="id" type="numeric" default="0">
	<cfproperty name="idProduit" type="numeric" default="0">
	<cfproperty name="libelleProduit" type="string" default="">
	<cfproperty name="themeId" type="numeric" default="0">
	<cfproperty name="themeLibelle" type="string" default="">
	<cfproperty name="operateurId" type="numeric" default="0">
	<cfproperty name="operateurLibelle" type="string" default="">
	<cfproperty name="typeTheme" type="string" default="">
	<cfproperty name="typeTrafic" type="string" default="">
	<cfproperty name="poids" type="numeric" default="0">
	<cfproperty name="pourControle" type="boolean" default="1">
	<cfproperty name="prixUnitaire" type="numeric" default="0">
	<cfproperty name="prixUnitaireRemise" type="numeric" default="0">
	<cfproperty name="remiseContrat" type="numeric" default="0">
	<cfproperty name="dateDebutVersion" type="date" default="">
	<cfproperty name="dateFinVersion" type="date" default="">
	<cfproperty name="dateDebut" type="date" default="">
	<cfproperty name="dateFin" type="date" default="">
	<cfproperty name="qte" type="numeric" default="0">
	<cfproperty name="montantTotal" type="numeric" default="0">
	<cfproperty name="montantCalculeBrut" type="numeric" default="0">
	<cfproperty name="montantCalculeRemise" type="numeric" default="0">
	<cfproperty name="difference" type="numeric" default="0">
	<cfproperty name="poidsDifference" type="numeric" default="0">
	<cfproperty name="montantMoyen" type="numeric" default="0">
	<cfproperty name="duree" type="numeric" default="0">
	<cfproperty name="boolPublic" type="numeric" default="0">
	

	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.id = 0;
		variables.idProduit = 0;
		variables.libelleProduit = "";
		variables.themeId = 0;
		variables.themeLibelle = "";
		variables.operateurId = 0;
		variables.operateurLibelle = "";
		variables.typeTheme = "";
		variables.typeTrafic = "";
		variables.poids = 0;
		variables.pourControle = 1;
		variables.prixUnitaire = 0;
		variables.prixUnitaireRemise = 0;
		variables.duree = 0;
		variables.remiseContrat = 0;
		variables.dateDebutVersion = "";
		variables.dateFinVersion = "";
		variables.dateDebut = "";
		variables.dateFin = "";
		variables.qte = 0;
		variables.montantTotal = 0;
		variables.montantMoyen = 0;
		variables.montantCalculeBrut = 0;
		variables.montantCalculeRemise = 0;
		variables.difference = 0;
		variables.poidsDifference = 0;
		variables.boolPublic = 0;
	</cfscript>

	<cffunction name="init" output="false" returntype="LigneFacturation">
		<cfreturn this>
	</cffunction>
	<cffunction name="getId" output="false" access="public" returntype="any">
		<cfreturn variables.Id>
	</cffunction>

	<cffunction name="setId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.Id = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIdProduit" output="false" access="public" returntype="any">
		<cfreturn variables.IdProduit>
	</cffunction>

	<cffunction name="setIdProduit" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IdProduit = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getLibelleProduit" output="false" access="public" returntype="any">
		<cfreturn variables.LibelleProduit>
	</cffunction>

	<cffunction name="setLibelleProduit" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.LibelleProduit = arguments.val>
	</cffunction>

	<cffunction name="getThemeId" output="false" access="public" returntype="any">
		<cfreturn variables.ThemeId>
	</cffunction>

	<cffunction name="setThemeId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.ThemeId = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getThemeLibelle" output="false" access="public" returntype="any">
		<cfreturn variables.ThemeLibelle>
	</cffunction>

	<cffunction name="setThemeLibelle" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.ThemeLibelle = arguments.val>
	</cffunction>

	<cffunction name="getOperateurId" output="false" access="public" returntype="any">
		<cfreturn variables.OperateurId>
	</cffunction>

	<cffunction name="setOperateurId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.OperateurId = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getOperateurLibelle" output="false" access="public" returntype="any">
		<cfreturn variables.OperateurLibelle>
	</cffunction>

	<cffunction name="setOperateurLibelle" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.OperateurLibelle = arguments.val>
	</cffunction>

	<cffunction name="getTypeTheme" output="false" access="public" returntype="any">
		<cfreturn variables.TypeTheme>
	</cffunction>

	<cffunction name="setTypeTheme" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.TypeTheme = arguments.val>
	</cffunction>

	<cffunction name="getTypeTrafic" output="false" access="public" returntype="any">
		<cfreturn variables.TypeTrafic>
	</cffunction>

	<cffunction name="setTypeTrafic" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.TypeTrafic = arguments.val>
	</cffunction>

	<cffunction name="getPoids" output="false" access="public" returntype="any">
		<cfreturn variables.Poids>
	</cffunction>

	<cffunction name="setPoids" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.Poids = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getPourControle" output="false" access="public" returntype="any">
		<cfreturn variables.PourControle>
	</cffunction>

	<cffunction name="setPourControle" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsBoolean(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.PourControle = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid boolean"/>
		</cfif>
	</cffunction>

	<cffunction name="getPrixUnitaire" output="false" access="public" returntype="any">
		<cfreturn variables.PrixUnitaire>
	</cffunction>

	<cffunction name="setPrixUnitaire" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.PrixUnitaire = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getPrixUnitaireRemise" output="false" access="public" returntype="any">
		<cfreturn variables.PrixUnitaireRemise>
	</cffunction>

	<cffunction name="setPrixUnitaireRemise" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.PrixUnitaireRemise = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getRemiseContrat" output="false" access="public" returntype="any">
		<cfreturn variables.RemiseContrat>
	</cffunction>

	<cffunction name="setRemiseContrat" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.RemiseContrat = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDateDebutVersion" output="false" access="public" returntype="any">
		<cfreturn variables.DateDebutVersion>
	</cffunction>

	<cffunction name="setDateDebutVersion" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DateDebutVersion = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getDateFinVersion" output="false" access="public" returntype="any">
		<cfreturn variables.DateFinVersion>
	</cffunction>

	<cffunction name="setDateFinVersion" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DateFinVersion = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getDateDebut" output="false" access="public" returntype="any">
		<cfreturn variables.DateDebut>
	</cffunction>

	<cffunction name="setDateDebut" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DateDebut = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getDateFin" output="false" access="public" returntype="any">
		<cfreturn variables.DateFin>
	</cffunction>

	<cffunction name="setDateFin" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DateFin = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getQte" output="false" access="public" returntype="any">
		<cfreturn variables.Qte>
	</cffunction>

	<cffunction name="setQte" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.Qte = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getMontantTotal" output="false" access="public" returntype="any">
		<cfreturn variables.MontantTotal>
	</cffunction>

	<cffunction name="setMontantTotal" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.MontantTotal = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getMontantMoyen" output="false" access="public" returntype="any">
		<cfreturn variables.montantMoyen>
	</cffunction>

	<cffunction name="setMontantMoyen" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.montantMoyen = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getDuree" output="false" access="public" returntype="any">
		<cfreturn variables.duree>
	</cffunction>

	<cffunction name="setDuree" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.duree = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getMontantCalculeBrut" output="false" access="public" returntype="any">
		<cfreturn variables.MontantCalculeBrut>
	</cffunction>

	<cffunction name="setMontantCalculeBrut" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.MontantCalculeBrut = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getMontantCalculeRemise" output="false" access="public" returntype="any">
		<cfreturn variables.MontantCalculeRemise>
	</cffunction>

	<cffunction name="setMontantCalculeRemise" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.MontantCalculeRemise = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDifference" output="false" access="public" returntype="any">
		<cfreturn variables.Difference>
	</cffunction>

	<cffunction name="setDifference" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.Difference = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getPoidsDifference" output="false" access="public" returntype="any">
		<cfreturn variables.PoidsDifference>
	</cffunction>

	<cffunction name="setPoidsDifference" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.PoidsDifference = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getBoolPublic" output="false" access="public" returntype="any">
		<cfreturn variables.boolPublic>
	</cffunction>

	<cffunction name="setBoolPublic" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.boolPublic = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	
</cfcomponent>