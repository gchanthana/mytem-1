<cfcomponent output="false" extends="AccessObject">
	
	
	<!---		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Liste des opÃ©rateurs du pÃ©rimetre
		
		Params out Operateur[]
	--->
	<cffunction name="getListeOperateursPerimetre" access="remote" output="false" returntype="Operateur[]">
		<cfset qListeOperateursGroupe = Evaluate("getListeOperateurs#getTypePerimetre()#AsQuery()")>
		<cfset tabOperateur = arrayNew(1)>			
		 
		<cfloop query="qListeOperateurs">
			<cfset pOperateur = createObject("component","Operateur")>
			
			<cfscript>			
				if(isdefined("qListeOperateurs.OPNOM"))
					pOperateur.setNom(qListeOperateurs.OPNOM);
				else
					pOperateur.setNom(qListeOperateurs.NOM);
				pOperateur.setId(qListeOperateurs.OPERATEURID);	
			</cfscript>
			
			<cfset arrayAppend(tabOperateur,pOperateur)>
		</cfloop>
		<cfreturn tabOperateur>
	</cffunction>
	
	
	
	<!---		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Liste des opÃ©rateurs du Groupe
		
		Params out query
	--->
	<cffunction name="getListeOperateursGroupeAsQuery" access="remote" output="false" returntype="query">
		<!--- 
			PROCEDURE			
				PKG_CV_GRCL.SF_LISTEOP
			PARAM	
			in			
				p_idGroupe_maitre		INTEGER
			out				  	
				p_retour				QUERY				
		 --->	
		
		<cfif this.getIdGroupeMaitre() gt 0>
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL.SF_LISTEOP">		
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idGroupe_maitre" value="#this.getIdGroupeMaitre()#"/>		
				<cfprocresult name="qListeOperateurs"/>
			</cfstoredproc>						
		<cfelse>
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE.L_ALL_OPERATEURS">
				<cfprocresult name="qResult"/>
			</cfstoredproc>
			<cfquery name="qListeOperateurs" dbtype="query">
				select OPERATEURID, NOM as OPNOM from qResult
			</cfquery>
		</cfif>
		<cfreturn qListeOperateurs>
	</cffunction>
	
			
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Liste des opÃ©rateur d'un groupe ligne
		
		Params out query
	--->
	<cffunction name="getListeOperateursGroupeLigneAsQuery" access="remote" output="false" returntype="query">		
		<!--- 
			PROCEDURE			
				PKG_CV_GL.SF_LISTEOP
			PARAM	
			in			
				p_idGroupe_maitre		INTEGER
			out				  	
				p_retour				QUERY				
		 --->	
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GLIG.SF_LISTEOP">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#this.getIdGroupeClient()#"/>
	        <cfprocresult name="qListeOperateurs"/>        
		</cfstoredproc>		
		<cfreturn qListeOperateurs>
	</cffunction>
	
</cfcomponent>