<cfcomponent output="false" alias="fr.consotel.consoview.facturation.suiviFacturation.controles.Operateur">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	<cfproperty name="nom" type="string" default="">
	<cfproperty name="id" type="numeric" default="0">
	<cfproperty name="idGroupeClient" type="numeric" default="0">
	

	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.nom = "";
		variables.id = 0;
		variables.idGroupeClient = 0;
	</cfscript>

	<cffunction name="init" output="false" returntype="Operateur">
		<cfreturn this>
	</cffunction>
	<cffunction name="getNom" output="false" access="public" returntype="any">
		<cfreturn variables.Nom>
	</cffunction>

	<cffunction name="setNom" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.Nom = arguments.val>
	</cffunction>

	<cffunction name="getId" output="false" access="public" returntype="any">
		<cfreturn variables.Id>
	</cffunction>

	<cffunction name="setId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.Id = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getIdGroupeClient" output="false" access="public" returntype="any">
		<cfreturn variables.idGroupeClient>
	</cffunction>

	<cffunction name="setIdGroupeClient" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.idGroupeClient = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="setProprietes" output="false" access="public" returntype="void">
		<cfargument name="proprietes" type="any" required="true">		
		<cfscript>			
			setNom(proprietes.OPNOM);
			SetId(proprietes.OPERATEURID);	
			setIdGroupeClient(proprietes.IDORGA);		
		</cfscript>
	</cffunction>
</cfcomponent>