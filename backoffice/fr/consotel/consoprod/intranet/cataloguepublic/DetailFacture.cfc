<cfcomponent output="false" alias="fr.consotel.consoprod.intranet.cataloguepublic.DetailFacture">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	<cfproperty name="compteFacturation" type="string" default="">
	<cfproperty name="sousCompte" type="string" default="">
	<cfproperty name="libelleProduit" type="string" default="">
	<cfproperty name="sousTete" type="string" default="">
	<cfproperty name="montant" type="numeric" default="0">
	<cfproperty name="nombreAppel" type="numeric" default="0">
	<cfproperty name="dureeAppel" type="numeric" default="0">
	<cfproperty name="quantite" type="numeric" default="0">
	<cfproperty name="libelle" type="date" default="">	
	<cfproperty name="gamme" type="string" default="">
	<cfproperty name="prixUnitaire" type="numeric" default="0">
	<cfproperty name="dateDebut" type="date" default="">
	<cfproperty name="dateFin" type="date" default="">	
	
	<cfproperty name="dateEmission" type="date" default="">
	
	<cfproperty name="numeroFacture" type="string" default="">	
	<cfproperty name="nomSite" type="string" default="">	
	<cfproperty name="adresse1" type="string" default="">
	<cfproperty name="adresse2" type="string" default="">
	<cfproperty name="zipCode" type="string" default="">	
	<cfproperty name="commune" type="string" default="">
	<cfproperty name="typeLigne" type="string" default="">
	<cfproperty name="codeTva" type="numeric" default="0">	
		 
	 
	
	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.numeroFacture = "";
		variables.compteFacturation = "";
		variables.sousCompte = "";
		variables.libelleProduit = "";
		variables.sousTete = "";
		variables.libelle = "";	
		variables.gamme = "";
		variables.typeLigne = "";
		
		variables.nomSite = "";	
		variables.adresse1 = "";
		variables.adresse2 = "";
		variables.zipCode = "";	
		variables.commune = "";
		
		variables.montant = 0;
		variables.nombreAppel = 0;
		variables.dureeAppel = 0;
		variables.quantite = 0;		
		variables.prixUnitaire = 0;
		variables.codeTva = 0;
		
		variables.dateDebut = "";
		variables.dateFin = "";	
		variables.dateEmission = "";
			
		
		
				
	</cfscript>	
	
	<cffunction name="init" output="false" returntype="Facture">
		<cfreturn this>
	</cffunction>

    <!--- Usage: GetnumeroFacture / SetnumeroFacture methods for numeroFacture value --->
    <cffunction name="getnumeroFacture" access="public" output="false" returntype="string">
       <cfreturn variables.numeroFacture />
    </cffunction>

    <cffunction name="setnumeroFacture" access="public" output="false" returntype="void">
       <cfargument name="numeroFacture" type="string" required="true" />
       <cfset variables.numeroFacture = arguments.numeroFacture />
    </cffunction>

    <!--- Usage: GetcompteFacturation / SetcompteFacturation methods for compteFacturation value --->
    <cffunction name="getCompteFacturation" access="public" output="false" returntype="string">
       <cfreturn variables.compteFacturation />
    </cffunction>

    <cffunction name="setCompteFacturation" access="public" output="false" returntype="void">
       <cfargument name="compteFacturation" type="string" required="true" />
       <cfset variables.compteFacturation = arguments.compteFacturation />
    </cffunction>
	
	
    <!--- Usage: GetsousCompte / SetsousCompte methods for sousCompte value --->
    <cffunction name="getsousCompte" access="public" output="false" returntype="string">
       <cfreturn variables.sousCompte />
    </cffunction>

    <cffunction name="setsousCompte" access="public" output="false" returntype="void">
       <cfargument name="sousCompte" type="string" required="true" />
       <cfset variables.sousCompte = arguments.sousCompte />
    </cffunction>


    <!--- Usage: GetlibelleProduit / SetlibelleProduit methods for libelleProduit value --->
    <cffunction name="getlibelleProduit" access="public" output="false" returntype="string">
       <cfreturn variables.libelleProduit />
    </cffunction>

    <cffunction name="setlibelleProduit" access="public" output="false" returntype="void">
       <cfargument name="libelleProduit" type="string" required="true" />
       <cfset variables.libelleProduit = arguments.libelleProduit />
    </cffunction>


    <!--- Usage: GetsousTete / SetsousTete methods for sousTete value --->
    <cffunction name="getsousTete" access="public" output="false" returntype="string">
       <cfreturn variables.sousTete />
    </cffunction>

    <cffunction name="setsousTete" access="public" output="false" returntype="void">
       <cfargument name="sousTete" type="string" required="true" />
       <cfset variables.sousTete = arguments.sousTete />
    </cffunction>

    <!--- Usage: GettypeLigne / SettypeLigne methods for typeLigne value --->
    <cffunction name="gettypeLigne" access="public" output="false" returntype="string">
       <cfreturn variables.typeLigne />
    </cffunction>

    <cffunction name="settypeLigne" access="public" output="false" returntype="void">
       <cfargument name="typeLigne" type="string" required="true" />
       <cfset variables.typeLigne = arguments.typeLigne />
    </cffunction>


    <!--- Usage: Getlibelle / Setlibelle methods for libelle value --->
    <cffunction name="getlibelle" access="public" output="false" returntype="string">
       <cfreturn variables.libelle />
    </cffunction>

    <cffunction name="setlibelle" access="public" output="false" returntype="void">
       <cfargument name="libelle" type="string" required="true" />
       <cfset variables.libelle = arguments.libelle />
    </cffunction>


    <!--- Usage: Getgamme / Setgamme methods for gamme value --->
    <cffunction name="getgamme" access="public" output="false" returntype="string">
       <cfreturn variables.gamme />
    </cffunction>

    <cffunction name="setgamme" access="public" output="false" returntype="void">
       <cfargument name="gamme" type="string" required="true" />
       <cfset variables.gamme = arguments.gamme />
    </cffunction>
	

    <!--- Usage: GetnomSite / SetnomSite methods for nomSite value --->
    <cffunction name="getnomSite" access="public" output="false" returntype="string">
       <cfreturn variables.nomSite />
    </cffunction>

    <cffunction name="setnomSite" access="public" output="false" returntype="void">
       <cfargument name="nomSite" type="string" required="true" />
       <cfset variables.nomSite = arguments.nomSite />
    </cffunction>


    <!--- Usage: Getadresse1 / Setadresse1 methods for adresse1 value --->
    <cffunction name="getadresse1" access="public" output="false" returntype="string">
       <cfreturn variables.adresse1 />
    </cffunction>

    <cffunction name="setadresse1" access="public" output="false" returntype="void">
       <cfargument name="adresse1" type="string" required="true" />
       <cfset variables.adresse1 = arguments.adresse1 />
    </cffunction>


    <!--- Usage: Getadresse2 / Setadresse2 methods for adresse2 value --->
    <cffunction name="getadresse2" access="public" output="false" returntype="string">
       <cfreturn variables.adresse2 />
    </cffunction>

    <cffunction name="setadresse2" access="public" output="false" returntype="void">
       <cfargument name="adresse2" type="string" required="true" />
       <cfset variables.adresse2 = arguments.adresse2 />
    </cffunction>

    <!--- Usage: GetzipCode / SetzipCode methods for zipCode value --->
    <cffunction name="getzipCode" access="public" output="false" returntype="string">
       <cfreturn variables.zipCode />
    </cffunction>

    <cffunction name="setzipCode" access="public" output="false" returntype="void">
       <cfargument name="zipCode" type="string" required="true" />
       <cfset variables.zipCode = arguments.zipCode />
    </cffunction>

    <!--- Usage: Getcommune / Setcommune methods for commune value --->
    <cffunction name="getcommune" access="public" output="false" returntype="string">
       <cfreturn variables.commune />
    </cffunction>

    <cffunction name="setcommune" access="public" output="false" returntype="void">
       <cfargument name="commune" type="string" required="true" />
       <cfset variables.commune = arguments.commune />
    </cffunction>

    
    <!--- Usage: Getmontant / Setmontant methods for montant value --->
    <cffunction name="getmontant" access="public" output="false" returntype="numeric">
       <cfreturn variables.montant />
    </cffunction>

    <cffunction name="setmontant" access="public" output="false" returntype="void">
       <cfargument name="montant" required="true" />
       <cfif (IsNumeric(arguments.montant)) OR (arguments.montant EQ "")>	
           <cfset variables.montant = arguments.montant />
       <cfelse>
           <cfthrow message="'#arguments.montant#' is not a valid numeric"/>
        </cfif>
    </cffunction>

    
    <!--- Usage: GetnombreAppel / SetnombreAppel methods for nombreAppel value --->
    <cffunction name="getnombreAppel" access="public" output="false" returntype="numeric">
       <cfreturn variables.nombreAppel />
    </cffunction>

    <cffunction name="setnombreAppel" access="public" output="false" returntype="void">
       <cfargument name="nombreAppel" required="true" />
       <cfif (IsNumeric(arguments.nombreAppel)) OR (arguments.nombreAppel EQ "")>	
           <cfset variables.nombreAppel = arguments.nombreAppel />
       <cfelse>
           <cfthrow message="'#arguments.nombreAppel#' is not a valid numeric"/>
        </cfif>
    </cffunction>

    
    <!--- Usage: GetdureeAppel / SetdureeAppel methods for dureeAppel value --->
    <cffunction name="getdureeAppel" access="public" output="false" returntype="numeric">
       <cfreturn variables.dureeAppel />
    </cffunction>

    <cffunction name="setdureeAppel" access="public" output="false" returntype="void">
       <cfargument name="dureeAppel" required="true" />
       <cfif (IsNumeric(arguments.dureeAppel)) OR (arguments.dureeAppel EQ "")>	
           <cfset variables.dureeAppel = arguments.dureeAppel />
       <cfelse>
           <cfthrow message="'#arguments.dureeAppel#' is not a valid numeric"/>
        </cfif>
    </cffunction>

    
    <!--- Usage: Getquantite / Setquantite methods for quantite value --->
    <cffunction name="getquantite" access="public" output="false" returntype="numeric">
       <cfreturn variables.quantite />
    </cffunction>

    <cffunction name="setquantite" access="public" output="false" returntype="void">
       <cfargument name="quantite" required="true" />
       <cfif (IsNumeric(arguments.quantite)) OR (arguments.quantite EQ "")>	
           <cfset variables.quantite = arguments.quantite />
       <cfelse>
           <cfthrow message="'#arguments.quantite#' is not a valid numeric"/>
        </cfif>
    </cffunction>
    
    <!--- Usage: GetprixUnitaire / SetprixUnitaire methods for prixUnitaire value --->
    <cffunction name="getprixUnitaire" access="public" output="false" returntype="numeric">
       <cfreturn variables.prixUnitaire />
    </cffunction>

    <cffunction name="setprixUnitaire" access="public" output="false" returntype="void">
       <cfargument name="prixUnitaire" required="true" />
       <cfif (IsNumeric(arguments.prixUnitaire)) OR (arguments.prixUnitaire EQ "")>	
           <cfset variables.prixUnitaire = arguments.prixUnitaire />
       <cfelse>
           <cfthrow message="'#arguments.prixUnitaire#' is not a valid numeric"/>
        </cfif>
    </cffunction>
	    
    <!--- Usage: GetcodeTva / SetcodeTva methods for codeTva value --->
    <cffunction name="getcodeTva" access="public" output="false" returntype="numeric">
       <cfreturn variables.codeTva />
    </cffunction>

    <cffunction name="setcodeTva" access="public" output="false" returntype="void">
       <cfargument name="codeTva" required="true" />
       <cfif (IsNumeric(arguments.codeTva)) OR (arguments.codeTva EQ "")>	
           <cfset variables.codeTva = arguments.codeTva />
       <cfelse>
           <cfthrow message="'#arguments.codeTva#' is not a valid numeric"/>
        </cfif>
    </cffunction>


    <!--- Usage: GetdateDebut / SetdateDebut methods for dateDebut value --->
    <cffunction name="getdateDebut" access="public" output="false" returntype="date">
       <cfreturn variables.dateDebut/>
    </cffunction>

    <cffunction name="setdateDebut" access="public" output="false" returntype="void">
       <cfargument name="dateDebut" required="true" />
       <cfif (IsDate(arguments.dateDebut)) OR (arguments.dateDebut EQ "")>	
           <cfset variables.dateDebut = arguments.dateDebut />
       <cfelse>
           <cfthrow message="'#arguments.dateDebut#' is not a valid date"/>
        </cfif>
    </cffunction>

    <!--- Usage: GetdateFin / SetdateFin methods for dateFin value --->
    <cffunction name="getdateFin" access="public" output="false" returntype="date">
		<cfreturn variables.dateFin/>
    </cffunction>

    <cffunction name="setdateFin" access="public" output="false" returntype="void">
       <cfargument name="dateFin" required="true" />
       <cfif (IsDate(arguments.dateFin)) OR (arguments.dateFin EQ "")>	
           <cfset variables.dateFin = arguments.dateFin />
       <cfelse>
           <cfthrow message="'#arguments.dateFin#' is not a valid date"/>
        </cfif>
    </cffunction>

    <!--- Usage: GetdateEmission / SetdateEmission methods for dateEmission value --->
    <cffunction name="getdateEmission" access="public" output="false" returntype="date">
	    <cfreturn variables.dateEmission/>   
    </cffunction>

    <cffunction name="setdateEmission" access="public" output="false" returntype="void">
       <cfargument name="dateEmission" required="true" />
    	<cfif (IsDate(arguments.dateEmission)) OR (arguments.dateEmission EQ "")>	 --->
           <cfset variables.dateEmission = arguments.dateEmission />
       <cfelse>
           <cfthrow message="'#arguments.dateEmission#' is not a valid date"/>
        </cfif>
    </cffunction>

</cfcomponent>