<cfcomponent output="false" extends="AccessObject">
	
	
	
	
	<cffunction name="getGrpProduitsFactureGroupePerimetre" access="remote" output="false" returntype="any">
		<cfargument name="myFacture" type="Facture" required="true"/>
		
		<cfset alisteGrpProduits = Evaluate("getGrpProduitsFacture#getTypePerimetre()#AsQuery(myFacture)")>
		<cfset qlisteGrpProduitsAbos = alisteGrpProduits[1]>
		<cfset qlisteGrpProduitsConsos = alisteGrpProduits[2]>
		
		<!--- =============== TOTAUX ============== --->
		<cfset montantTotal = 0>
		<cftry>
			<cfquery name="totalAbos" dbtype="query">
				select sum(MONTANT) as TOTAL from qlisteGrpProduitsAbos
			</cfquery>
			<cfquery name="totalConsos" dbtype="query">
				select sum(MONTANT) as TOTAL from qlisteGrpProduitsConsos
			</cfquery>	
			<cfif totalAbos.recordCount gt 0>
				<cfset montantTotal = montantTotal + totalAbos.TOTAL[1]>
				<cfelse>
				<cfset montantTotal = 0>
			</cfif>
			<cfif totalConsos.recordCount gt 0>
				<cfset montantTotal = montantTotal + totalConsos.TOTAL[1]>				
			</cfif>	
		<cfcatch type="any">
			<cfset montantTotal = 0>
		</cfcatch>	
		</cftry>
		<!--- =============== FIN TOTAUX ========== --->
		
		
		<!--- =============== GRP PRODUITS ABOS ========== --->
		<cfset tabProduitsAbos = arrayNew(1)>	
		
		<cfloop query="qlisteGrpProduitsAbos">
			<cfset grpProduitsAbos = createObject("component","GroupeProduitControle")>	
			<cfscript>		
				grpProduitsAbos.setId(qlisteGrpProduitsAbos.IDGRP_CTL);
				grpProduitsAbos.setLibelle(qlisteGrpProduitsAbos.GRP_CTL);
				grpProduitsAbos.setSegment(formatSegment(qlisteGrpProduitsAbos.GRP_SEGMENT));				
				grpProduitsAbos.setOperateurId(pRecherche.getOperateurId());
				if (montantTotal eq 0){
					grpProduitsAbos.setPoids(0);
				}else{
					grpProduitsAbos.setPoids(qlisteGrpProduitsAbos.MONTANT/montantTotal*100);
				}
			  	grpProduitsAbos.setPourControle(qlisteGrpProduitsAbos.A_CONTROLER);
				grpProduitsAbos.setPrixUnitaire(qlisteGrpProduitsAbos.TARIF_BRUT);
				grpProduitsAbos.setPrixRemise(qlisteGrpProduitsAbos.TARIF_REMISE);
				grpProduitsAbos.setMontant(qlisteGrpProduitsAbos.MONTANT); 
				grpProduitsAbos.setNbVersion(1); 
				grpProduitsAbos.setRemiseContrat(qlisteGrpProduitsAbos.PCT_REMISE); 
				grpProduitsAbos.setDateDebut(qlisteGrpProduitsAbos.DATEDEB); 
				grpProduitsAbos.setDateFin(qlisteGrpProduitsAbos.DATEFIN); 
				grpProduitsAbos.setType("ABO");
			</cfscript>
			<cfset arrayAppend(tabProduitsAbos,grpProduitsAbos)>
		</cfloop>
		
		<!--- =============== FIN GRP PRODUITS ABOS ========== --->
		
		
		<!--- =============== GRP PRODUITS CONSOS ========== --->
		<cfset qlisteGrpProduitsConsos = alisteGrpProduits[2]>
		<cfset tabProduitsConsos = arrayNew(1)>
		<cfloop query="qlisteGrpProduitsConsos">
			<cfset grpProduitsConsos = createObject("component","GroupeProduitControle")>			
			<cfscript>		
			//Initialize the CFC with the properties values.
				grpProduitsConsos.setId(qlisteGrpProduitsConsos.IDGRP_CTL);
				grpProduitsConsos.setLibelle(qlisteGrpProduitsConsos.GRP_CTL);
				grpProduitsConsos.setSegment(formatSegment(qlisteGrpProduitsConsos.GRP_SEGMENT));				
				grpProduitsConsos.setOperateurId(pRecherche.getOperateurId());
				if (montantTotal eq 0){
					grpProduitsConsos.setPoids(0);
				}else{
					grpProduitsConsos.setPoids(qlisteGrpProduitsConsos.MONTANT/montantTotal*100);
				}
			  	grpProduitsConsos.setPourControle(qlisteGrpProduitsConsos.A_CONTROLER);
				grpProduitsConsos.setPrixUnitaire(qlisteGrpProduitsConsos.TARIF_BRUT);
				grpProduitsConsos.setPrixRemise(qlisteGrpProduitsConsos.TARIF_REMISE);
				grpProduitsConsos.setMontant(qlisteGrpProduitsConsos.MONTANT); 
				grpProduitsConsos.setNbVersion(1); 
				grpProduitsConsos.setRemiseContrat(qlisteGrpProduitsConsos.PCT_REMISE); 
				grpProduitsConsos.setDateDebut(qlisteGrpProduitsConsos.DATEDEB); 
				grpProduitsConsos.setDateFin(qlisteGrpProduitsConsos.DATEFIN); 
				grpProduitsConsos.setType("CONSO");
			</cfscript>
			<cfset arrayAppend(tabProduitsConsos,grpProduitsConsos)>
		</cfloop>
		<!--- =============== FIN PRODUITS CONSOS ========== --->
		
		<cfset tabProduits = arrayNew(1)>		
		<cfset arrayappend(tabProduits,tabProduitsAbos)>
		<cfset arrayappend(tabProduits,tabProduitsConsos)>
				
		<cfreturn tabProduits>
	</cffunction>
	
	<cffunction name="formatSegment" access="private" output="false" returntype="string">
		<cfargument name="nSegment" type="numeric" required="true">
		<cfswitch expression="#nSegment#">
			<cfcase value="1">
				<cfreturn "Fixe">
			</cfcase>
			<cfcase value="2">
				<cfreturn "Mobile">
			</cfcase>
			<cfcase value="3">
				<cfreturn "Data">
			</cfcase>
			<cfdefaultcase>
				<cfreturn "Segment inconnu">
			</cfdefaultcase>
		</cfswitch>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Fournit la liste des groupe de produits ï¿½laborï¿½s par Consotel pour une Racine.
					  
		Param in 
			ParamsRecherche
		Params out
			query
			  		  	
	--->
	<cffunction name="getGrpProduitsFactureGroupeAsQuery" access="remote" output="false" returntype="array">
		<cfargument name="myFacture" type="Facture" required="true"/>
		<cfquery name="qListeGrpProduitsAbos" datasource="#SESSION.OFFREDSN#">
			SELECT  g1.*, nvl(d1.montant,0) AS montant
		    FROM
		        (
		            SELECT    pca.idgrp_ctl, SUM(df.montant) AS montant
		            FROM     detail_facture_abo df, produit_client pc, produit_catalogue pca
		            WHERE   df.idinventaire_periode = #myFacture.getPeriodeId()#
		                   AND df.idproduit_client=pc.idproduit_client 
		                   AND pca.idproduit_catalogue=pc.idproduit_catalogue
		            GROUP BY pca.idgrp_ctl
		        ) d1,
		        (
		            SELECT g.idgrp_ctl, g.grp_ctl, v.datedeb, v.datefin, v.tarif_brut,v.tarif_remise, v.pct_remise, g.grp_segment, 
		                  decode(nvl(eg.idgrp_ctl,0),0,1,0) AS a_controler 
		            FROM   grp_ctl g, version_grp_ctl v,
		                  (SELECT e.idgrp_ctl FROM exception_grp_ctl e WHERE e.idracine=#getIdGroupeMaitre()#) eg
		            WHERE g.operateurid= #myFacture.getOperateurId()#
		                  AND lower(g.grp_ctl) like lower('%'||''||'%')
		                  AND g.type_grp_ctl=1
		                  AND g.idgrp_ctl=v.idgrp_ctl AND v.idracine=#getIdGroupeMaitre()#
		                  AND g.idgrp_ctl=nvl('',g.idgrp_ctl)
		                  AND g.idgrp_ctl=eg.idgrp_ctl (+)
		            GROUP BY g.idgrp_ctl,g.grp_ctl, v.datedeb, v.datefin, v.tarif_brut,v.tarif_remise, v.pct_remise,eg.idgrp_ctl, g.grp_segment
		            HAVING v.datedeb=(SELECT MAX(datedeb) FROM version_grp_ctl v2 WHERE v2.idracine=#getIdGroupeMaitre()# AND v2.idgrp_ctl=g.idgrp_ctl)
		            UNION
		            SELECT g.idgrp_ctl, g.grp_ctl, 
		                  NULL AS datedeb, NULL AS datefin, NULL AS tarif_brut,NULL AS tarif_remise, NULL AS pct_remise, 
		                  g.grp_segment, decode(nvl(eg.idgrp_ctl,0),0,1,0) AS a_controler 
		            FROM   grp_ctl g, 
		                  (SELECT e.idgrp_ctl FROM exception_grp_ctl e WHERE e.idracine=#getIdGroupeMaitre()#) eg
		            WHERE g.operateurid= #myFacture.getOperateurId()#
		                  AND lower(g.grp_ctl) like lower('%'||''||'%')
		                  AND g.type_grp_ctl=1
		                  AND g.idgrp_ctl=nvl('',g.idgrp_ctl)
		                  AND g.idgrp_ctl=eg.idgrp_ctl (+)
		                  AND NOT EXISTS (  SELECT 1 
		                                    FROM  version_grp_ctl v2 
		                                    WHERE v2.idracine= #getIdGroupeMaitre()# AND v2.idgrp_ctl=g.idgrp_ctl)
		            GROUP BY g.idgrp_ctl,g.grp_ctl, eg.idgrp_ctl, g.grp_segment
		            ) g1            
		    WHERE g1.idgrp_ctl=d1.idgrp_ctl (+)
		    ORDER BY g1.grp_segment, g1.grp_ctl
		</cfquery>		
		
		<cfquery name="qListeGrpProduitsConsos" datasource="#SESSION.OFFREDSN#">
			SELECT  g1.*, nvl(d1.montant,0) AS montant
		    FROM
		        (
		            SELECT    pca.idgrp_ctl, SUM(df.montant) AS montant
		            FROM     detail_facture_abo df, produit_client pc, produit_catalogue pca
		            WHERE   df.idinventaire_periode = #myFacture.getPeriodeId()#
		                   AND df.idproduit_client=pc.idproduit_client 
		                   AND pca.idproduit_catalogue=pc.idproduit_catalogue
		            GROUP BY pca.idgrp_ctl
		        ) d1,
		        (
		            SELECT g.idgrp_ctl, g.grp_ctl, v.datedeb, v.datefin, v.tarif_brut,v.tarif_remise, v.pct_remise, g.grp_segment, 
		                  decode(nvl(eg.idgrp_ctl,0),0,1,0) AS a_controler 
		            FROM   grp_ctl g, version_grp_ctl v,
		                  (SELECT e.idgrp_ctl FROM exception_grp_ctl e WHERE e.idracine=#getIdGroupeMaitre()#) eg
		            WHERE g.operateurid= #myFacture.getOperateurId()#
		                  AND lower(g.grp_ctl) like lower('%'||''||'%')
		                  AND g.type_grp_ctl=2
		                  AND g.idgrp_ctl=v.idgrp_ctl AND v.idracine=#getIdGroupeMaitre()#
		                  AND g.idgrp_ctl=nvl('',g.idgrp_ctl)
		                  AND g.idgrp_ctl=eg.idgrp_ctl (+)
		            GROUP BY g.idgrp_ctl,g.grp_ctl, v.datedeb, v.datefin, v.tarif_brut,v.tarif_remise, v.pct_remise,eg.idgrp_ctl, g.grp_segment
		            HAVING v.datedeb=(SELECT MAX(datedeb) FROM version_grp_ctl v2 WHERE v2.idracine=#getIdGroupeMaitre()# AND v2.idgrp_ctl=g.idgrp_ctl)
		            UNION
		            SELECT g.idgrp_ctl, g.grp_ctl, 
		                  NULL AS datedeb, NULL AS datefin, NULL AS tarif_brut,NULL AS tarif_remise, NULL AS pct_remise, 
		                  g.grp_segment, decode(nvl(eg.idgrp_ctl,0),0,1,0) AS a_controler 
		            FROM   grp_ctl g, 
		                  (SELECT e.idgrp_ctl FROM exception_grp_ctl e WHERE e.idracine=#getIdGroupeMaitre()#) eg
		            WHERE g.operateurid= #myFacture.getOperateurId()#
		                  AND lower(g.grp_ctl) like lower('%'||''||'%')
		                  AND g.type_grp_ctl=2
		                  AND g.idgrp_ctl=nvl('',g.idgrp_ctl)
		                  AND g.idgrp_ctl=eg.idgrp_ctl (+)
		                  AND NOT EXISTS (  SELECT 1 
		                                    FROM  version_grp_ctl v2 
		                                    WHERE v2.idracine= #getIdGroupeMaitre()# AND v2.idgrp_ctl=g.idgrp_ctl)
		            GROUP BY g.idgrp_ctl,g.grp_ctl, eg.idgrp_ctl, g.grp_segment
		            ) g1            
		    WHERE g1.idgrp_ctl=d1.idgrp_ctl (+)
		    ORDER BY g1.grp_segment, g1.grp_ctl
		</cfquery>		
		
		<!---pkg_cv_grcl_facturation.liste_grp_ctl_abos( p_operateurid => 	:p_operateurid,
			                                             p_libelle => 		:p_libelle,
			                                             p_idracine => 		:p_idracine,
			                                             p_retour => 		:p_retour);
													
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_facturation.liste_grp_ctl_abos">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_operateurId" 		value="#paramsRecherche.getOperateurId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" 	variable="p_chaine" 		   	value="#paramsRecherche.getChaine()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGroupeMaitre" 	value="#THIS.getIdGroupeMaitre()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGrpCtl" 			value="0" null="true"/>
			<cfprocresult name="qListeGrpProduitsAbos"/>        
		</cfstoredproc> 
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_facturation.liste_grp_ctl_Consos">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_operateurId" 		value="#paramsRecherche.getOperateurId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" 	variable="p_chaine" 		   	value="#paramsRecherche.getChaine()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGroupeMaitre" 	value="#THIS.getIdGroupeMaitre()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGrpCtl"		 	value="0" null="true"/>
			<cfprocresult name="qListeGrpProduitsConsos"/>        
		</cfstoredproc>--->
		
		<cfset tabQListeGrpProduits =  arraynew(1)>
		<cfset arrayappend(tabQListeGrpProduits,qListeGrpProduitsAbos)>
		<cfset arrayappend(tabQListeGrpProduits,qListeGrpProduitsConsos)>		
		<cfreturn tabQListeGrpProduits>
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Fournit la liste des groupe de produits ï¿½laborï¿½s par Consotel pour un groupe de ligne.
					  
		Param in 
			ParamsRecherche
		Params out
			query
			  		  	
	--->
	<cffunction name="getGrpProduitsFactureGroupeLigneAsQuery" access="Remote" output="false" returntype="array">
		<cfargument name="myFacture" type="Facture" required="true"/>
		<cfset tabQListeGrpProduits = queryNew()>
		<!---pkg_cv_glig_facturation.liste_grp_ctl_abos( p_operateurid => 	:p_operateurid,
			                                             p_libelle => 		:p_libelle,
			                                             p_idracine => 		:p_idGroupeLigne,
			                                             p_retour => 		:p_retour);
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_glig_facturation.liste_grp_ctl_abos">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_operateurId" 		value="#paramsRecherche.getOperateurId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" 	variable="p_chaine" 		   	value="#paramsRecherche.getChaine()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGroupeLigne" 		value="#THIS.getIdGroupeClient()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGrpCtl" 			value="0" null="true"/>
			<cfprocresult name="qListeGrpProduitsAbos"/>        
		</cfstoredproc>
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_glig_facturation.liste_grp_ctl_Consos">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_operateurId" 		value="#paramsRecherche.getOperateurId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" 	variable="p_chaine" 		   	value="#paramsRecherche.getChaine()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGroupeLigne" 		value="#THIS.getIdGroupeClient()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGrpCtl"		 	value="0" null="true"/>
			<cfprocresult name="qListeGrpProduitsConsos"/>        
		</cfstoredproc>
		
		<cfset tabQListeGrpProduits =  arraynew(1)>
		<cfset arrayappend(tabQListeGrpProduits,qListeGrpProduitsAbos)>
		<cfset arrayappend(tabQListeGrpProduits,qListeGrpProduitsConsos)>		--->											
		<cfreturn tabQListeGrpProduits>
	</cffunction>

</cfcomponent>