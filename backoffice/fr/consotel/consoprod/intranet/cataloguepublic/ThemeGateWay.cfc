<!--- =========================================================================
Classe: ThemeGateWay
Auteur: 
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="ThemeGateWay" extends="AccessObject">
<!--- METHODS --->
	<cffunction name="getThemesBouygues" access="public" returntype="Theme[]" output="false">
	</cffunction>
	<cffunction name="getThemesConsoview" access="public" returntype="Theme[]" output="false">
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Retourne la liste des themes pour le controle de facturation
		
		Params out : 
			Theme[] 	
	--->
	<cffunction name="getThemesControle" access="public" returntype="Theme[]" output="false">		
		<cfset qlisteThemes = getThemeControleAsQuery(1)>	
		<cfset tabThemes = arrayNew(1)>			
		<cfloop query="qlisteThemes">
			<cfset ptheme = createObject("component","Theme")>			
			<cfscript>
			//Initialize the CFC with the properties values.
				ptheme.setId(qlisteThemes.IDTHEME_PRODUIT);
				ptheme.setLibelle(qlisteThemes.THEME_LIBELLE);
				ptheme.setTypeTheme(qlisteThemes.TYPE_THEME);
			//	ptheme.setIdCategorie(proprietes.);			
			</cfscript>
			<cfset arrayAppend(tabThemes,ptheme)>
		</cfloop>
		<cfreturn tabThemes>
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Retourne la liste des themes pour le controle de facturation
		
		Params out : 
			query 	
	--->
	<cffunction name="getThemeControleAsQuery" access="public" returntype="query" output="false">
		<cfargument name="idprofile" required="true" type="numeric">
		<!--- TODO: PKG_CV_FACTURATION.GET_THEMES_CONTROLE --->
		<!--- 
			PROCEDURE			
				PKG_CV_FACTURATION.GET_THEMES_CONTROLE
			PARAM	
			in			
				p_idGroupe_client		INTEGER
				p_produitClientId		INTEGER
				p_dateDebut				DATE
				p_dateFin				DATE
				
			out				  	
				p_retour				QUERY				
		 --->
		<!--- 
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_FACTURATION.GET_THEMES_CONTROLE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGroupe_client" 	value="#this.getIdGroupeClient()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_produitClientId"   	value="#pProduit.getId()#"/>
			<cfprocparam cfsqltype="CF_SQL_DATE"  	 	type="in" 	variable="p_dateDebut" 			value="#pParamsRecherche.getDateDebut()#"/>
			<cfprocparam cfsqltype="CF_SQL_DATE"  	 	type="in" 	variable="p_dateFin" 			value="#pParamsRecherche.getDateFin()#"/>
			<cfprocresult name="qListeVersion"/>        
		</cfstoredproc>
		--->
		
		<cfquery name="qListeVersion" datasource="#SESSION.OFFREDSN#">			
			select * from theme_produit t 
			where t.theme_libelle NOT LIKE '%vide%'
			AND t.theme_libelle NOT LIKE '%Remises%'
			AND t.theme_libelle NOT LIKE '%Remises%'
			AND t.theme_libelle NOT LIKE '%Rï¿½gularisations%'
			order by t.ordre_affichage
 
		</cfquery>		
		<cfreturn qListeVersion>
	</cffunction>
</cfcomponent>