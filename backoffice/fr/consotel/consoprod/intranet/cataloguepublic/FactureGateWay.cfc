<cfcomponent output="false" extends="AccessObject">
	
	
	<!--- <cffunction name="getFacture" access="remote" output="false" returntype="Facture">
		<cfargument name="facture" type="Facture">				
		<cfreturn facture.get()/>
	</cffunction>
	
	<cffunction name="saveFacture" access="remote" output="false" returntype="numeric">
		<cfargument name="facture" required="true" type="Facture">				
		<cfreturn facture.save()/>
	</cffunction> --->
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 10/30/2007
		
		Description : Fournit la liste des lignes de facturation de la facture qui concerne les produits marquï¿½s pour le contrï¿½le
					  et qui sont facturï¿½s sur toute la pï¿½riode de la facture
		Param in 
			Facture
		Param out	
			LigneFacturation[]
	--->
	<cffunction name="getDetailFacture" access="remote" output="false" returntype="LigneFacturation[]">
		<cfargument name="myfacture" type="Facture" required="true">		
		<cfreturn myfacture.getDetail()/>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 10/30/2007
		
		Description : Fournit la liste des lignes de facturation de la facture qui concerne les produits marquï¿½s pour le contrï¿½le
					  et qui sont facturï¿½s sur toute la pï¿½riode de la facture
		Param in 
			Facture
		Param out	
			query
	--->
	<cffunction name="getDetailFactureAsQuery" access="remote" output="false" returntype="Query">
		<cfargument name="myfacture" type="Facture" required="true">
		<cfreturn myfacture.getDetailAsQuery()/>
	</cffunction>

<!---
		
		Auteur : samuel.divioka
		
		Date : 11/06/2007
		
		Description : Retourne la liste des factures d'un client contenant le produit sur une pï¿½riode donnï¿½e.
					  La facture est retournï¿½ si et seulement si le produit est facturï¿½ ï¿½ 100% sur la pï¿½riode de la facture	
		Param in 
			LigneFacturation une agregation de lignes de facturation
		Param out
			Facture[]
	
	--->
	<cffunction name="getListeFacturesWithProduit" access="remote" output="false" returntype="Facture[]">
		<cfargument name="myLigneFacturation" type="LigneFacturation" required="true">
		<cfset qListeFacture = getListeFacturesAsQuery(myLigneFacturation)>
		
		<cfset tabFactures = arrayNew(1)>
		<cfset myFacture = "">
		
		<cfset i = 1>
		
		<cfLoop query="qListeFacture">
			<cfset myFacture = createObject("component","Facture").setProprietes(qListeFacture[i])>
			<cfset arrayAppend(tabFactures,myFacture)>
			<cfset i = i + 1>
		</cfLoop>
		
		<cfreturn tabFactures />
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/06/2007
		
		Description : la liste des organisation si on est en racine sinon l'orga sur laquel on se trouve
		
		Param in
		 
			
			
		Param out
			query
	
	--->
	<cffunction name="getListeOrganistaionsAsQuery" access="remote" output="false" returntype="query">
		<cfset type = getTypePerimetre()>
		
		<cfif LCase(type) eq "groupe">
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GLOBAL.GET_ORGANISATIONS">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe" value="#getidGroupe()#"/>
				<cfprocresult name="qListeOrgas"/>        
			</cfstoredproc>
			<cfreturn qListeOrgas/>
		<cfelse>
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GLOBAL.GET_ORGABYACCES">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupeLigne" value="#getidGroupe()#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_iduser" value="#getidUser()#"/>
				<cfprocresult name="qOrga"/>        
			</cfstoredproc>
			<cfquery name="qResult" dbtype="query">
				select * from qOrga where TYPE_ORGA <> 'OPE'
			</cfquery>
			<cfreturn qResult/>
		</cfif>
		
	</cffunction>

	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 10/30/2007
		
		Description : Retourne la liste des factures d'un client rï¿½pondant au critï¿½res de recherche
		
		Param in 
			ParamsRecherche les paramï¿½tre de rechercehe
		Param out
			Facture[]
	
	--->
	<cffunction name="getListeFacturesPerimetre" access="remote" output="false" returntype="Facture[]">
		<cfargument name="myParamsRecherche" type="ParamsRecherche" required="true">
		<cfset qListeFacture = Evaluate("getListeFactures#getTypePerimetre()#AsQuery(myParamsRecherche)")>		
		<cfset tabFactures = arrayNew(1)>
		<cfLoop query="qListeFacture">
			<cfset myFacture = createObject("component","Facture")>
			<cfscript>
				
				//myFacture.setOperateurId(qListeFacture.OPERATEURID);				
				myFacture.setOperateurLibelle(qListeFacture.OPNOM);
				myFacture.setNumero(qListeFacture.NUMERO_FACTURE);
				myFacture.setCompteFacturation(qListeFacture.COMPTE_FACTURATION);
				myFacture.setCompteFacturationId(qListeFacture.IDCOMPTE_FACTURATION);
				myFacture.setPeriodeId(qListeFacture.IDINVENTAIRE_PERIODE);
				myFacture.setDateDebut(qListeFacture.DATEDEB);
				myFacture.setDateFin(qListeFacture.DATEFIN);
				myFacture.setDateEmission(qListeFacture.DATE_EMISSION);
				myFacture.setMontant(qListeFacture.MONTANT_FACTURE);
				myFacture.setRefClientId(qListeFacture.IDREF_CLIENT);
				//myFacture.setLibelle(qListeFacture.LIBELLE);
								
				myFacture.setMontantVerifiable(qListeFacture.MONTANT_VERIFIABLE);
				myFacture.setMontantCalcule(qListeFacture.MONTANT_CALCULE);
				myFacture.setMontantCalculeFacture(qListeFacture.MONTANT_CALCULE + 20);
			
				myFacture.setVisee(qListeFacture.BOOL_VISA);
				myFacture.setViseeAna(qListeFacture.BOOL_VISAANA);
				myFacture.setControlee(qListeFacture.BOOL_VALIDE);
				myFacture.setExportee(qListeFacture.BOOL_EXPORTEE);
				//			
				myFacture.setCommentaireViser(qListeFacture.COMMENTAIRE_VISA);
				myFacture.setCommentaireViserAna(qListeFacture.COMMENTAIRE_VISAANA);
				myFacture.setCommentaireControler(qListeFacture.COMMENTAIRE_VALIDE);
				myFacture.setCommentaireExporter(qListeFacture.COMMENTAIRE_EXPORTEE);
				
			</cfscript>
			<cfset arrayAppend(tabFactures,myFacture)>
		</cfLoop>
		
		<cfreturn tabFactures />
	</cffunction>
		

	<!---
		
		Auteur : samuel.divioka
		
		Date : 10/30/2007
		
		Description : Retourne la liste des factures d'un client rï¿½pondant au critï¿½res de recherche
		
		Param in 
			ParamsRecherche les paramï¿½tre de rechercehe
		Param out
			query
	
	--->
	<cffunction name="getListeFacturesGroupeAsQuery" access="remote" output="false" returntype="query">
		<cfargument name="myParamsRecherche" type="ParamsRecherche" required="true">
		<!--- PROCEDURE PKG_CV_GRCL.ListFacture_v2 (    p_idracine                                     IN INTEGER,
			                                               p_operateurid                                  IN INTEGER,
			                                               p_NUMERO_FACTURE                               IN VARCHAR2, 
			                                               p_visee                                        IN INTEGER,
			                                               p_validee                                      IN INTEGER,
			                                               p_exportee                                     IN INTEGER,
			                                               p_datedeb                                      IN VARCHAR2,
			                                               p_datefin                                      IN VARCHAR2,
			                                               p_retour                                       OUT SYS_REFCURSOR) IS --->

		
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL_FACTURATION.ListFacture_v2">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#getIdGroupe()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_operateurid" value="#myParamsRecherche.getOperateurId()#" null="#iif((myParamsRecherche.getOperateurId() eq 0), de("yes"), de("no"))#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_numero_cle" value="#myParamsRecherche.getChaine()#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_viseesAna" value="#myParamsRecherche.getEtatViseAna()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_visees" value="#myParamsRecherche.getEtatVise()#"/>		
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_controlees" value="#myParamsRecherche.getEtatControle()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_exportees" value="#myParamsRecherche.getEtatExporte()#"/>			
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datedebut" value="#LSDateFormat(myParamsRecherche.getDateDebut(),'yyyy/mm/dd')#"/>	
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datefin" value="#LSDateFormat(myParamsRecherche.getDateFin(),'yyyy/mm/dd')#"/>    
			<cfprocresult name="qListeFacture"/>   
		</cfstoredproc>
		<cfreturn qListeFacture/>
	</cffunction>
	
	<cffunction name="getListeFacturesGroupeLigneAsQuery" access="remote" output="false" returntype="Query">
		<cfargument name="myParamsRecherche" type="ParamsRecherche" required="true">
		<!--- PKG_CV_GLIG_FACTURATION.ListFacture_v2(		   p_idGroupeClient                               IN INTEGER,
			                                               p_operateurid                                  IN INTEGER,
			                                               p_NUMERO_FACTURE                               IN VARCHAR2, 
			                                               p_visee                                        IN INTEGER,
			                                               p_validee                                      IN INTEGER,
			                                               p_exportee                                     IN INTEGER,
			                                               p_datedeb                                      IN VARCHAR2,
			                                               p_datefin                                      IN VARCHAR2,
			                                               p_retour                                       OUT SYS_REFCURSOR) IS --->
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GLIG_FACTURATION.ListFacture_v2">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#getIdGroupeLigne()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_operateurid" value="#myParamsRecherche.getOperateurId()#" null="#iif((myParamsRecherche.getOperateurId() eq 0), de("yes"), de("no"))#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_numero_cle" value="#myParamsRecherche.getChaine()#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_viseesAna" value="#myParamsRecherche.getEtatViseAna()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_visees" value="#myParamsRecherche.getEtatVise()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_controlees" value="#myParamsRecherche.getEtatControle()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_exportees" value="#myParamsRecherche.getEtatExporte()#"/>			
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datedebut" value="#LSDateFormat(myParamsRecherche.getDateDebut(),'yyyy/mm/dd')#"/>	
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datefin" value="#LSDateFormat(myParamsRecherche.getDateFin(),'yyyy/mm/dd')#"/>      
			<cfprocresult name="qListeFacture"/>   
		</cfstoredproc>		
		<cfreturn qListeFacture/>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 10/30/2007
		
		Description : Marque la facture comme contrï¿½lï¿½e ou comme non contrï¿½lï¿½e.					 
					  Retourne 1 en cas de succees sinon retourne -1.
		
		Param in 
			Facture
			forceControle = 0 (Boolean);
		Param out 
			Numeric (1 en cas de succes sinon -1)
	--->
	<cffunction name="updateEtatControle" access="remote" output="false" returntype="numeric">
		<cfargument name="myFacture" type="Facture" required="true">
		<cfreturn myFacture.updateEtatControle()>			
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 10/30/2007
		
		Description : Marque la facture comme contrï¿½lï¿½e analytriquement parlant ou comme non contrï¿½lï¿½e.					 
					  Retourne 1 en cas de succees sinon retourne -1.
		
		Param in 
			Facture
		Param out 
			Numeric (1 en cas de succes sinon -1)
	--->
	<cffunction name="updateEtatViseAna" access="remote" output="false" returntype="numeric">
		<cfargument name="myFacture" type="Facture" required="true">
		<cfreturn myFacture.updateEtatViseAna()>			
	</cffunction>
	
	
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 04/29/2008
		
		Description : 
					Marque la facture comme bon Ã  Ãªtre exportÃ© ou non exportÃ©					
					Retourne 1 en cas de succees sinon retourne -1.
		
		Param in 
			Facture
		Param out 
			Numeric (1 en cas de succes sinon -1)
	--->
	<cffunction name="updateEtatExporte" access="remote" output="false" returntype="numeric">
		<cfargument name="myFacture" type="Facture" required="true">		
		<cfreturn myFacture.updateEtatExporte()>
	</cffunction>	
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 10/30/2007
		
		Description : 
					Marque la facture comme visee ou non visee					
					Retourne 1 en cas de succees sinon retourne -1.
		
		Param in 
			Facture
		Param out 
			Numeric (1 en cas de succes sinon -1)
	--->
	<cffunction name="updateEtatVise" access="remote" output="false" returntype="numeric">
		<cfargument name="myFacture" type="Facture" required="true">		
		<cfreturn myFacture.updateEtatVise()>
	</cffunction>	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/19/2007
		
		Description : 	Marque la facture comme exportee ou non exportee
						Exporte une facture au format CSV
		Param in 
			Facture
		Param out	
			Numeric (1 en cas de succes sinon -1)
	--->
	<cffunction name="exporterCSV" access="remote" output="true" returntype="string">
		<cfargument name="myfacture" type="Facture" required="true">				
		<cfreturn myfacture.exporterCSV(getidGroupe())/>		
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/7/2007
		
		Description : evolution des montants des facture sur 12 mois
	
	--->
	<cffunction name="getEvolutionCoutByCF" access="remote" output="true" returntype="array">		
		<cfargument name="pFacture" type="Facture" required="true">
		<cfreturn getEvolutionAsTab(Evaluate("getEvolutionCoutByCF#getTypePerimetre()#AsQuery(pFacture)"))>
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/5/2007
		
		Description : evolution des coï¿½ts de facturation d'un cf pour un groupe
		
		In Facure
		
		out query	
	--->
	<cffunction name="getEvolutionCoutByCFGroupeAsQuery" access="remote" output="true" returntype="query">		
		<cfargument name="pFacture" type="Facture" required="true">
		 <!--- PROCEDURE PKG_CV_GRCL_FACTURATION.EVOCF(      p_idgroupe_client                              IN INTEGER,
                                                                   p_idcompte_facturation                         IN INTEGER,
                                                                   p_datedeb                                      IN VARCHAR2,
                                                                   p_datefin                                      IN VARCHAR2,
                                                                   p_retour                                       OUT SYS_REFCURSOR) 
		 --->		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL_FACTURATION.EVOCF">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#getidGroupe()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcompte_facturation" value="#pFacture.getCompteFacturationId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datedeb" value="#LSDateFormat(DateAdd('m',-11,now()),'YYYY/MM/DD')#"/>			
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datefin" value="#LSDateFormat(now(),'YYYY/MM/DD')#"/>		
			<cfprocresult name="qEvolution"/>   
		</cfstoredproc>	
		<cfreturn qEvolution>
	</cffunction>	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/5/2007
		
		Description : evolution des coï¿½ts de facturation d'un cf pour un groupe
		
		In Facure
		
		out query	
	--->
	<cffunction name="getEvolutionCoutByCFGroupeLigneAsQuery" access="remote" output="true" returntype="query">		
		<cfargument name="pFacture" type="Facture" required="true">
		 <!--- PROCEDURE PKG_CV_GRCL_FACTURATION.EVOCF(      p_idgroupe_client                              IN INTEGER,
                                                                   p_idcompte_facturation                         IN INTEGER,
                                                                   p_datedeb                                      IN VARCHAR2,
                                                                   p_datefin                                      IN VARCHAR2,
                                                                   p_retour                                       OUT SYS_REFCURSOR) 
		 --->		
		
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GLIG_FACTURATION.EVOCF">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#getidGroupeLigne()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcompte_facturation" value="#pFacture.getCompteFacturationId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datedeb" value="#LSDateFormat(DateAdd('m',-11,now()),'YYYY/MM/DD')#"/>			
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datefin" value="#LSDateFormat(now(),'YYYY/MM/DD')#"/>		
			<cfprocresult name="qEvolution"/>   
		</cfstoredproc>			
		<cfreturn qEvolution>
		
	</cffunction>	
				
		
		
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/7/2007
		
		Description : transpose les donnï¿½es par mois
	
	--->	
	<cffunction name="getEvolutionAsTab" returntype="array">
		<cfargument name="qEvolution" type="query" required="true">		
		
		<cfquery name="listeMois" dbtype="query">
			select MOIS from qEvolution group by MOIS order by MOIS ASC
		</cfquery>		
		
		
		<cfset coutMoyenConsos = 0>
		<cfset coutMoyenAbos = 0>
		
		<cfquery name="qCoutMoyenAbos" dbtype="query">
			select AVG(MONTANT_FINAL) as COUTMOYENABO from qEvolution where TYPE_THEME = 'Abonnements' and MONTANT_FINAL <> 0
		</cfquery>
		
		<cfquery name="qCoutMoyenConsos" dbtype="query">
			select AVG(MONTANT_FINAL) as COUTMOYENCONSO from qEvolution where TYPE_THEME = 'Consommations' and MONTANT_FINAL <> 0
		</cfquery>
		
		<cfif qCoutMoyenConsos.recordCount eq 0>
			<cfset coutMoyenConsos = 0>
		<cfelse>
			<cfset coutMoyenConsos = qCoutMoyenConsos.COUTMOYENCONSO[1]>
		</cfif>		
		
		<cfif qCoutMoyenAbos.recordCount eq 0>
			<cfset coutMoyenAbos = 0>
		<cfelse>
			<cfset coutMoyenAbos = qCoutMoyenAbos.COUTMOYENABO[1]>
		</cfif>		
		
		<cfset tab = arrayNew(1)>
		<cfloop query="listeMois">
			<cfset donnee = createobject("component","DataEvolution")>
			<cfset donnee.setMOIS(listeMois.MOIS)>
			<cfset arrayAppend(tab,donnee)>
		
		</cfloop>
				
		<cfloop query="qEvolution">
			<cfloop index="i" from="1" to="#arrayLen(tab)#">
				<cfif qEvolution.MOIS eq tab[i].getMOIS()>
					<cfif LCASE(qEvolution.TYPE_THEME) eq  "Abonnements">
						<cfset tab[i].setCOUTABO(qEvolution.MONTANT_FINAL)>
						<cfset tab[i].setRATIOCOUTABO(qEvolution.RATIO_MOYENNE)>
						<cfset tab[i].setCOUTMOYENABO(coutMoyenAbos)>
					<cfelse>
						<cfset tab[i].setCOUTCONSO(qEvolution.MONTANT_FINAL)>
						<cfset tab[i].setRATIOCOUTCONSO(qEvolution.RATIO_MOYENNE)>
						<cfset tab[i].setCOUTMOYENCONSO(coutMoyenConsos)>
					</cfif> 
						<cfset tab[i].setRATIOMOYENNE(100)>
				</cfif>					
			</cfloop>			
		</cfloop>			 		
		<cfreturn tab>
	</cffunction>
	
	<!---		
		Auteur : samuel.divioka		
		Date : 12/14/2007		
		Description : Exporte les donnes du graph des ï¿½volutions des ratio cout au format CSV
		Param in 	
			facture		
		Param out
			string le nom du fichier si ok sinon 'erreur'	
	--->
	<cffunction name="exporterEvolutionRatioCoutByCfCSV" access="public" output="true" returntype="any">
		<cfargument name="pFacture" type="Facture" required="true">
		<cftry>
			<cfset rootPath=expandPath("/")>
			<cfset UnicId = createUUID()>
			<cfset fileName = UnicId&"_"&"EvolutionRatioCoutsCompte_"& pFacture.getcompteFacturation()&".csv">	
			
			
			<cfsavecontent variable="contentObj">
				<cfsetting enablecfoutputonly="true"/>										 				
				<cfset aDetail =getEvolutionCoutByCF(pFacture)>				
				<cfset NewLine = Chr(13) & Chr(10)>		
				<cfheader name="Content-Disposition" value="inline;filename=#fileName#" charset="utf-8">
				<cfcontent type="text/plain">		
				<cfoutput>mois;montant abos;motant moyen abos;ratio abos;montant consos;montant moyen consos;ratio consos#NewLine#</cfoutput><cfloop index="i" from="1" to="#arrayLen(aDetail)#"><cfoutput>#TRIM(LSDateFormat(aDetail[i].getMOIS(),'DD/MM/YYYY'))#;#TRIM(LSNumberFormat(aDetail[i].getCOUTABO(),"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getCOUTMOYENABO(),"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getRATIOCOUTABO()/100,"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getCOUTCONSO(),"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getCOUTMOYENCONSO(),"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getRATIOCOUTCONSO()/100,"________.__"))#;#NewLine#</cfoutput></cfloop>
			</cfsavecontent>		

			<!--- Crï¿½ation du fichier CSV --->			
			<cffile action="write" file="#rootPath#/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/#fileName#" charset="utf-8"
					addnewline="true" fixnewline="true" output="#contentObj#">
			 
			<cfreturn "#fileName#">			
		<cfcatch type="any">					
				<cfreturn "error">	
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/13/2007
		
		Description : Export des donnï¿½es du graph des evolutions des coï¿½ts par compte de facturation
		
		Param in 	
			facture		
		Param out
			string le nom du fichier si ok sinon 'erreur'
	--->
	<cffunction name="exporterEvolutionCoutByCfCSV" access="public" output="true" returntype="any">
		<cfargument name="pFacture" type="Facture" required="true">
		<cftry>
			<cfset rootPath=expandPath("/")>
			<cfset UnicId = createUUID()>
			<cfset fileName = UnicId&"_"&"EvolutionCoutsCompte_"&pFacture.getcompteFacturation()&".csv">	
			
			<cfsavecontent variable="contentObj">
				<cfsetting enablecfoutputonly="true"/>										 				
				<cfset aDetail =getEvolutionCoutByCF(pFacture)>				
				<cfset NewLine = Chr(13) & Chr(10)>		
				<cfheader name="Content-Disposition" value="inline;filename=#fileName#" charset="utf-8">
				<cfcontent type="text/plain">		
				<cfoutput>mois;montant abos;motant moyen abos;ratio abos;montant consos;montant moyen consos;ratio consos#NewLine#</cfoutput><cfloop index="i" from="1" to="#arrayLen(aDetail)#"><cfoutput>#TRIM(LSDateFormat(aDetail[i].getMOIS(),'DD/MM/YYYY'))#;#TRIM(LSNumberFormat(aDetail[i].getCOUTABO(),"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getCOUTMOYENABO(),"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getRATIOCOUTABO()/100,"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getCOUTCONSO(),"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getCOUTMOYENCONSO(),"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getRATIOCOUTCONSO()/100,"________.__"))#;#NewLine#</cfoutput></cfloop>
			</cfsavecontent>		
			<!--- Crï¿½ation du fichier CSV --->			
			<cffile action="write" file="#rootPath#/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/#fileName#" charset="utf-8"
					addnewline="true" fixnewline="true" output="#contentObj#">
					 
			<cfreturn "#fileName#"> 		
		<cfcatch type="any">					
				<cfreturn "error">	
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/13/2007
		
		Description : Export des donnï¿½es du grid des facture
		
		Param in 	
			ParamsRecherche		
		Param out
			string le nom du fichier si ok sinon 'erreur'
	--->
	<cffunction name="exporterListeFacturesCSV" access="public" output="true" returntype="any">		
		<cfargument name="myParamsRecherche" type="ParamsRecherche" required="true">
		<cftry>
			<cfset rootPath=expandPath("/")>
			<cfset UnicId = createUUID()>
			<cfset fileName = UnicId&"_"&"EtatFactures.csv">	
			
			<cfsavecontent variable="contentObj">
				<cfsetting enablecfoutputonly="true"/>										 				
				<cfset aDetail = getListeFacturesPerimetre(myParamsRecherche)>								
				<cfset NewLine = Chr(13) & Chr(10)>		   
				<cfheader name="Content-Disposition" value="inline;filename=#fileName#" charset="utf-8">
				<cfcontent type="text/plain">
				<cfoutput>opï¿½rateur;compte facturation;numero facture;date ï¿½mission;montant facture;prct ctrl;montant controlable;produits controles (facture);produits controles (controle);visee;commentaire visa;visee ana;commentaire visa ana;controlee;commentaire controle;ERP;commentaire ERP;#NewLine#</cfoutput>
				<cfloop index="i" from="1" to="#arrayLen(aDetail)#"><cfif aDetail[i].getMontant() eq 0><cfset pct = LSNumberFormat(100,"________.__")><cfelse><cfset pct = LSNumberFormat(aDetail[i].getMontantVerifiable()/aDetail[i].getMontant()*100,"________.__")></cfif>
								
				<cfset commentaireViserAna = formateString(aDetail[i].getCommentaireViserAna())/>
				<cfset commentaireViser = formateString(aDetail[i].getCommentaireViser())/>
				<cfset commentaireControler = formateString(aDetail[i].getCommentaireControler())/>
				<cfset commentaireExporter = formateString(aDetail[i].getCommentaireExporter())/>
				
				<cfoutput>#TRIM(aDetail[i].getOperateurLibelle())#;#TRIM(toString(aDetail[i].getCompteFacturation()))#;#TRIM(toString(aDetail[i].getNumero()))#;#TRIM(LSDateFormat(aDetail[i].getDateEmission(),'DD/MM/YYYY'))#;#TRIM(LSNumberFormat(aDetail[i].getMontant(),"________.__"))#;#TRIM(pct)#;#TRIM(LSNumberFormat(aDetail[i].getMontantVerifiable(),"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getMontantCalculeFacture(),"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getMontantCalcule(),"________.__"))#;#TRIM(aDetail[i].getVisee())#;#commentaireViser#;#TRIM(aDetail[i].getViseeAna())#;#commentaireViserAna#;#TRIM(aDetail[i].getControlee())#;#commentaireControler#;#TRIM(aDetail[i].getExportee())#;#commentaireExporter#;#NewLine#</cfoutput></cfloop>
			</cfsavecontent>		
			<!--- Crï¿½ation du fichier CSV --->			
			<cffile action="write" file="#rootPath#/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/#fileName#" charset="utf-8" 
					addnewline="true" fixnewline="true" output="#contentObj#">
					 
			<cfreturn "#fileName#"> 		
		<cfcatch type="any">					
				<cfreturn "error">	
			</cfcatch>
		</cftry>
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/6/2007
		
		Description : Ressources hors inventaire facturï¿½es
		
		Ressoures (couple produit-ligne) facturï¿½es hors inventaire = 
		-	ressources qui n?ont jamais fait partie de l?inventaire et qui sont facturï¿½es ou 
		-	qui sont sortis de l?inventaire avant le debut de la pï¿½riode de la facture mais qui sont toujours facturï¿½es ou
		-	qui sont entrï¿½s dans l?inventaire aprï¿½s la fin de la facture et qui sont dï¿½jï¿½ facturï¿½es
		
		1 - On est d?accord que pour la partie abos, je prends tout ce qui est facturï¿½ sur ces ressources hors inventaire, et qui est de type de theme Abos.
		
		2 - Pour ce qui est des consos, tu me demandes de prendre, parmi les lignes des ressources hors inventaire, ceux qui ont un abo de sur theme ï¿½ Lignes ï¿½, et d?afficher les consos sur ces lignes.
		C?est bien ï¿½a ?
		
		Dans la feuille explicative, partie consos, tu me dis ï¿½ Faire remonter tous les produits, sauf ceux dï¿½jï¿½ identifiï¿½ par le processus ci-dessus afin d?eviter une remontï¿½ en double ï¿½. OK sauf que dans le 1er cas je ramene des abos et dans l?autre des consos.
		Donc il ne peut y avoir de remontï¿½ de doublon, je me trompe ?

	--->
	<cffunction access="remote" name="getMontantRessourcesHorsInventaire" returntype="array">
		<cfargument name="myFacture" type="Facture" required="true">
		<cfreturn myFacture.getMontantRessourcesHorsInventaire()>
	</cffunction>
	
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/11/2007
		
		Description : forurnit la liste des ressources hors inventaire d'une facture
	
	--->
	<cffunction access="remote" name="getDetailRessourcesHorsInventaire" returntype="Ressource[]">
		<cfargument name="myFacture" type="Facture" required="true">
		<cfargument name="typeTheme" type="string" required="true">
		<cfreturn myFacture.getDetailRessourcesHorsInventaire(typeTheme)>
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/11/2007
		
		Description : exporte la liste des ressources hors inventaire d'une facture au format CVS
	
	--->
	<cffunction name="exporterRessourcesHICSV" access="remote" output="true" returntype="string">
		<cfargument name="myfacture" type="Facture" required="true">
		<cfargument name="typeTheme" type="string" required="true">
		<cfreturn myfacture.exporterRessourcesHICSV(typeTheme)/>
	</cffunction>
	
	<!---
		Auteur : samuel.divioka
		
		Date : 23/04/2008
		
		Description : eclate la facture dans une organisation
	--->
	<cffunction name="detailFactureByOrga" access="remote" output="true" returntype="query">
		<cfargument name="myfacture" type="Facture" required="true">
		<cfargument name="idOrga" type="numeric" required="true">		
		<cfreturn myfacture.detailFactureByOrga(idOrga)/>
	</cffunction>
	
	
	<!---
		Auteur : samuel.divioka
		
		Date : 23/04/2008
		
		Description : total de la facture dans une organisation
	--->
	<cffunction name="totalFactureByOrga" access="remote" output="true" returntype="query">
		<cfargument name="myfacture" type="Facture" required="true">
		<cfargument name="idOrga" type="numeric" required="true">		
		<cfreturn myfacture.totalFactureByOrga(idOrga)/>
	</cffunction>
	
	
	<cffunction name="formateString" access="private" returntype="string">
		<cfargument name="chaine">
		<cfset chaineTmp1 = replace(TRIM(chaine),"-"," ","all")>
		<cfset chaineTmp2 = replace(chaineTmp1,";"," ","all")>
		<cfset chaineTmp3 = replace(chaineTmp2,#chr(13)#," ","all")>
		<cfset returnedChaine = replace(chaineTmp3,#chr(10)#," ","all")>
		<cfreturn returnedChaine>
	</cffunction>
</cfcomponent>