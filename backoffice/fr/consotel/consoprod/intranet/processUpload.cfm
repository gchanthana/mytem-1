<cftry>
	<cfset ImportTmp="/webroot/intranetTmp/">
 
<!--- 	Begin coying the file on Temp Directory		--->
	<cffile action="upload" 
			mode="777"
				filefield="filedata" 
					destination="#ImportTmp#" 
						nameconflict="overwrite" 
							accept="application/octet-stream"
								result="uploadfile"/> 

<!--- 	Begin copying the file on Final Directory	--->			
	<cfftp 	action="open" 
			server="pelican" 
			port="21" 
			username="intranet"
			password="intranet" 
			connection="ftp">
	
	<cfftp action="putfile" localfile="#ImportTmp##uploadfile.serverFile#"
			remotefile="./autres/import_orga/#uploadfile.serverFile#"
				connection="ftp">
	
	<cfftp action="close" connection="ftp">

<!--- 	Deleting the file on Tmp Directory			--->
	<cffile action="delete" file="#ImportTmp##uploadfile.serverFile#">
	
<!---	 Begin checking the file extension of uploaded files --->
		<cfset acceptedFileExtensions = "jpg,jpeg,gif,png,pdf,flv,txt,doc,rtf,zip,csv,xls"/>
		<cfset filecheck = listFindNoCase(acceptedFileExtensions,File.ServerFileExt)/>

		<cfif filecheck eq false>
			<cffile action="delete" file="#ImportTmp##File.ServerFile#"/>
		</cfif> 

<!--- 
Should any error occur output a pdf with all the details.
It is difficult to debug an error from this file because no debug information is 
diplayed on page as its called from within the Flash UI.  If your files are not uploading check 
to see if an errordebug.pdf has been generated.
--->
		<cfcatch type="any">
			<cfdocument format="PDF" overwrite="yes" filename="errordebug.pdf">
				<cfdump var="#cfcatch#"/>
			</cfdocument>
		</cfcatch>
</cftry>