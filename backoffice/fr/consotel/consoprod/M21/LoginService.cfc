<cfcomponent name="LoginService">
<!--- création d'un nouvel utilisateur --->
	<cffunction name="createUtilisateur" access="public" returntype="numeric" output="false">
		<cfargument name="nom" 				required="true" type="string"/>
		<cfargument name="prenom" 			required="true" type="string"/>
		<cfargument name="adresse" 			required="true" type="string"/>
		<cfargument name="codepostal" 		required="true" type="string"/>
		<cfargument name="ville" 			required="true" type="string"/>
		<cfargument name="telephone" 		required="true" type="string"/>
		<cfargument name="direction" 		required="true" type="string"/>
		<cfargument name="email" 			required="true" type="string"/>
		<cfargument name="password" 		required="true" type="string"/>
		<cfargument name="restrictionIP"	required="true" type="numeric"/>
		<cfargument name="idType"	required="true" type="numeric"/>
		<cfargument name="idRevendeur"	required="true" type="numeric"/>


		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M21.add_app_login">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_LOGIN_NOM" 		value="#nom#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_LOGIN_PRENOM" 	value="#prenom#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_adresse" 			value="#adresse#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_codepostal" 		value="#codepostal#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_ville" 			value="#ville#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_telephone" 		value="#telephone#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_direction" 		value="#direction#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="P_LOGIN_EMAIL" 		value="#email#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="P_LOGIN_PWD" 		value="#password#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="P_RESTRICTION_IP" 	value="#restrictionIP#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_IDRACINE" 		value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="P_TYPE" 			value="#idType#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="P_REVENDEUR" 		value="#idRevendeur#" null="#iif((idRevendeur eq -1), de("yes"), de("no"))#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
			</cfstoredproc>

<!---  le nouvel utilisateur hérite du meme style que celui qui crée le login  ei  SESSION.PERIMETRE.GROUP_CODE_STYLE  
       procédure affectant le style de l'utilisateur loggé à celui nouvellement créé' --->
 
 			<cfset idNewLogin =p_retour >
			<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
		    <cfset idGestionnaire = Session.USER.CLIENTACCESSID>
		    <cfset idCodeStyle = SESSION.PERIMETRE.GROUP_CODE_STYLE>
	
		    <cfstoredproc datasource="#session.offreDSN#" procedure="PKG_INTRANET.updateAffectation_V4">
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idNewLogin#"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idRacine#"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idGestionnaire#"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idCodeStyle#"/>
					<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
				</cfstoredproc>
	  

						
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_facturation_v3.addDefaultProfil">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#p_retour#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
			</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>

<!--- OK --->
	<cffunction name="updateUtilisateur" access="public" returntype="numeric" output="false">
		<cfargument name="appLogin"			required="true" type="numeric"/>
		<cfargument name="nom" 				required="true" type="string"/>
		<cfargument name="prenom" 			required="true" type="string"/>
		<cfargument name="adresse" 			required="true" type="string"/>
		<cfargument name="codepostal" 		required="true" type="string"/>
		<cfargument name="ville" 			required="true" type="string"/>
		<cfargument name="telephone" 		required="true" type="string"/>
		<cfargument name="direction" 		required="true" type="string"/>
		<cfargument name="email" 			required="true" type="string"/>
		<cfargument name="password" 		required="true" type="string"/>
		<cfargument name="restrictionIP"	required="true" type="numeric"/>
		<cfargument name="idType"	required="true" type="numeric"/>
		<cfargument name="idRevendeur"	required="true" type="numeric"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M21.upd_app_login">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_APP_LOGINID" 		value="#appLogin#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_LOGIN_NOM" 		value="#nom#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_LOGIN_PRENOM" 	value="#prenom#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_adresse" 			value="#adresse#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_codepostal" 		value="#codepostal#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_ville" 			value="#ville#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_telephone" 		value="#telephone#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_direction" 		value="#direction#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="P_LOGIN_EMAIL" 		value="#email#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="P_LOGIN_PWD" 		value="#password#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="P_RESTRICTION_IP" 	value="#restrictionIP#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="P_TYPE" 			value="#idType#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="P_REVENDEUR" 		value="#idRevendeur#" null="#iif((idRevendeur eq -1), de("yes"), de("no"))#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>
	
	<!--- Duplication des styles et accès d'un utlisateur vers un autre param : APP_LOGINID,ID_RACINE,CLIENTACCESSID,GROUP_CODE_STYLE --->
	<cffunction name="updateAffectation" output="false" description="Duplication des styles et accès" access="remote" returntype="NUMERIC">
		<cfargument name="tableau" required="true" type="array" />	
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_INTRANET.updateAffectation_V4">
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#tableau[2]#"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#tableau[1]#"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#SESSION.USER.CLIENTACCESSID#"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.PERIMETRE.GROUP_CODE_STYLE#"/>
					<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
				</cfstoredproc>
		<cfreturn result>
		 
</cffunction>
	
</cfcomponent>
