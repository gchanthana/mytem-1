<cfcomponent output="false" name="Site">

	<!--- ENREGISTREMENT D'UN SITE --->
	<cffunction name="addSite" access="public" returntype="numeric" description="Permet de creer un site">
		<cfargument name="IDgroupe_client"		type="numeric" required="true">
		<cfargument name="Libelle_site" 		type="string"  required="true">
		<cfargument name="ref_site" 			type="string"  required="true">
		<cfargument name="code_interne_site" 	type="string"  required="true">
		<cfargument name="adr_site" 			type="string"  required="true">
		<cfargument name="cp_site" 				type="string"  required="true">
		<cfargument name="commune_site" 		type="string"  required="true">
		<cfargument name="commentaire_site" 	type="string"  required="true">
		<cfargument name="idpays" 				type="numeric" required="true">
		<cfargument name="islivraison" 			type="numeric" required="true">
		<cfargument name="isfacturation" 		type="numeric" required="true">
		
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.addSite_V3">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDgroupe_client#"		null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Libelle_site#" 		null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ref_site#" 			null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#code_interne_site#" 	null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#adr_site#" 			null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#cp_site#" 			null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commune_site#" 		null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaire_site#" 	null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idpays#" 				null="false">
 				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#isfacturation#" 		null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#islivraison#" 		null="false"> 
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- MAJ D'UN SITE --->
	<cffunction name="updateSite" access="public" returntype="numeric" description="Permet de modifier les infos d un site">
		<cfargument name="IDsite" 				type="numeric" required="true">
		<cfargument name="Libelle_site" 		type="string"  required="true">
		<cfargument name="ref_site" 			type="string"  required="true">
		<cfargument name="code_interne_site" 	type="string"  required="true">
		<cfargument name="adr_site" 			type="string"  required="true">
		<cfargument name="cp_site" 				type="string"  required="true">
		<cfargument name="commune_site" 		type="string"  required="true">
		<cfargument name="commentaire_site" 	type="string"  required="true">
		<cfargument name="idpays" 				type="numeric" required="true">
		<cfargument name="islivraison" 			type="numeric" required="true">
		<cfargument name="isfacturaiton" 		type="numeric" required="true">
		
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.updateSite_V3">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDsite#" 				null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Libelle_site#" 		null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ref_site#" 			null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#code_interne_site#" 	null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#adr_site#" 			null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#cp_site#" 			null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commune_site#" 		null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaire_site#" 	null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idpays#" 				null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#isfacturaiton#" 		null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#islivraison#" 		null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>
	
	<!--- MAJ DE PLUSIEURS SITE --->
	<cffunction name="updateSites" access="public" returntype="array" description="Permet de modifier les infos d un site">
		<cfargument name="siteArray" 		type="Array"  	required="true">
		<cfargument name="whatIsChampsMod" 	type="numeric"  required="true"><!--- 0-> 'LIVRAISON', 1-> 'FACTURATION' --->
		<cfargument name="isactive" 		type="numeric"  required="true"><!--- 0-> 'NON', 1-> 'OUI' --->
		
			<cfset idsSite = ArrayNew(1)>
			<cfset checkOK = 1>
			
			<cfloop index="idx" from="1" to="#ArrayLen(siteArray)#">
				<cfif checkOK GTE 1>
			
					<cfset IDsite = siteArray[idx].id_site>
					<cfset Libelle_site = siteArray[idx].libelle_site>
					<cfset ref_site = siteArray[idx].ref_site>
					<cfset code_interne_site= siteArray[idx].code_interne_site>
					<cfset adr_site = siteArray[idx].adr_site>
					<cfset cp_site = siteArray[idx].cp_site>
					<cfset commune_site = siteArray[idx].commune_site>
					<cfset commentaire_site = siteArray[idx].commentaire_site>
					<cfset idpays = siteArray[idx].idpays_site>
					
					<cfif whatIsChampsMod EQ 0>
						<cfset islivraison = isactive>
						<cfset isfacturation = formatBoolToInt(siteArray[idx].IS_FACTURATION)>
					<cfelse>
						<cfset islivraison = formatBoolToInt(siteArray[idx].IS_LIVRAISON)>
						<cfset isfacturation = isactive>
					</cfif>

					<!--- <cfset islivraison = formatBoolToInt(siteArray[idx].IS_LIVRAISON)>
					<cfset isfacturaiton = formatBoolToInt(siteArray[idx].IS_FACTURATION)> --->
				
					<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.updateSite_V3">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDsite#" 				null="false">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Libelle_site#" 		null="false">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ref_site#" 			null="false">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#code_interne_site#" 	null="false">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#adr_site#" 			null="false">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#cp_site#" 			null="false">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commune_site#" 		null="false">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaire_site#" 	null="false">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idpays#" 				null="false">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#isfacturation#" 		null="false">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#islivraison#" 		null="false">
						<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
					</cfstoredproc>
				
					<cfif p_retour GT -1>
						<cfset checkOK = 1>
						<cfset ArrayAppend(idsSite, checkOK)>
					<cfelse>
						<cfset checkOK = -1>
						<cfset ArrayAppend(idsSite, checkOK)>
					</cfif>
				</cfif>
			</cfloop>
			
		<cfreturn idsSite>
	</cffunction>
	
	<cffunction name="formatBoolToInt" access="private" returntype="numeric" description="Permet de modifier les infos d un site">
		<cfargument name="value" 	type="any"  required="true">
		
			<cfset boolFormated = -1>
			
			<cfif value EQ 'NO'>
				<cfset boolFormated = 0>
			<cfelse>
				<cfset boolFormated = 1>
			</cfif>
			
		<cfreturn boolFormated>
	</cffunction>

	<!--- RECUPERATION DE LISTE DE SITES DISPONIBLES --->
	<cffunction name="getSite" access="public" returntype="query" description="Retourne la liste des sites d un groupe client">
		<cfargument name="IDGroupe_client" 	type="numeric" required="true">
		<cfargument name="Search_texte" 	type="string"  required="true">
		<cfargument name="maxRow" 			type="string"  required="true">
			
			<!--- <cfset maxRow = 100>
			<cfif maxRow neq 1 >
				<cfset maxRow = -1>
			</cfif> --->
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getSite">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDGroupe_client#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Search_texte#" 	null="false">
				<cfprocresult name="p_result"><!---  maxrows="#maxRow#" --->
			</cfstoredproc>
			
		<cfreturn p_result>
	</cffunction>

</cfcomponent>