<!---
Dans : fr.consotel.parcview
---> 
<cfcomponent name="Application">
	<cfset THIS.name = "intranetV1">
	<cfset THIS.applicationtimeout="#createtimespan(2,0,0,0)#">
	<cfset THIS.sessionmanagement = TRUE>
	<cfset THIS.setclientcookies = TRUE>
	<cfset THIS.sessionTimeout="#createTimeSpan(0,1,30,0)#">
	<cfset BASE_A_UTILISER="ROCOFFRE">

	<cfif Not IsDefined('SESSION.OffreDSN')>
		<cfset SESSION.OffreDSN="#BASE_A_UTILISER#">
	</cfif>
	
	<cfif Not IsDefined('SESSION.CDRDSN')>
		<cfset SESSION.CDRDSN="roccdr">
	</cfif>
		
	<cfset SetLocale("French (Standard)")>

	<cfparam name="session.status" default="0">	

	<cffunction name="onApplicationStart">
		<cfargument name="ApplicationScope"/>			
		<cflog file="#THIS.name#" type="Information" text="Application #THIS.name# Started">		
	 </cffunction>

	<cffunction name="onApplicationEnd">
		<cfargument name="ApplicationScope" required="true"/>
	   	<cflog file="#THIS.name#" type="Information" text="Application #THIS.name# Ended">
	</cffunction>

	<cffunction name="onSessionStart">						
		<cfif Not IsDefined('SESSION.OffreDSN')>
			<cfset SESSION.OffreDSN="#BASE_A_UTILISER#">
		</cfif>
		<cfif Not IsDefined('SESSION.CDRDSN')>
			<cfset SESSION.CDRDSN="roccdr">
		</cfif>
		<cflog file="#THIS.name#" type="Information" text="Session : #Session.sessionid# started">
	</cffunction>

	<!--- <cffunction name="onRequestStart" access="remote" returntype="boolean">		
		
		
		<cflog file="#THIS.name#" type="Information" text="Session : #Session.sessionid# started">
		<cfset session.REMOTE_ADDR = CGI.REMOTE_ADDR>
		<cfset session.REMOTE_HOST = CGI.REMOTE_HOST>
		
		<cfif isDefined("session.status")>
			<cfreturn 'TRUE'>
		</cfif>		
	</cffunction>

	<cffunction name="onRequestEnd">
	 	<cfargument type="String" name = "targetTemplate"required="true"/>
			<cfif isDefined("session.status")>
			<cfif SESSION.status EQ 0>
				<cfthrow type="BACKOFFICE_SESSION_TIMEOUT" message="100" detail="BACKOFFICE_SESSION_TIMEOUT">			
			</cfif>
			<cfif SESSION.idclient EQ 0>
				<cfthrow message="SESSION TIME OUT" detail="SESSION FATIGUE" errorcode="1">											
			</cfif>
			</cfif>	  
		<cfreturn TRUE>
	</cffunction>   --->
	
	<cffunction name="onSessionEnd">
		<cfargument name = "SessionScope" required="true"/>
	   	<cflog file="#THIS.name#" type="Information" text="Session: #arguments.SessionScope.sessionid# ended">
	</cffunction>
</cfcomponent>
