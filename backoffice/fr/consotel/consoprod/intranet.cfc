<cfcomponent name="intranet" hint="Diverses fonctions utilisée par l'application entière">
	<cfset BASE_A_UTILISER_ICI="ROCOFFRE">

<!--- Gestion de la mise en session Coldfusion ou de la recuperation de la session Coldfusion dans Flex --->
	<!--- Getter --->
	<cffunction name="getValue" output="true" access="remote" returntype="any">
		<cfargument name="cle" type="string" default=""/>		
		<cfreturn Evaluate("session." & cle)>
	</cffunction>	
	<!--- Setter pour les types String --->
	<cffunction name="setString" output="true" access="remote" >
		<cfargument name="cle" type="string" default=""/>	
		<cfargument name="valeur" type="string" default=""/>		
		<cfset setSessionValue(cle,valeur)>
	</cffunction>
	<!--- Setter pour les types Struct --->
	<cffunction name="setStruct" output="true" access="remote" >
		<cfargument name="cle" type="string" default=""/>	
		<cfargument name="valeur" type="struct" default=""/>	
		<cfset setSessionValue(cle,valeur)>
	</cffunction>
	<!--- Setter pour les types Query --->
	<cffunction name="setQuery" output="true" access="remote" >
		<cfargument name="cle" type="string" default=""/>		
		<cfargument name="valeur" type="query" default=""/>	
		<cfset setSessionValue(cle,valeur)>
	</cffunction>
	<!--- Méthode privée pour le Setter --->
	<cffunction name="setSessionValue" output="true" access="private" >
		<cfargument name="cle" type="any" default=""/>		
		<cfargument name="valeur" type="any" default=""/>	
		<cfif StructKeyExists(session,cle)>
			<cfset "session.#cle#"=valeur>
		<cfelse>
			<cfset structInsert(session,cle,valeur)>
		</cfif>
	</cffunction>
	<!--- Méthode privée pour supprimer une cle de la session Coldfusion --->
	<cffunction name="deleteSessionValue" output="true" access="private" >
		<cfargument name="cle" type="any" default=""/>		
		<cfargument name="valeur" type="any" default=""/>	
		<cfif StructKeyExists(session,cle)>
			<cfset structDelete(session,cle)>
		</cfif>
	</cffunction>
	
	
	<cffunction name="GetLoginInfos" output="true" access="remote" returntype="query">
		<cfargument name="frm_username" type="String" default="0">
		<cfset qGetUser=queryNew("mailNickname,cn,mail,department","VarChar,VarChar,VarChar,VarChar")>
		<cftry>
		<cfset userOK=1>
		<cfldap filter="sAMAccountName=#frm_username#" action="query" server="192.168.3.119" username="consotel\infofacture" password="edifact"
				attributes="mailNickname,cn,mail,physicalDeliveryOfficeName,wWWHomePage,info,description,department" start="OU=SBSUsers,OU=Users,OU=MyBusiness,DC=consotel,DC=fr" name="qGetUser">
		<cfcatch type="any">
			<cfset userOK=0>
		</cfcatch>
		</cftry>
		<cfif userOK eq 1>
			<cfif qGetuser.recordCount neq 0>
				<cfset session.mailNickname=#qGetuser.mailNickname#>
				<cfset session.cn=#qGetuser.cn#>
				<cfset session.department=#qGetuser.department#>
				<cfset session.mail=#qGetuser.mail#>
				<cfset session.Quarantaine=qGetuser.wWWHomePage>
				<cfset session.status=1>
				<cfset session.username=qGetuser.mailNickname>
				<cfset session.erreur="0">
				<cfset session.tempsFirstConnexion=Now()>
				<cfset session.NombreClics=0>
				<cfset session.user.email=qGetuser.cn>
				<cfset session.clientID=qGetuser.physicalDeliveryOfficeName>
				<cfset session.offreDSN="#BASE_A_UTILISER_ICI#">
			<cfelse>
				<cfset session.erreur="1">
				<cfset session.status=0>
				<cfset session.login="0">
			</cfif>
		<cfelse>
			<cfset session.erreur="1">
			<cfset session.status=0>
			<cfset session.login="0">
		</cfif>
		
		<cfreturn qGetUser>
	</cffunction>

	<cffunction name="getListeConsotel" output="true" access="remote" returntype="query">
		<cfldap action="query" filter="company=CONSOTEL" server="192.168.3.119" username="consotel\infofacture" password="edifact" 
				attributes="mailNickname,cn,mail,physicalDeliveryOfficeName" start="OU=SBSUsers,OU=Users,OU=MyBusiness,DC=consotel,DC=fr" name="getUser" sort="cn">
		<cfquery name="result" dbtype="query">
			select *, MAILNICKNAME as data, CN as label
			from getUser
		</cfquery>
		<cfreturn result>
	</cffunction>
		
	<cffunction name="endAccessSession" access="remote" returntype="numeric">
<!--- 		<cfset SESSION.status = 2>
		<cfset structDelete(session,"cdrdsn")>
		<cfset structDelete(session,"user")>
		<cfset structDelete(session,"PATH")>
		<cfset structDelete(session,"REMOTE_ADDR")>
		<cfset structDelete(session,"REMOTE_HOST")>
		<cfset structDelete(session,"parametre")>
		<cfset structDelete(session,"dataInventaire")>
		<cfset structDelete(session,"dataUsage")> --->
		<cfset structclear(session)>
		<cfreturn 1>
	</cffunction>
</cfcomponent>