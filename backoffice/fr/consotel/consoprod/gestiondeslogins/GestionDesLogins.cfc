<cfcomponent output="false">

	<cffunction name="DisplayLogin" access="remote" returntype="query">
		<cfargument name="operateurid" type="numeric" required="true">
		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_INTEGRATION_FACTURE.display_login">
			<cfprocparam value="#operateurid#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qDisplayLogin">
		</cfstoredproc>

		<cfreturn qDisplayLogin>
	</cffunction>

	<cffunction name="DisplayLogin_v2" access="remote" returntype="query">
		<cfargument name="SEARCHTEXTE" type="string" required="true">
		<cfargument name="ORDERBYTEXTE" type="string" required="true">
		<cfargument name="OFFSET" type="numeric" required="true">
		<cfargument name="LIMIT" type="numeric" required="true">
		<cfargument name="OPERATEURID" type="numeric" required="true">


		<cfset TAB_SEARCH = ListToArray(SEARCHTEXTE, ',', true)>
		<cfset SEARCH_TEXT_COLONNE = TAB_SEARCH[1]>
		<cfset SEARCH_TEXT = TAB_SEARCH[2]>

		<cfset TAB_ORDERBY = ListToArray(ORDERBYTEXTE, ',', true)>
		<cfset ORDER_BY_TEXTE_COLONNE = TAB_ORDERBY[1]>
		<cfset ORDER_BY_TEXTE = TAB_ORDERBY[2]>

		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_INTEGRATION_FACTURE.display_login_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#OPERATEURID#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT_COLONNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE_COLONNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#OFFSET#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#LIMIT#" null="false">
			<cfprocresult name="qDisplayLogin">
		</cfstoredproc>

		<cfreturn qDisplayLogin>
	</cffunction>

	<cffunction name="CreateUpdateLogin" access="remote" returntype="Numeric">
		<cfargument name="loginextranet" type="String" required="true">
		<cfargument name="mdp" type="String" required="true">
		<cfargument name="type" type="Numeric" required="true">
		<cfargument name="cdr" type="Numeric" required="true">
		<cfargument name="telecharger" type="Numeric" required="true">
		<cfargument name="operateurid" type="Numeric" required="true">
		<cfargument name="idracine_partenaire" type="Numeric" required="true">
		<cfargument name="partenaire" type="String" required="true">
		<cfargument name="idracine" type="Numeric" required="true">
		<cfargument name="racine" type="String" required="true">
		<cfargument name="commentaire" type="String" required="false">

		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_INTEGRATION_FACTURE.CREATE_OR_UPDATE_LOGIN">
			<cfprocparam value="#loginextranet#" cfsqltype="CF_SQL_STRING">
			<cfprocparam value="#mdp#" cfsqltype="CF_SQL_STRING">
			<cfprocparam value="#type#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#cdr#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#telecharger#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#operateurid#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#idracine_partenaire#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#partenaire#" cfsqltype="CF_SQL_STRING">
			<cfprocparam value="#idracine#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#racine#" cfsqltype="CF_SQL_STRING">
			<cfprocparam value="#commentaire#" cfsqltype="CF_SQL_STRING">
			<cfprocparam variable="qCreateUpdateLogin" type="out" cfsqltype="CF_SQL_INTEGER">
		</cfstoredproc>

		<cfreturn qCreateUpdateLogin>
	</cffunction>

	<cffunction name="DeleteLogin" access="remote" returntype="Numeric">
		<cfargument name="login" type="string" required="true">
		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_INTEGRATION_FACTURE.DELETE_LOGIN">
			<cfprocparam value="#login#" cfsqltype="CF_SQL_STRING">
			<cfprocparam variable="qDeleteLogin" type="out" cfsqltype="CF_SQL_STRING">
		</cfstoredproc>
		<cfreturn qDeleteLogin>
	</cffunction>

	<cffunction name="getListeOperateur" access="remote" returntype="query">
		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_INTRANET.GETLISTEOPERATEUR">
			<cfprocresult name="qDisplayOpe">
		</cfstoredproc>
		<cfreturn qDisplayOpe>
	</cffunction>

	<cffunction name="getListePartenaire" access="remote" returntype="query">
		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_INTEGRATION_FACTURE.intranet_get_all_partenaire">
			<cfprocresult name="qDisplayPartenaire">
		</cfstoredproc>
		<cfreturn qDisplayPartenaire>
	</cffunction>

		<cffunction name="getAllClients" access="remote" returntype="query">
		<cfstoredproc datasource="#session.OffreDSN#" procedure="mnt.PKG_MNT_IHM.getAllClients">
			<cfprocresult name="qDisplayClient">
		</cfstoredproc>
		<cfreturn qDisplayClient>
	</cffunction>

</cfcomponent>