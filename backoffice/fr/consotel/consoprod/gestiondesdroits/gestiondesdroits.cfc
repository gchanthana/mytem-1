<cfcomponent output="false">
 	
	<cffunction name="getListeProfil" access="remote" returntype="Query">
		<cfargument name="idracine" type="Numeric" required="false">
		
		<cfif Not IsDefined("idracine")>
			<cfset idracine = session.perimetre.ID_GROUPE>
		</cfif>
		
		<cfif idracine eq 0>
			<cfset idracine = session.perimetre.ID_GROUPE>
		</cfif>
		
		<cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_cv_grcl_facturation_v3.get_list_profils">
			<cfprocparam value="#idracine#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="qListeProfil">
		</cfstoredproc>
		
		<cfreturn qListeProfil>
	</cffunction>
	
	<cffunction name="getListeRacine" access="remote" returntype="Query">
		<cfargument name="idracine" type="Numeric" required="false" default="-1">
		<cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_intranet.get_racine_info_v1">
			<cfprocparam value="#idracine#" cfsqltype="CF_SQL_INTEGER" type="in" null="true">
			<cfprocresult name="qListeRacine">
		</cfstoredproc>
		
		<cfreturn qListeRacine>
	</cffunction>
	
	<cffunction name="getListeLogin" access="remote" returntype="Query">
		<cfargument name="idracine" type="Numeric" required="false">

		<cfif Not IsDefined("idracine")>
			<cfset idracine = session.perimetre.ID_GROUPE>
		</cfif>
		
		<cfif idracine eq 0>
			<cfset idracine = session.perimetre.ID_GROUPE>
		</cfif>
		
		<cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_intranet.getlistelogin_v2">
			<cfprocparam value="#idracine#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="qListeLogin">
		</cfstoredproc>
		
		<cfreturn qListeLogin>
	</cffunction>

	<cffunction name="creerProfil" access="remote" returntype="Numeric">
		<cfargument name="profil" type="String" required="true">
		<cfargument name="str" type="String" required="true">
		<cfargument name="idracine" type="Numeric" required="false">
		
		<cfif Not IsDefined("idracine")>
			<cfset idracine = session.perimetre.ID_GROUPE>
		</cfif>
		
		<cfif idracine eq 0>
			<cfset idracine = session.perimetre.ID_GROUPE>
		</cfif>
		
		<cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_cv_grcl_facturation_v3.create_ctf_profil">
			<cfprocparam value="#profil#" type="in" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#idracine#" type="in" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam variable="qCreateProfil" type="out" cfsqltype="CF_SQL_INTEGER">
		</cfstoredproc>
		
		<cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_cv_grcl_facturation_v3.add_profil_right">
			<cfprocparam value="#str#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#qCreateProfil#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam variable="qAddButton" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		
		<cfreturn qAddButton>
	</cffunction>
	
	<cffunction name="supprimerProfil" access="remote" returntype="Numeric">
		<cfargument name="idprofil" type="Numeric" required="true">
		<cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_cv_grcl_facturation_v3.delete_ctf_profil">
			<cfprocparam value="#idprofil#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam variable="qSupprimerProfil" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn qSupprimerProfil>
	</cffunction>
	
	<cffunction name="majProfil" access="remote" returntype="Numeric">
		<cfargument name="idprofil" type="Numeric" required="true">
		<cfargument name="profil" type="String" required="true">
		<cfargument name="tab" type="String" required="true">
		<cfargument name="idracine" type="Numeric" required="false">
		
		<cfif Not IsDefined("idracine")>
			<cfset idracine = session.perimetre.ID_GROUPE>
		</cfif>
		
		<cfif idracine eq 0>
			<cfset idracine = session.perimetre.ID_GROUPE>
		</cfif>
		
		<cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_cv_grcl_facturation_v3.updateProfil">
			<cfprocparam value="#idprofil#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#profil#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam variable="qMajProfil" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>

		<cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_cv_grcl_facturation_v3.add_profil_right">
					<cfprocparam value="#tab#" cfsqltype="CF_SQL_VARCHAR" type="in">
					<cfprocparam value="#idprofil#" cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocparam variable="qAddButton" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
	<cfreturn qMajProfil>
	</cffunction>
	
	<cffunction name="okProfil" access="remote" returntype="Numeric">
		<cfargument name="iduser" type="Numeric" required="true">
		<cfargument name="idracine" type="numeric" required="false">
		
		<cfif Not IsDefined("idracine")>
			<cfset idracine = session.perimetre.ID_GROUPE>
		</cfif>
		
		<cfif idracine eq 0>
			<cfset idracine = session.perimetre.ID_GROUPE>
		</cfif>
		
		<cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_cv_grcl_facturation_v3.getUserProfil">
			<cfprocparam value="#iduser#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#idracine#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam variable="qokProf" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn qokProf>	
	</cffunction>	
	
	<cffunction name="getProfil" access="remote" returntype="query">
		<cfargument name="idprofil" type="Numeric" required="true">
		<cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_cv_grcl_facturation_v3.get_right_profils">
			<cfprocparam value="#idprofil#" type="in" cfsqltype="CF_SQL_INTEGER"> 
			<cfprocresult name="qGetProfil">
		</cfstoredproc>
		<cfreturn qGetProfil>
	</cffunction>
	
	<cffunction name="getListeProfils" access="remote" returntype="query">
		<cfargument name="idracine" type="Numeric" required="false">
		
		<cfif Not IsDefined("idracine")>
			<cfset idracine = session.perimetre.ID_GROUPE>
		</cfif>
		
		<cfif idracine eq 0>
			<cfset idracine = session.perimetre.ID_GROUPE>
		</cfif>
		
		<cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_cv_grcl_facturation_v3.get_profils_rights">
			<cfprocparam value="#idracine#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="qGetProfil">
		</cfstoredproc>
		<cfreturn qGetProfil>	
	</cffunction>	
	
	<cffunction name="addprofil" access="remote" returntype="numeric">
		<cfargument name="iduser" type="Numeric" required="true">
		<cfargument name="idprofil" type="Numeric" required="true">
		<cfargument name="idracine" type="Numeric" required="false">
		
		<cfif Not IsDefined("idracine")>
			<cfset idracine = session.perimetre.ID_GROUPE>
		</cfif>
		
		<cfif idracine eq 0>
			<cfset idracine = session.perimetre.ID_GROUPE>
		</cfif>
		
		<cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_cv_grcl_facturation_v3.addprofillogin">
			<cfprocparam value="#iduser#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#idprofil#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#idracine#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam variable="qAddProfil" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn qAddProfil>
	</cffunction>
	
	<cffunction name="removeProfil" access="remote" returntype="numeric">
		<cfargument name="iduser" type="Numeric" required="true">
		<cfargument name="idprofil" type="numeric" required="true">
		<cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_cv_grcl_facturation_v3.removeuserprofil">
			<cfprocparam value="#iduser#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#idprofil#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam variable="qRemoveProfil" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>	
		<cfreturn qRemoveProfil>
	</cffunction>
	
	<cffunction name="getRules" access="remote" returntype="Any">
		<cfargument name="idracine" type="Numeric" required="true">
		<cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_cv_grcl_facturation_v3.getRules"> 
			<cfprocparam value="#idracine#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam variable="qGetRules" cfsqltype="CF_SQL_CLOB" type="out">
		</cfstoredproc>
		<cfreturn qGetRules>
	</cffunction>
	
	<!--- deprecated --->
	<cffunction name="getRules_v2" access="remote" returntype="Any">
		<cfargument name="idracine" type="Numeric" required="true">
		<cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_cv_grcl_facturation_v3.getRules_v2"> 
			<cfprocparam value="#idracine#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="qGetRules">
		</cfstoredproc>
		<cfreturn qGetRules>
	</cffunction>
	
	
	
	<cffunction name="addRules" access="remote" returntype="Numeric">
		<cfargument name="idracine" type="Numeric" required="true">
		<cfargument name="regles" type="XML" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_grcl_facturation_v3.addrules">
			<cfprocparam value="#idracine#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#regles#" cfsqltype="CF_SQL_CLOB" type="in">
			<cfprocparam variable="qAddRules" type="out" cfsqltype="CF_SQL_INTEGER">
		</cfstoredproc>
		<cfreturn qAddRules>
	</cffunction>
	
	<cffunction name="setDefautWorkflow" access="remote" returntype="Numeric">
		<cfargument name="id" type="Numeric" required="true">
		<cfstoredproc datasource="#session.offredsn#" procedure="Pkg_cv_grcl_facturation_v3.addDefaultRules">
			<cfprocparam value="#id#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam variable="result" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	
	<!--- AJOUT DU BT CTRL INV --->
	<cffunction name="addRules_v3" access="remote" returntype="Numeric">
		<cfargument name="idracine" type="Numeric" required="true">
		<cfargument name="regles" type="XML" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_grcl_facturation_v3.addrules_v3">
			<cfprocparam value="#idracine#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#regles#" cfsqltype="CF_SQL_CLOB" type="in">
			<cfprocparam variable="qAddRules" type="out" cfsqltype="CF_SQL_INTEGER">
		</cfstoredproc>
		<cfreturn qAddRules>
	</cffunction>
	
	<cffunction name="setDefautWorkflow_v3" access="remote" returntype="Numeric">
		<cfargument name="id" type="Numeric" required="true">
		<cfstoredproc datasource="#session.offredsn#" procedure="Pkg_cv_grcl_facturation_v3.addDefaultRules_v3">
			<cfprocparam value="#id#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam variable="result" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getRules_v3" access="remote" returntype="Any" hint="matrice wf pour une racine donnée">
		<cfargument name="idracine" type="Numeric" required="true">
		<cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_cv_grcl_facturation_v3.getRules_v3"> 
			<cfprocparam value="#idracine#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="qGetRules">
		</cfstoredproc>
		<cfreturn qGetRules>
	</cffunction>
	
	<!--- FIN AJOUT DU BT CTRL INV --->
</cfcomponent>