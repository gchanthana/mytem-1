<cfcomponent name="gestion">
	
	<cffunction name="getProjets" output="false" access="remote" returntype="query">
		<cfquery name="qGetProjets" datasource="#session.intranetDSN#">
			select t.idprojet, t.libelle_projet
			from projet t
			order by lower(t.libelle_projet)
		</cfquery>
		<cfquery dbtype="query" name="result">
			select libelle_projet as label, idprojet as data
			from qGetProjets
		</cfquery>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getEtats" output="false" access="remote" returntype="query">
		<cfquery name="qGetEtats" datasource="#session.intranetDSN#">
			select idetat_cfc, libelle_etat
			from etat_cfc
			order by idetat_cfc
		</cfquery>
		<cfquery dbtype="query" name="result">
			select libelle_etat as label, idetat_cfc as data
			from qGetEtats
		</cfquery>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getFonctions" output="false" access="remote" returntype="query">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.intranetDSN#" procedure="pkg_gestion.Liste_cfc">
			<cfprocparam value="#arr[1]#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#arr[2]#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#arr[3]#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getParams" output="false" access="remote" returntype="query">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.intranetDSN#" procedure="pkg_gestion.liste_param_cfc">
			<cfprocparam value="#arr[1]#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getParamsPLSQL" output="false" access="remote" returntype="query">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.intranetDSN#" procedure="pkg_gestion.liste_dataset_cfc">
			<cfprocparam value="#arr[1]#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getDataset" output="false" access="remote" returntype="query">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.intranetDSN#" procedure="pkg_gestion.liste_champs_base">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getListeParam" output="false" access="remote" returntype="query">
		<cfargument name="arr" type="array">
		<cfquery name="qgetFonctions" datasource="#session.intranetDSN#">
			select Nom_param as COLUMN_NAME, type_param as DATA_TYPE
			from param_cfc
			where io='I'
			group by Nom_param, type_param
		</cfquery>
		<cfreturn qgetFonctions>
	</cffunction>
	
	<cffunction name="AjoutFonction" output="false" access="remote" returntype="void">
		<cfargument name="arr" type="array">
		<cfquery name="qgetFonctions" datasource="#session.intranetDSN#">
			insert into CFC(IDPROJET,IDETAT_CFC,DESCRIPTION_CFC,METHODE_CFC,NOM_FICHIER_CFC,PACKAGE_PLSQL,PROCEDURE_PLSQL,app_loginid_dev,app_loginid_db)
			VALUES(#arr[1]#,1,'#arr[4]#','#arr[3]#','#arr[2]#','#arr[5]#','#arr[6]#',#arr[7]#,#arr[8]#)
		</cfquery>
	</cffunction>
	
	<cffunction name="SendMail" output="false" access="remote" returntype="void">
		<cfargument name="arr" type="array">
		<cfquery name="a" datasource="#session.intranetDSN#">
			select cfc.*,ec.libelle_etat, p.libelle_projet,pc.idparam_cfc, pc.nom_param, pc.type_param, pc.commentaires, pc.exemple, pc.io
			from cfc, etat_cfc ec, param_cfc pc, projet p
			WHERE cfc.Idcfc=pc.idcfc
			AND cfc.idprojet=p.idprojet
			AND cfc.idetat_cfc=ec.idetat_cfc
			AND cfc.idcfc=#arr[1]#
			order by cfc.idcfc, io, lower(nom_param)
		</cfquery>
		<cfsavecontent variable="b">
			<cfoutput query="a" group="idcfc">
				<tr>
					<td class="grid_normal">Projet</td>
					<td class="grid_normal">#libelle_projet#</td>
				</tr>
				<tr>
					<td class="grid_normal">MÃ©thode CFC</td>
					<td class="grid_normal"><strong>#methode_cfc#</strong></td>
				</tr>
				<tr>
					<td class="grid_normal">Description CFC</td>
					<td class="grid_normal">#description_cfc#</td>
				</tr>
				<tr>
					<td class="grid_normal">Etat</td>
					<td class="grid_normal">#libelle_etat#</td>
				</tr>
				<tr>
					<td class="grid_normal">date CrÃ©ation</td>
					<td class="grid_normal">#date_creation#</td>
				</tr>
				<tr>
					<td class="grid_normal">Fichier CFC</td>
					<td class="grid_normal">#nom_fichier_cfc#</td>
				</tr>
				<tr>
					<td class="grid_normal">Package PL/SQL</td>
					<td class="grid_normal">#package_plsql#</td>
				</tr>
				<tr>
					<td class="grid_normal">ProcÃ©dure PL/SQL</td>
					<td class="grid_normal">#procedure_plsql#</td>
				</tr>
				<cfoutput group="io">
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2" class="grid_title"><cfif io eq "I">ParamÃ©tres CFC<cfelse>Champs DATASET</cfif></td>
					</tr>
					<cfoutput>
						<tr>
							<td class="grid_normal">Nom</td>
							<td class="grid_normal">#nom_param#</td>
						</tr>
						<tr>
							<td class="grid_normal">Type</td>
							<td class="grid_normal">#type_param#</td>
						</tr>
						<tr>
							<td class="grid_normal">Commentaire</td>
							<td class="grid_normal">#commentaires#</td>
						</tr>
						<tr>
							<td class="grid_normal">Exemple</td>
							<td class="grid_normal">#exemple#</td>
						</tr>
						<tr height="1" color="cccccc"><td colspan="2"></td></tr>		
					</cfoutput>
				</cfoutput>
			</cfoutput>
		</cfsavecontent>
		<cfmail from="consoProd@consotel.fr" server="192.168.3.122" port="25" to="#arr[2]#@consotel.fr" type="HTML" subject="Modification de fonction"
					username="infofacture@consotel.fr" password="edifact" cc="#arr[3]#@consotel.fr">
			<style type="text/css">
			html body {
			margin:0;
			padding:0;
			background: ##D4D0C8;
				font:x-small Verdana,Sans-serif;
			  font-size:x-small;
			  }
			  
			  html>body {
				font-size: x-small;
			}
			
			table .menuconsoProd
				{
					border : none;
					background: transparent;
					font: normal 11px Tahoma,Arial, sans-serif;
					color : ##955011;
				}
				
				.menuconsoProd a{
					font : normal 11px EurostileBold,verdana,arial,helvetica,serif;
					color : ##955011;
					text-decoration: none;
					cursor : hand;
				}
				
				.menuconsoProd .a_black{
					font : normal 11px EurostileBold,verdana,arial,helvetica,serif;
					color : ##000000;
					text-decoration: underline;
					cursor : hand;
				}
				
				.menuconsoProd .a_disable{
					font  : normal 11px EurostileBold,verdana,arial,helvetica,serif;
					color : ##B9B9B9;
					text-decoration: none;
					cursor : hand;
				}
				.menuconsoProd a:link, a:visited{
					font : normal 11px EurostileBold,verdana,arial,helvetica,serif;
					color : ##955011;
					text-decoration:none;
					cursor : hand;
				}
				
				.menuconsoProd a:hover, a:active{
					font  : normal 11px EurostileBold,verdana,arial,helvetica,serif;
					color : red;
					text-decoration: underline;
					cursor : hand;
				}
				
				.menuconsoProd .grid_title
				{
					layout-grid: both loose 2px 2px;
					background: ##FFDFAE;
					border-left: 15px solid ##955011;
					border-right: 0px solid ##FFDFAE;
					border-bottom: 1px solid Black;
					border-top: 1px solid Black;
					color: rgb(0 , 0, 0);
					font: normal 11px Tahoma,Arial, sans-serif;
					padding: 2px;
				}
				
				.menuconsoProd .grid_normal
					{
						background: ##FDFDFB;
						color: rgb(0 , 0, 0);
						font: normal 11px Tahoma,Arial, sans-serif;
						padding: 2px 2px;
					}
			</style>
			<table class="menuconsoProd">
				<tr>
					<td colspan="2" class="grid_title">
						Fonction ModifiÃ©e
					</td>
				</tr>
				#b#
			</table>
		</cfmail>
	</cffunction>
	
	<cffunction name="ModificationFonction" output="false" access="remote" returntype="void">
		<cfargument name="arr" type="array">
		<cfquery name="qgetFonctions" datasource="#session.intranetDSN#">
			update CFC
			set IDPROJET=#arr[2]#,
			IDETAT_CFC=#arr[8]#,
			DESCRIPTION_CFC='#arr[5]#',
			METHODE_CFC='#arr[4]#',
			NOM_FICHIER_CFC='#arr[3]#',
			PACKAGE_PLSQL='#arr[6]#',
			PROCEDURE_PLSQL='#arr[7]#',
			app_loginid_dev=#arr[9]#,
			app_loginid_db=#arr[10]#
			where idcfc=#arr[1]#
		</cfquery>
		<cfset sendMail(arr)>
	</cffunction>
	
	<cffunction name="editParam" output="false" access="remote" returntype="void">
		<cfargument name="arr" type="array">
		<cfquery name="qgetFonctions" datasource="#session.intranetDSN#">
			update PARAM_CFC
			set nom_param='#trim(arr[2])#',
			type_param='#trim(arr[3])#',
			commentaires='#trim(arr[4])#',
			exemple='#trim(arr[5])#'
			where idparam_cfc=#arr[1]#
		</cfquery>
	</cffunction>
	
	<cffunction name="AddDataset" output="true" access="remote" returntype="void">
		<cfargument name="arr" type="array">
		<cfloop from="1" to="#ArrayLen(arr[2])#" index="i">
			<cfquery name="qgetFonctions" datasource="#session.intranetDSN#">
				insert into param_cfc(idcfc, nom_param,type_param,commentaires,exemple,io)
				values(#arr[1]#,'#arr[2][i].TABLE_NAME#.#arr[2][i].COLUMN_NAME#','#arr[2][i].DATA_TYPE#','
				<cfif structKeyExists(arr[2][i],"COMMENTS")>
					#arr[2][i].COMMENTS#
				</cfif>
				','
				<cfif structKeyExists(arr[2][i],"EXEMPLE")>
					#arr[2][i].EXEMPLE#		
				</cfif>
				','O')
			</cfquery>
		</cfloop>
 	</cffunction>
	
	<cffunction name="AddParam" output="true" access="remote" returntype="void">
		<cfargument name="arr" type="array">
		<cfloop from="1" to="#ArrayLen(arr[2])#" index="i">
			
			<cfquery name="qgetFonctions" datasource="#session.intranetDSN#">
				insert into param_cfc(idcfc, nom_param,type_param,commentaires,exemple,io)
				values(#arr[1]#,'#arr[2][i].COLUMN_NAME#','#arr[2][i].DATA_TYPE#','
				<cfif structKeyExists(arr[2][i],"COMMENTS")>
					#arr[2][i].COMMENTS#
				</cfif>
				','
				<cfif structKeyExists(arr[2][i],"EXEMPLE")>
					#arr[2][i].EXEMPLE#		
				</cfif>
				','I')
			</cfquery>
		</cfloop>
 	</cffunction>

	<cffunction name="DeleteDataset" output="false" access="remote" returntype="void">
		<cfargument name="arr" type="array">
		<cfquery name="qgetFonctions" datasource="#session.intranetDSN#">
			delete from param_cfc
			where idparam_cfc=#arr[1]#
		</cfquery>
 	</cffunction>
	
	<cffunction name="deleteParamsByCFC" output="false" access="remote" returntype="void">
		<cfargument name="id" type="numeric">
		<cfquery name="qgetFonctions" datasource="#session.intranetDSN#">
			delete from param_cfc
			where idcfc=#id#
		</cfquery>
 	</cffunction>

	<cffunction name="DeleteFonction" output="false" access="remote" returntype="void">
		<cfargument name="arr" type="array">
		#deleteParamsByCFC(arr[1])#
		<cfquery name="qgetFonctions" datasource="#session.intranetDSN#">
			delete from cfc
			where idcfc=#arr[1]#
		</cfquery>
 	</cffunction>
	
	<cffunction name="getUsers" output="false" access="remote" returntype="query">
		<cfldap action="query" filter="department=IT" server="192.168.3.123" 
				username="consotel\infofacture" password="edifact" 
				attributes="cn,mail,description" start="OU=SBSUsers,OU=Users,OU=MyBusiness,DC=consotel,DC=fr" 
				name="getUser" sort="cn">
		<cfquery name="result" dbtype="query">
			select cn as label, cn as data, mail, description
			from getUser
			order by cn asc
		</cfquery>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getUsersByType" output="false" access="remote" returntype="query">
		<cfargument name="arr" type="array">
		<cfldap action="query" filter="department=IT" server="192.168.3.123" 
				username="consotel\infofacture" password="edifact" 
				attributes="cn,mail,description,physicalDeliveryOfficeName" start="OU=SBSUsers,OU=Users,OU=MyBusiness,DC=consotel,DC=fr" 
				name="getUser" sort="cn">
		<cfquery name="result" dbtype="query">
			select cn as label, cn as data, mail, description, physicalDeliveryOfficeName
			from getUser
			where description='#arr[1]#'
			order by cn asc
		</cfquery>
		<cfreturn result>
	</cffunction>
</cfcomponent>