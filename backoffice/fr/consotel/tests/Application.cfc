<!---
Dans : fr.consotel.consoview
---> 
<cfcomponent name="Application">
	<cfset THIS.name = "ConsoTel ConsoViewBackOffice V3.0">
	<cfset THIS.applicationtimeout="#createtimespan(2,0,0,0)#">
	<cfset THIS.sessionmanagement = TRUE>
	<cfset THIS.setclientcookies = TRUE>
	<cfset THIS.sessionTimeout="#createTimeSpan(0,1,0,0)#"> 

	<cffunction name="onApplicationStart">
		<cfargument name="ApplicationScope"/>
		<cflog file="#THIS.name#" type="Information" text="Application #THIS.name# Started">
	 </cffunction>
 
	<cffunction name="onApplicationEnd">
		<cfargument name="ApplicationScope" required="true"/>
	   	<cflog file="#THIS.name#" type="Information" text="Application #THIS.name# Ended">
	</cffunction>

	<cffunction name="onSessionStart">
		<cfif Not IsDefined('SESSION.dataBaseValueChange')>
			<cfset session.dataBaseValueChange = 0>
		</cfif>
		<cfset structDelete(session,"user")>
		<cfset structDelete(session,"perimetre")>
		<cfif Not IsDefined('SESSION.status')>
			<cfset SESSION.status = 0>
		</cfif>
		
		<cfif Not IsDefined('SESSION.OffreDSN')>
			<cfset SESSION.OffreDSN="ROCOFFRE"> <!--- rocoffre --->
		</cfif>
		
		<cfif Not IsDefined('SESSION.PARCVIEWDSN')>
			<cfset SESSION.PARCVIEWDSN="clparcview">
		</cfif>
		
		<cfif Not IsDefined('SESSION.CDRDSN')>
			<cfset SESSION.CDRDSN="roccdr">
		</cfif>
		<cflog file="#THIS.name#" type="Information" text="Session : #Session.sessionid# started">
	</cffunction>

	<cffunction name="onRequestStart" access="remote" returntype="boolean">
		<cfset session.REMOTE_ADDR = CGI.REMOTE_ADDR>
		<cfset session.REMOTE_HOST = CGI.REMOTE_HOST>
		<cfif isDefined("session.status")>
			<cfreturn TRUE>
		</cfif>
	</cffunction>

	<cffunction name="onRequestEnd">
		<cfargument type="String" name = "targetTemplate"required="true"/>
		<cfif isDefined("session.status")>
			<cfif SESSION.status EQ 0>
				<cfthrow type="BACKOFFICE_SESSION_TIMEOUT" message="100" detail="BACKOFFICE_SESSION_TIMEOUT">
			<cfelseif SESSION.status EQ 3>
				<cfthrow type="ADMIN_SESSION_DISCONNECT" message="101" detail="ADMIN_SESSION_DISCONNECT">
			</cfif>
		</cfif>
		<cfif isDefined("session.dataBaseValueChange")>
			<cfif SESSION.dataBaseValueChange GT 0>
				<cfthrow type="DATABASE_VALUE_CHANGED" message="102" detail="DATABASE_VALUE_CHANGED">
			</cfif>
		</cfif>
		<cfreturn TRUE>
	</cffunction>
	
	<cffunction name="onSessionEnd">
		<cfargument name = "SessionScope" required="true"/>
	   	<cflog file="#THIS.name#" type="Information" text="Session: #arguments.SessionScope.sessionid# ended">
	</cffunction>
	<!---
 	<cffunction name="onError">
		<cfargument name="Exception" required="true"/>
		<cfargument type="String" name = "EventName" required="true"/>
		<cfif isDefined("exception.rootcause")>
			<cfif exception.rootcause.type EQ "BACKOFFICE_SESSION_TIMEOUT">
				<cfthrow type="BACKOFFICE_SESSION_TIMEOUT" message="100" detail="BACKOFFICE_SESSION_TIMEOUT">
			<cfelseif exception.rootcause.type EQ "ADMIN_SESSION_DISCONNECT">
				<cfthrow type="ADMIN_SESSION_DISCONNECT" message="101" detail="ADMIN_SESSION_DISCONNECT">
			<cfelseif exception.rootcause.type EQ "DATABASE_VALUE_CHANGED">
				<cfthrow type="DATABASE_VALUE_CHANGED" message="102" detail="DATABASE_VALUE_CHANGED">
			<cfelse>
				<cfthrow type="#exception.rootcause.type#"
						message="#exception.rootcause.message#"
						detail="#exception.rootcause.detail#">
			</cfif>
		<cfelse>
			<cfthrow type="BACKOFFICE_REMOTING_ERROR"
					message="#exception.message#"
					detail="#exception.detail#">
	   	</cfif>
	</cffunction>
	--->
</cfcomponent>
 