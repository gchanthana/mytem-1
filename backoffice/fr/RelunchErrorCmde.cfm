<cfset to_day = lsdateformat(now(),'YYYY/MM/DD')>

<CFQUERY name="qCommandes" datasource="ROCOFFRE">
SELECT ioa.date_action AS date_envoi, ia.*,ap.login_email AS mail_exp,ap.login_nom ||' '|| ap.login_prenom AS expediteur, c.email AS mail_dest,c.nom ||' '||c.prenom AS destinat ,gc.libelle_groupe_client,a.* FROM
(SELECT * 
FROM commande cmd
MINUS
SELECT * 
FROM commande cmd 
WHERE cmd.idcommande IN (SELECT l.idcommande FROM cv_log_mail l)) a,
	groupe_client gc,
   cde_contact c,
   app_login ap,
   inv_actions ia,
   inv_operation_action ioa       
WHERE to_char(a.date_create,'yyyy/mm/dd')>'2010/06/13' 
AND a.idinv_etat IN (3070,3073,3071,3067)
AND a.idracine=gc.idgroupe_client
AND a.idcde_contact = c.idcde_contact
AND a.idcommande = ioa.idcommande
AND ioa.userid = ap.app_loginid
AND ia.idinv_etat = a.idinv_etat
AND ia.idinv_actions = ioa.idinv_actions
ORDER BY a.idcommande
</CFQUERY>

<cfloop query="qCommandes"> 

<cfset ref = NUMERO_OPERATION>
<cfset idtypecommande = IDTYPE_OPERATION>
<cfset numero = NUMERO_OPERATION>
<cfset libelle = LIBELLE>
<cfset ref_client = CODE_INTERNE>
<cfset ref_distributeur = REF_REVENDEUR>

<cfset IDCMDE = IDCOMMANDE>
<cfset IDGESTION = USER_CREATE>
<cfset racine = LIBELLE_GROUPE_CLIENT>
<cfset expediteurMail = MAIL_EXP>
<cfset expediteurNom = EXPEDITEUR>
<cfset destinataire = MAIL_DEST>
<cfset MSG = MESSAGE>

<cfset cc = ''>
<cfset bcc = ''>

<cfset laDate = lsdateFormat(DATE_ENVOI,'DD/MM/YYYY')>
<cfset bcopie = 'YES'>

<cfset message1=
"
#expediteurNom# 
#expediteurMail#
Date : #laDate#
Societe : #racine#

Libelle de la demande #libelle#

Numero de la demande #numero#

Reference client : #ref_client#


Reference revendeur/operateur : #ref_distributeur#

#MSG#

Cordialement,
#expediteurNom#.">




<!--- <cfmail from="samuel.divioa@consotel.fr" server="mail.consotel.fr:26" to="samuel.divioka@consotel.fr" type="text" subject="commande">
#message1#
</cfmail> --->
 
<cfset mailObj = createObject("component","fr.consotel.consoview.util.Mail")/>
<cfset mailObj.settype("text")/>

<!--- <cfset mailObj.setfichierJoin("")/> --->

<cfset mailObj.setmodule("Commande Mobile")  />
<cfset mailObj.setmessage("#message#")  />
<cfset mailObj.setcharset("utf-8")   /> 
<cfset mailObj.setCc(cc)  />
<cfset mailObj.setBcc(bcc)  />
<cfset mailObj.setexpediteur(expediteurMail)  />
<cfset mailObj.setcopiePourExpediteur(bcopie)/>
<cfset mailObj.setrepondreA(expediteurMail)/>
<cfset mailObj.setDestinataire(destinataire)/>
<cfset mailObj.setSujet("Ref. : #ref_client#")/>


<cfset tab=arrayNew(1)>
<cfset arrayAppend(tab,IDCMDE)>

<cfset result = envoyer(mailObj,
		racine,
			tab,
				idtypecommande,
					IDGESTION,
						expediteurNom,
							destinataire,"")>

<cfdump var="#result#">
</cfloop>

<cffunction name="envoyer" access="public" returntype="any">
		<cfargument name="mail" 				type="fr.consotel.consoview.util.Mail" 	required="true">
		<cfargument name="racine" 				type="string" 							required="true">
		<cfargument name="idsCommande" 			type="Array" 							required="true">		
		<cfargument name="idtypecommande" 		type="Numeric" 							required="true">
		<cfargument name="idGestionnaire" 		type="numeric" 							required="true">
		<cfargument name="gestionnaire" 		type="string" 							required="true">
		<cfargument name="desti" 				type="string" 							required="true">		
		<cfargument name="pathJoin" 			type="String" 							required="false">

		
			<cfset p_retour = -1>
			<cfloop index="idx" from="1" to="#ArrayLen(idsCommande)#">
				<cfset idCommande = idsCommande[idx]>
				<cfset str1 = 'SIZE="10"'>
				<cfset str2 = 'SIZE="2"'>		
				<cfset message1 = #Replace(mail.getMessage(),str1,str2,"all")#>
		
				<cfset str1 = "&apos;">
				<cfset str2 = "'">
				<cfset message = #Replace(message1,str1,str2,"all")#>
		
				<cfset listeMailEmp = "">
				<cfset cc = mail.getCc()>
				
				<cfif mail.getCopiePourExpediteur() eq "YES"> 
					<cfif len(mail.getCc()) eq 0>
						<cfset cc = mail.getExpediteur()>
					<cfelse>
						<cfset cc = mail.getCc() & "," & mail.getExpediteur()>
					</cfif>
				</cfif>
				
				
				<cfobject name="sr" type="component" component="fr.consotel.consoview.util.publicreportservice.ScheduleRequest">
				
				
				<cfswitch expression="#idtypecommande#">
					 <cfcase value="1083"><!--- NOUVELLE COMMANDE --->
					 	<cfset sr.setReportRequest("/consoview/gestion/flotte/BDCMobile-v3/BDCMobile-v3.xdo","AVALIDER","pdf")>
					 </cfcase>
					
					 <cfdefaultcase>
					  	<cfset sr.setReportRequest("/consoview/gestion/flotte/BDCMobile-v3/BDCMobile-v3.xdo","AVALIDER","pdf")>
					 </cfdefaultcase>
				</cfswitch>
				
				<cfset sr.AddParameter("P_IDCOMMANDE","#idCommande#")>
				<cfset sr.AddParameter("P_IDGESTIONNAIRE","#idGestionnaire#")>
				<cfset sr.AddParameter("P_RACINE","#racine#")>
				<cfset sr.AddParameter("P_GESTIONNAIRE","#gestionnaire#")>
				<cfset sr.AddParameter("P_MAIL","#desti#")>
				
				<cfset sr.setScheduleRequest("consoview","public","#createUUID()#")>
				<cfset sr.setNotificationScheduleRequest("samuel.divioka@consotel.fr",true,false,false)>
				<cfset sr.setEmailDeliveryRequest("#mail.getDestinataire()#","#cc#","#mail.getBcc()#","#mail.getDestinataire()#","#mail.getExpediteur()#",
				                                                     "#message#","#mail.getModule()# : #mail.getSujet()#")>
				                                                     
				<cfset sr.setBurstScheduleRequest(false)>
				<cfset ret = sr.scheduleReport()>
				<cfset status = "n.c.">
			</cfloop>
		<cfreturn ret>
</cffunction>

<!--- 
<cffunction name="insertlogCommande" returntype="numeric">
	
		<cfargument name="idRacine" 		type="numeric"	 						required="true">
		<cfargument name="libelleRacine" 	type="string"	 						required="true">
		<cfargument name="idGestionnaire" 	type="numeric"	 						required="true">
		<cfargument name="gestionnaire" 	type="string"	 						required="true">
		<cfargument name="mailGestionnaire"	type="string"	 						required="true">
		<cfargument name="mail" 			type="fr.consotel.consoview.util.Mail" 	required="true">
		<cfargument name="idCommande" 		type="numeric" 							required="true">
		<cfargument name="jobId" 			type="numeric"							required="true">
		<cfargument name="status" 			type="string" 							required="true" default="n.c.">
		
		<cfset cc = mail.getCc()>
				
		<cfif mail.getCopiePourExpediteur() eq "YES">
			<cfif len(mail.getCc()) eq 0>
				<cfset cc = mail.getExpediteur()>
			</cfif>
		</cfif>
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m16.insert_log_commande">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_IDCOMMANDE" type="in"   value="#idCommande#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_IDGESTIONNAIRE" type="in"   value="#idGestionnaire#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_RACINE" type="in"   value="#libelleRacine#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_GESTIONNAIRE" type="in"   value="#gestionnaire#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_MAIL" type="in"   value="#mailGestionnaire#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_FROM" type="in"   value="#mail.getExpediteur()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_TO" type="in"   value="#mail.getDestinataire()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_CC" type="in"   value="#cc#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_BCC" type="in"   value="#mail.getBcc()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_MODULE" type="in"   value="#mail.getModule()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_SUBJECT" type="in"   value="#mail.getModule()#,#mail.getSujet()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_MESSAGE" type="in"   value="#message#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_JOB_ID" type="in"   value="#jobId#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_STATUS" type="in"   value="#status#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_IDRACINE" type="in"   value="#idRacine#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_RESULT" type="out"  />
		</cfstoredproc>		
		<cfreturn P_RESULT>
</cffunction>

<cffunction name="getlogsCommande" returntype="query">	
	<cfargument name="idRacine" type="Numeric" 	required="true">
	<cfargument name="dateDebut" type="Date" 	required="true">
	<cfargument name="dateFin" type="Date" 		required="true">
	
	<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m16.get_logs_commande">
		<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="IDRACINE" type="in" value="#idRacine#"/>
		<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_date_debut" type="in"   value="#lsDateFormat(dateDebut,'YYYY/MM/DD')#"/>
		<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_date_fin" type="in"   value="#lsDateFormat(dateDebut,'YYYY/MM/DD')#"/>
		<cfprocresult name="P_RESULT">
	</cfstoredproc>
	<cfreturn P_RESULT>
</cffunction> --->