<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="fr-FR">
	<head profile="http://gmpg.org/xfn/11">
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
		<meta name="description" content="Dectection OS"/>
		<meta http-equiv="pragma" content="no-cache"/>
		<title>Redirection en cours...</title>
	</head>
	<body>
		<cfset isSecure=TRUE>
		<cfset serverDNS="backoffice4.consotel.fr">
		<cfset contextPath="">
		<cfif isDebugMode()>
			<cfset serverDNS=CGI.SERVER_NAME>
			<cfset contextPath="/backoffice">
			<cfset isSecure=FALSE>
		</cfif>
		<cfset enrollPage="/detection.cfm">
		<cfif fileExists(expandPath(contextPath & "/detection.cfm"))>
			<cfset enrollPage=contextPath & "/detection.cfm">
		</cfif>
		<cfset newurl=urlDecode("http" & (isSecure ? "s":"") & "://" & serverDNS & enrollPage & "?" & cgi.query_string)>
		<cflocation url="#newurl#">
	</body>
</html>