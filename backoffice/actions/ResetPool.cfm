<cfflush interval="10">
<cfset prefix = "[CloseConnections] ">

<cfset admin = "">
<cfset authStatus = "">
<cfset datasourceObj = "">
<cfset dsn = URL.DSN>
<cfset email = "samuel.divioka@consotel.fr">

<cfif structkeyExists(URL,"EMAIL")>
	<cfset email = URL.EMAIL>
</cfif>

<cfset datasourceArgs = "">

<cfset admin = createObject("component","cfide.adminapi.administrator")>

<cflog application="true" text="#prefix#Tentative de Connexion ..........................................">
		
<cfset nowh = Hour(now())&':'&Minute(now())&':'&Second(now())>
<cfset debutTrancheH = "19:00:00">
<cfset finTrancheH = "07:00:00">
<cfset tmp=''>
<cfset boolTrancheOk = false>
<cfset timeOut = 20>
<cfset output=""/>



	<cfif force eq false>
		<cfoutput>La tranche horraire est ok si on est avant #finTrancheH# ou apres #debutTrancheH#.<br></cfoutput>
		<cfoutput>Vérification de la tranche horraire<br></cfoutput>
		<cfoutput>il est #nowh#<br></cfoutput>
		
		<cfif DateDiff("n", nowh, debutTrancheH) lt 0>	
			
			<cfoutput>après #debutTrancheH#<br></cfoutput>
			<cfset boolTrancheOk = true>
						
		<cfelse>
		
			<cfoutput>avant #debutTrancheH#<br></cfoutput>
			<cfset boolTrancheOk = false>
			
		</cfif>
		
		<cfif DateDiff("n", nowh, finTrancheH) gt 0>
			
			<cfoutput>avant #finTrancheH#<br></cfoutput>
			<cfset boolTrancheOk = boolTrancheOk or true>
			
		<cfelse>
		
			<cfoutput>après #finTrancheH#<br></cfoutput>
			<cfset boolTrancheOk = boolTrancheOk or false>
			
		</cfif>
		
		<cfoutput>Tranche horraire :</cfoutput>
		<cfif boolTrancheOk>
			<cfoutput>OK Démarrage du traitement.....<br></cfoutput>
		<cfelse>
			<cfoutput>NOK. Fin de traitement.<br></cfoutput>
			<cfmail from="supervision@consotel.fr" to="#email#" server="mail.consotel.fr" port="26" subject="[PROCESS] Reset Connections" type="html">
				<cfoutput >[PROCESS] Réinitialisation des pools de connections<br><br></cfoutput>
				<cfoutput>ERROR. Fin de traitement</cfoutput>
			</cfmail>
			<cfabort>
		</cfif>
	</cfif>	

<cfsavecontent variable="output" >
	
	<cfoutput>
	Tentative de Connexion .....<br>
	</cfoutput>
	
	<cfset authStatus = admin.login("coldfusion")>
	<cfif authStatus neq true>
		
		<cflog application="true" text="#prefix#Connexion impossible fin du traitement .................................">
		<cfelse>
		<cflog application="true" text="#prefix#Connexion Admin OK">
		<cfoutput>
			Connexion Admin OK ....<br>
		</cfoutput>
		
		
		<cfset  datasourceObj = createObject("component","cfide.adminapi.datasource")>		
		<cftry>	
			<cfset  datasourceArgs = datasourceObj.GETDATASOURCES(dsn)>		
			
			<cfcatch>
				<cflog application="true" text="#prefix#Le dsn #dsn# est inconnu. Fin de traitement.">
				<cfoutput>
					Le dsn #dsn# est inconnu. Fin de traitement.<br>
				</cfoutput>
				<cfset  admin.logout()>
				<cflog application="true" text="#prefix#Déconnexion Admin 2.">
				<cfoutput>
				Déconnexion Admin 2.<br>
				</cfoutput>
				<cfmail from="supervision@consotel.fr" to="#email#" server="mail.consotel.fr" port="26" subject="[PROCESS] Reset Connections" type="html">
					<cfoutput >[PROCESS] Réinitialisation des pools de connections<br><br></cfoutput>
					<cfoutput>ERROR. Le dsn #dsn# est inconnu sur <b>#cgi.HTTP_HOST#</b>. Fin de traitement.</cfoutput>
				</cfmail>
				<cfabort>
			</cfcatch>
		</cftry> 	
		
		<cfoutput>
			Les connections sont-elles désactivées sur le dsn #dsn#? #datasourceArgs["disable"]#<br>
		</cfoutput>	
		
		<cfif #datasourceArgs["disable"]#>
			<cflog application="true" text="#prefix#Les connexions sont déjà inactives ......">
			<cfoutput>
				Les connexions sont déjà inactives.......<br>
			</cfoutput>
		<cfelse>
			<cflog application="true" text="#prefix#Les connexions sont actives ...................................">
			<cfoutput>
				Les connexions sont actives............<br>
			</cfoutput>
			<cflog application="true" text="#prefix#Désactivation des connexions sur #dsn#  ..............................................">
			<cfoutput>
				Désactivation des connexions sur #dsn# ....<br>
			</cfoutput>
			<cfset  datasourceArgs["disable"] = true>
			<cfset  datasourceObj.setOther(argumentCollection=datasourceArgs)>
			<cfthread action="sleep" duration="#(3 * 1000)#"/>
		
			<cfoutput>
				<p>
				vaut false si les connections sont inactives<br>	
				#datasourceArgs["disable"]# -> Connexions désactivées ....<br>
				</p>
			</cfoutput>	
		</cfif>	
		
		
		<cfif !#datasourceArgs["disable"]#>
			<cflog application="true" text="#prefix#Les connexions sont déjà actives. Fin de traitement.">
			<cfoutput>
				Les connexions sont déjà actives. Fin de traitement.<br>
			</cfoutput>
			<cfset  admin.logout()>
			<cflog application="true" text="#prefix#Déconnexion Admin 4.">
			<cfoutput>
			Déconnexion Admin 4.<br>
			</cfoutput>
			<cfabort>	
		<cfelse>
			<cflog application="true" text="#prefix#Activation des connexions sur #dsn# ..............................................">
			<cfoutput>
				Activation des connexions sur #dsn# ....<br>
			</cfoutput>
			<cfset  datasourceArgs["disable"] = false>		
			<cfset  datasourceObj.setOther(argumentCollection=datasourceArgs)>
			<cfoutput>
			<p>
			vaut false si les connections sont actives<br>
			#datasourceArgs["disable"]# -> Connexions Activées ....<br>
			</p>
			</cfoutput>	
		</cfif>
	
		
		<cfset  admin.logout()>
		<cflog application="true" text="#prefix#Déconnexion 1.">
		<cfoutput>
		Déconnexion Admin 1.<br>
		</cfoutput>	
	</cfif>

</cfsavecontent>

<cfmail from="supervision@consotel.fr" to="#email#" server="mail.consotel.fr" port="26" subject="[PROCESS] Reset Connections" type="html">
	<cfoutput >[PROCESS] Réinitialisation des pools de connections sur <b>#cgi.HTTP_HOST#</b><br><br></cfoutput>
	<cfoutput>#output#</cfoutput>
</cfmail>
