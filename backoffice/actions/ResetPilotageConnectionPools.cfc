<cfcomponent displayname="actions.ResetAllConnectionPools"  output="true" >
	<cffunction name="main" access="remote" output="true" returntype="void">
		<cfargument name="dsn" type="string" required="true">
		<cfargument name="force" type="boolean" required="false" default="false">		
		<cfargument name="adminEmail" type="string" required="false" default="samuel.divioka@consotel.fr">
				
		<cfset nowh = Hour(now())&':'&Minute(now())&':'&Second(now())>
		<cfset debutTrancheH = "19:00:00">
		<cfset finTrancheH = "07:00:00">
		<cfset tmp=''>
		<cfset boolTrancheOk = false>
		<cfset timeOut = 20>
		<cfset output=""/>
		<cfset tmp=""/>
		
		<cfif force eq false>
				<cfoutput>La tranche horraire est ok si on est avant #finTrancheH# ou apres #debutTrancheH#.<br></cfoutput>
				<cfoutput>Vérification de la tranche horraire<br></cfoutput>
				<cfoutput>il est #nowh#<br></cfoutput>
				
				<cfif DateDiff("n", nowh, debutTrancheH) lt 0>	
					
					<cfoutput>après #debutTrancheH#<br></cfoutput>
					<cfset boolTrancheOk = true>
								
				<cfelse>
				
					<cfoutput>avant #debutTrancheH#<br></cfoutput>
					<cfset boolTrancheOk = false>
					
				</cfif>
				
				<cfif DateDiff("n", nowh, finTrancheH) gt 0>
					
					<cfoutput>avant #finTrancheH#<br></cfoutput>
					<cfset boolTrancheOk = boolTrancheOk or true>
					
				<cfelse>
				
					<cfoutput>après #finTrancheH#<br></cfoutput>
					<cfset boolTrancheOk = boolTrancheOk or false>
					
				</cfif>
				
				<cfoutput>Tranche horraire :</cfoutput>
				<cfif boolTrancheOk>
					<cfoutput>OK Démarrage du traitement.....<br></cfoutput>
				<cfelse>
					<cfoutput>NOK. Fin de traitement.<br></cfoutput>
					<cfreturn>
				</cfif>
		</cfif>			
		<br><br><br>
		
		
		<cfset tabBack = arrayNew(1)>				 
		<cfset arrayAppend(tabBack,'http://back-pilotage-recette-1.consotel.fr')>
		<cfset arrayAppend(tabBack,'http://back-pilotage-1.consotel.fr')>
		<cfset arrayAppend(tabBack,'http://back-pilotage-2.consotel.fr')>
		
		<cfloop array="#tabBack#" index="backoffice">
			<cfoutput><strong>#backoffice# : </strong>#Hour(now())&':'&Minute(now())&':'&Second(now())#<br></cfoutput>
			<cfhttp url="#backoffice#/actions/ResetConnectionPool.cfm?DSN=#dsn#" result="page" timeout="#timeOut#">
			<cfoutput>#page.FileContent#<br></cfoutput>
			<cfoutput>================================================================================================<br></cfoutput>
			<cfoutput>================================================================================================<br></cfoutput>
			
			<cfset tmp = tmp & "#backoffice# : </strong>#Hour(now())&':'&Minute(now())&':'&Second(now())#<br>">
			<cfset tmp = tmp & page.FileContent & '<br>'>
			<cfset tmp = tmp & '================================================================================================<br>'>
			<cfset tmp = tmp & '================================================================================================<br>'>
			<cfset tmp = tmp & '<br><br><br>'>
		</cfloop>
		
		<cfmail from="supervision@consotel.fr" to="#adminEmail#" server="mail.consotel.fr" port="26" subject="[PROCESS] Reset Connections" type="html">
			<cfoutput >[PROCESS] Réinitialisation des pools de connections<br><br></cfoutput>
			<cfoutput>#tmp#</cfoutput>
		</cfmail>
		
	</cffunction>
</cfcomponent>