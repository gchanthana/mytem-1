package
{
	public class Revision@module@
	{
		public static const revisionNumber:String = "@revision@";
		public static const revisionDate:String = "@timestamp@";
		public static const urlBackoffice:String = "@urlbackoffice@";
		public static const societe:String = "saaswedo";
		public static const urlAssets:String = "@urlAssets@";
		public static const owner:String = "@owner@";
	}
}